<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>登录</title>
<link type="text/css" href="style/css.css" rel="stylesheet" />
<link type="text/css" href="style/media.css" rel="stylesheet" />
 <link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>
<script type="text/javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/js/messages_cn.js"></script>  

</head>

<body style="background:#f0f2f5;">
 <?php include_once 'head.php' ; ?>
    
   <div class="zc">
     	<div class="w1200">
        	<div class="tit">
            	<h3 style="color:#fff"><i></i>登录百汇金融账号<i></i></h3>
                
            </div>
            
            <div class="login1">
            	<div class="login1_lf">
            	  <form class="login-form" action="/action.php?type=login" method="POST">
                	<label><span>账户名</span>
					<input type="text" placeholder="请输入您的手机号码" name="mobile" class="mobile" /></label>
                    <label><span>密码</span>
					<input type="password" placeholder="输入您的密码"  name="password" class="password" /></label>
					<div class="mz">
					
					<span style="color:#f00"><input type="checkbox" />记住密码</span>
					
					<span style="color:#f00">
						<a href="getpasswd.php" target="_blank" style="color:#f00">找回密码</a>
					</span>
					
					</div>
					<div class="btn"><button  type="submit">立即登录</button></div>
					<p>没有账号，请<a href="register.php">注册</a></p>
                 </form>
                <div class="mzsm">
                	<h3>免责声明</h3>
                    <p> 百汇金融是一家立足于全球化布局、多元化发展的大型金融集团。集团所经营金融服务业务包含外汇、证券、基金、资产管理、保险及黄金珠宝产业，在全球范围受多个国家及地区政府金融监管机构及相关政府机构许可和监管，重点围绕全球多功能金融全牌照服务平台，旨在为全球投资者打造世界一流先进有保障的网上交易技术支持及值得信赖的客户服务，是全球领先的金融服务商。</p>    
       
                </div>
                </div>
                <div class="login1_lr">
                	<h3>其他通道</h3>
                    <div class="button"><a href="./register.php" style="color:#fff">注册</a></div>
                	<div class="wz">
                    <p>注册成为百汇金融牛人社区用户，您可以：</p>
                    <p>畅享多经纪商和多品种汇集的创新外汇社区交易平台，轻松管理所有交易</p>
                    <p>使用先进的跨平台服务牛人系统，自动同步精准跟随</p>
                    <p>第一时间获取全球财经资讯，与同行热切互动</p>
                    <p>享受多样化的获取收益渠道</p>
                    <!-- <p>如有疑问，请点击<a href="#"> 这里</a></p> -->
                    </div>
                </div>
            </div>
            
        </div>
     </div>
   <script>
   	$(".login-form").validate({
	onfocusout: function(element) { $(element).valid(); },
	rules:{
		mobile:{
			required:true,
			/* isMobile:true,
			remote:{                                          //验证用户名是否存在
               type:"POST",
               url:"/action.php?type=check_mobile",          
               data:{
                 mobile:function(){return $(".login-form .mobile").val();}
               } 
               
            }, */

		},
	},
	 messages: {
			username: {
				required: "请输入用户名",
				remote: "用户名不存在",
			},
			password: {
				required: "请输入密码",
				minlength: "密码不能小于5个字符",
			},
			mobile:{
				required: "请输入手机号",
				remote: "手机号不存在",
			}
	},
	submitHandler: function(form) 
   {      
       $(form).ajaxSubmit({success:function(data){
			data=data.replace(/(^\s*)|(\s*$)/g, ""); 
			switch(data){
				case 'invalidate_mobile':
				alert('手机不存在');
				break;
				case 'invalidate_password':
				alert('密码不正确');
				break;
				case 'success':
				window.location.href="./admin_user/";
				break;
				default:
				alert('登录失败');
			}
			 
		}
		});     
	 }  
	});
	jQuery.validator.addMethod("isMobile", function(value, element) {  
    var length = value.length;  
    var mobile = /1[3456789]{1}\d{9}$/;  
    return this.optional(element) || (length == 11 && mobile.test(value));  
}, "手机号码不正确");  
   </script>
   <?php include_once 'foot.php' ; ?>
</body>
</html>
