<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">


    <link href="assets/js/stackable/stacktable.css" rel="stylesheet">
    <link href="assets/js/stackable/responsive-table.css" rel="stylesheet">
	<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
	<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>

    <style type="text/css">
	    canvas#canvas4 {
	        position: relative;
	        top: 20px;
	    }
	    .chicang-btn{
			    padding: 4px 4px;
				background-color: #af9350;
				color: #fff;
				border-radius: 5px;
				margin-right:4px;
				margin-left: 4px;
		}
		.chicang-btn1{
			    padding: 4px 4px;
				background-color: #af9350;
				color: #fff;
				border-radius: 5px;
		}
		.jl{background:#fff;margin:2%;padding:2%;}
		.jl table{width:100%;border:1px solid #af9350;text-align:center}
		.jl table tr th{color:#fff;padding:1% 0;background:#af9350;;text-align:center}
		.jl table tr td{padding:1% 0;}
		.jl table tr:nth-child(even){background:#f5f5f5;}
		.ym{text-align:center;}
		.ym .pagination{margin-left: 60px;}
		.ym ul li {float:left}
		.ym ul li a,.ym ul li input{border:1px solid #af9350;color:#af9350;margin:0px 5px;    padding: 4px;}
		.ym ul li a.on{background:#af9350;color:#fff;}
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php";?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->
    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">
        	<!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>仓位总结</span>
                        </h2>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->
            <!-- BREADCRUMB -->
            <div class="content-wrap">
	            <ul id="breadcrumb">
	                <li>
	                    <span class="entypo-home"></span>
	                </li>
	                <li><i class="fa fa-lg fa-angle-right"></i>
	                </li>
	                <li><a href="#" title="Sample page 1">会员管理</a>
	                </li>
	                <li><i class="fa fa-lg fa-angle-right"></i>
	                </li>
	                <li><a href="#" title="Sample page 1">仓位总结</a>
	                </li>
	                <li class="pull-right">
	                    <div class="input-group input-widget">

	                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
	                    </div>
	                </li>
	            </ul>

	            <!-- END OF BREADCRUMB -->
				<!-- 仓位总结 started-->

				<div class="padding-md">
					<div class="md_lf">
						<div style="float:left">

						
						</div>
					</div>
				</div>
				<div class="main-container">
					<form method="get" class="form-inline">
						<div class="form-group fy" style="width:100%">
							<input type="text" class="form-control" placeholder="MT4账号" size="12" name="search" value="<?=$_GET['search']?>">
							<select name="mt4_groupid" id="mt4_groupid" class="form-control round-input"  placeholder="MT4组" >
								<option value="" >全部</option>
									<?php
								$mt4_groups=$res->fn_rows("select * from mt4_group   order by mt4_groupid asc ");
								foreach($mt4_groups as $mt4_group){
								?>
								<option value="<?=$mt4_group[mt4_groupid]?>" <?php if($mt4_group[mt4_groupid]==$_GET[mt4_groupid]) echo 'selected="selected"'; ?>><?=$mt4_group[groupname]?></option>

								<?php } ?>  
							</select>
							
							
								<select name="groupid" id="groupid" class="form-control round-input"  placeholder="MT4组" >
								<option value="" >全部</option>
									<?php
								$mt4_groups=$res->fn_rows("select * from user_group   order by gid asc ");
								foreach($mt4_groups as $mt4_group){
								?>
								<option value="<?=$mt4_group[gid]?>" <?php if($mt4_group[gid]==$_GET[groupid]) echo 'selected="selected"'; ?>><?=$mt4_group[gname]?></option>

								<?php } ?>
							</select>
							
							
							<input type="text" placeholder="注册时间" class="form-control" id="reservation" name="reservation" style="width:320px" value="<?=$_GET[reservation]?>" autocomplete="off">
							<button type="submit" class="btn btn-default  colyy">搜索</button>
							
						</div>
					</form>
<div><button class="btn btn-primary" id="export-btn" >导出数据</button></div>
				   <div class="jl">
		            	<table cellpadding="0" cellspacing="0" class="userstable">
		                	<tr>
								<th>姓名</th>
								<th>MT4账号</th>
								<th>推荐人</th>
								<th>下级关系</th>
								<th>路径</th>
								<th>佣金</th>
								<th>余额</th>
								<th>净值</th>
								<th>持仓单量</th>
								<th>持仓手数</th>
								<th>持仓盈亏</th>
								<th>总交易数</th>
								<th>总交易手数</th>
								<th>总交易盈亏</th>
								<th>总入金量</th>
								<th>总出金量</th>
								<th>总库存费</th>
								<th>总手续费</th>
								<th>注册时间</th>
							</tr>
	             	<?php
						if($_GET[page]){
							$page=$_GET[page];
						}else{
							$page=1;
						}	
						$qian=($page-1)*10;

						if($_GET[search]!= null){
							$sqlxs.=" and (t1.nickname like '%$_GET[search]%' || t2.mt4 like '%$_GET[search]%' )";
							$aplus.="&search=$_GET[search]";
						}
						if($_GET[mt4_groupid]!= null){
							$sqlxs.=" and t1.mt4_groupid = '$_GET[mt4_groupid]' ";
							$aplus.="&mt4_groupid=$_GET[mt4_groupid]";
						}
						if($_GET[groupid]!= null){
							$sqlxs.=" and t1.groupid = '$_GET[groupid]' ";
							$aplus.="&mt4_groupid=$_GET[mt4_groupid]";
						}
						if($_GET['reservation']!= null){
								$reservation=explode("~",$_GET['reservation']);
								$start_time=strtotime($reservation[0]);
								$end_time=strtotime($reservation[1]);
								$sqlxs2.=" and CloseTime>'$start_time' and CloseTime<'$end_time'";
								$aplus.="&reservation=$_GET[reservation]";
								$sqlxs3.=" and time>'$start_time' and time<'$end_time'";
						}

						if($_GET[uid]!= null){
							$uids=$res->fn_select("select getChildList('$_GET[uid]') uids");
							$uids_arr = array_slice(explode(",",$uids[uids]),2);
							$uids_str = " and t1.uid = '$uid'";
							foreach($uids_arr as $uid1){
								$uids_str .= " or t1.uid = '$uid1'";
							}
						}
							$xiaxians=$res->fn_rows("select t1.uid,t1.nickname,t1.tuid,t1.tuid,t1.regtime,t1.groupid,t2.mt4 mt5,t3.*,t4.rujin,t5.chujin,t6.groupname,t9.yongjin,t7.nickname tname,t8.realname from users t1 left join mt4 t2 on t1.uid= t2.uid left join( select sum(Lots) Lots, sum(Profit) Profit, sum(Commission) Commission, sum(Swap) Swap, count(*) dnum, mt4 from danzi where Operation<= 1 and status= 2 and Ticket> 0 $sqlxs2 group by mt4) t3 on t2.mt4= t3.mt4 left join( select sum(money) rujin, uid from caiwu where(beizhu= '奖金' or beizhu= '入金') $sqlxs3 group by uid) t4 on t1.uid= t4.uid left join( select sum(money) chujin, uid from caiwu where (beizhu= '提现') $sqlxs3 group by uid) t5 on t1.uid= t5.uid  left join( select sum(money) yongjin, uid from caiwu where type= '代理佣金' $sqlxs3 group by uid) t9 on t1.uid= t9.uid  left join mt4_group t6 on t1.mt4_groupid= t6.mt4_groupid  left join users t7 on t1.tuid= t7.uid  left join idcard t8 on t1.uid= t8.uid where 1=1  $uids_str $sqlxs limit $qian,50");
 
 
								$total_dnum = 0;
								$total_lots = 0;
								$total_profit = 0;
								$total_rujin = 0;
								$total_chujin = 0;

								foreach($xiaxians as $user){
									$total_dnum += $user[dnum];
									$total_lots += $user[Lots];
									$total_profit += $user[Profit];
									$total_rujin += $user[rujin];
									$total_chujin += $user[chujin];
												?>
     
								<tr data-mt4="<?=$user[mt5]?>">
									<td><?=$user[realname]?></td>
									<td><?=$user[mt5]?>
									<a href="javascript:;" data-mt4="<?=$user[mt5]?>" class="chicang-btn"  onclick="chiCang('<?=$user[mt5]?>')">当前持仓</a><a href="./am_dingdan.php?search=<?=$user[mt5]?>">历史持仓</a></td>
									<td><?=$user[tname]?></td>
									<td>
										<? if($user[groupid]<7) {?>
										<a href="am_guanxi.php?uid=<? echo $user[uid].$aplus; ?>" >查看</a>
										<?}?>
									</td>
									  
									
									<?php
					
					
					$tuid=$user[uid];
					  
					$shangxian=$res->fn_select("select t1.uid,t1.tuid,t1.nickname,t1.groupid,t2.groupid tgroupid,t8.realname from users t1 left join users t2 on t1.tuid=t2.uid left join idcard t8 on t1.uid=t8.uid   where t1.uid = '$tuid' and ( t1.uid !=t1.tuid or t1.tuid  is null )");

					$arr=array();
						$i=0;
						while($shangxian){
						$arr[$i]['uid']=$shangxian['uid'];
						$arr[$i]['nickname']=$shangxian['realname'];
					    $tuid=$shangxian['tuid'];
						$shangxian=$res->fn_select("select t1.uid,t1.tuid,t1.nickname,t1.groupid,t2.groupid tgroupid,t8.realname from users t1 left join users t2 on t1.uid=t2.tuid left join idcard t8 on t1.uid=t8.uid  where t1.uid = '$tuid' and ( t1.uid !=t1.tuid or t1.tuid  is null )");
						$i++;
					} 
					$arr=array_reverse($arr);
				
					$str="";
					for($i=0;$i<count($arr);$i++){
						
						if($i==count($arr)-1){
							$str=$str.$arr[$i]['nickname'];
						}else{
							$str=$str.$arr[$i]['nickname'].'->';
						}
						
						
					}
					
					
					?>
									<td><?=$str?></td>     
									
									
									<td>$<?=$user['yongjin']?$user['yongjin']:0?></span></td>
									<td>$<span class="yue" rel="yue" data-mt4="<?=$user[mt5]?>">0</span></td>
									<td>$<span rel="equity" class="equity" data-mt4="<?=$user[mt5]?>">0</span></td>
									<td><span rel="current-count" class="current-count" data-mt4="<?=$user[mt5]?>">0</span>笔</td>
									<td><span rel="current-lots" class="current-lots" data-mt4="<?=$user[mt5]?>">0</span>手</td>
									<td>$<span rel="current-profit" class="current-profit" data-mt4="<?=$user[mt5]?>">0</span></td>
									<td><?=$user[dnum]?$user[dnum]:0?>笔</td>
									<td><?=$user[Lots]?round($user[Lots],2):0?>手</td>
									<td>$<?=round($user[Profit]?$user[Profit]:0,2)?></td>
									<td>$<?=$user[rujin]?$user[rujin]:0?>
									<a href="javascript:void(0)" onclick="Financial('<?=$user[uid]?>',1)">详细</a>
									</td>
									<td>$<?=$user[chujin]?$user[chujin]:0?>
									<a href="javascript:void(0)" onclick="Financial('<?=$user[uid]?>',2)">详细</a>
									</td>
									<td>$<?=round($user[Swap]?$user[Swap]:0,2)?></td>
									<td>$<?=round($user[Commission]?$user[Commission]:0,2)?></td>
									<td><?=date("Y-m-d H:i:s",$user[regtime])?></td>
								</tr>
							<?php } ?>
	                </table>

				
	            </div>

	            <div class="ym">
	            	<ul class="pagination">

				    <?php

						$sql2="select * from users t1 where  1=1  $sqlxs";
						$num=$res->fn_num($sql2);
						$ye=(int)($num/50+1);
						$url=strval($_SERVER['REQUEST_URI']);
					?>
						 <li> <a href="<?echo $url;?>&page=1" >首页</a></li>

					<?php
						if($page<5){
							$iiikaishi=1;
							if($ye-$page>5){
								$iiijishu=5;
							}else{
									$iiijishu=$ye;
							}
						}elseif($ye-$page<5){
							$iiikaishi=$ye-5;
							$iiijishu=$ye;
						}else{
							$iiikaishi=$page-2;
							$iiijishu=$page+2;
						}

						for($iii=$iiikaishi;$iii<=$iiijishu;$iii++){
					?>

	 					<li  <?php if($page==$iii){echo 'class="active"';}?>> <a href="<?echo $url;?>&page=<?=$iii?>" ><?=$iii?></a></li>
					<?php
					}
					?>
						<li >  <a href="<?echo $url;?>&page=<?=$ye?>" >尾页</a></li>
	            </div>

				<div class="total" style="display:none">
					合计  总人数：<span class="total-unum"><?=count($xiaxians)?></span>，
					总交易笔数：<span class="total-dnum"><?=$total_dnum?></span>，
					总交易手数：<span class="total-dnum"><?=$total_lots?>手</span>，
					总交易盈亏：$<span class="total-profit"><?=$total_profit?></span>，
					总持仓笔数：<span class="total-cdnum"></span> 笔，
					总持仓手数：<span class="total-clots"></span> 手，
					总持仓盈亏：$<span class="total-cprofit"></span>，
					总入金量：$<span class="total-rujin"><?=$total_rujin?></span>，
					总出金量：$<span class="total-chujin"><?=$total_chujin?></span>
				</div>

	 		


	        </div>
	    </div>
	</div>
<!-- 当前持仓 -->
<div class="modal fade" id="myModal7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="height:500px;width:1000px">
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">当前持仓</h4>
      </div>
      <div class="modal-body"  style="height:600px;overflow-y:auto">
			 <table id="chicang-example-table" class="table large-only">
						<thead>
							<tr class="text-right">
						
								<th style="width:5%;">ID</th>
								<th style="width:10%;">单号</th>
								<th style="width:10%;">MT4</th>
								<th style="width:10%;">商品</th>
								<th style="width:10%;">操作</th>
								<th style="width:10%;">开仓价</th>
								<th style="width:10%;">止损价/止盈价</th>
								<th style="width:5%;">手数</th>
								<th style="width:10%;">开仓时间</th>
								<th style="width:10%;">获利</th>
								<th style="width:10%;">操作</th>
							</tr>
					   </thead>
						<tbody>	
						</tbody>
				
			</table>
      </div>
      <div class="modal-footer">
   
      </div>
    </div>
  </div>
</div>

<form name="export-form" id="export-form" action="action.php?type=exportexcel" method="post" target="e" style="display:none">
		<input type="hidden"  name="xtitle" id="xtitle" value="" />
		<input type="hidden"  name="xhead" id="xhead" value="" />
		<input type="hidden"  name="xsql" id="xsql" value="" />
</form>
    <!--  END OF PAPER WRAP -->

	<script type="text/javascript" src="assets/js/jquery.js"></script>

	<!-- GAGE -->
	<script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>

    <!-- /MAIN EFFECT -->
<script type="text/javascript" src="assets/js/stackable/stacktable.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap3-editable/inputs-ext/address/address.js"></script>
<script type="text/javascript" src="assets/js/bootstrap3-editable/inputs-ext/typeaheadjs/lib/typeahead.js"></script>
<script type="text/javascript" src="assets/js/bootstrap3-editable/inputs-ext/typeaheadjs/typeaheadjs.js"></script>
<script type="text/javascript" src="assets/js/bootstrap3-editable/inputs-ext/wysihtml5/wysihtml5.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>
<script src="assets/js/tree/lib/jquery.cookie.js" type="text/javascript"></script>
<script src="assets/js/tree/jquery.treeview.js" type="text/javascript"></script>

	<!--socoket start-->
	<script type="text/javascript" src="/js/swfobject.js"></script>
	<script type="text/javascript" src="/js/web_socket.js"></script>
	<script type="text/javascript" src="/js/json.js"></script>
	<script type="text/javascript" src="/static/websocket_config.js"  media="all"></script>
	<script>
	
	
	

	$("#export-btn").click(function(){
		var xtitle="仓位列表";
		var xhead='真实姓名,mt4账号,佣金,总交易手数,总交易盈亏,总入金量,总出金量,总库存费,总手续费';
		var xsql="<?php echo "select t8.realname,t2.mt4 mt5,t9.yongjin,t3.Lots,t3.Profit,t4.rujin,t5.chujin,t3.Swap,t3.Commission from users t1 left join mt4 t2 on t1.uid= t2.uid  left join(select sum(Lots) Lots, sum(Profit) Profit, sum(Commission) Commission, sum(Swap) Swap, count(*) dnum, mt4  from danzi where Operation<= 1  and status= 2   and Ticket> 0 $sqlxs2 group by mt4) t3 on t2.mt4= t3.mt4 left join(select sum(money) rujin, uid from caiwu where(beizhu= '奖金'   or beizhu= '入金') $sqlxs3 group by uid) t4 on t1.uid= t4.uid  left join(select sum(money) chujin, uid from caiwu where (beizhu= '提现')". $sqlxs3 ."group by uid) t5 on t1.uid= t5.uid left join(select sum(money) yongjin, uid from caiwu where type= '代理佣金'". $sqlxs3." group by uid) t9 on t1.uid= t9.uid  left join mt4_group t6 on t1.mt4_groupid= t6.mt4_groupid  left join users t7 on t1.tuid= t7.uid  left join idcard t8 on t1.uid= t8.uid where 1= 1 $sqlxs "; ?>";
	 	 $("#xtitle").val(xtitle);
		$("#xhead").val(xhead);
		$("#xsql").val(xsql);
		$("#export-form").submit();
		return false;  
	});	
	
	
	
	
	
	
	
	
	
	var mt4s=new Array();
$(".userstable tbody tr").each(function(){
		var mt4=$(this).attr("data-mt4");
		if(mt4){
			var keys=mt4.split(","); //字符分割 
			for (i=0;i<keys.length ;i++ ) 
			{
				mt4s.push(keys[i]);
			}
		}
	
	});
	function chiCang(key){
		ws.send('{"type":"current","beizhu":"beizhu","key":"'+key+'","token":"pong"}');
		$("#myModal7").modal('show');
	}	
	 function init() {
		ws = new WebSocket("ws://"+WS_HOST2+":"+WS_PORT2);
		ws.onopen = function() {
    	    timeid && window.clearTimeout(timeid);
		   	console.log("连接成功11");
			getYue();
      };
		if(reconnect>6){
			window.clearTimeout(timeid);
		}
		ws.onmessage = function(e) {
				//console.log("收到数据："+e.data);  
				var data = JSON.parse(e.data);
				switch(data['type']){
				   
				case 'current':
				//console.log(data['beizhu']);
						if(data['beizhu']=='beizhu'){
						var html='';	
						var operation={0:"BUY",1:"SELL",2:"BUY LIMIT",3:"SELL LIMIT",4:"BUY STOP",5:"SELL STOP"};
					
						for(i in data['data']){
						
							var order=data['data'][i];
						
							html+='       <tr data-order="'+order.order+'" data-mt4="'+order.login+'" data-operation="'+order.cmd+'">'
								+'			<td>'+(parseInt(i)+1)+'</td>'
								+'			<td>'+order.order+'</td>'
								+'			<td>'+order.login+'</td>'
								+'			<td>'+order.symbol+'</td>'
								+'			<td>'+operation[order.cmd]+'</td>'
								+'			<td>'+order.openprice+'</td>'
								+'			<td>'+order.sl+'/'+order.tp+'</td>'
								+'			<td>'+(order.volume/100).toFixed(2)+'</td>'
								+'			<td>'+(new Date(parseInt(order.opentime) * 1000).toLocaleString().replace('上午','').replace('下午',''))+'</td>'
								+'			<td>'+order.profit.toFixed(2)+'</td>'
								+'			<td><a href"javascript:;" onclick="pingCang(this)" class="btn btn-primary"><i class="icon-direction"></i>&nbsp;&nbsp;平仓</a></td>'
                                +'        </tr>';	
						}
						
	                	
						$("#myModal7 tbody").html(html);
						
			
						}else{
						
						var operation={0:"BUY",1:"SELL",2:"BUY LIMIT",3:"SELL LIMIT",4:"BUY STOP",5:"SELL STOP"};
						var num=0;
						var lots=0;
						var profit=0;
						var eruity=0;
						for(i in data['data']){
							num++;
							var order=data['data'][i];
							lots=lots+(order.volume/100);  
							profit=profit+order.profit;
						
						}
						
	                	
				
						var yue=$(".userstable tbody tr .yue[data-mt4='"+data['beizhu']+"'] ").html();
						
					   $(".userstable tbody tr .current-count[data-mt4='"+data['beizhu']+"'] ").html(num);
					   $(".userstable tbody tr .current-lots[data-mt4='"+data['beizhu']+"'] ").html(lots.toFixed(2));
					   $(".userstable tbody tr .current-profit[data-mt4='"+data['beizhu']+"'] ").html(profit.toFixed(2));
					   $(".userstable tbody tr .equity[data-mt4='"+data['beizhu']+"'] ").html(parseFloat(parseFloat(yue)+parseFloat(profit)).toFixed(2));
					  
						}
						
						
						
						break;
						
						
						case 'history':  
						
						var html='';	
						var operation={0:"BUY",1:"SELL",2:"BUY LIMIT",3:"SELL LIMIT",4:"BUY STOP",5:"SELL STOP"};
						
						for(i in data['data']){
						
							html+='       <tr data-order="'+order.order+'" data-mt4="'+order.login+'" data-operation="'+order.cmd+'">'
								+'			<td>'+(parseInt(i)+1)+'</td>'
								+'			<td>'+order.order+'</td>'
								+'			<td>'+order.login+'</td>'
								+'			<td>'+order.symbol+'</td>'
								+'			<td>'+operation[order.cmd]+'</td>'
								+'			<td>'+order.openprice+'</td>'
								+'			<td>'+order.sl+'/'+order.tp+'</td>'
								+'			<td>'+(order.volume/100).toFixed(2)+'</td>'
								+'			<td>'+(new Date(parseInt(order.opentime) * 1000).toLocaleString().replace('上午','').replace('下午',''))+'</td>'
								+'			<td>'+order.profit.toFixed(2)+'</td>'
								+'			<td><a href"javascript:;" onclick="pingCang(this)" class="btn btn-primary"><i class="icon-direction"></i>&nbsp;&nbsp;平仓</a></td>'
                                +'        </tr>';	
						}
						
		                $("#myModal7").modal('show'); 
						$("#myModal7 tbody").html(html);
						
			
						
						
						break;
						
						case 'queryuser':
					
						if(data['code']==200){
							var uinfo=data['data'][0];
							//alert(uinfo.yue);
							
								var profit = $(".userstable tbody tr .current-profit[data-mt4='"+data['beizhu']+"'] ").html();
							$(".userstable tbody tr .yue[data-mt4='"+data['beizhu']+"'] ").html(uinfo.yue);
							    
						   $(".userstable tbody tr .equity[data-mt4='"+data['beizhu']+"'] ").html(parseFloat(parseFloat(uinfo.yue)+parseFloat(profit)).toFixed(2));
						}
						getYue();
					break;	
				}
		
		};
	  ws.onclose = function() {
		  reconnect ++;
    	  console.log("连接关闭，定时重连");
    	  timeid = window.setTimeout(init, 3000);
      };
      ws.onerror = function() {
			reconnect ++;
    	    console.log("出现错误");
		    timeid = window.setTimeout(init, 3000);
      };
	}
	
	
		$(".chicang-btn1").click(function(){
		iscurrent1 = 1;
		var key = $(this).attr("data-mt4");
		ws.send('{"type":"history","key":"'+key+'","beizhu":"current","token":"pong"}');
	});
	
	
		function getYue(){
		if(mt4s.length==0){return false}
		var mt4=mt4s.shift();
		ws.send('{"type":"current","key":"'+mt4+'","token":"pong","beizhu":"'+mt4+'"}');
		var data=JSON.stringify({"type":"queryuser","key":mt4,"beizhu":mt4,"token":"pong"});
		//console.log(data);
	
		ws.send(data);  
	
	}
	
	   init();
			$('#reservation').daterangepicker({
							
					timePicker: true,
					timePicker12Hour : false, //是否使用12小时制来显示时间  
					maxDate: '<?=date('Y-m-d H:i')?>', 
					format: 'YYYY-MM-DD HH:mm',
					separator:'~',
					 ranges : {  
						//'最近1小时': [moment().subtract('hours',1), moment()],  
						'今日': [moment().startOf('day'), moment()],  
						'昨日': [moment().subtract('days', 1).startOf('day'), moment().subtract('days', 1).endOf('day')],  
						'最近7日': [moment().subtract('days', 6).startOf('day'), moment()],  
					
					},
					locale : {  
						applyLabel : '确定',  
						cancelLabel : '取消',  
						fromLabel : '起始时间',  
						toLabel : '结束时间',  
						customRangeLabel : '自定义',  
						daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],  
						monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',  
								'七月', '八月', '九月', '十月', '十一月', '十二月' ],  
						firstDay : 1  
					}  
				},
				function(start, end, label) {
					console.log(start.toISOString(), end.toISOString(), label);
				});
			
</script>

<script>

function Financial(uid,type){
	layer.open({
  type: 2,
  title: '财务',
  closeBtn: 1, //不显示关闭按钮
  shade: [0],
  area: ['540px', '415px'],

  content: ['/admin_user/caiwu1.php?type='+type+'&uid='+uid+"&reservation=<?=$_GET['reservation']?>", 'no'], //iframe的url，no代表不显示滚动条

});
}

</script>


<?php include_once 'foot.php'; ?>
</body>

</html>