<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

	
     <link href="assets/js/stackable/stacktable.css" rel="stylesheet">
    <link href="assets/js/stackable/responsive-table.css" rel="stylesheet">
<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>


    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>出金管理</span>
                        </h2>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">出金管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">未审核出金列表</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
				<div class="row">
				
					    <div class="body-nest" id="inlineClose">
                               <div class="form_center" style="width:100%">
                                    <form role="form" class="form-inline " method="get">
									
										<div class="form-group">
									
											 <select class="form-control" name="pagesize">
												<option value="10" <?php if($_GET['pagesize']=='10'){ echo 'selected="selected"'; } ?>>每页10条</option>
												<option value="30" <?php if($_GET['pagesize']=='30'){ echo 'selected="selected"'; } ?>>每页30条</option>
												<option value="50" <?php if($_GET['pagesize']=='50'){ echo 'selected="selected"'; } ?>>每页50条</option>
												<option value="100"  <?php if($_GET['pagesize']=='100'){ echo 'selected="selected"'; } ?>>每页100条</option>
											 </select>
										
												 <input type="text" class="form-control" placeholder="账号/用户名" size="12" name="search" value="<?=$_GET[search]?>">
									
											
												
												<button type="submit" class="btn btn-success">搜索</button>
										</div>
											
										
                                    </form>
                                </div>

                        </div>
							
                   <div class="col-sm-12">

                        <div class="nest" id="StackableClose">
                            <div class="title-alt">
                                <h6>未审核提现列表</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#tStackableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                              

                            </div>

                            <div class="body-nest" id="StackableStatic">

                                <table id="responsive-example-table" class="table large-only">
                                    <tbody>
                                        <tr class="text-right">
									
											<th style="width:5%;">ID</th>
											<th style="width:10%;">用户名</th>
											<th style="width:10%;">MT4账号</th>
											<th style="width:10%;">提现账号</th>
											<th style="width:5%;">提现用户</th>
											<th style="width:15%;">金额</th>
											<th style="width:10%;">银行</th>
											<th style="width:10%;">银行地址</th>
											<th style="width:10%;">时间</th>
											<th style="width:10%;">审核状态</th>
											<th style="width:5%;">操作</th>
                                        </tr>
										
<?php 
$pagesize=10;
if($_GET[pagesize]!= null){
	$pagesize=$_GET[pagesize];
	$aplus="&pagesize=$pagesize";
}

if($_GET[page]){
	$page=$_GET[page];
}else{
	$page=1;
}
$qian=($page-1)*$pagesize;



if($_GET[search]!= null){
	$sqlxs.=" and t2.nickname like '%$_GET[search]%' ";
	$aplus.="&search=$_GET[search]";
}


	$sql="select t1.*,t2.nickname,t3.message,t4.mt4 from tixian t1 left join users t2 on t1.uid=t2.uid left join mt4 t4 on t1.mtid=t4.mtid left join (select * from message where type=3) t3 on t1.uid=t3.uid where 1=1 and t1.status!=1 and t2.nickname!=''  $sqlxs order by tid desc limit $qian,$pagesize";

	$users =$res->fn_rows($sql);
	foreach($users as $user){
?>	
			<tr>
				<td><?=$user[tid]?></td>
				<td><a href="am_users.php?mid=<?=$user[uid]?>"><?=$user[nickname]?></a></td>
				<td><?=$user[mt4]?></td>
				<td><?=$user[account]?></td>
				<td><?=$user[name]?></td>
				<td>$<?=$user[jine]?> <span class="label label-important">$<?=$user[jine2]?></span>  <span class="label label-primary">汇率：<?=$user[exchange]?></span></td>
				<td><?=$user[bank]?></td>
				<td><?=$user[bankaddress]?></td>
				<td><?=date('Y-m-d H:i:s',$user[time])?></td>
	<td>
<?php if($user[status]==0){ ?>
				<a type="button"  class="btn btn-warning idcard"  onclick="editTixian('<?=$user[tid]?>','<?=$user[status]?>','<?=$user[message]?>')">待审核</a>
	<?php }else if($user[status]==-1){　?>
		<a type="button"  class="btn btn-danger idcard"  onclick="editTixian('<?=$user[tid]?>','<?=$user[status]?>','<?=$user[message]?>')">已驳回</a>
	<?php }else { ?>
		<a type="button"  class="btn btn-success idcard"  onclick="editTixian('<?=$user[tid]?>','<?=$user[status]?>','<?=$user[message]?>')">已审核</a>
	<?php } ?>
   </td>

											<td>
			<div class="btn-group">
				<button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">操作
					<span class="caret"></span>
				</button>
				<ul role="menu" class="dropdown-menu">
<li><a href="javascript:;"  onclick="editTixian('<?=$user[tid]?>','<?=$user[status]?>','<?=$user[message]?>')"><i class="icon-document-edit"></i>&nbsp;&nbsp;驳回</a></li>
<li><a href="javascript:;" onclick="tiXian('<?=$user[tid]?>','<?=$user[mt4]?>','<?=$user[jine]?>','<?=$user[jine2]?>','<?=$user[exchange]?>')"><i class="fontawesome-money"></i>&nbsp;&nbsp;支付</a></li>

				</ul>
	</div>							
											
											</td>
											
                                        </tr>
	<?php } ?>
                                    </tbody>
                                </table>

 <ul class="pagination">
     <?php
	$sql2="select t1.*,t2.nickname from tixian t1 left join users t2 on t1.uid=t2.uid  where 1=1  and t1.status!=1 $sqlxs order by tid desc";
$num=$res->fn_num($sql2);
$ye=(int)($num/$pagesize+1);
?>
 <li> <a href="am_tixian_daishenhe.php?page=1<?=$aplus?>" >首页</a></li>

<?php
if($page<5){
	$iiikaishi=1;
	if($ye-$page>5){
		$iiijishu=5;
	}else{
			$iiijishu=$ye;
	}
}elseif($ye-$page<5){
	$iiikaishi=$ye-5;
	$iiijishu=$ye;
}else{
	$iiikaishi=$page-2;
	$iiijishu=$page+2;
}

for($iii=$iiikaishi;$iii<=$iiijishu;$iii++){
?>
 <li  <?php if($page==$iii){echo 'class="active"';}?>> <a href="am_tixian_daishenhe.php?page=<?=$iii?><?=$aplus?>" ><?=$iii?></a></li>
<?php
}
?>
<li >  <a href="am_tixian_daishenhe.php?page=<?=$ye?><?=$aplus?>" >尾页</a></li>
           </ul>
		   
                            </div>

                        </div>


                    </div>


                </div>
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->

	<!-- Modal2 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="height:500px;overflow:auto;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
				<div class="form-horizontal" >
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">审核状态：</label>
						<div class="col-lg-10">
							<select name="idstatus" id="idstatus" class="form-control round-input" onchange="if(this.value=='-1') $('#bohui').show();else $('#bohui').hide();">
									<option value="0">未审核</option>
									<option value="1">已审核</option>
									<option value="-1">驳回</option>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="bohui" style="display:none">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">驳回原因：</label>
						<div class="col-lg-10">
							<input type="text" name="message" id="message" class="form-control round-input" value="">
						</div>
					</div>
				</div>
			
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-primary"  id="submit-btn">提交修改</button>
      </div>
    </div>
  </div>
</div>
 
 
 	<!-- Modal1 -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
			<div class="form-horizontal" >
					<input type="hidden" name="tid" id="tid" class="form-control round-input" value="" readonly>
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">MT4账号：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="mt4" id="mt4" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">MT4余额：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="mt4-yue" id="mt4-yue" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">提现余额(美金)：</label>
						<div class="col-lg-6 input-group ">
							<span class="input-group-addon">&nbsp;$</span>
							<input type="text" name="jine" id="jine" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">提现余额(r人民币)：</label>
						<div class="col-lg-6 input-group ">
							<span class="input-group-addon">￥</span>
							<input type="text" name="jine2" id="jine2" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4  control-label" for="inputPassword1">汇率：</label>
						<div class="col-lg-6 input-group ">
							<span class="input-group-addon"><i class="fontawesome-exchange"></i></span>
							<input type="text" name="exchange" id="exchange" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="tixian-btn">确定支付</button>
      </div>
    </div>
  </div>
</div>

	<!-- Modal2 -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

 <script type="text/javascript" src="assets/js/jquery.js"></script>

    <!-- GAGE -->


   <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
	<script type="text/javascript" src="/js/layer/layer.js"></script>
   <!-- /MAIN EFFECT -->
    <script type="text/javascript" src="assets/js/stackable/stacktable.js"></script>
	   <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.js"></script>
	 <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
	   <!--socoket start-->
  <script type="text/javascript" src="/js/swfobject.js"></script>
  <script type="text/javascript" src="/js/web_socket.js"></script>
  <script type="text/javascript" src="/js/json.js"></script>
<script type="text/javascript" src="/static/websocket_config.js"  media="all"></script>
<script>
  function init() {
		ws = new WebSocket("ws://"+WS_HOST2+":"+WS_PORT2);
		ws.onopen = function() {
    	    timeid && window.clearTimeout(timeid);
		   	console.log("连接成功");
      };
		if(reconnect>6){
			window.clearTimeout(timeid);
		}
		ws.onmessage = function(e) {
				console.log("收到数据："+e.data);
				var data = JSON.parse(e.data);
				switch(data['type']){
	
					case 'queryuser':
						if(data['code']==200){
							var dataobj=data['data'];
							$("#mt4-yue").val(dataobj[0].yue);	
						
						}	
					 break;

				}
		
		};
	  ws.onclose = function() {
		  reconnect ++;
    	  console.log("连接关闭，定时重连");
    	  timeid = window.setTimeout(init, 3000);
      };
      ws.onerror = function() {
			reconnect ++;
    	    console.log("出现错误");
		    timeid = window.setTimeout(init, 3000);
      };
	}
	init();
	
	$('#reservation').daterangepicker({
							
					timePicker: true,
					timePicker12Hour : false, //是否使用12小时制来显示时间  
					maxDate: '<?=date('Y-m-d H:i')?>', 
					format: 'YYYY-MM-DD HH:mm',
					separator:'~',
					 ranges : {  
						//'最近1小时': [moment().subtract('hours',1), moment()],  
						'今日': [moment().startOf('day'), moment()],  
						'昨日': [moment().subtract('days', 1).startOf('day'), moment().subtract('days', 1).endOf('day')],  
						'最近7日': [moment().subtract('days', 6).startOf('day'), moment()],  
					
					},
					locale : {  
						applyLabel : '确定',  
						cancelLabel : '取消',  
						fromLabel : '起始时间',  
						toLabel : '结束时间',  
						customRangeLabel : '自定义',  
						daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],  
						monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',  
								'七月', '八月', '九月', '十月', '十一月', '十二月' ],  
						firstDay : 1  
					}  
				},
				function(start, end, label) {
					console.log(start.toISOString(), end.toISOString(), label);
				});

var tid;
function editTixian(id,status,message){
	tid=id;
	$("#myModal").modal('show');	
	$("#idstatus").val(status).trigger('onchange');
	$("#message").val(message);
}
$("#submit-btn").click(function(){
		
		var status=$("#idstatus").val();
		var message=$("#message").val();
	
		
		if(!message && status==-1){
			    layer.tips('驳回原因不能为空!', '#message');
				return false;
		}
		
		
		$.post("action.php?type=addtixian",{tid:tid,status:status,message:message},function(data,status){
			if(data=="success"){
				alert("提交成功");
				window.location.reload();
			}else{
				alert("提交失败");
			}
		
		});
	 
		
	});
	
	function tiXian(tid,mt4,jine,jine2,exchange){
		$("#tid").val(tid);
		$("#mt4").val(mt4);
		$("#jine").val(jine);
		$("#jine2").val(jine2);
		$("#exchange").val(exchange);
		ws.send('{"type":"queryuser","key":"'+mt4+'","beizhu":"xxx","token":"pong"}');
		$('#myModal1').modal('show')
	}
	
	$("#tixian-btn").click(function(){
		var yue=$("#mt4-yue").val();
		var jine=$("#jine").val();
		if(jine>yue){alert('余额不足');return false;}
		$.post("action.php?type=chujin",{tid:$("#tid").val()},function(data,status){
			
		if(typeof data =='object'){
				var json='{"type":"payment","name":"","key":"'+data.mt4+'","money":"'+data.jine+'","ptype":2,"beizhu":"xxx","token":"pong"}';
				console.log(json);
				ws.send(json);
				$('#myModal2').modal('show')
				$('#myModal2 .modal-body').html('出金成功'); 
				$('#myModal2').on('hidden.bs.modal', function (e) {
					window.location.reload();
				});
		}else{
				$("#myModal2").modal('show');
				$("#myModal2 .modal-body").html("操作失败");
		}
		},'json');
	});
</script>


<?php include_once 'foot.php'; ?>
</body>

</html>