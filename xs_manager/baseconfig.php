<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->


    <link rel="stylesheet" href="assets/css/style.css?123">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="assets/js/progress-bar/number-pb.css">
  <link rel="stylesheet" type="text/css" href="assets/js/parsley/src/parsley.css">
<link href="assets/js/bootstrap-datepicker/css/bootstrap-datetimepicker.css" rel="stylesheet" />

    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="/assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>系统设置</span>
                        </h2>

                    </div>

                  
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">系统设置</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">基础设置</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
			
				  <!--  基础配置-->
		
                    <div class="col-sm-12">
                        <div class="nest" id="elementClose">
                            <div class="title-alt">
                                <h6>基础配置</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#elementClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#element">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="element">

			<?php
				$baseconfig=$res->fn_select("select * from baseconfig where id=1");
				$juhe=file_get_contents("http://web.juhe.cn:8080/finance/exchange/rmbquot?type=&bank=&key=e7b5c620b426ff19344509f78162f0b7");
				$juhe=json_decode($juhe,true);
				$result=$juhe[result];
			?>
<div class="panel-body">
	<form method="get" class="demo-form form-horizontal bucket-form" data-parsley-validate>
		<div class="form-group">
			<label class="col-sm-3 control-label">SMTP服务器</label>
			<div class="col-sm-6">
				<input type="text" name="server" class="form-control" value="<?=$baseconfig[server]?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">SMTP服务器-PORT</label>
			<div class="col-sm-6">
				<input type="text" name="server_port" class="form-control" value="<?=$baseconfig[server_port]?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">账号</label>
			<div class="col-sm-6">
				<input type="text" name="account" class="form-control" value="<?=$baseconfig[account]?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">密码</label>
			<div class="col-sm-6">
				<input type="text" name="password" class="form-control round-input" value="<?=$baseconfig[password]?>">
			</div>
		</div>
	  
		<div class="form-group">
			<label class="col-sm-3 control-label">发件邮箱</label>
			<div class="col-sm-6">
				<div class="fileinput fileinput-new " data-provides="fileinput">
					  <input type="text" name="mail" class="form-control round-input" value="<?=$baseconfig[mail]?>">
				</div>
			</div>
		</div>
	   <div class="form-group">
			<label class="col-sm-3 control-label">设置汇率</label>
				<div class="col-sm-6">
				<input type="text" name="exchange" id="exchange" class="form-control round-input" value="<?=$baseconfig['exchange']?>">
				
				 <span class="help-block ">
				 <input type="checkbox" name="exchange_status" id="exchange_status" value="1" style="width:14px;height:14px" <?php if($baseconfig[exchange_status]) echo 'checked="true"'; ?>/>
				 自动汇率：当前美元兑人民币汇率为：<?=round($result[0][data1][bankConversionPri]/100,4)?></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">入金加点</label>
			<div class="col-sm-6">
				<input type="text" name="rujin" class="form-control" value="<?=$baseconfig[rujin]?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">出金减点</label>
			<div class="col-sm-6">
				<input type="text" name="chujin" class="form-control" value="<?=$baseconfig[chujin]?>">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-3 control-label">入金下限</label>
			<div class="col-sm-6">
				<input type="text" name="min_rujin" class="form-control" value="<?=$baseconfig[min_rujin]?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">出金下限</label>
			<div class="col-sm-6">
				<input type="text" name="min_chujin" class="form-control" value="<?=$baseconfig[min_chujin]?>">
			</div>
		</div>
		
		 <div class="form-group">
			<label class="col-sm-3 control-label">注册信息发邮箱</label>
			<div class="col-sm-6">
				<div class="fileinput fileinput-new " data-provides="fileinput">
					  <input type="text" name="my_email" class="form-control round-input" value="<?=$baseconfig[my_email]?>">
				</div>
			</div>
		</div>
		 <div class="form-group">
			<label class="col-sm-3 control-label">是否发送注册信息</label>
			<div class="col-sm-6">
				
					  <input type="radio" name="issend"  <?=$baseconfig[issend]?'checked="checked"':''?> value="1">是
					  <input type="radio" name="issend" <?=$baseconfig[issend]?'':'checked="checked"'?> value="0">否
			
			</div>
		</div>
		 <div class="form-group">
			<label class="col-sm-3 control-label">是否加密注册信息</label>
			<div class="col-sm-6">
				
					  <input type="radio" name="ismd5"  <?=$baseconfig[ismd5]?'checked="checked"':''?> value="1">是
					  <input type="radio" name="ismd5" <?=$baseconfig[ismd5]?'':'checked="checked"'?> value="0">否
			
			</div>
		</div>
		 <div class="form-group">
			<label class="col-sm-3 control-label">有持仓是否允许出金</label>
			<div class="col-sm-6">
				
					  <input type="radio" name="allow_chujin"  <?=$baseconfig[allow_chujin]?'checked="checked"':''?> value="1">是
					  <input type="radio" name="allow_chujin" <?=$baseconfig[allow_chujin]?'':'checked="checked"'?> value="0">否
			
			</div>
		</div>
		
		 <div class="form-group">
			<label class="col-sm-3 control-label">是否开启推荐码设置</label>
			<div class="col-sm-6">
				
					  <input type="radio" name="istuijian"  <?=$baseconfig[istuijian]?'checked="checked"':''?> value="1">是
					  <input type="radio" name="istuijian" <?=$baseconfig[istuijian]?'':'checked="checked"'?> value="0">否
			
			</div>
		</div>
		
		 <div class="form-group">
			<label class="col-sm-3 control-label">随机开户/顺序开户</label>
			<div class="col-sm-6">
				
					  <input type="radio" name="openStatus"  <?=$baseconfig[openStatus]?'checked="checked"':''?> value="1">随机
					  <input type="radio" name="openStatus" <?=$baseconfig[openStatus]?'':'checked="checked"'?> value="0">顺序
			
			</div>
		</div>
		
		
		 <div class="form-group">
			<label class="col-sm-3 control-label">是否开启转账</label>
			<div class="col-sm-6">
				
					  <input type="radio" name="transfer"  <?=$baseconfig[transfer]?'checked="checked"':''?> value="1">开启
					  <input type="radio" name="transfer" <?=$baseconfig[transfer]?'':'checked="checked"'?> value="0">关闭
			
			</div>
		</div>
		
		
		<div class="form-group">
			<label class="col-sm-3 control-label">mt4杠杆</label>
			<div class="col-sm-6">
				<input type="text" name="leverage" class="form-control" value="<?=$baseconfig[leverage]?>" data-parsley-id="35">
			</div>
		</div>
		
		
		<div class="form-group">
			<label class="col-sm-3 control-label">出金开始时间</label>
			<div class="col-sm-6">
				<input type="text" name="starttime" class="form-control" value="<?=$baseconfig[starttime]?>" data-parsley-id="35">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">出金结束时间</label>
			<div class="col-sm-6">
				<input type="text" name="endtime" class="form-control" value="<?=$baseconfig[endtime]?>" data-parsley-id="35">
			</div>
		</div>
		
			<div class="form-group">
			<label class="col-sm-3 control-label">入金开始时间</label>
			<div class="col-sm-6">
				<input type="text" name="chustarttime" class="form-control" value="<?=$baseconfig[chustarttime]?>" data-parsley-id="35">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">入金结束时间</label>
			<div class="col-sm-6">
				<input type="text" name="chuendtime" class="form-control" value="<?=$baseconfig[chuendtime]?>" data-parsley-id="35">
			</div>
		</div>
		
		
		<div class="col-lg-offset-3 col-lg-10">
			<button class="btn btn-info" type="submit">提交</button>
		</div>
                                    </form>
                                </div>

                            </div>

							</div>
						</div>
				   <!-- end 基础配置-->
				 
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->

	
	<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

    <!-- RIGHT SLIDER CONTENT -->
   

    <!-- END OF RIGHT SLIDER CONTENT-->
    <script type="text/javascript" src="assets/js/jquery.js"></script>
    <script src="assets/js/progress-bar/src/jquery.velocity.min.js"></script>
    <script src="assets/js/progress-bar/number-pb.js"></script>
    <script src="assets/js/progress-bar/progress-app.js"></script>



    <!-- MAIN EFFECT -->
    <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>




    <!-- GAGE -->

<script type="text/javascript" src="assets/js/bootstrap-datepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-datepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
	
<script type="text/javascript" src="assets/js/parsley/dist/parsley.js"></script>
<script type="text/javascript" src="assets/js/parsley/dist/i18n/zh_cn.js"></script>


<script>

  $('.demo-form').parsley().on('form:submit', function (formInstance) {

	var options = {
		url: 'action.php?type=addbaseconfig',
		type: 'post',
		data: $(".demo-form").serialize(),
		success: function (data) {
			if(data=="success"){
				$('#myModal').modal('show')
				$('#myModal .modal-body').html('提交成功'); 
				$('#myModal').on('hidden.bs.modal', function (e) {
					setTimeout(function(){window.location.reload()},3000);   
				})
				
			}else{
				alert('提交失败');
			}
		}
     };
	$.ajax(options);
	return false;
  });
  
  	$("#exchange_status").change(function(){
		if(this.checked){
			$("#exchange").attr("readonly",true);
		}else{
			$("#exchange").removeAttr("readonly");
		}
	});
	$("#exchange_status").change();
</script>

<?php include_once 'foot.php'; ?>
</body>

</html>
