<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

	
     <link href="assets/js/stackable/stacktable.css" rel="stylesheet">
    <link href="assets/js/stackable/responsive-table.css" rel="stylesheet">
<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>

    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
	input.isReverse{width: 40px !important; text-indent: 0px !important;background-color: #ccc;color:#fff}
input.isReverse.on{background-color: #e94f4f;}
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>策略管理</span>
                        </h2>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">策略管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">策略列表</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
				<div class="row">
				
					    <div class="body-nest" id="inlineClose">
                               <div class="form_center" style="width:100%">
                                    <form role="form" class="form-inline " method="get">
									
										<div class="form-group">
									
											 <select class="form-control" name="pagesize">
												<option value="10" <?php if($_GET['pagesize']=='10'){ echo 'selected="selected"'; } ?>>每页10条</option>
												<option value="30" <?php if($_GET['pagesize']=='30'){ echo 'selected="selected"'; } ?>>每页30条</option>
												<option value="50" <?php if($_GET['pagesize']=='50'){ echo 'selected="selected"'; } ?>>每页50条</option>
												<option value="100"  <?php if($_GET['pagesize']=='100'){ echo 'selected="selected"'; } ?>>每页100条</option>
											 </select>
										
												 <input type="text" class="form-control" placeholder="MT4号" size="12" name="search" value="<?=$_GET[search]?>">
									
												<button type="submit" class="btn btn-success">搜索</button>
										</div>
											
										
                                    </form>
                                </div>

                        </div>
							
                   <div class="col-sm-12">

                        <div class="nest" id="StackableClose">
                            <div class="title-alt">
                                <h6>策略列表</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#tStackableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                

                            </div>

                            <div class="body-nest" id="StackableStatic">

                                <table id="responsive-example-table" class="table large-only ">
                                    <thead class="cf">
                                        <tr class="text-right">
									
                                            <th style="width:5%;">ID</th>
                                            <th style="width:10%;">MT4</th>
											<th style="width:10%;">信号源MT4</th>
											<th style="width:10%;">跟单比例</th>
											<th style="width:10%;">最大跟单量</th>
											<th style="width:10%;">最大交易手数</th>
											<th style="width:10%;">最小交易手数</th>
											<th style="width:10%;">强制止盈点数</th>
											<th style="width:10%;">强制止损点数</th>
											<th style="width:10%;">是否开启反向跟单</th>
											<th style="width:5%;">操作</th>
                                        </tr>
									 </thead>
						    <tbody>
										
<?php 
$pagesize=10;
if($_GET[pagesize]!= null){
	$pagesize=$_GET[pagesize];
	$aplus="&pagesize=$pagesize";
}

if($_GET[page]){
	$page=$_GET[page];
}else{
	$page=1;
}
$qian=($page-1)*$pagesize;

if($_GET[search]!= null){
	$sqlxs.=" and  t1.mt4 like '%$_GET[search]%' ";
	$aplus.="&search=$_GET[search]";
}
if($_GET[mt4]!= null){
	$sqlxs.=" and t1.mt4='$_GET[mt4]' ";
	$aplus.="&mt4=$_GET[mt4]";
}
 if($_GET[mmt4]!= null){
	$sqlxs.=" and t1.mmt4='$_GET[mmt4]' ";
	$aplus.="&mmt4=$_GET[mmt4]";
}
	$sql="select t1.*,t2.mtid,t2.uid,t3.nickname uname,t5.nickname uuname from mt4_contact t1 left join mt4 t2 on t1.mt4=t2.mt4 left join users t3 on t2.uid=t3.uid left join mt4 t4 on t1.mmt4=t4.mt4 left join users t5 on t4.uid=t5.uid where 1=1 $sqlxs limit $qian,$pagesize";  
	
	
	$users =$res->fn_rows($sql);
	foreach($users as $user){
?>	
	<tr>
	<td><?=$user[mcid]?></td>
	<td>
	<a href="am_mt4.php?mtid=<?=$user[mtid]?>"><?=$user[mt4]?></a>
	<a href="am_mt4.php?mid=<?=$user[uid]?>">
	<font color="red">(<?=$user[uname]?>)</font>
	</a>
	</td>
	<td><?=$user[mmt4]?><font color="red">(<?=$user[uuname]?>)</font></td>
	<td><?=$user[percent]?>%</td>
	<td><?=$user[maxgendan]?$user[maxgendan]:"无限制"?></td>
	<td><?=$user[maxvolume]?$user[maxvolume]:"无限制"?></td>
	<td><?=$user[minvolume]?$user[minvolume]:"无限制"?></td>
	<td><?=$user[takeprofit]?$user[takeprofit]:"无限制"?></td>
	<td><?=$user[stoploss]?$user[stoploss]:"无限制"?></td>
	<td><?=$user[isreverse]?"是":"否"?></td>
	<td>
	<div class="btn-group">
		<button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">操作
			<span class="caret"></span>
		</button>
	<ul role="menu" class="dropdown-menu">

<li><a href="javascript:;" onclick="edit('<?=$user[mt4]?>','<?=$user[mmt4]?>','<?=$user[percent]?>','<?=$user[maxgendan]?>','<?=$user[maxvolume]?>','<?=$user[minvolume]?>','<?=$user[takeprofit]?>','<?=$user[stoploss]?>','<?=$user[isreverse]?>');"><i class="icon-document-edit"></i>&nbsp;&nbsp;修改</a></li>


<li><a href="javascript:;" onclick="deleteMt4_contact('<?=$user[mcid]?>','<?=$user[mt4]?>','<?=$user[mmt4]?>')"><i class="entypo-cancel-circled"></i>&nbsp;&nbsp;删除</a></li>

		</ul>
	</div>	
			
			
											</td>
                                        </tr>
	<?php } ?>
                                    </tbody>
                                </table>

 <ul class="pagination">
     <?php
		$sql2="select t1.mt4,t3.nickname uname,t5.nickname uuname from mt4_contact t1 left join mt4 t2 on t1.mt4=t2.mt4 left join users t3 on t2.uid=t3.uid left join mt4 t4 on t1.mmt4=t4.mt4 left join users t5 on t4.uid=t5.uid where 1=1 $sqlxs";  
$num=$res->fn_num($sql2);

    $myPage=new pager($num,intval($page),$pagesize,"active");     
     $pageStr= $myPage->GetPagerContent();    
     echo $pageStr;   
?>

           </ul>
		   
                            </div>

                        </div>


                    </div>


                </div>
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->
		<!-- Modal1 -->
<div class="modal fade" id="myModal1" tabindex="1" role="dialog" aria-labelledby="myModalLabel1" style="    z-index: 9999;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel1">系统提示</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
	<!-- Modal2 -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="width:850px">
    <div class="modal-content" style="height:780px;overflow:auto;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">系统提示</h4>
      </div>
      <div class="modal-body">
			<input type="hidden" name="uid" id="uid" value=""/>
				<div class="form-horizontal" >
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">目标账号：</label>
						<div class="col-lg-10">
							<div class="input-group col-lg-6">
								<input type="text" name="mt4" id="mt4" class="form-control round-input" value="" readonly>
							</div>
							
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">信号源账号：</label>
						<div class="col-lg-10">
							<div class="input-group col-lg-12">
								<input type="text" name="mmt4s" id="mmt4s" class="form-control round-input"  style="width:100%">
							</div>
							<span>
                                如88106164,88106163,多个用英文‘,’ 隔开
                            </span>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">设置跟单比例：</label>
						<div class="col-lg-10">
							<div class="input-group col-lg-6">
								<input type="text" name="txtmultlple" id="txtmultlple" class="form-control round-input" value="100"  onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="5" placeholder="请输入数字"><span class="input-group-addon add-on ">%</span>
							</div>
							<span>
                                如设置为200%，则策略师下0.1手，您的MT4账户会跟进0.2手
                            </span>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">最大跟单量：</label>
						<div class="col-lg-10">
							<div class="input-group col-lg-6">
								<select  id="txtBmaxcount" class="form-control round-input" >	
									<option value="0">无限制</option>
									<option value="10">10</option>
									<option value="20">20</option>
									<option value="30">30</option>
									<option value="40">40</option>
									<option value="50">50</option>
									<option value="60">60</option>
									<option value="70">70</option>
									<option value="80">80</option>
									<option value="90">90</option>
									<option value="100">100</option>
								</select>
								<span class="input-group-addon add-on ">单</span>
							</div>
							<span>
									如：设置为10单，则您的MT4同时持仓这个策略师的10张交易单，未满10单时才会跟进后续单子。
							</span>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">最大交易手数：</label>
						<div class="col-lg-10">
							<div class="input-group col-lg-6">
							<input type="text" name="txtmaxsize" id="txtmaxsize" class="form-control round-input"  onkeypress="if(!this.value.match(/^[\+\-]?\d*?\.?\d*?$/))this.value=this.t_value;else this.t_value=this.value;if(this.value.match(/^(?:[\+\-]?\d+(?:\.\d+)?)?$/))this.o_value=this.value" onkeyup="if(!this.value.match(/^[\+\-]?\d*?\.?\d*?$/))this.value=this.t_value;else this.t_value=this.value;if(this.value.match(/^(?:[\+\-]?\d+(?:\.\d+)?)?$/))this.o_value=this.value" onblur="if(!this.value.match(/^(?:[\+\-]?\d+(?:\.\d+)?|\.\d*?)?$/))this.value=this.o_value;else{if(this.value.match(/^\.\d+$/))this.value=0+this.value;if(this.value.match(/^\.$/))this.value=0;this.o_value=this.value}" maxlength="5" placeholder="请输入数字" readonly />
							<span class="input-group-addon add-on ">手</span>
							<span class="input-group-addon add-on "><input type="checkbox"  id="checkMaxVolume" checked="true"/>无限制</span>
							</div>
							<span>当按比例计算出来的手数大于最大交易手数时，会以最大交易手数入场。</span>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">最小交易手数：</label>
						<div class="col-lg-10">
							<div class="input-group col-lg-6">
							<input type="text" name="txtminsize" id="txtminsize" class="form-control round-input" onkeypress="if(!this.value.match(/^[\+\-]?\d*?\.?\d*?$/))this.value=this.t_value;else this.t_value=this.value;if(this.value.match(/^(?:[\+\-]?\d+(?:\.\d+)?)?$/))this.o_value=this.value" onkeyup="if(!this.value.match(/^[\+\-]?\d*?\.?\d*?$/))this.value=this.t_value;else this.t_value=this.value;if(this.value.match(/^(?:[\+\-]?\d+(?:\.\d+)?)?$/))this.o_value=this.value" onblur="if(!this.value.match(/^(?:[\+\-]?\d+(?:\.\d+)?|\.\d*?)?$/))this.value=this.o_value;else{if(this.value.match(/^\.\d+$/))this.value=0+this.value;if(this.value.match(/^\.$/))this.value=0;this.o_value=this.value}" maxlength="5" placeholder="请输入数字" readonly />
							<span class="input-group-addon add-on ">手</span>
							<span class="input-group-addon add-on "><input type="checkbox"  id="checkMinVolume" checked="true"/>无限制</span>
							</div>
							<span>当按比例计算出来的手数小于最小交易手数时，会以最小交易手数入场</span>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">强制止盈点数：</label>
						<div class="col-lg-10">
							<div class="input-group col-lg-6">
								<select  id="txtwin" class="form-control round-input" >	
									<option value="0">无限制</option>
									<option value="10">10</option>
									<option value="20">20</option>
									<option value="30">30</option>
									<option value="40">40</option>
									<option value="50">50</option>
									<option value="60">60</option>
									<option value="70">70</option>
									<option value="80">80</option>
									<option value="90">90</option>
									<option value="100">100</option>
								</select>
								<span class="input-group-addon add-on ">点</span>
							</div>
							<span>
								如：设置为50点，则从该策略师处接受的喊单最多只承受50点浮盈，到达50点后，系统自动将该交易单平仓。
							</span>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">强制止损点数：</label>
						<div class="col-lg-10">
							<div class="input-group col-lg-6">
								<select  id="txtloss" class="form-control round-input" >	
									<option value="0">无限制</option>
									<option value="10">10</option>
									<option value="20">20</option>
									<option value="30">30</option>
									<option value="40">40</option>
									<option value="50">50</option>
									<option value="60">60</option>
									<option value="70">70</option>
									<option value="80">80</option>
									<option value="90">90</option>
									<option value="100">100</option>
								</select>
								<span class="input-group-addon add-on ">点</span>
							</div>
							<span>
								如：设置为50点，则从该策略师处接受的喊单最多只承受50点浮亏，到达50点后，系统自动将该交易单平仓。
							</span>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">是否开启反向跟单：</label>
						<div class="col-lg-10">
							<div class="input-group col-lg-6 flat-checkbox">

								  <input type="button" data-reverse="0" class="btn button-df isReverse on"  value="否" />
                                  <input type="button" data-reverse="1" class="btn button-df isReverse"  value="是" />
							</div>
							<span>  
							如：勾选“反向跟单”后，该策略师推送的每一条交易信号，您的交易账户都会反向生成一笔相同交易单。<br>
                            例：策略师买入黄金，您将卖出黄金，只是交易单的手数仍然是根据您预设的“跟单手数”或比例生成。
							</span>
						</div>
					</div>
					
				</div>
				<iframe name="e" style="display:none"></iframe>
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-primary"  id="submit-btn">提交修改</button>
      </div>
    </div>
  </div>
</div>
	
		<!-- Modal6 -->
<div class="modal fade" id="myModal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" >系统提示</h4>
      </div>
      <div class="modal-body">
			<input type="hidden" name="mt42" id="mt42" /> 
			<input type="hidden" name="mmt42" id="mmt42" /> 
			跟单关系已取消，是否同时平仓跟随此老师的订单？
      </div>
      <div class="modal-footer">
			<button type="button" class="btn btn-success" data-dismiss="modal" id="cancelMt4contact-btn" data-op="1">是</button>
			<button type="button" class="btn btn-primary" data-dismiss="modal" >否</button>
      </div>
    </div>
  </div>
</div>


    <script type="text/javascript" src="assets/js/jquery.js"></script>

    <!-- GAGE -->


   <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
   <!-- /MAIN EFFECT -->
    <script type="text/javascript" src="assets/js/stackable/stacktable.js"></script>
	   <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.js"></script>
	 <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
	  <script type="text/javascript" src="/js/layer/layer.js"></script>

 <!--socoket start-->
  <script type="text/javascript" src="/js/swfobject.js"></script>
  <script type="text/javascript" src="/js/web_socket.js"></script>
  <script type="text/javascript" src="/js/json.js"></script>
<script type="text/javascript" src="/static/websocket_config.js"  media="all"></script>

 <script>
	$("input.isReverse").click(function(){
		$("input.isReverse").removeClass("on");
		$(this).addClass("on");
	});
	
	
	
	function edit(mt4,mmt4,percent,maxgendan,maxvolume,minvolume,takeprofit,stoploss,isreverse){
		$("#mt4").val(mt4);
		$("#mmt4s").val(mmt4);
		$("#txtmultlple").val(percent);
		$("#txtBmaxcount").val(maxgendan);	
		$("#txtmaxsize").val(maxvolume);
		$("#txtminsize").val(minvolume);
		$("#txtwin").val(takeprofit);
		$("#txtloss").val(stoploss);
		$("input.isReverse").removeClass('on');
		$("input.isReverse[data-reverse='"+isreverse+"']").addClass('on');
		$("#myModal2").modal('show');	
	}
	
	$("#submit-btn").click(function(){
		
		var mt4=$("#mt4").val();
		var mmt4s=$("#mmt4s").val();
		var percent=$("#txtmultlple").val();
		var maxgendan=$("#txtBmaxcount").val();	
		var maxvolume=$("#txtmaxsize").val();
		var minvolume=$("#txtminsize").val();
		var takeprofit=$("#txtwin").val();
		var stoploss=$("#txtloss").val();
		var isreverse=$("input.isReverse.on").attr('data-reverse');

	
		if(!percent || percent==0){
			    layer.tips('跟单比例不能为空!', '#txtmultlple');
				return false;
		}
		if(!maxgendan || maxgendan==0){
			//    layer.tips('最大跟单单量不能为空!', '#txtBmaxcount');
			//	return false;
		}
		if(!mmt4s || mmt4s==0){
			    layer.tips('信号源账号!', '#mmt4s');
				return false;
		}
		/*	if(!maxvolume || maxvolume==0){
			    layer.tips('最大交易手数不能为空!', '#txtmaxsize');
				return false;
		}
		if(!minvolume || minvolume==0){
			    layer.tips('最小交易手数不能为空!', '#txtminsize');
				return false;
		}
	if(!takeprofit || takeprofit==0){
			    layer.tips('强制止盈点数不能为空!', '#txtwin');
				return false;
		} 
		if(!stoploss || stoploss==0){
			    layer.tips('强制止损点数不能为空!', '#txtloss');
				return false;
		}*/
		
		$.post("action.php?type=addmt4contact",{mt4:mt4,mmt4s:mmt4s,percent:percent,maxgendan:maxgendan,maxvolume:maxvolume,minvolume:minvolume,takeprofit:takeprofit,stoploss:stoploss,isreverse:isreverse},function(data,status){
			if(data=="success"){
				$('#myModal1').modal('show')
				$('#myModal1 .modal-body').html('提交成功'); 
				$('#myModal1').on('hidden.bs.modal', function (e) {
					setTimeout(function(){window.location.reload()},1000);
				});
			}else{
				$('#myModal1').modal('show')
				$('#myModal1 .modal-body').html('提交失败'); 
				$('#myModal1').on('hidden.bs.modal', function (e) {
					setTimeout(function(){window.location.reload()},1000);
				});
			}
		
		});
	});
	
	$("#btnreturn").click(function(){
		window.location.reload();
	});
	
	$("#checkMaxVolume").click(function(){
		if(this.checked){
			$("#txtmaxsize").attr("readonly",true);
			$("#txtmaxsize").val(0);
		}else{
			$("#txtmaxsize").removeAttr("readonly");
		}
	});
	
	$("#checkMinVolume").click(function(){
		if(this.checked){
			$("#txtminsize").attr("readonly",true);
			$("#txtminsize").val(0);
		}else{
			$("#txtminsize").removeAttr("readonly");
		}
	});
	
	function deleteMt4_contact(mcid,mt4,mmt4){
		if(!window.confirm('确认删除操作吗？')){ return false;}
		$("#mt42").val(mt4);
		$("#mmt42").val(mmt4);
		$.post("action.php?type=deleteMt4_contact",{mcid:mcid},function(data,status){
			if(data=="success"){
				$('#myModal6').modal('show')
				
			}else{
				$('#myModal1').modal('show')
				$('#myModal1 .modal-body').html('提交失败'); 
				$('#myModal1').on('hidden.bs.modal', function (e) {
					setTimeout(function(){window.location.reload()},1000);
				});
			}
		
		});
	}
	
	$("#cancelMt4contact-btn").click(function(){
		var mt4 = $("#mt42").val();
		var mmt4 = $("#mmt42").val();
		$('#myModal1').modal('show')
		$('#myModal1 .modal-body').html('订单处理中。。。'); 
		$.post("action.php?type=cancelgendan",{mt4:mt4,mmt4:mmt4},function(data,status){
			if(data=="success"){
				$('#myModal1').modal('show')
				$('#myModal1 .modal-body').html('处理完成'); 
				$('#myModal1').on('hidden.bs.modal', function (e) {
					setTimeout(function(){window.location.reload()},1000);
				});
			}else{
				$('#myModal1').modal('show')
				$('#myModal1 .modal-body').html('提交失败'); 
				$('#myModal1').on('hidden.bs.modal', function (e) {
					setTimeout(function(){window.location.reload()},1000);
				});
			}
		
		});
	});
   </script>



<?php include_once 'foot.php'; ?>
</body>

</html>
