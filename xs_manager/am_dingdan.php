<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>lin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

    
     <link href="assets/js/stackable/stacktable.css" rel="stylesheet">
    <link href="assets/js/stackable/responsive-table.css" rel="stylesheet">
<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />


    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
        <?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>订单管理</span>
                        </h2>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">订单管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">历史订单列表</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
                <div class="row">
                
<div class="body-nest" id="inlineClose">
   <div class="form_center" style="width:100%">
        <form role="form" class="form-inline " method="get">
        
            <div class="form-group">
        
                 <select class="form-control" name="pagesize">
                    <option value="10" <?php if($_GET['pagesize']=='10'){ echo 'selected="selected"'; } ?>>每页10条</option>
                    <option value="30" <?php if($_GET['pagesize']=='30'){ echo 'selected="selected"'; } ?>>每页30条</option>
                    <option value="50" <?php if($_GET['pagesize']=='50'){ echo 'selected="selected"'; } ?>>每页50条</option>
                    <option value="100"  <?php if($_GET['pagesize']=='100'){ echo 'selected="selected"'; } ?>>每页100条</option>
                    <option value="100"  <?php if($_GET['pagesize']=='500'){ echo 'selected="selected"'; } ?>>每页500条</option>
                    <option value="100"  <?php if($_GET['pagesize']=='1000'){ echo 'selected="selected"'; } ?>>每页1000条</option>
                 </select>
            
                     <input type="text" class="form-control" placeholder="单号/账号/用户名" size="12" name="search" value="<?=$_GET[search]?>">
                    <input type="text" class="form-control" placeholder="代理商" size="12" name="tname" value="<?=$_GET[tname]?>">
                     <input type="text" class="form-control" placeholder="商品" size="12" name="symbol" value="<?=$_GET[symbol]?>">
                    <input type="text" placeholder="平仓时间" class="form-control" id="reservation" name="reservation" style="width:320px" value="<?=$_GET[reservation]?>" autocomplete="off"> 
                     <select class="form-control" name="dtype">
                    <option value="">全部订单</option>
                
                    <option value="1" <?php if($_GET['dtype']=='1'){ echo 'selected="selected"'; } ?>>未平仓订单</option>
                    <option value="2" <?php if($_GET['dtype']=='2'){ echo 'selected="selected"'; } ?>>已平仓订单</option>
        
                 </select>
                    <button type="submit" class="btn btn-success">搜索</button>
                <!--    <button name="genshui" value=1  type="submit" class="btn btn-primary">查看跟随</button>-->
                    <button class="btn btn-primary" id="export-btn" >导出数据</button>
            </div>
                
            
        </form>
    </div>

                        </div>
                            
                   <div class="col-sm-12">

                        <div class="nest" id="StackableClose">
                            <div class="title-alt">
                                <h6>历史订单列表</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#tStackableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                              

                            </div>

                            <div class="body-nest" id="StackableStatic">

                                <table id="responsive-example-table" class="table large-only">
                                    <tbody>
                                        <tr class="text-right">
                                    
                                            <th style="width:5%;">ID</th>
                                            <th style="width:10%;">单号</th>
                                            <th style="width:10%;">MT4</th>
                                            <th style="width:5%;">代理商</th>
                                            <th style="width:2.5%;">商品</th>
                                            <th style="width:2.5%;">操作</th>
                                            <th style="width:2.5%;">类型</th>
                                            <th style="width:10%;">开仓价/平仓价</th>
                                            <th style="width:10%;">止损价/止盈价</th>
                                            <th style="width:2.5%;">手数</th>
                                            <th style="width:2.5%;">盈利</th>
                                            <th style="width:5%;">COMMENT</th>
                                            <th style="width:5%;">手续费</th>
                                            <th style="width:5%;">仓息</th>
                                            <th style="width:5%;">备注</th>
                                            <th style="width:5%;">开仓时间</th>
                                            <th style="width:5%;">平仓时间</th>
                                            <th style="width:2.5%;">是否返佣</th>
                                            <th style="width:2.5%;">是否分润</th>
                                            <th style="width:2.5%;">操作</th>
                                        </tr>
                                        
<?php 
$pagesize=10;
if($_GET[pagesize]!= null){
    $pagesize=$_GET[pagesize];
    $aplus="&pagesize=$pagesize";
}

if($_GET[page]){
    $page=$_GET[page];
}else{
    $page=1;
}
$qian=($page-1)*$pagesize;



if($_GET[search]!= null){
    $sqlxs.=" and (t1.Ticket like '%$_GET[search]%' || t1.mt4 like '%$_GET[search]%' || t3.nickname like '%$_GET[search]%' )";
    $aplus.="&search=$_GET[search]";
}
if($_GET[symbol]!= null){
    $sqlxs.=" and t1.Symbol = '$_GET[symbol]' ";
    $aplus.="&symbol=$_GET[symbol]";
}
if($_GET['reservation']!= null){
        $reservation=explode("~",$_GET['reservation']);
        $start_time=strtotime($reservation[0]);
        $end_time=strtotime($reservation[1]);
        $sqlxs.=" and t1.CloseTime>'$start_time' and t1.CloseTime<'$end_time'";
        $aplus.="&reservation=$_GET[reservation]";
}
if($_GET[tname]!= null){
    $tuser = $res->fn_select("select uid from users where nickname like '%$_GET[tname]%' ");
    $uids = $res->fn_select("select  getChildList($tuser[uid]) uids");
        
    $uids_arr = array_slice(explode(",",$uids[uids]),2);
    $uids_str = "t3.uid = '$tuser[uid]'";
    foreach($uids_arr as $uid){
        $uids_str .= " or t2.uid = '$uid'";
    }
    $sqlxs.=" and ($uids_str) ";
    $aplus.="&tname=$_GET[tname]";
}
if($_GET['dtype']!= null){
  $dtype=$_GET['dtype'];
            $sqlxs.=" and t1.status=$dtype";
   
}
if($_GET[mt4]){
    $sqlxs.="and t1.mt4 in ($_GET[mt4])";
    $aplus.="&mt4=$_GET[mt4]";
}
     $sql="select t1.did,t1.mt4,t1.Ticket,t1.Symbol,t1.Operation,t1.status,t1.OpenPrice,t8.realname,t1.ClosePrice,t1.StopLoss,t1.TakeProfit,t9.realname trealname,t1.Lots,t1.Profit,t1.Comment,t1.Commission,t1.Swap,t1.beizhu,t1.OpenTime,t1.CloseTime,t1.pcount,t1.isfanyong,t1.isfenrun,t2.uid,t3.nickname,t4.ticket tticket,t5.nickname tname from danzi t1 left join mt4 t2 on t1.mt4=t2.mt4 left join users t3 on t2.uid=t3.uid left join danzi t4 on t1.ddid=t4.did left join users t5 on t3.tuid=t5.uid left join idcard t8 on t2.uid=t8.uid left join idcard t9 on t5.uid=t9.uid  where t1.Symbol!=''  $sqlxs  order by t1.did desc limit $qian,$pagesize";


if($_GET[genshui]){
    $ticket=$_GET[search];
$sql="select t1.did,t1.mt4,t1.Ticket,t1.Symbol,t1.Operation,t1.status,t1.OpenPrice,t1.ClosePrice,t1.StopLoss,t8.realname,t9.realname trealname,t1.TakeProfit,t1.Lots,t1.Profit,t1.Comment,t1.Commission,t1.Swap,t1.beizhu,t1.OpenTime,t1.CloseTime,t1.pcount,t1.isfanyong,t1.isfenrun,t2.uid,t3.nickname,t4.ticket tticket,t5.nickname tname from danzi t1 left join mt4 t2 on t1.mt4=t2.mt4 left join users t3 on t2.uid=t3.uid left join danzi t4 on t1.ddid=t4.did left join users t5 on t3.tuid=t5.uid left join idcard t8 on t2.uid=t8.uid left join idcard t9 on t5.uid=t9.uid  where t1.Symbol!=''  and (t1.did in  (select ddid did from danzi where ticket='$ticket') or t1.ddid in (select ddid from danzi where ticket='$ticket')) order by t1.did asc ";
}
    $users =$res->fn_rows($sql);
    
    $OPERATION = array(
        '0' => 'BUY',
        '1' => 'SELL',
        '2' => 'BUY_LIMIT',
        '3' => 'SELL_LIMIT',
        '4' => 'BUY_STOP',
        '5' => 'SELL_STOP',
        '6' => 'BALANCE',   
    );
    foreach($users as $user){
        
?>  
<tr>  
    <td><?=$user[did]?></td>
     <td><p><?=$user[Ticket]?></p></td>
    <td>
    <a href="am_mt4.php?mid=<?=$user[uid]?>">
    <p><?=$user[mt4]?></p><font color="red">（<?=$user[realname]?>）</font>     
    </a>
    </td>
    <td><?=$user[trealname]?></td>
    <td><?=$user[Symbol]?></td>
    <td><?php 
    if (!$user[Ticket]) echo '<font color="red">跟单失败</font>';
    else echo ($user[status]==2)?'<font color="red">已平仓</font>':($user[pcount]?'<font color="red">平仓失败</font>':'已开仓')?></td>
    <td><?=$OPERATION[$user[Operation]]?></td>
    <td><?=$user[OpenPrice]?>/<?=$user[ClosePrice]?></td>
    <td><?=$user[StopLoss]?>/<?=$user[TakeProfit]?></td>
    <td><?=$user[Lots]?></td>
    <td><?=$user[Profit]?></td>
    <td><?=$user[Comment]?></td>
    <td><?=$user[Commission]?></td>
    <td><?=$user[Swap]?></td>
    <td><?=$user[beizhu]?></td>
    <td><?=date('Y-m-d H:i:s',$user[OpenTime]-3600*8)?></td>
    <td><?=$user[CloseTime]>0?date('Y-m-d H:i:s',$user[CloseTime]-3600*8):'-----'?></td>
    <td>
    <?php if($user['isfanyong']){ ?>        
            <span class="label label-success " style="cursor:pointer" >是</span>
    <?php }else{ ?>
            <span class="label label-important " style="cursor:pointer" >否</span>
    <?php } ?>
            </td>
            <td>
    <?php if($user['isfenrun']){ ?>     
            <span class="label label-success " style="cursor:pointer" >是</span>
    <?php }else{ ?>
            <span class="label label-important " style="cursor:pointer" >否</span>
    <?php } ?>
            </td>
    <td>
    <div class="btn-group">
        <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">操作
            <span class="caret"></span>
        </button>
    <ul role="menu" class="dropdown-menu">

<li><a href="adddingdan.php?did=<?=$user[did]?>"><i class="icon-document-edit"></i>&nbsp;&nbsp;修改资料</a></li>
<li><a href="/sys/delete.php?table=danzi&field=did&id=<?=$user[did]?>" 
    onclick="if(!window.confirm('确认删除操作吗？')){ return false;}" ><i class="entypo-cancel-circled"></i>&nbsp;&nbsp;删除单子</a></li>
        </ul>
    </div>
    </td>
</tr>
    <?php } ?>
                                    </tbody>
                                </table>

 <ul class="pagination">
     <?php
if($_GET[mt4]){
    $sql2="select t1.did,t2.uid,t3.nickname from danzi t1 left join mt4 t2 on t1.mt4=t2.mt4 left join users t3 on t2.uid=t3.uid  left join users t5 on t3.tuid=t5.uid where t1.Symbol!=''  $sqlxs and t1.mt4 in ($_GET[mt4]) order by did desc";
}else{
    $sql2="select t1.did,t2.uid,t3.nickname from danzi t1 left join mt4 t2 on t1.mt4=t2.mt4 left join users t3 on t2.uid=t3.uid  left join users t5 on t3.tuid=t5.uid where t1.Symbol!=''  $sqlxs order by did desc ";
}
if($_GET[genshui]){
    $sql2="";
}
$num=$res->fn_num($sql2);

    $myPage=new pager($num,intval($page),$pagesize,"active");     
     $pageStr= $myPage->GetPagerContent();    
     echo $pageStr;   
?>
 
           </ul>
           
           <div class="pull-right">
    <label style="margin:20px">
    <?php
if($_GET[mt4]){
    $sql3="select count(*) num,sum(t1.Lots) lots, sum(t1.Profit) profit, sum(t1.Commission) commission, sum(t1.Swap) swap from danzi t1 left join mt4 t2 on t1.mt4=t2.mt4 left join users t3 on t2.uid=t3.uid  left join users t5 on t3.tuid=t5.uid where   t1.status=2 and t1.Ticket>0  and t1.Operation<=1 and  t1.Symbol!='' and t1.Ticket>0 $sqlxs and t1.mt4 in ($_GET[mt4])  order by did desc";

}else{
    $sql3="select count(*) num,sum(t1.Lots) lots, sum(t1.Profit) profit, sum(t1.Commission) commission, sum(t1.Swap) swap from danzi t1 left join mt4 t2 on t1.mt4=t2.mt4 left join users t3 on t2.uid=t3.uid  left join users t5 on t3.tuid=t5.uid where   t1.status=2 and t1.Ticket>0  and t1.Operation<=1 and t1.Symbol!='' and t1.Ticket>0 $sqlxs  order by did desc ";
}
    $tongji=$res->fn_select($sql3);
    ?>
    单子总量:<?=$tongji[num]?>，手数:<?=round($tongji[lots],2)?>，总收益:<?=round($tongji[profit],2)?>，总手续费:<?=round($tongji[commission],2)?>，总仓息:<?=round($tongji[swap],2)?>
    </label>
</div>  
                            </div>

                        </div>


                    </div>


                </div>
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->

<form name="export-form" id="export-form" action="action.php?type=exportexcel" method="post" target="e" style="display:none">
        <input type="hidden"  name="xtitle" id="xtitle" value="" />
        <input type="hidden"  name="xhead" id="xhead" value="" />
        <input type="hidden"  name="xsql" id="xsql" value="" />
    </form>
    <iframe name="e" style="display:none"></iframe> 
    
    <script type="text/javascript" src="assets/js/jquery.js"></script>

    <!-- GAGE -->


   <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
   <!-- /MAIN EFFECT -->
    <script type="text/javascript" src="assets/js/stackable/stacktable.js"></script>
       <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.js"></script>
     <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
     
<script>

            $('#reservation').daterangepicker({
                            
                    timePicker: true,
                    timePicker12Hour : false, //是否使用12小时制来显示时间  
                    maxDate: '<?=date('Y-m-d H:i')?>', 
                    format: 'YYYY-MM-DD HH:mm',
                    separator:'~',
                     ranges : {  
                        //'最近1小时': [moment().subtract('hours',1), moment()],  
                        '今日': [moment().startOf('day'), moment()],  
                        '昨日': [moment().subtract('days', 1).startOf('day'), moment().subtract('days', 1).endOf('day')],  
                        '最近7日': [moment().subtract('days', 6).startOf('day'), moment()],  
                    
                    },
                    locale : {  
                        applyLabel : '确定',  
                        cancelLabel : '取消',  
                        fromLabel : '起始时间',  
                        toLabel : '结束时间',  
                        customRangeLabel : '自定义',  
                        daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],  
                        monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',  
                                '七月', '八月', '九月', '十月', '十一月', '十二月' ],  
                        firstDay : 1  
                    }  
                },
                function(start, end, label) {
                    console.log(start.toISOString(), end.toISOString(), label);
                });
<?php
 $sql3="select t1.Ticket,t1.mt4,t8.realname,t5.nickname tname,t1.Symbol,CASE WHEN t1.status = '1' THEN '已开仓' ELSE '已平仓' END  status,t1.OpenPrice,t1.ClosePrice,t1.StopLoss,t1.TakeProfit,t1.Lots,t1.Profit,t1.Comment,t1.Commission,t1.Swap,t1.beizhu,FROM_UNIXTIME(t1.OpenTime,'%Y-%m-%d %H:%i:%s') OpenTime,FROM_UNIXTIME(t1.CloseTime,'%Y-%m-%d %H:%i:%s') CloseTime from danzi t1 left join mt4 t2 on t1.mt4=t2.mt4 left join users t3 on t2.uid=t3.uid left join danzi t4 on t1.ddid=t4.did  left join users t5 on t3.tuid=t5.uid left join idcard t8 on t3.uid=t8.uid  where t1.Symbol!=''  $sqlxs  order by t1.did desc";


if($_GET[genshui]){
    $ticket=$_GET[search];
    $sql3="select t1.Ticket,t1.mt4,t8.realname,t5.nickname tname,t1.Symbol,CASE WHEN t1.status = '1' THEN '已开仓' ELSE '已平仓' END  status,t1.OpenPrice,t1.ClosePrice,t1.StopLoss,t1.TakeProfit,t1.Lots,t1.Profit,t1.Comment,t1.Commission,t1.Swap,t1.beizhu,FROM_UNIXTIME(t1.OpenTime,'%Y-%m-%d %H:%i:%s') OpenTime,FROM_UNIXTIME(t1.CloseTime,'%Y-%m-%d %H:%i:%s') CloseTime from danzi t1 left join mt4 t2 on t1.mt4=t2.mt4 left join users t3 on t2.uid=t3.uid left join danzi t4 on t1.ddid=t4.did  left join users t5 on t3.tuid=t5.uid left join idcard t8 on t3.uid=t8.uid  where t1.Symbol!=''  and (t1.did in  (select ddid did from danzi where ticket='$ticket') or t1.ddid in (select ddid from danzi where ticket='$ticket')) order by t1.did asc ";
}

?>  
    $("#export-btn").click(function(){
        var xtitle="订单列表";
        var xhead='单号,MT4,用户名,代理商,商品,操作,开仓价,平仓价,止损价,止盈价,手数,盈利,COMMENT,手续费,仓息,备注,开仓时间,平仓时间';
        var xsql="<?=$sql3?>";
        $("#xtitle").val(xtitle);
        $("#xhead").val(xhead);
        $("#xsql").val(xsql);
        $("#export-form").submit();
        return false;
    });
</script>


<?php include_once 'foot.php'; ?>
</body>

</html>
