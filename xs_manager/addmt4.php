<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css?123">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="assets/js/progress-bar/number-pb.css">
  <link rel="stylesheet" type="text/css" href="assets/js/parsley/src/parsley.css">
<link href="assets/js/bootstrap-datepicker/css/bootstrap-datetimepicker.css" rel="stylesheet" />
    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>会员管理</span>
                        </h2>

                    </div>

                  
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">会员管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">添加MT4</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
			
				  <!--  新增会员-->
		
                    <div class="col-sm-12">
                        <div class="nest" id="elementClose">
                            <div class="title-alt">
                                <h6>添加MT4</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#elementClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#element">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="element">

		 <?php
$user=$res->fn_select("select * from users where uid ='$_GET[mid]'");
$mt4=$res->fn_select("select * from mt4 where mtid ='$_GET[mtid]'");
?>
		<div class="panel-body">
			<form action="action.php?type=addmt4&mid=<?=$user[uid]?>" method="post" class="demo-form form-horizontal bucket-form" data-parsley-validate >
				<input type="hidden" name="uid" id="uid" value="<?=$user[uid]?>" />
				<input type="hidden" name="mtid" id="mtid"  value="<?=$_GET[mtid]?>" />
				<div class="form-group">
					<label class="col-sm-1 control-label">MT4账号</label>
					<div class="col-sm-3">
						<input type="text" name="mt4" id="mt4" class="form-control" data-parsley-required="true" data-parsley-trigger="focusout" value="<?=$mt4[mt4]?>" >
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label">MT4密码</label>
					<div class="col-sm-3">
						<input type="text" name="mt4pwd" id="mt4pwd" class="form-control" value="<?=$mt4[password]?>" onfocus="this.type='password'">
					</div>
				</div>

			<div class="form-group">
					<label class="col-sm-1 control-label">只读密码</label>
					<div class="col-sm-3">
						<input type="text" name="mt4pwd_read" id="mt4pwd_read" class="form-control" value="<?=$mt4[password2]?>" onfocus="this.type='password'">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-1 control-label">MT4组</label>
					<div class="col-sm-3">
						
						 <select name="mt4_groupid" id="mt4_groupid" class="form-control round-input" >
							<?php
						$mt4_groups=$res->fn_rows("select * from mt4_group   order by mt4_groupid desc ");
						foreach($mt4_groups as $mt4_group){
						?>
						<option value="<?=$mt4_group[mt4_groupid]?>" <?php if($mt4_group[mt4_groupid]==$mt4_groupid) echo 'selected="selected"'; ?>><?=$mt4_group[groupname]?></option>

						<?php } ?>
						</select> 
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-1 control-label">MT4杠杆</label>
					<div class="col-sm-3">
						
						 	<input type="text" name="leverage" id="leverage" class="form-control" data-parsley-required="true" data-parsley-trigger="focusout" value="<?=$mt4[leverage]?$mt4[leverage]:100?>" >
						</select> 
					</div>
				</div>
				<?php if($u[groupid]==10){ ?>
				
					<div class="form-group ">
					<label class="col-sm-1 control-label">MT4-IP</label>
					<div class="col-sm-3">
					 
						 <select name="svid" class="form-control round-input" >
							<?php
						$rs=$res->fn_sql("select * from server  where status=1  order by ip asc ");
						while($server=mysql_fetch_array($rs)){
						?>      
						<option value="<?=$server[svid]?>" <?php if($mt4[svid]==$server[svid])echo "selected='selected'";?>><?=$server[ip]?></option>

						<?php } ?>
						</select>  
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-1 control-label">MT4-端口</label>
					<div class="col-sm-3">
						<input type="text" name="mt4port" id="mt4port" class="form-control round-input" value="<?=$mt4['port']?>">
						 <span>可以使用端口号22000-22100</span>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-1 control-label">跟随基数</label>
					<div class="col-sm-3">
						<input type="text" name="base" id="base" class="form-control round-input" value="<?=$mt4['base']?>">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-1 control-label">信号等级</label>
					<div class="col-sm-3">
						 <select name="level" class="form-control round-input" >
							<option value="1" <?php if($mt4[level]==1)echo "selected='selected'";?>>低胜率</option>
							<option value="2" <?php if($mt4[level]==2)echo "selected='selected'";?>>中胜率</option>
							<option value="3" <?php if($mt4[level]==3)echo "selected='selected'";?>>高胜率</option>
						</select>  
					</div>
				</div>
				
				<?php } ?>
				
				<div class="col-lg-offset-1 col-lg-10">
					<button class="btn btn-info" type="submit">提交</button>
				</div>
			</form>
		</div>

                            </div>

							</div>
						</div>
				   <!-- end 新增会员-->
				 
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->

	
	<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal13 -->
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
			<div class="form-horizontal" >
					<input type="hidden" name="tid" id="tid" class="form-control round-input" value="" readonly>
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">MT4账号：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="mt4_email" id="mt4_email" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">MT4密码：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="mt4pwd_email" id="mt4pwd_email" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">观摩密码：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="mt4pwd_read_email" id="mt4pwd_read_email" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">MT4邮箱：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="mt4mail_email" id="mt4mail_email" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="email-btn">发送邮件</button>
      </div>
    </div>
  </div>
</div>
	<!-- Modal6 -->
<div class="modal fade" id="myModal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
			是否开设新账号？
      </div>
      <div class="modal-footer">
			<button type="button" class="btn btn-success" data-dismiss="modal" id="newuser-btn">是</button>
			<button type="button" class="btn btn-primary" data-dismiss="modal" id="user-btn">否</button>
      </div>
    </div>
  </div>
</div>
    <!-- RIGHT SLIDER CONTENT -->
   

    <!-- END OF RIGHT SLIDER CONTENT-->
    <script type="text/javascript" src="assets/js/jquery.js"></script>
    <script src="assets/js/progress-bar/src/jquery.velocity.min.js"></script>
    <script src="assets/js/progress-bar/number-pb.js"></script>
    <script src="assets/js/progress-bar/progress-app.js"></script>



    <!-- MAIN EFFECT -->
    <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>




    <!-- GAGE -->

<script type="text/javascript" src="assets/js/bootstrap-datepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-datepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
	
<script type="text/javascript" src="assets/js/parsley/dist/parsley.js"></script>
<script type="text/javascript" src="assets/js/parsley/dist/i18n/zh_cn.js"></script>
	  <!--socoket start-->
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/web_socket.js"></script>
<script type="text/javascript" src="/js/json.js"></script>
<script type="text/javascript" src="/static/websocket_config.js"  media="all"></script>
<script>
 function init() {
		ws = new WebSocket("ws://"+WS_HOST2+":"+WS_PORT2);
		ws.onopen = function() {
    	    timeid && window.clearTimeout(timeid);
		   	console.log("连接成功");
      };
		if(reconnect>6){
			window.clearTimeout(timeid);
		}
		ws.onmessage = function(e) {
				console.log("收到数据："+e.data);
				var data = JSON.parse(e.data);
				switch(data['type']){
					case 'adduser':
						$('#myModal').modal('hide');
						if(data['code']==200){
							var uinfo=data['data'];
							var beizhu=data['beizhu'].split("-");
							var mt4pwd_read=beizhu[0];
							var uid=beizhu[1];
							var mt4_groupid=$("#mt4_groupid").val();
							var leverage=$("#leverage").val();
							
							$.post("action.php?type=addmt4",{uid:uid,mt4:uinfo.key,mt4pwd:uinfo.password,mt4pwd_read:mt4pwd_read,svid:134,mt4_groupid:mt4_groupid,leverage:leverage},function(data,status){

								if(typeof data=="object"){
									$("#mt4_email").val(uinfo.key);
									$("#mt4pwd_email").val(uinfo.password);
									$("#mt4pwd_read_email").val(mt4pwd_read);
									$("#mt4mail_email").val(uinfo.email);
									$('#myModal3').modal('show');
								}else if(data=="invalidate_mt4"){
					
									$('#myModal').modal('show')
									$('#myModal .modal-body').html('MT4已存在'); 
								
								}else if(data=="invalidate_port"){
									
									$('#myModal').modal('show')
									$('#myModal .modal-body').html('端口不可用已'); 
								
								}
							},'json');
						}else{
							$('#myModal').modal('show');
							$('#myModal .modal-body').html('申请失败'); 
						}
						
					break;
					
					
				}
		
		};
	  ws.onclose = function() {
		  reconnect ++;
    	  console.log("连接关闭，定时重连");
    	  timeid = window.setTimeout(init, 3000);
      };
      ws.onerror = function() {
			reconnect ++;
    	    console.log("出现错误");
		    timeid = window.setTimeout(init, 3000);
      };
	}
   init();
  $('.demo-form').parsley().on('form:submit', function (formInstance) {
	$('#myModal6').modal('show');
	
	return false;
  });
  
  $("#newuser-btn").click(function(){
		var uid = $("#uid").val();
		var mt4 = $("#mt4").val();
		var mtid = $("#mt4").val();
		var mt4pwd = $("#mt4pwd").val();
		var mt4pwd_read = $("#mt4pwd_read").val();
		var mt4_groupid = $("#mt4_groupid").val();
		var leverage = $("#leverage").val();
		var mt4_groupname = $("#mt4_groupid option:selected").text();
		$.post("action.php?type=getmt42",{uid:uid,mt4:mt4,mtid:mtid},function(data,status){
			
			if(typeof data=="object"){
				var json='{"type":"adduser","name":"'+data.realname+'","email":"'+data.email+'","password":"'+mt4pwd+'","group":"'+mt4_groupname+'","login":"'+mt4+'","city":"suzhou","province":"jiangsu","country":"china","address":"","password_investor":"'+mt4pwd_read+'","password_change":"1","postcode":"215007","telephone":"'+data.mobile+'","leverage":'+leverage+',"beizhu":"'+mt4pwd_read+'-'+data.uid+'","token":"pong"}';
				console.log(json);
				ws.send(json);
				$('#myModal').modal('show');
				$('#myModal .modal-body').html('MT4申请中。。。。'); 
			}else if(data=="invalidate_mt4"){
				$('#myModal').modal('show')
				$('#myModal .modal-body').html('MT4已存在'); 
			}else if(data=="invalidate_port"){
				$('#myModal').modal('show')
				$('#myModal .modal-body').html('端口不可用已'); 
			
			}
		},"json");
	});
	
	$("#user-btn").click(function(){
		var options = {
			url: 'action.php?type=addmt4',
			type: 'post',
			dataType:'json',
			data: $(".demo-form").serialize(),
			success: function (data) {
				if(typeof data == 'object'){
					$('#myModal').modal('show')
					$('#myModal .modal-body').html('提交成功'); 
					$('#myModal').on('hidden.bs.modal', function (e) {
							<?php if($_GET[mid]){ ?>
						setTimeout(function(){window.history.back()},1000);
						<?php }else{ ?>
						setTimeout(function(){window.location.reload()},1000);
						<?php } ?>
					})
					
				}else if(data=="invalidate_mt4"){
					
					$('#myModal').modal('show')
					$('#myModal .modal-body').html('MT4已存在'); 
				
				}else if(data=="invalidate_port"){
					
					$('#myModal').modal('show')
					$('#myModal .modal-body').html('端口不可用已'); 
				
				}
			}
		 };
		$.ajax(options);
	});
	
	$("#email-btn").click(function(){
		var mt4=$("#mt4_email").val();
		var mt4pwd=$("#mt4pwd_email").val();
		var mt4pwd_read=$("#mt4pwd_read_email").val();
		var email=$("#mt4mail_email").val();
		$.post("/mail/send_mt4.php",{mt4:mt4,mt4pwd:mt4pwd,mt4pwd_read:mt4pwd_read,email:email},function(data,status){
				$('#myModal3').modal('hide');
				$('#myModal1').modal('show');
				$('#myModal1 .modal-body').html('发送成功'); 
				$('#myModal1').on('hidden.bs.modal', function (e) {
					window.location.reload();
				});
		});
	});
</script>

<?php include_once 'foot.php'; ?>
</body>

</html>
