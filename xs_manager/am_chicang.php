<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

	
     <link href="assets/js/stackable/stacktable.css" rel="stylesheet">
    <link href="assets/js/stackable/responsive-table.css" rel="stylesheet">
<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />


    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>订单管理</span>
                        </h2>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">订单管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">当前持仓列表</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
				<div class="row">

                   <div class="col-sm-12">

                        <div class="nest" id="StackableClose">
                            <div class="title-alt">
                                <h6>当前持仓列表</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#tStackableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>


                            </div>

                            <div class="body-nest" id="StackableStatic">

                                <table id="responsive-example-table" class="table large-only">
                                    <thead>
                                        <tr class="text-right">

											<th style="width:5%;">ID</th>
											<th style="width:10%;">单号</th>
											<th style="width:10%;">MT4</th>
											<th style="width:10%;">商品</th>
											<th style="width:10%;">操作</th>
											<th style="width:10%;">开仓价</th>
											<th style="width:10%;">止损价/止盈价</th>
											<th style="width:5%;">手数</th>
											<th style="width:10%;">开仓时间</th>
											<th style="width:10%;">获利</th>
                                        </tr>
								  </thead>
									<tbody>

                                    </tbody>
                                </table>

                            </div>

                        </div>


                    </div>


                </div>
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->

<form name="export-form" id="export-form" action="action.php?type=exportexcel" method="post" target="e" style="display:none">
		<input type="hidden"  name="xtitle" id="xtitle" value="" />
		<input type="hidden"  name="xhead" id="xhead" value="" />
		<input type="hidden"  name="xsql" id="xsql" value="" />
	</form>
	<iframe name="e" style="display:none"></iframe>	



    <script type="text/javascript" src="assets/js/jquery.js"></script>

    <!-- GAGE -->


<script type="text/javascript" src="assets/js/preloader.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/load.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
<!-- /MAIN EFFECT -->
<script type="text/javascript" src="assets/js/stackable/stacktable.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
		  <!--socoket start-->
  <script type="text/javascript" src="/js/swfobject.js"></script>
  <script type="text/javascript" src="/js/web_socket.js"></script>
  <script type="text/javascript" src="/js/json.js"></script>
<script type="text/javascript" src="/static/websocket_config.js"  media="all"></script> 
<script>
function init() {
		ws = new WebSocket("ws://"+WS_HOST2+":"+WS_PORT2);
		ws.onopen = function() {
    	    timeid && window.clearTimeout(timeid);
		   	console.log("连接成功");
			ws.send('{"type":"queryall","beizhu":"xxx","token":"pong"}');

      };
		if(reconnect>6){
			window.clearTimeout(timeid);
		}
		ws.onmessage = function(e) {
				console.log("收到数据："+e.data);
				var data = JSON.parse(e.data);

				switch(data['type']){

					case 'queryall':
						var danzis = data.data;

						var profit=0;
						var lots=0;
						for(i in danzis){
							showChicang(danzis[i],i);
						}

					break;

				}

		};
	  ws.onclose = function() {
		  reconnect ++;
    	  console.log("连接关闭，定时重连");
    	  timeid = window.setTimeout(init, 3000);
      };
      ws.onerror = function() {
			reconnect ++;
    	    console.log("出现错误");
		    timeid = window.setTimeout(init, 3000);
      };
	}
	   init();
	   $('#reservation').daterangepicker({

			timePicker: true,
			timePicker12Hour : false, //是否使用12小时制来显示时间  
			maxDate: '<?=date('Y-m-d H:i')?>', 
			format: 'YYYY-MM-DD HH:mm',
			separator:'~',
			 ranges : {  
				//'最近1小时': [moment().subtract('hours',1), moment()],  
				'今日': [moment().startOf('day'), moment()],  
				'昨日': [moment().subtract('days', 1).startOf('day'), moment().subtract('days', 1).endOf('day')],  
				'最近7日': [moment().subtract('days', 6).startOf('day'), moment()],  

			},
			locale : {  
				applyLabel : '确定',  
				cancelLabel : '取消',  
				fromLabel : '起始时间',  
				toLabel : '结束时间',  
				customRangeLabel : '自定义',  
				daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],  
				monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',  
						'七月', '八月', '九月', '十月', '十一月', '十二月' ],  
				firstDay : 1  
			}  
		},
		function(start, end, label) {
			console.log(start.toISOString(), end.toISOString(), label);
		});


		function showChicang(order,key){
			var 	html='       <tr  data-order="'+order.order+'" data-mt4="'+order.login+'">'
						+'			<td>'+(parseInt(key)+1)+'</td>'
						+'			<td>'+order.order+'</td>'
						+'			<td>'+order.login+'</td>'
						+'			<td>'+order.symbol+'</td>'
						+'			<td>'+(order.cmd==0?'买':'卖')+'</td>'
						+'			<td>'+order.openprice+'</td>'
						+'			<td>'+order.sl+'/'+order.tp+'</td>'
						+'			<td>'+(order.volume/100).toFixed(2)+'</td>'
						+'			<td>'+(new Date(parseInt(order.opentime-3600*8) * 1000).toLocaleString().replace('上午','').replace('下午',''))+'</td>'
						+'			<td>'+order.profit.toFixed(2)+'</td>'
						+'        </tr>';

			$("#responsive-example-table tbody").append(html);
		}

		function pingCang(obj){
			console.log($(obj).parent().parent().attr("data-mt4"));
			$("#mt4").val($(obj).parent().parent().attr("data-mt4"));
			$("#ticket").val($(obj).parent().parent().attr("data-order"));
			$('#myModal2').modal('show');
		}


	</script>


<?php include_once 'foot.php'; ?>
</body>

</html>
