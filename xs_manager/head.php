 <?php
header('P3P: CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"');
include("../sys/conn.php");
include("../sys/mysql.class.php");
include("../sys/page.class.php");

if(!$_SESSION[aid]){
	echo "<script type='text/javascript'>window.location='login.php';</script>";
	exit;
}else{
$u=$res->fn_select("select * from admin where aid = '$_SESSION[aid]'");
if(!$u){
	echo "<script type='text/javascript'>alert('对不起，这里不是你该来的地方，请换个账号登陆！');window.location='login.php';</script>";
	exit;
}

}

$php_self=substr($_SERVER['PHP_SELF'],strrpos($_SERVER['PHP_SELF'],'/')+1);

	$weishenhe_mt4=$res->fn_num("select t1.mtid from mt4 t1 left join users t2 on t1.uid=t2.uid where 1=1  and t2.groupid!=10 and t1.shenhe=0");
	$weishenhe_tixian=$res->fn_num("select t1.tid from tixian t1  where 1=1  and t1.status=0 and uid in (select uid from users)");
	$weishenhe_idcard=$res->fn_num("select t1.id from idcard t1 left join users t2 on t1.uid=t2.uid where 1=1  and t1.idstatus=0 and t1.uid in (select uid from users) and t2.groupid!=10 ");
	$weishenhe_handan=$res->fn_num("select t1.hid from handan t1  where 1=1  and t1.status=0 and uid in (select uid from users)");
?>
 <nav role="navigation" class="navbar navbar-static-top">
        <div class="container-fluid" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="entypo-menu"></span>
                </button>
                <button class="navbar-toggle toggle-menu-mobile toggle-left" type="button">
                    <span class="entypo-list-add"></span>
                </button>

            </div>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
                <div id="nt-title-container" class="navbar-left running-text visible-lg">
                    <ul class="date-top">
                        <li class="entypo-calendar" style="margin-right:5px"></li>
                        <li id="Date"></li>
                    </ul>

                    <ul id="digital-clock" class="digital">
                        <li class="entypo-clock" style="margin-right:5px"></li>
                        <li class="hour"></li>
                        <li>:</li>
                        <li class="min"></li>
                        <li>:</li>
                        <li class="sec"></li>
                        <li class="meridiem"></li>
                    </ul>
                </div>

                <ul style="margin-right:0;" class="nav navbar-nav navbar-right">
                    <li>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <img alt="" class="admin-pic img-circle" src="assets/img/10.jpg">Hi, <?=$u[username]?> <b class="caret"></b>
                        </a>
                        <ul style="margin-top:14px;" role="menu" class="dropdown-setting dropdown-menu">
                            <li>
                                <a href="/sys/off.php?url1=<?php echo dirname(__FILE__) ?>">
                                    <span class="entypo-logout"></span>&#160;&#160;退出</a>
								 <a href="addadmin.php">
                                    <span class="entypo-user"></span>&#160;&#160;修改密码</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="icon-gear"></span>&#160;&#160;设置</a>
                        <ul role="menu" class="dropdown-setting dropdown-menu">

                            <li class="theme-bg">
                                <div id="button-bg"></div>
                                <div id="button-bg2"></div>
                                <div id="button-bg3"></div>
                                <div id="button-bg5"></div>
                                <div id="button-bg6"></div>
                                <div id="button-bg7"></div>
                                <div id="button-bg8"></div>
                                <div id="button-bg9"></div>
                                <div id="button-bg10"></div>
                                <div id="button-bg11"></div>
                                <div id="button-bg12"></div>
                                <div id="button-bg13"></div>
                            </li>
                        </ul>
                    </li>
                    <li class="hidden-xs">
                        <a class="toggle-left" href="#">
                            <span style="font-size:20px;" class="entypo-list-add"></span>
                        </a>
                    </li>
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>