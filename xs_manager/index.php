<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->

    <link rel="stylesheet" href="assets/css/style.css?<?=rand(1000,9999)?>">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

	<link rel="stylesheet" type="text/css" href="assets/js/progress-bar/number-pb.css">
	<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />

    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
	input[type='checkbox']{width:16px;height:16px}
	tr.selected td{background-color:#ffc!important}
	tr.selected.error td{background-color:#d9534f!important;color:#fff}
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->

<?php include_once 'head.php';?>
    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
      
					<?php include_once 'left.php'; ?>
       
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>控制面板</span>
                        </h2>

                    </div>

                  
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">主页</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">控制面板</a>
                </li>
               
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
			<div class="row">
				<div class="body-nest" id="inlineClose">
				   <div class="form_center" style="width:100%">
						<form role="form" class="form-inline " method="get">
							<div class="form-group">
								<input type="text" placeholder="时间范围" class="form-control" id="reservation" name="reservation" style="width:320px" value="<?=$_GET[reservation]?>" autocomplete="off"> 
								<button type="submit" class="btn btn-primary">搜索</button>
							</div>
						</form>
					</div>
                </div>
			</div>
		<?php
			if($_GET['reservation']!= null){
					$reservation=explode("~",$_GET['reservation']);
					$start_time=strtotime($reservation[0]);
					$end_time=strtotime($reservation[1]);
					
					$sqlxs1=" and regtime>'$start_time' and regtime<'$end_time'";
					$sqlxs2=" and time>'$start_time' and time<'$end_time'";
					$sqlxs3=" and CloseTime>'$start_time' and CloseTime<'$end_time'";
			}
		?>
            <div class="row" style = "margin-top:20px">
                    <div class="col-lg-3">
                        <div class="dashboard-stat red" id="dashboard1">
                            <div class="headline ">
                                <h3>
                                    <span>
                                        <i class="entypo-users"></i>&nbsp;&nbsp;网站注册情况</span>
                                </h3>
                                <div class="titleClose">
                                    <a href="#dashboard1" class="gone">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                            </div>

                            <div class="value">
                                <span><i class="entypo-users"></i></span>
								<a href="am_users.php"><?=$res->fn_num("select uid from users where groupid<>10  $sqlxs1")?></a>

                            </div>

                            <div class="details">
                                <i class="fa fa-caret-up fa-lg"></i>&nbsp;&nbsp; 未审核：<?=$weishenhe_idcard?>
							</div>
						</div>
                    </div>
                  
				    <div class="col-lg-3">
                        <div class="dashboard-stat blue" id="dashboard2">
                            <div class="headline ">
                                <h3>
                                    <span>
                                        <i class="fa fa-money"></i>&nbsp;&nbsp;总返佣金额</span>
                                </h3>
                                <div class="titleClose">
                                    <a href="#dashboard2" class="gone">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                            </div>

                            <div class="value">
                                <span><i class="fa fa-money"></i></span>
							<a href="am_fanyong_list.php"><?php 
							$fanyong=$res->fn_select("select sum(money) money from fanyong where status=1 $sqlxs2");
							echo round($fanyong[money],2);
							?></a>

                            </div>
		
                              <div class="details">
                                <i class="fa fa-caret-up fa-lg"></i>
							</div>
						</div>
                    </div>
					
					
					 <div class="col-lg-3">
                        <div class="dashboard-stat green" id="dashboard3">
                            <div class="headline ">
                                <h3>
                                    <span>
                                        <i class="fa fa-money"></i>&nbsp;&nbsp;出金量</span>
                                </h3>
                                <div class="titleClose">
                                    <a href="#dashboard3" class="gone">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                            </div>

                            <div class="value" >
                                <span><i class="fa fa-money"></i></span>
							<a href="am_tixian.php"><?php 
							$chujin=$res->fn_select("select sum(jine) money from tixian where status=1 $sqlxs2");
							
							?><?=round($chujin[money],2);?></a>	
                            </div>
		
                              <div class="details">
                                <i class="fa fa-caret-up fa-lg"></i>
								<?php 
							$chujin2=$res->fn_select("select count(*) num from tixian where status=0 $sqlxs2");
							
							?>		
								
								<em>未支付：<?=$chujin2[num];?>笔</em>
							</div>
						</div>
                    </div>
					
					
					 <div class="col-lg-3">
                        <div class="dashboard-stat yellow" id="dashboard4">
                            <div class="headline ">
                                <h3>
                                    <span>
                                        <i class="fa fa-money"></i>&nbsp;&nbsp;入金量</span>
                                </h3>
                                <div class="titleClose">
                                    <a href="#dashboard4" class="gone">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                            </div>

                            <div class="value">
                                <span><i class="fa fa-money"></i></span>
								<a href="am_rujin.php"><?php 
							$rujin=$res->fn_select("select sum(jine) money from rujin where status=1 $sqlxs2");
							
							?><?=round($rujin[money],2);?></a>

                            </div>
		
                              <div class="details">
                                <i class="fa fa-caret-up fa-lg"></i>
								
							</div>
						</div>
                    </div>
					
					
					<div class="col-lg-3">
                        <div class="dashboard-stat green" id="dashboard5">
                            <div class="headline ">
                                <h3>
                                    <span>
                                        <i class="entypo-briefcase"></i>&nbsp;&nbsp;当前持仓手数/笔数</span>
                                </h3>
                                <div class="titleClose">
                                    <a href="#dashboard5" class="gone">
                                        <span class="entypo-cancel"></span>
										
                                    </a>
                                </div>
                            </div>

                            <div class="value" style="font-size:42px">
                                <span><i class="entypo-briefcase"></i></span>
								<a href="am_chicang.php"></a>
							
                            </div>
		
                              <div class="details">
                                <i class="fa fa-caret-up fa-lg"></i>&nbsp;&nbsp; 获利：$
								<em></em>
							</div>
						</div>
                    </div>
					
					
					<div class="col-lg-3">
                        <div class="dashboard-stat red" id="dashboard6">
                            <div class="headline ">
                                <h3>
                                    <span>
                                        <i class="entypo-briefcase"></i>&nbsp;&nbsp;已平仓手数/笔数</span>
                                </h3>
                                <div class="titleClose">
                                    <a href="#dashboard6" class="gone">
                                        <span class="entypo-cancel"></span>
										
                                    </a>
                                </div>
                            </div>

                            <div class="value" style="font-size:42px">
                                <span><i class="entypo-briefcase"></i></span>
							<?php 
							$pingcang=$res->fn_select("select count(did) num, sum(Lots) lots,sum(Profit) profit from danzi left join mt4 on danzi.mt4 = mt4.mt4  where danzi.status=2 and mt4.svid = 134 and Ticket > 0 and Operation<=1 $sqlxs3");
							
							?>		
								
								<a href="am_dingdan.php"><?=round($pingcang[lots],2);?></a>/<a href="am_dingdan.php"><?=$pingcang[num];?></a>
							
                            </div>
		
                              <div class="details">
                                <i class="fa fa-caret-up fa-lg"></i>&nbsp;&nbsp; 获利：$
								<em><?=round($pingcang[profit],2);?></em>
							</div>
						</div>
                    </div>
					
				
					
					<div class="col-lg-3">
                        <div class="dashboard-stat purple" id="dashboard8">
                            <div class="headline ">
                                <h3>
                                    <span>
                                        <i class="fa fa-money"></i>&nbsp;&nbsp;浮动盈亏</span>
                                </h3>
                                <div class="titleClose">
                                    <a href="#dashboard8" class="gone">
                                        <span class="entypo-cancel"></span>
										
                                    </a>
                                </div>
                            </div>

                            <div class="value" >
                                <span><i class="fa fa-money"></i></span>
								<a href="am_chicang.php"></a>
							
                            </div>
		
                              <div class="details">
                                <i class="fa fa-caret-up fa-lg"></i>
							</div>
						</div>
                    </div>
					
					
                </div>
				
				<div class="row" >
					<div class="col-sm-12">
						<div class="nest" id="StackableClose">
							<div class="title-alt">
								 <h6>持仓列表</h6>
								 <div class="titleToggle">
									<span class="">
										<input type="checkbox" class="select-all" style=";position:relative;top:2px">
										全选
									</span>
								    <a type="button" class="btn btn-primary pingcang-btn">
										<span class="fontawesome-money"></span>&nbsp;&nbsp;平仓
									</a>
									 <a type="button" class="btn btn-danger refresh-btn">
										<span class="fontawesome-refresh"></span>&nbsp;&nbsp;<b>刷新</b>
									</a>
                                </div>
							</div>
							<div class="body-nest">
								 <table id="chicang-example-table" class="table large-only table-striped ">
											<thead>
												<tr class="text-right">
											
													<th style="width:5%;">ID</th>
													<th style="width:15%;">单号</th>
													<th style="width:10%;">MT4</th>
													<th style="width:5%;">商品</th>
													<th style="width:5%;">操作</th>
													<th style="width:10%;">开仓价</th>
													<th style="width:10%;">止损价/止盈价</th>
													<th style="width:5%;">手数</th>
													<th style="width:10%;">开仓时间</th>
													<th style="width:10%;">COMMENT</th>
													<th style="width:10%;">获利</th>
													<th style="width:5%;">操作</th>
												</tr>
										   </thead>
											<tbody>	
											</tbody>
									
								</table>
							</div>
							<div class="title-alt" style="margin-top:0px">
								
								 <div class="titleToggle">
									<span class="">
										<input type="checkbox" class="select-all" style=";position:relative;top:2px">
										全选
									</span>
								    <a type="button" class="btn btn-primary pingcang-btn">
										<span class="fontawesome-money"></span>&nbsp;&nbsp;平仓
									</a>
									 <a type="button" class="btn btn-danger refresh-btn">
										<span class="fontawesome-refresh"></span>&nbsp;&nbsp;<b>刷新</b>
									</a>
                                </div>
							</div>
						</div>
					</div>
				</div>
            </div>
            <!--  / DEVICE MANAGER -->

            <div class="content-wrap">
                <div class="row">
                    <div class="col-lg-6">
                    
                    </div>
                  
                </div>
                <!-- /END OF CONTENT -->



                <!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">


                </div>
                <!-- / END OF FOOTER -->


            </div>
        </div>
    </div>
	
    <!--  END OF PAPER WRAP -->

    <!-- RIGHT SLIDER CONTENT -->
   	<!-- Modal1 -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>

  
<!-- Modal17 -->
<div class="modal fade" id="myModal7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="height:500px;width:1000px">
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">当前持仓</h4>
      </div>
      <div class="modal-body"  style="height:600px;overflow-y:auto">
			<div class="title-alt">
				<label >MT4:<span rel="data-mt4" class="label label-primary "></span> </label>
			</div>
			<table id="chicang-example-table" class="table large-only" >
						<thead>
							<tr class="text-right">
						
								<th style="width:5%;">ID</th>
								<th style="width:10%;">单号</th>
								<th style="width:10%;">商品</th>
								<th style="width:10%;">操作</th>
								<th style="width:10%;">开仓价</th>
								<th style="width:20%;">止损价/止盈价</th>
								<th style="width:10%;">手数</th>
								<th style="width:15%;">开仓时间</th>
								<th style="width:10%;">获利</th>
								
							</tr>
					   </thead>
						<tbody>	
						</tbody>
				<tbody>

				</tbody>
			</table>
      </div>
      <div class="modal-footer">
   
      </div>
    </div>
  </div>
</div>

    <!-- END OF RIGHT SLIDER CONTENT-->
    <script type="text/javascript" src="assets/js/jquery.js"></script>
    <script src="assets/js/progress-bar/src/jquery.velocity.min.js"></script>
    <script src="assets/js/progress-bar/number-pb.js"></script>
    <script src="assets/js/progress-bar/progress-app.js"></script>



    <!-- MAIN EFFECT -->
    <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>



	  <!--socoket start-->
  <script type="text/javascript" src="/js/swfobject.js"></script>
  <script type="text/javascript" src="/js/web_socket.js"></script>
  <script type="text/javascript" src="/js/json.js"></script>
<script type="text/javascript" src="/static/websocket_config.js"  media="all"></script>
    <!-- GAGE -->
    <script src="assets/js/jhere-custom.js"></script>
<script>
	 function init() {
		ws = new WebSocket("ws://"+WS_HOST2+":"+WS_PORT2);
		ws.onopen = function() {
    	    timeid && window.clearTimeout(timeid);
		   	console.log("连接成功");
			
			ws.send('{"type":"queryall","beizhu":"xxx","token":"pong"}');

      };
		if(reconnect>6){
			window.clearTimeout(timeid);
		}
		ws.onmessage = function(e) {
				console.log("收到数据："+e.data);
				var data = JSON.parse(e.data);
				
				switch(data['type']){
				   
					case 'queryall':
						var danzis = data.data;
					
						var profit=0;
						var lots=0;
						var html='';
						var operation={0:"BUY",1:"SELL",2:"BUY LIMIT",3:"SELL LIMIT",4:"BUY STOP",5:"SELL STOP"};						
						for(i in danzis){
							
							profit+=parseFloat(danzis[i].profit);
							lots+=parseFloat(danzis[i].volume);
							var order = danzis[i];
						
							html+='       <tr data-order="'+order.order+'" data-mt4="'+order.login+'"  data-operation="'+order.cmd+'">'
								+'			<td>'+(parseInt(i)+1)+'</td>'
								+'			<td>'+order.order+(order.comment.indexOf("xs_")>=0?'  <a class="btn-xs  btn-warning" onclick="openGendan(this)">老师持仓</a>':'')
								+'			</td>'
								+'			<td>'+order.login+'</td>'
								+'			<td>'+order.symbol+'</td>'
								+'			<td>'+operation[order.cmd]+'</td>'
								+'			<td>'+order.openprice+'</td>'
								+'			<td>'+order.sl+'/'+order.tp+'</td>'
								+'			<td>'+(order.volume/100).toFixed(2)+'</td>'
								+'			<td>'+(new Date(parseInt(order.opentime) * 1000).toLocaleString().replace('上午','').replace('下午',''))+'</td>'
								+'			<td>'+order.comment+'</td>'
								+'			<td>'+order.profit.toFixed(2)+'</td>'
								+'			<td><input type="checkbox" name="chicang-box[]" value="'+order.order+'"/></td>'
                                +'        </tr>';	
						}
						lots = lots / 100;
						$("#dashboard5 .value a").html(lots.toFixed(2)+"/"+danzis.length);
						$("#dashboard5 .details em").html(profit.toFixed(2));
						$("#dashboard8 .value a").html(profit.toFixed(2));
						
						$("#chicang-example-table tbody").html(html);
					//	TableManageTableSelect.init();
						shift_select();
						$('#myModal1').modal('hide');
					break;
					case 'pingcang':
					case 'delcang':
						if(data['code'] == "200"){
							var ticket =  data['dnum'];
							$("#chicang-example-table tr[data-order='"+ticket+"']").remove();
							$.post("action.php?type=pingcang",{ticket:ticket},function(data,status){});
						}else{
							
							$("#chicang-example-table tr[data-order='"+ticket+"'] td:nth-child(2)").append('<font color="red">平仓失败</font>');
						}
						dealPingchang(danzis);
					break;
				}
		
		};
	  ws.onclose = function() {
		  reconnect ++;
    	  console.log("连接关闭，定时重连");
    	  timeid = window.setTimeout(init, 3000);
      };
      ws.onerror = function() {
			reconnect ++;
    	    console.log("出现错误");
		    timeid = window.setTimeout(init, 3000);
      };
	}
	   init();
	   var timeid2;
	 function init2() {
		ws2 = new WebSocket("ws://"+WS_HOST+":"+WS_PORT);
		ws2.onopen = function() {
    	    timeid2 && window.clearTimeout(timeid2);
		   	console.log("连接成功");
			//getYue();
		};
		if(reconnect>6){
			window.clearTimeout(timeid2);
		}
		ws2.onmessage = function(e) {
				console.log("收到数据："+e.data);
				var data = JSON.parse(e.data);
				switch(data['type']){
					case 'userinfo':
						var info=data['data'];
						var port= data['port'];
						var mt4 = MT4_PORT[port];
						$("#myModal7 .title-alt span[rel='data-mt4']").html(mt4);
						$("#myModal7 .title-alt span[rel='data-balance']").html(info.Balance.toFixed(2));
						$("#myModal7 .title-alt span[rel='data-equity']").html(info.Equity.toFixed(2));
						$("#myModal7").modal('show');
					break;
					case 'current':
						
						var html='';
						var port= data['port'];
						for(i in data['data']){ 
							var order=data['data'][i];
							
							html+='       <tr data-order="'+order.Ticket+'" data-mt4="'+mt4+'" data-port="'+port+'">'
								+'			<td>'+(i+1)+'</td>'
								+'			<td>'+order.Ticket+'</td>'
								+'			<td>'+order.Symbol+'</td>'
								+'			<td>'+(order.Operation==0?'买':'卖')+'</td>'
								+'			<td>'+order.OpenPrice+'</td>'
								+'			<td>'+order.TakeProfit+'/'+order.StopLoss+'</td>'
								+'			<td>'+order.Lots.toFixed(2)+'</td>'
								+'			<td>'+order.OpenTime.replace('T',' ')+'</td>'
								+'			<td>'+order.Profit.toFixed(2)+'</td>'
						
                                +'        </tr>';	
						}
						$('#myModal1').modal('hide');
						$("#myModal7").modal('show');
						$("#myModal7 tbody").html(html);
					break;
					
				}
		
		};
	  ws2.onclose = function() {
		  reconnect ++;
    	  console.log("连接关闭，定时重连");
    	  timeid2 = window.setTimeout(init2, 3000);
      };
      ws2.onerror = function() {
			reconnect ++;
    	    console.log("出现错误");
		    timeid2 = window.setTimeout(init2, 3000);
      };
	}
	  init2();
	   $('#reservation').daterangepicker({
							
			timePicker: true,
			timePicker12Hour : false, //是否使用12小时制来显示时间  
			maxDate: '<?=date('Y-m-d H:i')?>', 
			format: 'YYYY-MM-DD HH:mm',
			separator:'~',
			 ranges : {  
				//'最近1小时': [moment().subtract('hours',1), moment()],  
				'今日': [moment().startOf('day'), moment()],  
				'昨日': [moment().subtract('days', 1).startOf('day'), moment().subtract('days', 1).endOf('day')],  
				'最近7日': [moment().subtract('days', 6).startOf('day'), moment()],  
			
			},
			locale : {  
				applyLabel : '确定',  
				cancelLabel : '取消',  
				fromLabel : '起始时间',  
				toLabel : '结束时间',  
				customRangeLabel : '自定义',  
				daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],  
				monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',  
						'七月', '八月', '九月', '十月', '十一月', '十二月' ],  
				firstDay : 1  
			}  
		},
		function(start, end, label) {
			console.log(start.toISOString(), end.toISOString(), label);
		});
		
		
		/*var handleDataTableSelect=function(){
			"use strict";
			0!==$("#chicang-example-table").length&&$("#chicang-example-table").DataTable({select:!0,responsive:!0})
		},
		TableManageTableSelect=function(){
			"use strict";
			return{
				init:function(){handleDataTableSelect()}
			}
		}();*/
	$(".select-all").click(function(){

		if($(this).is(':checked')){ 
			$("#chicang-example-table tbody input[type='checkbox']").prop('checked',true)
		}else{ 
			$("#chicang-example-table tbody input[type='checkbox']").prop('checked',false)
		}
		
	});
	var danzis=new Array();
	$(".pingcang-btn").click(function(){
		danzis=new Array();
		$("#chicang-example-table tbody tr").each(function(){
			if($(this).find("input[type='checkbox']").prop("checked")){
				var mt4=$(this).attr("data-mt4");
				var ticket=$(this).attr("data-order");
				var operation=$(this).attr("data-operation");
				danzis.push([mt4,ticket,operation]);
			}
		});
		console.log(danzis);
		dealPingchang(danzis);
	});
	
	function dealPingchang(danzis){
	
		if(danzis.length==0) return false;
		
		var arr=danzis.shift();
		if(arr[2]<=1){
			var json = '{"type":"pingcang","key":"'+arr[0]+'","dnum":"'+arr[1]+'","xtype":"1","price":"","token":"pong"}';
		}else{
			var json = '{"type":"delcang","key":"'+arr[0]+'","dnum":"'+arr[1]+'","beizhu":"'+arr[1]+'","token":"pong"}';
		}
		console.log(json);
		addLog(json)
		ws.send(json);
	}
	function  addLog(msg){
			$.post("action.php?type=addlog",{description:msg,title:"平仓"},function(data,status){});
	}
	 function shift_select(){  
		$("#chicang-example-table tbody tr").click(function (e){  
			if($(e.target).is('a')){
				
				return;
			}
			if(e.shiftKey){  //shift  key
				var iBegin=$("#chicang-example-table tbody tr.selected").index();
				var iIndex=$("#chicang-example-table tbody tr").index($(this));
				var iMin = Math.min(iBegin,iIndex);
				var iMax = Math.max(iBegin,iIndex);
				$("#chicang-example-table tbody tr").removeClass("selected");
				$("#chicang-example-table tbody tr input[type='checkbox']").prop('checked',false);
				for(i=iMin;i<=iMax;i++){  
					$("#chicang-example-table tbody tr:eq("+i+")").addClass("selected") 
					$("#chicang-example-table tbody tr:eq("+i+") input[type='checkbox']").prop('checked',true);
				}  
			}else if(e.ctrlKey){   //ctrl  key
				if(!$(this).hasClass("selected")){
					$(this).addClass("selected");  
					$(this).find("input[type='checkbox']").prop('checked',true);
				}else{
					$(this).removeClass("selected");  
					$(this).find("input[type='checkbox']").prop('checked',false);
				}
			}else{   //  key
				if(!$(this).hasClass("selected")){
					$("#chicang-example-table tbody tr").removeClass("selected");
					$("#chicang-example-table tbody tr input[type='checkbox']").prop('checked',false);
					$(this).addClass("selected");  
					$(this).find("input[type='checkbox']").prop('checked',true);
				}else{
					$("#chicang-example-table tbody tr").removeClass("selected");
					$("#chicang-example-table tbody tr input[type='checkbox']").prop('checked',false);
				}
			}
		
		});
	 }
	 
	 var openGendan=function(obj){
		 
		 var ticket = $(obj).parent().parent().attr("data-order");
		 $.post("action.php?type=opengendan",{ticket:ticket},function(data,status){
			if(typeof data == "object"){
				var mt4 = data.mt4;
				var mt4port = data.port;
				$("#myModal7 .title-alt span[rel='data-mt4']").html(mt4);
				var data=JSON.stringify({"type":"current","port":mt4port});
				console.log(data);
				ws2.send(data);
				$('#myModal1').modal('show');
				$('#myModal1 .modal-body').html('查询中。。。。'); 
			}else{
				$('#myModal1').modal('show');
				$('#myModal1 .modal-body').html('查询失败'); 
				
			}
		},"json");
	 }
  document.onselectstrat=function(event){   //避免文字被选中  
      event = window.event||event;  
      event.returnValue = false;  
    }  
	$(".refresh-btn").click(function(){
	
		$("#chicang-example-table tbody").html("");
		ws.send('{"type":"queryall","beizhu":"xxx","token":"pong"}');
		$('#myModal1').modal('show');
		$('#myModal1 .modal-body').html('查询中。。。。'); 
	});
	</script>


<?php include_once 'foot.php'; ?>
</body>

</html>
