<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

	
     <link href="assets/js/stackable/stacktable.css" rel="stylesheet">
    <link href="assets/js/stackable/responsive-table.css" rel="stylesheet">
<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>


    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>出金管理</span>
                        </h2>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">出金管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1"><?=$_GET["daishenhe"]?"未审核出金列表":"出金列表"?></a>
                </li>
    
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
				<div class="row">
				
					    <div class="body-nest" id="inlineClose">
                               <div class="form_center" style="width:100%">
                                    <form role="form" class="form-inline " method="get">
									
										<div class="form-group">
									
											 <select class="form-control" name="pagesize">
												<option value="10" <?php if($_GET['pagesize']=='10'){ echo 'selected="selected"'; } ?>>每页10条</option>
												<option value="30" <?php if($_GET['pagesize']=='30'){ echo 'selected="selected"'; } ?>>每页30条</option>
												<option value="50" <?php if($_GET['pagesize']=='50'){ echo 'selected="selected"'; } ?>>每页50条</option>
												<option value="100"  <?php if($_GET['pagesize']=='100'){ echo 'selected="selected"'; } ?>>每页100条</option>
											 </select>
										   <input type="hidden" class="form-control" placeholder="姓名 / mt4" size="12" name="daishenhe" value="<?=$_GET['daishenhe']?>">
											<input type="text" class="form-control" placeholder="姓名 / mt4" size="12" name="search">
											<input type="text" placeholder="出金时间" class="form-control" id="reservation" name="reservation" style="width:320px" value="<?=$_GET[reservation]?>" autocomplete="off"> 
									 			
								 
											<select class="form-control" name="status">
												<option value="" <?php if($_GET['status']==''){ echo 'selected="selected"'; } ?>>全部</option>
												<option value="0" <?php if($_GET['status']=='0'){ echo 'selected="selected"'; } ?>>待审核</option>
												<option value="1" <?php if($_GET['status']=='1'){ echo 'selected="selected"'; } ?>>已审核</option>
												<option value="-1" <?php if($_GET['status']=='-1'){ echo 'selected="selected"'; } ?>>已驳回</option>
											</select>
												<button type="submit" class="btn btn-success">搜索</button>
												<button class="btn btn-primary" id="export-btn" >导出数据</button>
										</div>
											
										
                                    </form>
                                </div>

                        </div>
							
                   <div class="col-sm-12">

                        <div class="nest" id="StackableClose">
                            <div class="title-alt">
                                <h6>订单列表</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#tStackableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                              

                            </div>

                            <div class="body-nest" id="StackableStatic">

                                <table id="responsive-example-table" class="table large-only">
                                    <tbody>
                                        <tr class="text-right">
									
											<th style="width:5%;">ID</th>
											<th style="width:5%;">真实姓名</th>
											<th style="width:5%;">MT4账号</th>
											<th style="width:5%;">提现用户</th>
											<th style="width:10%;">银行卡号</th>
											<th style="width:20%;">金额</th>
											<th style="width:10%;">银行/省市支行</th>
											<th style="width:10%;">时间</th>
											<th style="width:10%;">审核状态</th>
											<th style="width:10%;">审核时间</th>
											<th style="width:5%;">MT4支付状态</th>
											<th style="width:5%;">操作</th>
                                        </tr>
										
									<?php 
									$pagesize=10;
									if($_GET[pagesize]!= null){
										$pagesize=$_GET[pagesize];
										$aplus="&pagesize=$pagesize";
									}

									if($_GET[page]){
										$page=$_GET[page];
									}else{
										$page=1;
									}
									$qian=($page-1)*$pagesize;

									
									if($_GET[uid]!= null){
										$sqlxs.=" and t1.uid = '$_GET[uid]' ";
										$aplus.="&uid=$_GET[uid]";
									}

									if($_GET[status]!= null){
										$sqlxs.=" and t1.status = '$_GET[status]'";
									}

									if($_GET[search]!= null){
									$sqlxs.=" and (t4.realname like '%$_GET[search]%' || t3.mt4 like '%$_GET[search]%' )";
									$aplus.="&search=$_GET[search]";
									}

									if($_GET['reservation']!= null){
											$reservation=explode("~",$_GET['reservation']);
											$start_time=strtotime($reservation[0]);
											$end_time=strtotime($reservation[1]);
											$sqlxs.=" and t1.time>'$start_time' and t1.time<'$end_time'";
									}
									$sqlxs2 = $sqlxs;
									if($_GET["daishenhe"]!= null){
										$sqlxs.=" and t1.status=0 ";
										$aplus.="&daishenhe=1";
									}

										$sql="select t1.*,t3.mt4,t3.port,t4.bankcardnum,t4.bankcardaddr,t4.realname from tixian t1 
										left join idcard t2 on t1.uid=t2.uid
										left join mt4 t3 on t1.mtid=t3.mtid 
										left join idcard t4 on t1.uid=t4.uid 
										where 1=1 $sqlxs order by tid desc limit $qian,$pagesize";

										$users =$res->fn_rows($sql);
										foreach($users as $user){
									?>	
												<tr>
													<td><?=$user[tid]?></td>
													<td><a href="am_users.php?mid=<?=$user[uid]?>"><?=$user[realname]?></a></td>
													<td>
													<?=$user[mt4]?>
													<?php if($user[mt4]){ ?>
													<a class=" btn-xs  btn-warning current"  data-mt4="<?=$user[mt4]?>" data-port="<?=$user[port]?>" onclick="chiCang('<?=$user[mt4]?>')">持</a>
													<?php } ?>
													</td>
													<td><?=$user[name]?></td>
													<td><?=$user[account]?$user[account]:$user[bankcardnum]?></td>
													<td>$<?=$user[jine]?> <span class="label label-important">￥<?=$user[jine2]?></span>  <span class="label label-primary">汇率：<?=$user[exchange]?></span></td>
													<td><?=$user[bankaddress]?$user[bankaddress]:$user[bankcardaddr]?></td>
													<td><?=date('Y-m-d H:i:s',$user[time])?></td>
													<td>
															<?php if($user[status]==0){ ?>
																		<a type="button"  class="btn-sm btn-warning idcard"  onclick="editTixian('<?=$user[tid]?>','<?=$user[status]?>','<?=$user[message]?>')">待审核</a>
															<?php }else if($user[status]==-1){　?>
																		<a type="button"  class="btn-sm btn-danger idcard"  onclick="editTixian('<?=$user[tid]?>','<?=$user[status]?>','<?=$user[message]?>')">已驳回</a>
															<?php }else { ?>
																		<a type="button"  class="btn-sm btn-success idcard"  onclick="editTixian('<?=$user[tid]?>','<?=$user[status]?>','<?=$user[message]?>')">已审核</a>
															<?php } ?>
												   	</td>
												  	<td><?=$user[shtime]?date('Y-m-d H:i:s',$user[shtime]):"-----"?></td>
													<td>
														<?php if($user[paystatus]){ ?>
														<span class="label label-success">出金成功</span>
														<?php }else{ ?>
														<span class="label label-warning">未出金</span>			
														<?php } ?>	
													</td>
													
													<td>
															<div class="btn-group">
																<button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">操作
																	<span class="caret"></span>
																</button>
																<ul role="menu" class="dropdown-menu">
																<li><a href="javascript:;"  onclick="editTixian('<?=$user[tid]?>','<?=$user[status]?>','<?=$user[message]?>')"><i class="icon-document-edit"></i>&nbsp;&nbsp;驳回</a></li>
												<!--<?php if(!$user[paystatus]){ ?>
												<li><a href="javascript:;" onclick="tiXian('<?=$user[tid]?>','<?=$user[mt4]?>','<?=$user[jine]?>','<?=$user[jine2]?>','<?=$user[exchange]?>')"><i class="fontawesome-money"></i>&nbsp;&nbsp;出金</a></li>
												<?php } ?>-->
																</ul>
															</div>							
																							
													</td>
																				
									<?php } ?> </tr>

                                    </tbody>
                                </table>

 <ul class="pagination">
     <?php
	$sql2="select t1.uid,
       t4.realname
  from tixian t1
  left join idcard t4 on t1.uid= t4.uid

  left join mt4 t3 on t1.uid= t3.uid
 where 1= 1  $sqlxs order by tid desc";
	 
$num=$res->fn_num($sql2);
 $myPage=new pager($num,intval($page),$pagesize,"active");     
     $pageStr= $myPage->GetPagerContent();    
     echo $pageStr;
?>
 
           </ul>
		    <div class="pull-right">
	<label style="margin:20px">
	<?php

	$sql3="select  count(tid) num,sum(t1.jine) jine from tixian t1 left join users t2 on t1.uid=t2.uid  where 1=1  $sqlxs2 and status = 1 and paystatus =1 ";

	$tongji=$res->fn_select($sql3);
	?>
	出金总金额:<?=$tongji[jine]?>，总笔数:<?=$tongji[num]?>
	</label>
</div>  
		   
		   
		   
                            </div>

                        </div>


                    </div>


                </div>
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->

	<!-- Modal2 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="height:500px;overflow:auto;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
				<div class="form-horizontal" >
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">审核状态：</label>
						<div class="col-lg-10">
							<select name="idstatus" id="idstatus" class="form-control round-input" onchange="if(this.value=='-1') $('#bohui').show();else $('#bohui').hide();">
									<option value="0">未审核</option>
									<option value="1">已审核</option>
									<option value="-1">驳回</option>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="bohui" style="display:none">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">驳回原因：</label>
						<div class="col-lg-10">
							<input type="text" name="message" id="message" class="form-control round-input" value="">
						</div>
					</div>
				</div>
			
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-primary"  id="submit-btn">提交修改</button>
      </div>
    </div>
  </div>
</div>
 
 
 	<!-- Modal1 -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
			<div class="form-horizontal" >
					<input type="hidden" name="tid" id="tid" class="form-control round-input" value="" readonly>
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">MT4账号：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="mt4" id="mt4" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">MT4余额：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="mt4-yue" id="mt4-yue" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">提现余额(美金)：</label>
						<div class="col-lg-6 input-group ">
							<span class="input-group-addon">&nbsp;$</span>
							<input type="text" name="jine" id="jine" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">提现余额(人民币)：</label>
						<div class="col-lg-6 input-group ">
							<span class="input-group-addon">￥</span>
							<input type="text" name="jine2" id="jine2" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4  control-label" for="inputPassword1">汇率：</label>
						<div class="col-lg-6 input-group ">
							<span class="input-group-addon"><i class="fontawesome-exchange"></i></span>
							<input type="text" name="exchange" id="exchange" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="tixian-btn">确定出金</button>
      </div>
    </div>
  </div>
</div>

	<!-- Modal2 -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal17 -->
<div class="modal fade" id="myModal7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="height:500px;width:1000px">
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">当前持仓</h4>
      </div>
      <div class="modal-body"  style="height:600px;overflow-y:auto">
			 <table id="chicang-example-table" class="table large-only">
						<thead>
							<tr class="text-right">
						
								<th style="width:5%;">ID</th>
								<th style="width:10%;">单号</th>
								<th style="width:10%;">MT4</th>
								<th style="width:10%;">商品</th>
								<th style="width:10%;">操作</th>
								<th style="width:10%;">开仓价</th>
								<th style="width:10%;">止损价/止盈价</th>
								<th style="width:5%;">手数</th>
								<th style="width:10%;">开仓时间</th>
								<th style="width:10%;">获利</th>
								<th style="width:10%;">操作</th>
							</tr>
					   </thead>
						<tbody>	
						</tbody>
				
			</table>
      </div>
      <div class="modal-footer">
   
      </div>
    </div>
  </div>
</div>
	
	
<form name="export-form" id="export-form" action="action.php?type=exportexcel" method="post" target="e" style="display:none">
		<input type="hidden"  name="xtitle" id="xtitle" value="" />
		<input type="hidden"  name="xhead" id="xhead" value="" />
		<input type="hidden"  name="xsql" id="xsql" value="" />
	</form>
	<iframe name="e" style="display:none"></iframe>
    <script type="text/javascript" src="assets/js/jquery.js"></script>
   <!-- GAGE -->
<script type="text/javascript" src="assets/js/preloader.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/load.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
   <!-- /MAIN EFFECT -->
<script type="text/javascript" src="assets/js/stackable/stacktable.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap3-editable/inputs-ext/address/address.js"></script>
<script type="text/javascript" src="assets/js/bootstrap3-editable/inputs-ext/typeaheadjs/lib/typeahead.js"></script>
<script type="text/javascript" src="assets/js/bootstrap3-editable/inputs-ext/typeaheadjs/typeaheadjs.js"></script>
<script type="text/javascript" src="assets/js/bootstrap3-editable/inputs-ext/wysihtml5/wysihtml5.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>
<script src="assets/js/tree/lib/jquery.cookie.js" type="text/javascript"></script>
<script src="assets/js/tree/jquery.treeview.js" type="text/javascript"></script>
	  <!--socoket start-->
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/web_socket.js"></script>
<script type="text/javascript" src="/js/json.js"></script>
<script type="text/javascript" src="/static/websocket_config.js"  media="all"></script>

<script>
  function init() {
		ws = new WebSocket("ws://"+WS_HOST2+":"+WS_PORT2);
		ws.onopen = function() {
    	    timeid && window.clearTimeout(timeid);
		   	console.log("连接成功");
      };
		if(reconnect>6){
			window.clearTimeout(timeid);
		}
		ws.onmessage = function(e) {
				console.log("收到数据："+e.data);
				var data = JSON.parse(e.data);
				switch(data['type']){
	
					case 'queryuser':
						if(data['code']==200){
							var dataobj=data['data'];
							$("#mt4-yue").val(dataobj[0].yue);	
						
						}	
					 break;
					case 'current':
						
						var html='';	
						var operation={0:"BUY",1:"SELL",2:"BUY LIMIT",3:"SELL LIMIT",4:"BUY STOP",5:"SELL STOP"};
						for(i in data['data']){ 
							var order=data['data'][i];
							html+='       <tr data-order="'+order.order+'" data-mt4="'+order.login+'" data-operation="'+order.cmd+'">'
								+'			<td>'+(parseInt(i)+1)+'</td>'
								+'			<td>'+order.order+'</td>'
								+'			<td>'+order.login+'</td>'
								+'			<td>'+order.symbol+'</td>'
								+'			<td>'+operation[order.cmd]+'</td>'
								+'			<td>'+order.openprice+'</td>'
								+'			<td>'+order.sl+'/'+order.tp+'</td>'
								+'			<td>'+(order.volume/100).toFixed(2)+'</td>'
								+'			<td>'+(new Date(parseInt(order.opentime) * 1000).toLocaleString().replace('上午','').replace('下午',''))+'</td>'
								+'			<td>'+order.profit.toFixed(2)+'</td>'
								+'			<td><a href"javascript:;" onclick="pingCang(this)" class="btn btn-primary"><i class="icon-direction"></i>&nbsp;&nbsp;平仓</a></td>'
                                +'        </tr>';	
						}
						
						
					
						$("#myModal7").modal('show');
		
						$("#myModal7 tbody").html(html);
					break;
				}
		
		};
	  ws.onclose = function() {
		  reconnect ++;
    	  console.log("连接关闭，定时重连");
    	  timeid = window.setTimeout(init, 3000);
      };
      ws.onerror = function() {
			reconnect ++;
    	    console.log("出现错误");
		    timeid = window.setTimeout(init, 3000);
      };
	}
	init();
	$('#reservation').daterangepicker({				
		timePicker: true,
		timePicker12Hour : false, //是否使用12小时制来显示时间  
		maxDate: '<?=date('Y-m-d H:i')?>', 
		format: 'YYYY-MM-DD HH:mm',
		separator:'~',
		 ranges : {  
			//'最近1小时': [moment().subtract('hours',1), moment()],  
			'今日': [moment().startOf('day'), moment()],  
			'昨日': [moment().subtract('days', 1).startOf('day'), moment().subtract('days', 1).endOf('day')],  
			'最近7日': [moment().subtract('days', 6).startOf('day'), moment()],  
		
		},
		locale : {  
			applyLabel : '确定',  
			cancelLabel : '取消',  
			fromLabel : '起始时间',  
			toLabel : '结束时间',  
			customRangeLabel : '自定义',  
			daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],  
			monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',  
					'七月', '八月', '九月', '十月', '十一月', '十二月' ],  
			firstDay : 1  
		}  
	},
	function(start, end, label) {
		console.log(start.toISOString(), end.toISOString(), label);
	});
	$('#reservation').daterangepicker({
							
					timePicker: true,
					timePicker12Hour : false, //是否使用12小时制来显示时间  
					maxDate: '<?=date('Y-m-d H:i')?>', 
					format: 'YYYY-MM-DD HH:mm',
					separator:'~',
					 ranges : {  
						//'最近1小时': [moment().subtract('hours',1), moment()],  
						'今日': [moment().startOf('day'), moment()],  
						'昨日': [moment().subtract('days', 1).startOf('day'), moment().subtract('days', 1).endOf('day')],  
						'最近7日': [moment().subtract('days', 6).startOf('day'), moment()],  
					
					},
					locale : {  
						applyLabel : '确定',  
						cancelLabel : '取消',  
						fromLabel : '起始时间',  
						toLabel : '结束时间',  
						customRangeLabel : '自定义',  
						daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],  
						monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',  
								'七月', '八月', '九月', '十月', '十一月', '十二月' ],  
						firstDay : 1  
					}  
				},
				function(start, end, label) {
					console.log(start.toISOString(), end.toISOString(), label);
				});

var tid;
function editTixian(id,status,message){
	tid=id;
	$("#myModal").modal('show');	
	$("#idstatus").val(status).trigger('onchange');
	$("#message").val(message);
}
$("#submit-btn").click(function(){
		
		var status=$("#idstatus").val();
		var message=$("#message").val();
	
		
		if(!message && status==-1){
			    layer.tips('驳回原因不能为空!', '#message');
				return false;
		}
		
		
		$.post("action.php?type=addtixian",{tid:tid,status:status,message:message},function(data,status){
			if(data=="success"){
				alert("提交成功");
				window.location.reload();
			}else{
				alert("提交失败");
			}
		
		});
	 
		
	});
	
	function tiXian(tid,mt4,jine,jine2,exchange){
		$("#tid").val(tid);
		$("#mt4").val(mt4);
		$("#jine").val(jine);
		$("#jine2").val(jine2);
		$("#exchange").val(exchange);
	//	ws.send('{"type":"queryuser","key":"'+mt4+'","beizhu":"xxx","token":"pong"}');
		$('#myModal1').modal('show')
	}
	
	$("#tixian-btn").click(function(){
		var yue=parseFloat($("#mt4-yue").val());
		var jine=parseFloat($("#jine").val());
		console.log(yue);console.log(jine);
		if(jine>yue){alert('余额不足');return false;}
		$.post("action.php?type=chujin",{tid:$("#tid").val()},function(data,status){
			
		if(typeof data =='object'){
			//	var json='{"type":"payment","name":"","key":"'+data.mt4+'","money":"'+data.jine+'","ptype":2,"beizhu":"xxx","token":"pong"}';
			//	console.log(json);
				//ws.send(json);
				$('#myModal2').modal('show');
				$('#myModal2 .modal-body').html('出金成功'); 
				$('#myModal2').on('hidden.bs.modal', function (e) {
					window.location.reload();
				});
		}else{
				$("#myModal2").modal('show');
				$("#myModal2 .modal-body").html("操作失败");
		}
		},'json');
	});
	


	function chiCang(key){
		ws.send('{"type":"current","key":"'+key+'","token":"pong"}');
	}
		function pingCang(obj){
			console.log($(obj).parent().parent().attr("data-mt4"));
			$("#chicang-mt4").val($(obj).parent().parent().attr("data-mt4"));
			$("#chicang-ticket").val($(obj).parent().parent().attr("data-order"));
			
			$('#myModal8').modal('show');
				
		}
	$("#export-btn").click(function(){
        var xtitle="出金列表";
        var xhead='ID,真实姓名,MT4账号,提现用户,银行卡号,金额,人民币,汇率,银行,银行地址,时间,审核状态';
        var xsql="<?="select t1.tid,t2.realname,t3.mt4,t1.name,t1.account,t1.jine,t1.jine2,t1.exchange,t1.bank,t1.bankaddress,FROM_UNIXTIME(t1.time,'%Y-%m-%d %H:%i:%s') time,CASE WHEN t1.status =1 THEN '已审核' WHEN t1.status =-1 THEN '已驳回' ELSE '待审核' END from tixian t1 left join idcard t2 on t1.uid=t2.uid left join mt4 t3 on t1.mtid=t3.mtid left join idcard t4 on t1.uid=t4.uid where 1=1 $sqlxs order by tid desc"?>";
        $("#xtitle").val(xtitle);
        $("#xhead").val(xhead);
        $("#xsql").val(xsql);
        $("#export-form").submit();
        return false;
    });
		   
		
		$("#pingcang-btn").click(function(){
			
			var mt4 = $("#chicang-mt4").val();
			var ticket = $("#chicang-ticket").val();
			var operation = $("#chicang-operation").val();
			if(operation<=1){
				var json = '{"type":"pingcang","key":"'+mt4+'","dnum":"'+ticket+'","xtype":"1","price":"","token":"pong"}';
			}else{
				var json = '{"type":"delcang","key":"'+mt4+'","dnum":"'+ticket+'","beizhu":"'+ticket+'","token":"pong"}';
			}
			console.log(json);
			addLog(json)
			ws.send(json);
			
			$('#myModal8').modal('hide');
			$('#myModal1').modal('show');
			$('#myModal1 .modal-body').html('平仓中。。。。'); 
	
		});
		
		function  addLog(msg){
			$.post("action.php?type=addlog",{description:msg,title:"平仓"},function(data,status){});
		}
</script>


<?php include_once 'foot.php'; ?>
</body>

</html>
