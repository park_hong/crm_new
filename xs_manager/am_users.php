<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->

    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">


    <link href="assets/js/stackable/stacktable.css" rel="stylesheet">
    <link href="assets/js/stackable/responsive-table.css" rel="stylesheet">
	<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
  	<link rel="stylesheet" href="assets/js/tree/jquery.treeview.css">
  	<link href="assets/js/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
	<link href="assets/js/bootstrap3-editable/inputs-ext/address/address.css" rel="stylesheet" />
	<link href="assets/js/bootstrap3-editable/inputs-ext/typeaheadjs/lib/typeahead.css" rel="stylesheet" />
	<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>

    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
		.editable-input input{padding: 0px;  width: 100px !important;}
		
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>会员管理</span>
                        </h2>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="" title="Sample page 1">会员管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li>
                	 
                <?php 
                if($_GET[daishenhe]){ echo '<a href="http://trader.baihueigd.com/xs_manager/am_users.php?daishenhe=1" title="Sample page 1">待审合会员列表</a>';
                } else { echo '<a href="http://trader.baihueigd.com/xs_manager/am_users.php" title="Sample page 1">会员列表</a>';
            		}?>


                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
				<div class="row">
				
					    <div class="body-nest" id="inlineClose">
                               <div class="form_center" style="width:100%">
                                    <form role="form" class="form-inline " method="get">
									
										<div class="form-group">
									
											 <select class="form-control" name="pagesize">
												<option value="10" <?php if($_GET['pagesize']=='10'){ echo 'selected="selected"'; } ?>>每页10条</option>
												<option value="30" <?php if($_GET['pagesize']=='30'){ echo 'selected="selected"'; } ?>>每页30条</option>
												<option value="50" <?php if($_GET['pagesize']=='50'){ echo 'selected="selected"'; } ?>>每页50条</option>
												<option value="100"  <?php if($_GET['pagesize']=='100'){ echo 'selected="selected"'; } ?>>每页100条</option>
											 </select>
												 <select class="form-control" name="groupid">
													<option value="">所有用户组</option>
													<?php 
													$groups=$res->fn_rows("select * from user_group ") ;
													foreach($groups as $group){ 
													?>
														<option value="<?=$group[gid]?>" <?php if($_GET['groupid']==$group[gid]){ echo 'selected="selected"'; } ?>><?=$group[gname]?></option>
													<?php }　?>
												 </select>
										
										
									
												 <input type="text" class="form-control" placeholder="姓名 / mt4 / 昵称" size="12" name="search1">
												 <input type="text" class="form-control" placeholder="邮箱/手机号" size="12" name="search2">
									
												<input type="text" placeholder="申请时间" class="form-control" id="reservation" name="reservation" style="width:320px" value="<?=$_GET[reservation]?>" autocomplete="off"> 
												
												<button type="submit" class="btn btn-primary">搜索</button>
												<button class="btn btn-primary" id="export-btn" >导出数据</button>
										</div>
											
										
                                    </form>
                                </div>

                        </div>
							
                   <div class="col-sm-12">

                        <div class="nest" id="StackableClose">
                            <div class="title-alt">
                                <h6>系统账户列表</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#tStackableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
								    <a type="button" class="btn btn-primary" id="add_btn" href="adduser.php">
										<span class="entypo-plus-squared"></span>&nbsp;&nbsp;添加账号
									</a>
                                    <a class="nav-toggle-alt" href="#StackableStatic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="StackableStatic">

                                <table id="responsive-example-table" class="table large-only  userstable" style="word-break:break-all; word-wrap:break-all;">
                                    <tbody>
                                        <tr class="text-right">
									
                                            <th style="width:5%;">ID</th>
                                            <th style="width:5%;">真实姓名</th>
											<th style="width:15%;">MT4账号</th>
											<th style="width:5%;">MT4组</th>
											<th style="width:5%;">上家</th>
											<th style="width:5%;">路径</th>
                                            <th style="width:5%;">用户组</th>
											<th style="width:5%;">直推人数</th>
											<th style="width:5%;">推荐关系</th>
											<th style="width:5%;">昵称</th>
											<th style="width:5%;">手机号</th>
											<th style="width:5%;">Email</th>
											<th style="width:5%;">身份证</th>
											<th style="width:5%;">返佣规则</th>
											  <th style="width:5%;">登录时间</th>
                                            <th style="width:5%;">注册时间</th>
                                            <th style="width:5%;">禁用CRM账户</th>
											 <th style="width:5%;">出入金方式</th>
											<th style="width:5%;">操作</th>
                                        </tr>
										
<?php 
$pagesize=10;
if($_GET[pagesize]!= null){
	$pagesize=$_GET[pagesize];
}

if($_GET[page]){
	$page=$_GET[page];
}else{
	$page=1;
}
$qian=($page-1)*$pagesize;

if($_GET[groupid]!= null){
	$sqlxs.=" and t1.groupid = '$_GET[groupid]'";
}

if($_GET[daishenhe]!= null){
	$sqlxs.=" and t6.idstatus != 1 ";
}


if($_GET[search1]!= null){
	$sqlxs.=" and ( t6.realname like '%$_GET[search1]%'  or t2.mt4 like '%$_GET[search1]%' or t1.nickname  like '%$_GET[search1]%' )";
}

if($_GET[search2]!= null){
	$sqlxs.=" and ( t1.mobile  like '%$_GET[search2]%' or t1.email  like '%$_GET[search2]%')";
}

if($_GET['reservation']!= null){
		$reservation=explode("~",$_GET['reservation']);
		$start_time=strtotime($reservation[0]);
		$end_time=strtotime($reservation[1]);
		$sqlxs.=" and t1.regtime>'$start_time' and t1.regtime<'$end_time'";
}

   if($_GET[tmid]!= null){
	   	$sqlxs.=" and t1.tuid='$_GET[tmid]'";

	}

	if($_GET[mid]!= null){
		$sqlxs.=" and t1.uid='$_GET[mid]'";

	}
$sql="select t1.uid,t1.tuid,t1.groupid,t1.nickname,t1.mobile,t1.email,t1.money,t1.isban,t1.ttypes,t2.equity,t1.logintime,t1.regtime,t1.timelimit,t2.mt4,t2.password mt4pwd ,t2.port,t2.status,t2.listenstatus,t3.gname,t3.percent,t4.nickname tuijianname , t5.num,t6.id idid,t6.realname,t6.idstatus,t7.groupname 
		from users t1 
		left join (select group_concat(mt4) mt4,password,group_concat(port) port,uid,status,listenstatus,svid,money,equity from mt4 group by uid ) t2 on t1.uid=t2.uid 
		left join user_group t3 on t1.groupid=t3.gid 
		left join users t4 on t1.tuid=t4.uid 
		left join (select count(*) num,tuid  from users group by tuid) t5 on t1.uid=t5.tuid 
		left join idcard t6 on t1.uid=t6.uid 
		left join mt4_group t7 on t1.mt4_groupid=t7.mt4_groupid  where 1=1 and t1.groupid!=10  $sqlxs order by t1.uid desc limit $qian,$pagesize";
	 
	
	$users =$res->fn_rows($sql);

	foreach($users as $user){
		$ttypes = explode(",",$user['ttypes']);
?>	
		<tr data-mt4="<?=$user[mt4]?>">
			<td><?=$user[uid]?></td>
			<td>
			<a href="/admin_user/index.php?uid=<?=$user[uid]?>" target="_blank">
			<?=$user[realname]?>
			</a>
			</td>
			<td>
			
			<?php 
				if($user[mt4]){
				$mt4s=explode(",",$user[mt4]);	
				foreach($mt4s as $mt4){
				
			?>
				<label>
				<?=$mt4?>
				<span class="label label-important yue" rel='0' data-mt4="<?=$mt4?>">获取中...</span>
				<a class="btn-xs btn-danger" onclick="yueReflash('<?=$mt4?>')"><i class="fontawesome-refresh"></i></a> 
				<a class=" btn-xs  btn-warning current"  data-mt4="<?=$mt4?>" data-port="<?=$ports[$key]?>" onclick="chiCang('<?=$mt4?>')">持</a>
				</label>
				<?php }} ?>	
			
			</td>
			<td><?=$user[groupname]?></td>
			<td><?=$user[tuijianname]?'<a href="am_users.php?mid='.$user[tuid].'">'.$user[tuijianname]."</a>":'无'?></td>
				<?php
					
					
					$tuid=$user[uid];
					  
					$shangxian=$res->fn_select("select t1.uid,t1.tuid,t1.nickname,t1.groupid,t2.groupid tgroupid,t8.realname from users t1 left join users t2 on t1.tuid=t2.uid left join idcard t8 on t1.uid=t8.uid   where t1.uid = '$tuid' and ( t1.uid !=t1.tuid or t1.tuid  is null )");

					$arr=array();
						$i=0;
						while($shangxian){
						$arr[$i]['uid']=$shangxian['uid'];
						$arr[$i]['nickname']=$shangxian['realname'];
					    $tuid=$shangxian['tuid'];
						$shangxian=$res->fn_select("select t1.uid,t1.tuid,t1.nickname,t1.groupid,t2.groupid tgroupid,t8.realname from users t1 left join users t2 on t1.uid=t2.tuid left join idcard t8 on t1.uid=t8.uid  where t1.uid = '$tuid' and ( t1.uid !=t1.tuid or t1.tuid  is null )");
						$i++;
					} 
					$arr=array_reverse($arr);
				
					$str="";
					for($i=0;$i<count($arr);$i++){
						
						if($i==count($arr)-1){
							$str=$str.$arr[$i]['nickname'];
						}else{
							$str=$str.$arr[$i]['nickname'].'->';
						}
						
						
					}
					
					
					?>
									<td><?=$str?></td>     
									
			<td><?=$user[gname]?></td>
			<td><?php
			if($user[num]){
				echo '<a href="am_users.php?tmid='.$user[uid].'">'.$user[num]."</a>";
			}else{
				echo 0;
			}?></td>
			<td>
			<?php if($user[num]){ ?>
			<a type="button" class="tuijiantree " rel="<?=$user[uid]?>">点击查看</a>
			<?php }else{ ?>
				无
			<?php } ?>
			</td>
			<td><?=$user[nickname]?></td>
			<td><?=$user[mobile]?></td>
			<td><?=$user[email]?></td>
			<td>
			<?php if(!$user[idid]){ ?>
				<a type="button"  class="btn btn-default idcard" rel="<?=$user[uid]?>">未上传</a>
			<?php }else if($user[idstatus]==0){ ?>
				<a type="button"  class="btn btn-warning idcard" rel="<?=$user[uid]?>">待审核</a>
			<?php }else if($user[idstatus]==-1){　?>
				<a type="button"  class="btn btn-danger idcard" rel="<?=$user[uid]?>">已驳回</a>
			<?php }else { ?>
				<a type="button"  class="btn btn-primary idcard" rel="<?=$user[uid]?>">已审核</a>
			<?php } ?>
			</td>
		<td>
			<?php if($user[groupid]>=4 && $user[groupid]<=6){ ?>
			<a type="button" class="peijiguize " rel="<?=$user[uid]?>">点击编辑</a>
			<?php }else if($user[groupid]<=3){ ?>
			<a type="button" class="peijiguize2 ">点击编辑(<?=$user[percent]?>)</a>
			<?php }else { ?>
			无
			<?php } ?>
			</td>
				<td><?=date('Y-m-d H:i:s',$user[logintime])?></td>
			<td><?=date('Y-m-d H:i:s',$user[regtime])?></td>
			<td>
			<?php if($user[isban]){ ?>
				<span  class="label label-sm label-success">是</span>
			<?php }else { ?>
				<span  class="label label-sm label-danger">否</span>
			<?php } ?>
			</td>
			<td>
			<?php if( in_array(1,$ttypes ) ){ ?>
				<span  class="label label-sm label-primary">人民币出金</span>
			<?php } ?>
			<?php if( in_array(2,$ttypes )){ ?>
				<span  class="label label-sm label-primary">USDT出金</span>
			<?php } ?>
				<?php if( in_array(3,$ttypes ) ){ ?>
				<span  class="label label-sm label-primary">人民币入金</span>
			<?php } ?>
			<?php if( in_array(4,$ttypes )){ ?>
				<span  class="label label-sm label-primary">USDT入金</span>
			<?php } ?>
			</td>
		
			<td>
			
			<div class="btn-group">
				<button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">操作
					<span class="caret"></span>
				</button>
			<ul role="menu" class="dropdown-menu">
				<li><a href="adduser.php?mid=<?=$user[uid]?>"><i class="icon-document-edit"></i>&nbsp;&nbsp;修改资料</a></li>   
				<li><a href="javascript:void(0)" onclick="addmt4type(<?=$user[uid]?>)"><i class="entypo-user-add"></i>&nbsp;&nbsp;添加MT4</a></li>
				<li><a href="am_mt4.php?mid=<?=$user[uid]?>"><i class="entypo-briefcase"></i>&nbsp;&nbsp;查看MT4</a></li>
				<li><a href="am_dingdan.php?mt4=<?=$user[mt4]?>"><i class="entypo-list"></i>&nbsp;&nbsp;查看订单</a></li>
			
				<li><a href="am_caiwu.php?uid=<?=$user[uid]?>"><i class="entypo-basket"></i>&nbsp;&nbsp;查看财务</a></li>
				<li><a href="am_tixian.php?uid=<?=$user[uid]?>"><i class="entypo-basket"></i>&nbsp;&nbsp;查看出金记录</a></li>
				<li><a href="am_rujin.php?uid=<?=$user[uid]?>"><i class="entypo-basket"></i>&nbsp;&nbsp;查看入金记录</a></li>
				<li><a href="addbonus.php?uid=<?=$user[uid]?>"><i class="fontawesome-money"></i>&nbsp;&nbsp;添加奖金</a></li>
				<li><a href="addbonus1.php?uid=<?=$user[uid]?>"><i class="fontawesome-money"></i>&nbsp;&nbsp;冲账功能</a></li>
				<li><a href="am_user_log.php?uid=<?=$user[uid]?>"><i class="entypo-briefcase"></i>&nbsp;&nbsp;查看操作日志</a></li>
				<li><a href="javascript:;" onclick="mt4Delete('<?=$user[mt4]?>','<?=$user[port]?>','<?=$user[uid]?>')"><i class="entypo-cancel-circled"></i>&nbsp;&nbsp;删除用户</a></li>

				<li><a href="/admin_user/index.php?uid=<?=$user[uid]?>" target="_blank"><i class="entypo-user"></i>&nbsp;&nbsp;会员中心</a></li>
				<li><a href="javascript:;" class="statistic-btn" data-uid="<?=$user[uid]?>" data-username="<?=$user[nickname]?>" data-mt4="<?=$user[mt4]?>" ><i class="entypo-user" ></i>&nbsp;&nbsp;统计信息</a></li>

			</ul>
											</div>
											</td>
                                        </tr>
	<?php } ?>
                                    </tbody>
                                </table>

	<ul class="pagination">
 <?php
 
 $sql2="select t1.uid from users t1 left join (select group_concat(mt4) mt4,password,group_concat(port) port,uid,status,listenstatus,svid,money,equity from mt4 group by uid ) t2 on t1.uid=t2.uid left join user_group t3 on t1.groupid=t3.gid left join users t4 on t1.tuid=t4.uid left join (select count(*) num,tuid  from users group by tuid) t5 on t1.uid=t5.tuid left join idcard t6 on t1.uid=t6.uid left join mt4_group t7 on t1.mt4_groupid=t7.mt4_groupid  where 1=1 and t1.groupid!=10  $sqlxs order by t1.uid desc";
 $num=$res->fn_num($sql2);

    $myPage=new pager($num,intval($page),$pagesize,"active");     
     $pageStr= $myPage->GetPagerContent();    
     echo $pageStr;    

?>	
	</ul>


                        </div>


                    </div>


                </div>
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->
	
		<!-- Modal1 -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
	<!-- Modal2 -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document"style="width:600px;">
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
		
      </div>
      <div class="modal-body" style="height:500px;overflow:auto;">
	  
		
			<input type="hidden" name="uid" id="uid" value=""/>
				<div class="form-horizontal" >
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">真实姓名：</label>
						<div class="col-lg-10">
							<input type="text" name="realname" id="realname" class="form-control round-input" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">身份证号：</label>
						<div class="col-lg-10">
							<input type="text" name="idnum" id="idnum" class="form-control round-input" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">身份正面：</label>
						<div class="col-lg-10">
							<input type="hidden" id="idcard1_input">
							<form id="idcard1_form" action="action.php?type=idcard1" method="post" enctype="multipart/form-data" target="e">
								<input type="file" name="file" id="idcard1_file" onchange="$('#idcard1_form').submit()" style="width: 164px;">
							</form>
							<img id="idcard1" style="width:210px;display:none"  />
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">身份背面：</label>
						<div class="col-lg-10">
							<input type="hidden" id="idcard2_input">
							<form id="idcard2_form" action="action.php?type=idcard2" method="post" enctype="multipart/form-data" target="e">
								<input type="file" name="file"  id="idcard2_file"  onchange="$('#idcard2_form').submit()" style="width: 164px;">
							</form>
							
							<img id="idcard2" style="width:210px;display:none" />
						</div>
					</div>
						<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">银行卡号：</label>
						<div class="col-lg-10">
							<input type="text" name="bankcardnum" id="bankcardnum" class="form-control round-input" value="">
						</div>
					</div>
						<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">开户地址：</label>
						<div class="col-lg-10">
							<input type="text" name="bankcardaddr" id="bankcardaddr" class="form-control round-input" value="">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">银行卡正面：</label>
						<div class="col-lg-10">
							<input type="hidden" id="bankcard_input">
							<form id="bankcard_form" action="action.php?type=bankcard" method="post" enctype="multipart/form-data" target="e">
								<input type="file" name="file"  id="bankcard_file"  onchange="$('#bankcard_form').submit()" style="width: 164px;">
							</form>
							
							<img id="bankcard" style="width:210px;display:none" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">审核状态：</label>
						<div class="col-lg-10">
							<select name="idstatus" id="idstatus" class="form-control round-input" onchange="if(this.value=='-1') $('#bohui').show();else $('#bohui').hide();">
									<option value="0">未审核</option>
									<option value="1">已审核</option>
									<option value="-1">驳回</option>
							</select>
						</div>
					</div>
					<div class="form-group" id="bohui" style="display:none">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">驳回原因：</label>
						<div class="col-lg-10">
							<input type="text" name="message" id="message" class="form-control round-input" value="">
						</div>
					</div>
				</div>
				<iframe name="e" style="display:none"></iframe>
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-primary"  id="submit-btn">提交修改</button>
      </div>
    </div>
  </div>
</div>
	
	<!-- Modal13 -->
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
			<div class="form-horizontal" >
					<input type="hidden" name="tid" id="tid" class="form-control round-input" value="" readonly>
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">MT4账号：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="mt4" id="mt4" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">MT4密码：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="mt4pwd" id="mt4pwd" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">观摩密码：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="mt4pwd_read" id="mt4pwd_read" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">MT4邮箱：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="mt4mail" id="mt4mail" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="email-btn">发送邮件</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal14 -->
	<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="height:500px;width:1000px">
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
			<table class="table table-bordered table-striped table-peijiguize" >
			     <thead>
				<tr>
					<th class="col-md-1"></th>
					<th class="col-md-2">黄金</th>
					<th class="col-md-2">白银</th>
					<th class="col-md-2">原油</th>
					<th class="col-md-2">货币兑</th>
					<th class="col-md-2">Nasusd</th>
					<th class="col-md-2">Spxusd</th>
					
				</tr>
			</thead>
			<tbody>
	<?php
		 $mt4_groups=$res->fn_rows("select * from mt4_group  where 1=1 order by mt4_groupid asc ");	
		foreach($mt4_groups as $mt4_group){
	?>
	<tr>
		<td><?=$mt4_group[groupname]?></td>
		<td>
			<a href="#" data-type="text" data-pk="1"  data-gid="<?=$mt4_group[mt4_groupid]?>" data-sbid="1" data-title="" class="peijiguize-edit editable editable-click ">点击编辑</a>
			<i class="fa fa-pencil"></i>
		</td>
		
		<td>
			<a href="#" data-type="text" data-pk="1"  data-gid="<?=$mt4_group[mt4_groupid]?>" data-sbid="2" data-title="" class="peijiguize-edit  editable editable-click ">点击编辑</a>
			<i class="fa fa-pencil"></i>
		</td>
		<td>
			<a href="#" data-type="text" data-pk="1"  data-gid="<?=$mt4_group[mt4_groupid]?>" data-sbid="3" data-title="" class="peijiguize-edit  editable editable-click ">点击编辑</a>
			<i class="fa fa-pencil"></i>
		</td>
		<td>
			<a href="#" data-type="text" data-pk="1"  data-gid="<?=$mt4_group[mt4_groupid]?>" data-sbid="4" data-title="" class="peijiguize-edit  editable editable-click ">点击编辑</a>
			<i class="fa fa-pencil"></i>
		</td>
		<td>
			<a href="#" data-type="text" data-pk="1"  data-gid="<?=$mt4_group[mt4_groupid]?>" data-sbid="5" data-title="" class="peijiguize-edit  editable editable-click ">点击编辑</a>
			<i class="fa fa-pencil"></i>
		</td>
		<td>
			<a href="#" data-type="text" data-pk="1"  data-gid="<?=$mt4_group[mt4_groupid]?>" data-sbid="6" data-title="" class="peijiguize-edit  editable editable-click ">点击编辑</a>
			<i class="fa fa-pencil"></i>
		</td>
	</tr>
	<?php } ?>			
				</tbody>
			</table>
      </div>
      <div class="modal-footer">
   
      </div>
    </div>
  </div>
</div>
	
	
	<!-- Modal15 -->
<div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
			<div class="form-horizontal" >
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">总经理：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="usergroup1" id="usergroup1" class="form-control round-input" value="" >
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">总监：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="usergroup2" id="usergroup2" class="form-control round-input" value="" >
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">业务：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="usergroup3" id="usergroup3" class="form-control round-input" value="" >
						</div>
					</div>
					
					
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="usergroup-btn">提交数据</button>
      </div>
    </div>
  </div>
</div>

		<!-- Modal6 -->
<div class="modal fade" id="myModal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
			是否开设新账号？
      </div>
      <div class="modal-footer">
			<button type="button" class="btn btn-success" data-dismiss="modal" id="newuser-btn">是</button>
			<button type="button" class="btn btn-primary" data-dismiss="modal" >否</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal17 -->
<div class="modal fade" id="myModal7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="height:500px;width:1000px">
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">当前持仓</h4>
      </div>
      <div class="modal-body"  style="height:600px;overflow-y:auto">
			 <table id="chicang-example-table" class="table large-only">
						<thead>
							<tr class="text-right">
						
								<th style="width:5%;">ID</th>
								<th style="width:10%;">单号</th>
								<th style="width:10%;">MT4</th>
								<th style="width:10%;">商品</th>
								<th style="width:10%;">操作</th>
								<th style="width:10%;">开仓价</th>
								<th style="width:10%;">止损价/止盈价</th>
								<th style="width:5%;">手数</th>
								<th style="width:10%;">开仓时间</th>
								<th style="width:10%;">获利</th>
								<th style="width:10%;">操作</th>
							</tr>
					   </thead>
						<tbody>	
						</tbody>
				
			</table>
      </div>
      <div class="modal-footer">
   
      </div>
    </div>
  </div>
</div>
	
<div class="modal fade" id="myModal8" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">确定平掉改单?</h4>
      </div>
      <div class="modal-body">
			
			<div class="form-horizontal" >
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">单号：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="chicang-ticket" id="chicang-ticket" class="form-control round-input" value="" >
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">Mt4账号：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="chicang-mt4" id="chicang-mt4" class="form-control round-input" value="" >
						</div>
					</div>
				
					
			</div>
      </div>
      <div class="modal-footer">
			<button type="button" class="btn btn-success" data-dismiss="modal" id="pingcang-btn">是</button>
			<button type="button" class="btn btn-primary" data-dismiss="modal" >否</button>
      </div>
    </div>
  </div>
</div>

	
<div class="modal fade" id="myModal9" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="width:800px;">
    <div class="modal-content"  >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">统计信息</h4>
      </div>
      <div class="modal-body"  style="overflow-y:auto;background:#efefef">
			<div class="row">
				<span class="label label-success statistic-username">用户名：<b></b></span>
				<span class="label label-primary statistic-mt4">MT4：<b></b></span>
			</div>
				<div class="row" style = "margin-top:20px">
					 <div class="col-sm-4">
                        <div class="dashboard-stat red" id="dashboard1">
                            <div class="headline ">
                                <h3>
                                    <span>
                                        <i class="fa fa-money"></i>&nbsp;&nbsp;总入金量</span>
                                </h3>
                                <div class="titleClose">
                                    <a href="#dashboard1" class="gone">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                            </div>

                            <div class="value">
                                <span><i class="fa fa-money"></i></span>
								<a>0</a>

                            </div>

                            <div class="details">
                               
							</div>
						</div>
                    </div>
                  
				    <div class="col-sm-4">
                        <div class="dashboard-stat blue" id="dashboard2">
                            <div class="headline ">
                                <h3>
                                    <span>
                                        <i class="fa fa-money"></i>&nbsp;&nbsp;总出金量</span>
                                </h3>
                                <div class="titleClose">
                                    <a href="#dashboard2" class="gone">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                            </div>

                            <div class="value">
                                <span><i class="fa fa-money"></i></span>
								<a>0</a>

                            </div>
		
                              <div class="details">
                               
							</div>
						</div>
                    </div>
					
					
					 <div class="col-sm-4">
                        <div class="dashboard-stat green" id="dashboard3">
                            <div class="headline ">
                                <h3>
                                    <span>
                                        <i class="fa fa-money"></i>&nbsp;&nbsp;净入金量</span>
                                </h3>
                                <div class="titleClose">
                                    <a href="#dashboard3" class="gone">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                            </div>

                            <div class="value" >
                                <span><i class="fa fa-money"></i></span>
								<a>0</a>
                            </div>
		
                              <div class="details">
                              
							</div>
						</div>
                    </div>
				</div>
				
				<div class="row" style = "margin-top:20px">
					 <div class="col-sm-4">
                        <div class="dashboard-stat red" id="dashboard4">
                            <div class="headline ">
                                <h3>
                                    <span>
                                        <i class="fa fa-money"></i>&nbsp;&nbsp;历史订单盈亏</span>
                                </h3>
                                <div class="titleClose">
                                    <a href="#dashboard1" class="gone">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                            </div>

                            <div class="value">
                                <span><i class="fa fa-money"></i></span>
								<a>0</a>

                            </div>

                            <div class="details">
                               
							</div>
						</div>
                    </div>
                  
				    <div class="col-sm-4">
                        <div class="dashboard-stat blue" id="dashboard5">
                            <div class="headline ">
                                <h3>
                                    <span>
                                        <i class="fa fa-money"></i>&nbsp;&nbsp;持仓订单浮动盈亏</span>
                                </h3>
                                <div class="titleClose">
                                    <a href="#dashboard2" class="gone">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                            </div>

                            <div class="value">
                                <span><i class="fa fa-money"></i></span>
								<a>0</a>

                            </div>
		
                              <div class="details">
                               
							</div>
						</div>
                    </div>
					
					
					 <div class="col-sm-4">
                        <div class="dashboard-stat green" id="dashboard6">
                            <div class="headline ">
                                <h3>
                                    <span>
                                        <i class="fa fa-money"></i>&nbsp;&nbsp;返佣金额</span>
                                </h3>
                                <div class="titleClose">
                                    <a href="#dashboard3" class="gone">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                            </div>

                            <div class="value" >
                                <span><i class="fa fa-money"></i></span>
								<a>0</a>
                            </div>
		
                            <div class="details">
							</div>
						</div>
                    </div>
				</div>
				
      </div>
      <div class="modal-footer">
			<button type="button" class="btn btn-success" data-dismiss="modal" id="pingcang-btn">确定</button>
      </div>
    </div>
  </div>
</div>

<form name="export-form" id="export-form" action="action.php?type=exportexcel" method="post" target="e" style="display:none">
		<input type="hidden"  name="xtitle" id="xtitle" value="" />
		<input type="hidden"  name="xhead" id="xhead" value="" />
		<input type="hidden"  name="xsql" id="xsql" value="" />
</form>
<iframe name="e" style="display:none"></iframe>
	
    <script type="text/javascript" src="assets/js/jquery.js"></script>

   <!-- GAGE -->
<script type="text/javascript" src="assets/js/preloader.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/load.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
   <!-- /MAIN EFFECT -->
<script type="text/javascript" src="assets/js/stackable/stacktable.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap3-editable/inputs-ext/address/address.js"></script>
<script type="text/javascript" src="assets/js/bootstrap3-editable/inputs-ext/typeaheadjs/lib/typeahead.js"></script>
<script type="text/javascript" src="assets/js/bootstrap3-editable/inputs-ext/typeaheadjs/typeaheadjs.js"></script>
<script type="text/javascript" src="assets/js/bootstrap3-editable/inputs-ext/wysihtml5/wysihtml5.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>
<script src="assets/js/tree/lib/jquery.cookie.js" type="text/javascript"></script>
<script src="assets/js/tree/jquery.treeview.js" type="text/javascript"></script>
	  <!--socoket start-->
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/web_socket.js"></script>
<script type="text/javascript" src="/js/json.js"></script>
<script type="text/javascript" src="/static/websocket_config.js"  media="all"></script>


<script>
	var mt4s=new Array();
$(".userstable tbody tr").each(function(){
		var mt4=$(this).attr("data-mt4");
		if(mt4){
			var keys=mt4.split(","); //字符分割 
			for (i=0;i<keys.length ;i++ ) 
			{
				mt4s.push(keys[i]);
			}
		}
	
	});
	
function addmt4type(uid){
	layer.confirm('是否开设新的mt4', {
		  btn: ['否','是'] //按钮
		}, function(){
		  window.location.href="./addmt4.php?mid="+uid;
		}, function(){
		   
			$.post("action.php?type=getmt4",{uid:uid},function(data,status){
				var json='{"type":"adduser","name":"'+data.realname+'","email":"'+data.email+'","password":"'+data.mt4pwd+'","group":"'+data.groupname+'","login":"'+data.login+'","city":"suzhou","province":"jiangsu","country":"china","address":"","password_investor":"'+data.mt4pwd_read+'","password_change":"1","postcode":"215007","telephone":"'+data.mobile+'","leverage":'+data.leverage+',"beizhu":"'+data.mt4pwd_read+'-'+data.uid+'-'+data.leverage+'","token":"pong"}';
				ws.send(json);
				$('#myModal2').modal('hide');
				$('#myModal1').modal('show');
				
				$('#myModal1 .modal-body').html('MT4申请中。。。。'); 
			},"json");
		});
}	


	function init() {
		ws = new WebSocket("ws://"+WS_HOST2+":"+WS_PORT2);
		ws.onopen = function() {
    	    timeid && window.clearTimeout(timeid);
		   	console.log("连接成功");
			getYue();
      };
		if(reconnect>6){
			window.clearTimeout(timeid);
		}
		ws.onmessage = function(e) {
				console.log("收到数据："+e.data);
				var data = JSON.parse(e.data);
				switch(data['type']){
				   
					case 'current':
						
						var html='';	
						var operation={0:"BUY",1:"SELL",2:"BUY LIMIT",3:"SELL LIMIT",4:"BUY STOP",5:"SELL STOP"};
						for(i in data['data']){ 
							var order=data['data'][i];
							html+='       <tr data-order="'+order.order+'" data-mt4="'+order.login+'" data-operation="'+order.cmd+'">'
								+'			<td>'+(parseInt(i)+1)+'</td>'
								+'			<td>'+order.order+'</td>'
								+'			<td>'+order.login+'</td>'
								+'			<td>'+order.symbol+'</td>'
								+'			<td>'+operation[order.cmd]+'</td>'
								+'			<td>'+order.openprice+'</td>'
								+'			<td>'+order.sl+'/'+order.tp+'</td>'
								+'			<td>'+(order.volume/100).toFixed(2)+'</td>'
								+'			<td>'+(new Date(parseInt(order.opentime) * 1000).toLocaleString().replace('上午','').replace('下午',''))+'</td>'
								+'			<td>'+order.profit.toFixed(2)+'</td>'
								+'			<td><a href"javascript:;" onclick="pingCang(this)" class="btn btn-primary"><i class="icon-direction"></i>&nbsp;&nbsp;平仓</a></td>'
                                +'        </tr>';	
						}
						
						
					
						$("#myModal7").modal('show');
		
						$("#myModal7 tbody").html(html);
					break;
					case 'adduser':
						$('#myModal2').modal('hide');
						if(data['code']==200){
							var uinfo=data['data'];
							var beizhu=data['beizhu'].split("-");
							var mt4pwd_read=beizhu[0];
							var uid=beizhu[1];
							var leverage=beizhu[2];
							$.post("action.php?type=addmt4",{uid:uid,mt4:uinfo.key,mt4pwd:uinfo.password,mt4pwd_read:mt4pwd_read,svid:134,leverage:leverage},function(data,status){
								if(typeof data=="object"){
									$("#mt4").val(uinfo.key);
									$("#mt4pwd").val(uinfo.password);
									$("#mt4pwd_read").val(mt4pwd_read);
									$("#mt4mail").val(uinfo.email);
									$('#myModal3').modal('show');
								}
							},'json');
						}else{
							$('#myModal1').modal('show');
							$('#myModal1 .modal-body').html('申请失败'); 
						}
						
					break;
					case 'queryuser':
				
						if(data['code']==200){
							var uinfo=data['data'][0];
							$(".userstable tbody tr .yue[data-mt4='"+data['beizhu']+"'] ").html(uinfo.yue);
						}
						getYue();
					break;
					case 'pingcang':
					case 'delcang':
						if(data['code'] == "200"){
							$('#myModal1 .modal-body').html('平仓成功'); 
							var ticket =  data['dnum'];
							$("#chicang-example-table tr[data-order='"+ticket+"']").remove();
						}else{
							
							$('#myModal1 .modal-body').html('平仓失败'); 
						}
					break;
				}
		
		};
	  ws.onclose = function() {
		  reconnect ++;
    	  console.log("连接关闭，定时重连");
    	  timeid = window.setTimeout(init, 3000);
      };
      ws.onerror = function() {
			reconnect ++;
    	    console.log("出现错误");
		    timeid = window.setTimeout(init, 3000);
      };
	}
	   init();
			$('#reservation').daterangepicker({
							
					timePicker: true,
					timePicker12Hour : false, //是否使用12小时制来显示时间  
					maxDate: '<?=date('Y-m-d H:i')?>', 
					format: 'YYYY-MM-DD HH:mm',
					separator:'~',
					 ranges : {  
						//'最近1小时': [moment().subtract('hours',1), moment()],  
						'今日': [moment().startOf('day'), moment()],  
						'昨日': [moment().subtract('days', 1).startOf('day'), moment().subtract('days', 1).endOf('day')],  
						'最近7日': [moment().subtract('days', 6).startOf('day'), moment()],  
					
					},
					locale : {  
						applyLabel : '确定',  
						cancelLabel : '取消',  
						fromLabel : '起始时间',  
						toLabel : '结束时间',  
						customRangeLabel : '自定义',  
						daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],  
						monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',  
								'七月', '八月', '九月', '十月', '十一月', '十二月' ],  
						firstDay : 1  
					}  
				},
				function(start, end, label) {
					console.log(start.toISOString(), end.toISOString(), label);
				});
				
	function mt4Delete(mt4,mt4port,uid){
		
		if(!window.confirm('确认删除操作吗？')){ return false;}

		ports=mt4port.split(","); //字符分割 
		for (i=0;i<ports.length ;i++ ) 
		{ 

			var data=JSON.stringify({"type":"loginout","port":ports[i]});
			console.log(data);
			ws.send(data);
		} 
			console.log(mt4);
		$.get("action.php?type=deluser",{uid:uid,mt4:mt4},function(data,status){
			window.location.reload();
		});
	}
	
	$(".idcard").click(function(){
		var rel=$(this).attr('rel');
		$("#uid").val(rel);
		$.get("action.php?type=getidcard",{uid:rel},function(data,status){
			
			$("#realname").val(data.realname);
			$("#idnum").val(data.idnum);
			$("#bankcardnum").val(data.bankcardnum);
			$("#bankcardaddr").val(data.bankcardaddr);
			$("#idcard1_input").val(data.idcard1);
			$("#idcard2_input").val(data.idcard2);
			$("#bankcard_input").val(data.bankcard);
			$("#idstatus").val(data.idstatus).trigger('onchange');	
			$("#message").val(data.message);
			
			$("#idcard1").hide().prop('src','');
			$("#idcard2").hide().prop('src','');
			$("#bankcard").hide().prop('src','');
			data.idcard1 && $("#idcard1").show().prop('src','/up/'+data.idcard1);
			data.idcard2 && $("#idcard2").show().prop('src','/up/'+data.idcard2);
			data.bankcard && $("#bankcard").show().prop('src','/up/'+data.bankcard);
			$("#myModal2").modal('show');	
		},'json');
	});
	
	$(".peijiguize").click(function(){
		var rel=$(this).attr('rel');
		$("#uid").val(rel);
		$(".table-peijiguize tr a[data-gid]").attr("data-uid",rel).attr("data-value",0).html("点击编辑");

		$.get("action.php?type=getpeijiguize",{uid:rel},function(data,status){
			console.log(data);
			for(i in data){
				var row=data[i];	
				for(j in row){
					$(".table-peijiguize tr a[data-gid='"+i+"'][data-sbid='"+j+"']").attr("data-uid",rel).attr("data-value",row[j]).html(row[j]);
				}
			}
			
			$("#myModal4").modal('show');	
		},'json');
	});
	
	$(".peijiguize2").click(function(){
		$.get("action.php?type=getpeijiguize2",{},function(data,status){
			console.log(data);
			if(typeof data == "object"){
				$("#usergroup1").val(data[0].percent);
				$("#usergroup2").val(data[1].percent);
				$("#usergroup3").val(data[2].percent);
			}
			$("#myModal5").modal('show');	
		},'json');
	});
	
	$("#submit-btn").click(function(){
		
		var uid=$("#uid").val();
		var realname=$("#realname").val();
		var idnum=$("#idnum").val();
		var bankcardnum=$("#bankcardnum").val();
		var bankcardaddr=$("#bankcardaddr").val();
		var idstatus=$("#idstatus").val();
		var idcard1_input=$("#idcard1_input").val();
		var idcard2_input=$("#idcard2_input").val();
		var bankcard_input=$("#bankcard_input").val();
		var message=$("#message").val();

		
		$.post("action.php?type=addidcard",{uid:uid,realname:realname,idnum:idnum,bankcardnum:bankcardnum,bankcardaddr:bankcardaddr,idcard1:idcard1_input,idcard2:idcard2_input,bankcard:bankcard_input,idstatus:idstatus,message:message},function(data,status){
			if(typeof data == 'object'){
				
				
			  	
				alert("提交成功");
			
				$('#myModal2').modal('hide');
				$('#myModal6').modal('show');
			
			}else if(data == 'success'){
				if(idstatus==-1){
					$.ajax({
					 url: "/mail/send_email1.php",  
					 type: "POST",
					 data:{message:message,uid:uid},
					
					 success: function(data,status){ 					
					 }
				 });
				}
				$('#myModal2').modal('hide');
				$('#myModal1').modal('show');
				$('#myModal1 .modal-body').html('修改成功'); 
			}else {
				$('#myModal2').modal('hide');
				$('#myModal1').modal('show');
				$('#myModal1 .modal-body').html('提交失败'); 
			}
		
		},'json');
	 
		
	});
	
	$(".tuijiantree").click(function(){
		var rel=$(this).attr('rel');
		$("#uid").val(rel);
		$.get("/admin_user/action.php?type=gettuijian",{uid:rel},function(data,status){
			tree='<div style=" height: 400px;overflow: auto;">';
			tree+='<div id="browser-count" style=" position: absolute;right:50px;font-weight: bold;">当前显示人数：<span class="label label-success">0</span></div>';
		    tree+='<ul id="browser" class="treeview" > <span class="entypo-users" rel="'+rel+'">我自己</span> <ul>';	
			for(i in data){
				var xiaxian=data[i];
				if(xiaxian.num){
					tree += '<li><span class="entypo-users" rel="'+xiaxian.uid+'" onclick="getTuijian(this)">'+xiaxian.nickname+'('+xiaxian.gname+')</span></li>';
				}else{
					tree += '<li><span>'+xiaxian.nickname+'('+xiaxian.gname+')</span></li>';
				}
			}
			tree+='</ul></ul></div>';
		
			$("#myModal1").modal('show');
			console.log(tree);
			$("#myModal1 .modal-body").html(tree);
			$("#browser").treeview({
				animated: "fast",
				collapsed: false,
				unique: true,
				persist: "cookie",
				toggle: function() {
					window.console && console.log("%o was toggled", this);
				}
			});
				$("#browser-count span").text($("#browser li").length);
		},'json');
	});
	var tree='';	
	function domBuit(xianxians){
	    tree += "<ul>";
		for(i in xianxians){
			xiaxian=xianxians[i];
			if(xiaxian.xiaxian){
				tree += '<li><span class="entypo-home">'+xiaxian.nickname+'('+xiaxian.gname+')</span> ';
				domBuit(xiaxian.xiaxian);
			}else{
				tree += '<span>'+xiaxian.nickname+'('+xiaxian.gname+')</span> ';
			}
			
		
			tree += '</li>';
		}
		
		tree += "</ul>";
	}
	
   function  getTuijian(e){
	
		var uid=$(e).attr('rel');
		if($(e).parent().find(' > ul > li').length>0){
			return false;
		}
		var that=e;
		$.get("/admin_user/action.php?type=gettuijian",{uid:uid},function(data,status){

			if(typeof data=='object'){
				var  html='<ul>';
				for(i in data){
					var xiaxian=data[i];
					if(xiaxian.num){
						html += '<li><span class="entypo-users"  rel="'+xiaxian.uid+'" onclick="getTuijian(this)">'+xiaxian.nickname+'('+xiaxian.gname+')</span></li>';
					}else{
						html += '<li><span>'+xiaxian.nickname+'('+xiaxian.gname+')</span></li>';
					}
				}
				  html +='</ul>';
				$(that).parent().append(html);
				/*$("#browser").treeview({
					animated: "fast",
					collapsed: false,
					unique: true,
					persist: "cookie",
					toggle: function() {
						window.console && console.log("%o was toggled", that);
					}
				});*/
					$("#browser-count span").text($("#browser li").length);
			}
		},'json');
		
	}
	var tree='';	
	function domBuit(xianxians){
	    tree += "<ul>";
		for(i in xianxians){
			xiaxian=xianxians[i];
			if(xiaxian.xiaxian){
				tree += '<li><span class="entypo-home">'+xiaxian.nickname+'('+xiaxian.gname+')</span> ';
				domBuit(xiaxian.xiaxian);
			}else{
				tree += '<span>'+xiaxian.nickname+'('+xiaxian.gname+')</span> ';
			}
			
		
			tree += '</li>';
		}
		
		tree += "</ul>";
	}
	
	function chiCang(key){
		ws.send('{"type":"current","key":"'+key+'","token":"pong"}');
	}
	
	
	$("#newuser-btn").click(function(){
		var uid = $("#uid").val();
		$.post("action.php?type=getmt4",{uid:uid},function(data,status){
			var json='{"type":"adduser","name":"'+data.realname+'","email":"'+data.email+'","password":"'+data.mt4pwd+'","group":"'+data.groupname+'","login":"'+data.login+'","city":"suzhou","province":"jiangsu","country":"china","address":"","password_investor":"'+data.mt4pwd_read+'","password_change":"1","postcode":"215007","telephone":"'+data.mobile+'","leverage":'+data.leverage+',"beizhu":"'+data.mt4pwd_read+'-'+data.uid+'-'+data.leverage+'","token":"pong"}';
			ws.send(json);
			$('#myModal2').modal('hide');
			$('#myModal1').modal('show');
			$('#myModal1 .modal-body').html('MT4申请中。。。。'); 
		},"json");
	});
	$("#email-btn").click(function(){
		var mt4=$("#mt4").val();
		var mt4pwd=$("#mt4pwd").val();
		var mt4pwd_read=$("#mt4pwd_read").val();
		var email=$("#mt4mail").val();
		$.post("/mail/send_mt4.php",{mt4:mt4,mt4pwd:mt4pwd,mt4pwd_read:mt4pwd_read,email:email},function(data,status){
				$('#myModal3').modal('hide');
				$('#myModal1').modal('show');
				$('#myModal1 .modal-body').html('发送成功'); 
				$('#myModal1').on('hidden.bs.modal', function (e) {
					window.location.reload();
				});
		});
	});
	function getYue(){
		if(mt4s.length==0){return false}
		var mt4=mt4s.shift();
	
		var data=JSON.stringify({"type":"queryuser","key":mt4,"beizhu":mt4,"token":"pong"});
		console.log(data);
		ws.send(data);
	
	}
$("#usergroup-btn").click(function(){
		var usergroup1 = $("#usergroup1").val();
		var usergroup2 = $("#usergroup2").val();
		var usergroup3 = $("#usergroup3").val();
		$.post("action.php?type=addpeijiguize2",{usergroup1:usergroup1,usergroup2:usergroup2,usergroup3:usergroup3},function(data,status){
			if(data == "success"){
				$('#myModal5').modal('hide');
				$('#myModal1').modal('show');
				$('#myModal1 .modal-body').html('提交成功'); 
				$('#myModal1').on('hidden.bs.modal', function (e) {
					window.location.reload();
				});
			}else{
				$('#myModal5').modal('hide');
				$('#myModal1').modal('show');
				$('#myModal1 .modal-body').html('提交失败'); 
				$('#myModal1').on('hidden.bs.modal', function (e) {
					window.location.reload();
				});
			}
		});
	});
	function yueReflash(mt4){
		$(".userstable tbody .yue[data-mt4='"+mt4+"']").attr('rel',0).html('获取中..');	
		
			mt4s.push(mt4);
		
			getYue();
	}
	var handleEditableFieldConstruct = function() {
		$.fn.editable.defaults.mode ='inline';
		$.fn.editable.defaults.inputclass = 'form-control input-sm';
		$.fn.editable.defaults.url = './action.php?type=addpeijiguize';
		$('.peijiguize-edit').editable({
			url: './action.php?type=addpeijiguize',
			params: function(params) {
				params.uid = $(this).attr('data-uid');	
				params.gid = $(this).attr('data-gid');
				params.sbid = $(this).attr('data-sbid');
				console.log(params);
				return params;
			},
			validate: function(value) {
				if($.trim(value) === '') { 
					return 'This field is required';
				}
			},
			success: function(response, newValue){	
			
				if(response=="invalidate_value"){
		
					$(this).html($(this).attr('data-value'));
					$(this).addClass('alert-error').removeClass('alert-success').html($(this).attr('data-value'));
					layer.msg('比例超过上线分配比例!', {icon: 2});
					return false;
				}
			}
		});

	};
	handleEditableFieldConstruct();
	
	
	function pingCang(obj){
			console.log($(obj).parent().parent().attr("data-mt4"));
			$("#chicang-mt4").val($(obj).parent().parent().attr("data-mt4"));
			$("#chicang-ticket").val($(obj).parent().parent().attr("data-order"));
			
			$('#myModal8').modal('show');
				
		}
		
		$("#pingcang-btn").click(function(){
			
			var mt4 = $("#chicang-mt4").val();
			var ticket = $("#chicang-ticket").val();
			var operation = $("#chicang-operation").val();
			if(operation<=1){
				var json = '{"type":"pingcang","key":"'+mt4+'","dnum":"'+ticket+'","xtype":"1","price":"","token":"pong"}';
			}else{
				var json = '{"type":"delcang","key":"'+mt4+'","dnum":"'+ticket+'","beizhu":"'+ticket+'","token":"pong"}';
			}
			console.log(json);
			addLog(json)
			ws.send(json);
			
			$('#myModal8').modal('hide');
			$('#myModal1').modal('show');
			$('#myModal1 .modal-body').html('平仓中。。。。'); 
	
		});
		
		function  addLog(msg){
			$.post("action.php?type=addlog",{description:msg,title:"平仓"},function(data,status){});
		}
		
	<?php
 $sql3="select t1.uid,t6.realname,t2.mt4,t7.groupname,t4.nickname tuijianname , t3.gname, t1.nickname,t1.mobile,t1.email, t6.idnum, t6.bankcardnum, t6.bankcardaddr from users t1 left join (select group_concat(mt4) mt4,password,group_concat(port) port,uid,status,listenstatus,svid,money,equity from mt4 group by uid ) t2 on t1.uid=t2.uid left join user_group t3 on t1.groupid=t3.gid left join users t4 on t1.tuid=t4.uid left join (select count(*) num,tuid  from users group by tuid) t5 on t1.uid=t5.tuid left join idcard t6 on t1.uid=t6.uid left join mt4_group t7 on t1.mt4_groupid=t7.mt4_groupid  where 1=1 and t1.groupid!=10  $sqlxs order by t1.uid desc ";

?>
	$("#export-btn").click(function(){
		var xtitle="会员列表";
		var xhead='ID,真实姓名,mt4账号,组,推荐人,用户组,昵称,手机号,email,身份证号,银行卡号,银行卡开户地址';
		var xsql="<?=$sql3?>";
		$("#xtitle").val(xtitle);
		$("#xhead").val(xhead);
		$("#xsql").val(xsql);
		$("#export-form").submit();
		return false;
	});	
	
	
	$(".statistic-btn").click(function(){
		
		$('#myModal1').modal('show');
		$('#myModal1 .modal-body').html('查询中。。。。'); 
		var uid 		= $(this).data("uid");
		var username 	= $(this).data("username");
		var mt4 		= $(this).data("mt4");
		
		$.get("action.php?type=get_statistics",{uid:uid},function(data,status){

			if(typeof data=='object'){	
				$('#myModal1').modal('hide');
				$('#myModal9').modal('show');
				
				$("#myModal9 .statistic-username b").html( username );
				$("#myModal9 .statistic-mt4 b").html( mt4 );
				
				$("#dashboard1 .value a").html( data.rujin );
				$("#dashboard2 .value a").html( data.chujin );
				$("#dashboard3 .value a").html( data.rujin2 );
				$("#dashboard4 .value a").html( data.profit1 );
				$("#dashboard5 .value a").html( data.profit2 );
				$("#dashboard6 .value a").html( data.fanyong );
			}else{
				$('#myModal1').modal('show');
				$('#myModal1 .modal-body').html('查询失败'); 
			}
		},'json');
		
	});
</script>


<?php include_once 'foot.php'; ?>
</body>

</html>
