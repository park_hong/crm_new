<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

	
     <link href="assets/js/stackable/stacktable.css" rel="stylesheet">
    <link href="assets/js/stackable/responsive-table.css" rel="stylesheet">

<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />

    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>入金列表</span>
                        </h2>

                    </div>

                  
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">入金管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">入金列表</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
				<div class="row">
				
<div class="body-nest" id="inlineClose">
	   <div class="form_center" style="width:100%">
			<form role="form" class="form-inline " method="get">
			
				<div class="form-group">
			
					 <select class="form-control" name="pagesize">
						<option value="10" <?php if($_GET['pagesize']=='10'){ echo 'selected="selected"'; } ?>>每页10条</option>
						<option value="30" <?php if($_GET['pagesize']=='30'){ echo 'selected="selected"'; } ?>>每页30条</option>
						<option value="50" <?php if($_GET['pagesize']=='50'){ echo 'selected="selected"'; } ?>>每页50条</option>
						<option value="100"  <?php if($_GET['pagesize']=='100'){ echo 'selected="selected"'; } ?>>每页100条</option>
					 </select>
				
		<input type="text" class="form-control" placeholder="账号/用户名" size="12" name="search" value="<?=$_GET[search1]?>">
		 <input type="text" class="form-control" placeholder="订单号" size="12" name="order_num" value="<?=$_GET[order_num]?>">
		 <input type="text" class="form-control" placeholder="代理商" size="12" name="tname" value="<?=$_GET[tname]?>">
		 <input type="text" class="form-control" placeholder="MT4" size="12" name="mt4" value="<?=$_GET[mt4]?>">
		 
		 <input type="text" placeholder="入金时间" class="form-control" id="reservation" name="reservation" style="width:320px" value="<?=$_GET[reservation]?>" autocomplete="off"> 
		 
		  <select class="form-control" name="status">
						<option value="" <?php if($_GET['status']==null){ echo 'selected="selected"'; } ?>>全部</option>
						<option value="1" <?php if($_GET['status']=='1'){ echo 'selected="selected"'; } ?>>支付成功</option>
						<option value="0" <?php if($_GET['status']=='0'){ echo 'selected="selected"'; } ?>>支付失败</option>
		 </select>
					 
						<button type="submit" class="btn btn-success">搜索</button>
						<button class="btn btn-primary" id="export-btn" >导出数据</button>
						<button class="btn btn-danger" onclick="delRujin(0)">批量删除全部失败信息</button>
						
				</div>
					
				
			</form>
		</div>

                        </div>
							
                   <div class="col-sm-12">

                        <div class="nest" id="StackableClose">
                            <div class="title-alt">
                                

                                <div class="titleClose">
                                    <a class="gone" href="#tStackableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#StackableStatic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="StackableStatic">

                                <table id="responsive-example-table" class="table large-only">
                                    <thead>
                                        <tr class="text-right">
											<th style="width:5%;"><input type="checkbox" onclick="selectAll()"/></th>
                                            <th style="width:5%;">ID</th>
                                            <th style="width:5%;">姓名</th>
											<th style="width:10%;">MT4</th>
											<th style="width:5%;">用户组</th>
											<th style="width:10%;">入金余额（$）</th>
                                            <th style="width:10%;">入金余额（￥）</th>
											<th style="width:10%;">申请时间</th>
											<th style="width:10%;">到账时间</th>
											<th style="width:5%;">通道</th>
											<th style="width:10%;">单号</th>
											<th style="width:5%;">状态</th>
											<th style="width:10%;">MT4支付状态</th>
									
                                        </tr>
									</thead>

									 <tbody>
<?php
	$pagesize=10;
	if($_GET[pagesize]!= null){
	$pagesize=$_GET[pagesize];
	}
	
	if($_GET[page]){
		$page=$_GET[page];
	}else{
		$page=1;
	}
	$qian=($page-1)*$pagesize;
	

if($_GET[search]!= null){
	$sqlxs.=" and t6.realname like '%$_GET[search]%'";
}
if($_GET[tname]!= null){
	$tuser = $res->fn_select("select uid from users where nickname like '%$_GET[tname]%' ");
	$uids = $res->fn_select("select  getChildList($tuser[uid]) uids");
	$sqlxs.=" and t2.uid in ($uids[uids]) ";
	$aplus.="&tname=$_GET[tname]";
}

if($_GET[order_num]!= null){
	$sqlxs.=" and t1.order_num = '$_GET[order_num]'";
}
if($_GET[mt4]!= null){
	$sqlxs.=" and t4.mt4 = '$_GET[mt4]' ";

}
if($_GET[status]!= null){
	$sqlxs.=" and t1.status = '$_GET[status]'";

}

if($_GET['reservation']!= null){
		$reservation=explode("~",$_GET['reservation']);
		$start_time=strtotime($reservation[0]);
		$end_time=strtotime($reservation[1]);
		$sqlxs.=" and t1.time>'$start_time' and t1.time<'$end_time'";
}

	$rujins=$res->fn_rows("select distinct(t1.rid),t1.uid,t1.jine,t1.jine2,t1.time,t1.successtime,t1.paytype,t1.order_num,t1.status,t1.paystatus,t1.uid,t6.realname,t3.gname,t4.mt4,t5.rc_name from rujin t1 left join users t2 on t1.uid=t2.uid left join user_group t3 on t2.groupid=t3.gid left join mt4 t4 on t1.mtid=t4.mtid left join rujin_channel t5 on t1.paytype = t5.rcid left join idcard t6 on t1.uid=t6.uid where 1=1  $sqlxs order by rid desc limit $qian,$pagesize");
	foreach($rujins as $rujin){
?>	
                                        <tr>
                                            <td><input type="checkbox"  name="rid[]" value="<?=$rujin[rid]?>" /></td>
                                            <td><?=$rujin[rid]?></td>
                                            <td><a href="am_users.php?mid=<?=$rujin[uid]?>"><?=$rujin[realname]?></a></td>
											<td><?=$rujin[mt4]?></td>
											<td><?=$rujin[gname]?></td>
											<td>$<?=$rujin[jine]?></td>
											<td>￥<?=$rujin[jine2]?></td>
											<td><?=date('Y-m-d H:i',$rujin[time])?></td>
											<td><?=$rujin[successtime]?date('Y-m-d H:i',$rujin[successtime]):"----"?></td>
											<td><?=$rujin[rc_name]?></td>
											<td><?=$rujin[order_num]?></td>
										
											<td>
<?php if($rujin[status]){ ?>
<span class="label label-success">已支付</span>
<?php }else{ ?>
	<span class="label label-warning">未支付</span>					
<?php } ?>
											</td>
												<td>



		
	<a href="javascript:;" onclick="rujin('<?=$rujin[rid]?>','<?=$rujin[mt4]?>','<?=$rujin[jine]?>')" class=" btn-sm btn-primary"><i class="fontawesome-money"></i>&nbsp;&nbsp;支付</a>	
	<a href="javascript:;" onclick="delRujin('<?=$rujin[rid]?>')" class=" btn-sm btn-danger"><i class="entypo-cancel-circled"></i>&nbsp;&nbsp;删除</a>	
 
								
			
		
			
						
											
											</td>
                                        </tr>
	<?php } ?>
                                    </tbody>
                                </table>
<div class="row" style="padding-left:20px">						

	<button type="button" class="btn btn-primary  m-r-5 m-b-5" onclick="selectAll()">全选</button>
	<button type="button" class="btn btn-primary  m-r-5 m-b-5" onclick="deleteAll()">批量删除</button>
</div>
<ul class="pagination">
     <?php
	 $sql2="select t1.rid,t2.nickname,t3.gname from rujin t1 left join users t2 on t1.uid=t2.uid left join user_group t3 on  t2.groupid=t3.gid left join mt4 t4 on t1.mtid=t4.mtid  left join idcard t6 on t1.uid=t6.uid where 1=1  $sqlxs order by rid desc ";

	$num=$res->fn_num($sql2);
	$myPage=new pager($num,intval($page),$pagesize,"active");     
	$pageStr= $myPage->GetPagerContent();    
	echo $pageStr;
?>
   </ul>
   <div class="pull-right">
	<label style="margin:20px">
	<?php

	$sql3="select  count(*) num,sum(t1.jine) jine from rujin t1 left join users t2 on t1.uid=t2.uid left join user_group t3 on  t2.groupid=t3.gid left join mt4 t4 on t1.mtid=t4.mtid where 1=1 and t1.status=1 $sqlxs  ";

	$tongji=$res->fn_select($sql3);
	?>
入金总金额:<?=$tongji[jine]?>，总笔数:<?=$tongji[num]?>

 
	</label>
</div>  
   
                            </div>

                        </div>


                    </div>


                </div>
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->
	
		<!-- Modal1 -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
			<div class="form-horizontal" >
					<input type="hidden" name="rid" id="rid" class="form-control round-input" value="" readonly>
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">MT4账号：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="mt4" id="mt4" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">入金金额：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="jine" id="jine" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="rujin-btn">确定支付</button>
      </div>
    </div>
  </div>
</div>

	<!-- Modal2 -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
<form name="export-form" id="export-form" action="action.php?type=exportexcel" method="post" target="e" style="display:none">
		<input type="hidden"  name="xtitle" id="xtitle" value="" />
		<input type="hidden"  name="xhead" id="xhead" value="" />
		<input type="hidden"  name="xsql" id="xsql" value="" />
	</form>
	<iframe name="e" style="display:none"></iframe>
    <script type="text/javascript" src="assets/js/jquery.js"></script>

    <!-- GAGE -->


   <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
   <!-- /MAIN EFFECT -->
    <script type="text/javascript" src="assets/js/stackable/stacktable.js"></script>

    <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.js"></script>
	 <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
	  <!--socoket start-->
  <script type="text/javascript" src="/js/swfobject.js"></script>
  <script type="text/javascript" src="/js/web_socket.js"></script>
  <script type="text/javascript" src="/js/json.js"></script>
<script type="text/javascript" src="/static/websocket_config.js"  media="all"></script>
	<script>
	
 function init() {
		ws = new WebSocket("ws://"+WS_HOST2+":"+WS_PORT2);
		ws.onopen = function() {
    	    timeid && window.clearTimeout(timeid);
		   	console.log("连接成功");
      };
		if(reconnect>6){
			window.clearTimeout(timeid);
		}
		ws.onmessage = function(e) {
				console.log("收到数据："+e.data);
				var data = JSON.parse(e.data);
				switch(data['type']){
	
					case 'queryuser':
						if(data['code']==200){
							var dataobj=data['data'];
							$("#mt4-yue").val(dataobj[0].yue);	
						
						}	
					 break;

				}
		
		};
	  ws.onclose = function() {
		  reconnect ++;
    	  console.log("连接关闭，定时重连");
    	  timeid = window.setTimeout(init, 3000);
      };
      ws.onerror = function() {
			reconnect ++;
    	    console.log("出现错误");
		    timeid = window.setTimeout(init, 3000);
      };
	}
	init();
	$('#reservation').daterangepicker({				
		timePicker: true,
		timePicker12Hour : false, //是否使用12小时制来显示时间  
		maxDate: '<?=date('Y-m-d H:i')?>', 
		format: 'YYYY-MM-DD HH:mm',
		separator:'~',
		 ranges : {  
			//'最近1小时': [moment().subtract('hours',1), moment()],  
			'今日': [moment().startOf('day'), moment()],  
			'昨日': [moment().subtract('days', 1).startOf('day'), moment().subtract('days', 1).endOf('day')],  
			'最近7日': [moment().subtract('days', 6).startOf('day'), moment()],  
		
		},
		locale : {  
			applyLabel : '确定',  
			cancelLabel : '取消',  
			fromLabel : '起始时间',  
			toLabel : '结束时间',  
			customRangeLabel : '自定义',  
			daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],  
			monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',  
					'七月', '八月', '九月', '十月', '十一月', '十二月' ],  
			firstDay : 1  
		}  
	},
	function(start, end, label) {
		console.log(start.toISOString(), end.toISOString(), label);
	});
$('#reservation').daterangepicker({
				
		timePicker: true,
		timePicker12Hour : false, //是否使用12小时制来显示时间  
		maxDate: '<?=date('Y-m-d H:i')?>', 
		format: 'YYYY-MM-DD HH:mm',
		separator:'~',
		 ranges : {  
			//'最近1小时': [moment().subtract('hours',1), moment()],  
			'今日': [moment().startOf('day'), moment()],  
			'昨日': [moment().subtract('days', 1).startOf('day'), moment().subtract('days', 1).endOf('day')],  
			'最近7日': [moment().subtract('days', 6).startOf('day'), moment()],  
		
		},
		locale : {  
			applyLabel : '确定',  
			cancelLabel : '取消',  
			fromLabel : '起始时间',  
			toLabel : '结束时间',  
			customRangeLabel : '自定义',  
			daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],  
			monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',  
					'七月', '八月', '九月', '十月', '十一月', '十二月' ],  
			firstDay : 1  
		}  
	},
	function(start, end, label) {
		console.log(start.toISOString(), end.toISOString(), label);
	});
				
				
	function rujin(rid,mt4,jine){
		$("#rid").val(rid);
		$("#mt4").val(mt4);
		$("#jine").val(jine);
		$('#myModal1').modal('show');
	}
	
	$("#rujin-btn").click(function(){
	
		$.post("action.php?type=rujin",{rid:$("#rid").val()},function(data,status){
			
		if(typeof data =='object'){
				var json='{"type":"payment","name":"Auto_'+$("#rid").val()+'","key":"'+data.mt4+'","money":"'+data.jine+'","ptype":1,"beizhu":"xxx","token":"pong"}';
				console.log(json);
				ws.send(json);
				$('#myModal2').modal('show');
				$('#myModal2 .modal-body').html('入金成功'); 
				$('#myModal2').on('hidden.bs.modal', function (e) {
					window.location.reload();
				});
		}else{
				$("#myModal2").modal('show');
				$("#myModal2 .modal-body").html("操作失败");
				
				
				
		}
		},'json');
	});
	
	function delRujin(rid){
		if(!window.confirm('确认删除操作吗？')){ return false;}
	
		$.post("action.php?type=delrujin",{rid:rid},function(data,status){
			$('#myModal2').modal('show');
			$('#myModal2 .modal-body').html('删除成功'); 
			$('#myModal2').on('hidden.bs.modal', function (e) {
				window.location.reload();
			});
		});
	}
	
	$("#export-btn").click(function(){
		var xtitle="入金列表";
		var xhead='ID,姓名,MT4账号,用户组,入金余额（$）,入金金额（￥）,申请时间,到账时间,通道,单号,状态';
		var xsql="<?="select distinct(t1.rid),t5.realname,t4.mt4,t3.gname,t1.jine,t1.jine2,FROM_UNIXTIME(t1.time,'%Y-%m-%d %H:%i:%s') time,FROM_UNIXTIME(t1.successtime,'%Y-%m-%d %H:%i:%s') successtime,t1.paytype,t1.order_num,CASE WHEN t1.status =1 THEN '已支付' ELSE '未支付' END  from rujin t1 left join users t2 on t1.uid=t2.uid left join user_group t3 on t2.groupid=t3.gid left join mt4 t4 on t1.mtid=t4.mtid left join idcard t5 on t1.uid=t5.uid where 1=1 $sqlxs order by rid desc"?>";

		$("#xtitle").val(xtitle);
		$("#xhead").val(xhead);
		$("#xsql").val(xsql);
		$("#export-form").submit();
		return false;
	});
	
	function selectAll(){
		$("tbody input[type='checkbox']").each(function(){
			 if($(this).prop('checked')){
				 $(this).removeAttr('checked');
			 }else{
				 $(this).prop('checked','checked');
			
			 }
		
		});
		
	}
	
	
	function deleteAll(){
		
		if(!window.confirm('确认删除操作吗？')){ return false;}
	
		var rids = [];
		$("tbody input[type='checkbox']").each(function(){
			if($(this).prop('checked')){
			
				rids.push($(this).val());
			}	
		});
		$.post("action.php?type=delrujin",{rid:rids},function(data,status){
			$('#myModal2').modal('show');
			$('#myModal2 .modal-body').html('删除成功'); 
			$('#myModal2').on('hidden.bs.modal', function (e) {
				window.location.reload();
			});
		});
	}
	
	</script>

<?php include_once 'foot.php'; ?>
</body>

</html>
