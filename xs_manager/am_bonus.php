<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

	
     <link href="assets/js/stackable/stacktable.css" rel="stylesheet">
    <link href="assets/js/stackable/responsive-table.css" rel="stylesheet">
<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />


    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">

            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>奖金管理</span>
                        </h2>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">奖金管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">奖金列表</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
				<div class="row">
				
					    <div class="body-nest" id="inlineClose">
                               <div class="form_center" style="width:100%">
                                    <form role="form" name="form1" class="form-inline " method="get">
									
										<div class="form-group">
									
											 <select class="form-control" name="pagesize" onchange="from1.submit()">
												<option value="10" <?php if($_GET['pagesize']=='10'){ echo 'selected="selected"'; } ?>>每页10条</option>
												<option value="30" <?php if($_GET['pagesize']=='30'){ echo 'selected="selected"'; } ?>>每页30条</option>
												<option value="50" <?php if($_GET['pagesize']=='50'){ echo 'selected="selected"'; } ?>>每页50条</option>
												<option value="100"  <?php if($_GET['pagesize']=='100'){ echo 'selected="selected"'; } ?>>每页100条</option>
											 </select>
											
												 <input type="text" class="form-control" placeholder="姓名/MT4号" size="12" name="search">
									
												<button type="submit" class="btn btn-success">搜索</button>
												<button class="btn btn-primary" id="export-btn" >导出数据</button>
										</div>
											
										
                                    </form>
                                </div>

                        </div>
							
                   <div class="col-sm-12">

                        <div class="nest" id="StackableClose">
                            <div class="title-alt">
                                <h6>奖金列表</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#tStackableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                             

                            </div>

                            <div class="body-nest" id="StackableStatic">

                                <table id="responsive-example-table" class="table large-only">
                                    <tbody>
                                        <tr class="text-right">
									
                                            <th style="width:10%;">ID</th>
                                            <th style="width:15%;">真实姓名</th>
											<th style="width:15%;">MT4</th>
											<th style="width:15%;">操作人</th>
                                            <th style="width:15%;">奖金</th>
											<th style="width:15%;">备注</th>
											<th style="width:15%;">时间</th>
                                        </tr>
										
<?php 
$pagesize=10;
if($_GET[pagesize]!= null){
	$pagesize=$_GET[pagesize];
	$aplus="&pagesize=$pagesize";
}

if($_GET[page]){
	$page=$_GET[page];
}else{
	$page=1;
}
$qian=($page-1)*$pagesize;



if($_GET[search]!= null){
	$sqlxs.=" and  (t2.realname like '%$_GET[search]%' or t3.mt4 like '%$_GET[search]%') ";
	$aplus.="&search=$_GET[search]";
}

$sql="select t1.*,t2.realname,t3.mt4,t4.username aname from bonus t1 left join idcard t2  on t1.uid=t2.uid  left join mt4 t3 on t1.mtid=t3.mtid  left join admin t4 on t1.aid=t4.aid where 1=1 $sqlxs order by bid desc limit $qian,$pagesize";

	$users =$res->fn_rows($sql);
	foreach($users as $user){
?>	
                                        <tr>
<td><?=$user[bid]?></td>
<td><?=$user[realname]?></td>
<td><?=$user[mt4]?></td>
<td><?=$user[aname]?></td>
<td><span class="label label-important"><?=$user[money]?></span></td>
<td><?=$user[beizhu]?></td>
<td><?=date('Y-m-d H:i',$user[time])?></td>

                                        </tr>
	<?php } ?>
                                    </tbody>
                                </table>

 <ul class="pagination">
     <?php
	 $sql2="select t1.* from bonus t1 left join idcard t2 on t1.uid=t2.uid where 1=1 $sqlxs ";
$num=$res->fn_num($sql2);
$myPage=new pager($num,intval($page),$pagesize,"active");     
$pageStr= $myPage->GetPagerContent();    
echo $pageStr;
?>
 
           </ul>
		   
                            </div>

                        </div>


                    </div>


                </div>
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->
	<!-- Modal1 -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
			<div class="form-horizontal" >
					<input type="hidden" name="bid" id="bid" class="form-control round-input" value="" readonly>
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">MT4账号：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="mt4" id="mt4" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">入金金额：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="jine" id="jine" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="bonus-btn">确定支付</button>
      </div>
    </div>
  </div>
</div>

	<!-- Modal2 -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
	
<form name="export-form" id="export-form" action="action.php?type=exportexcel" method="post" target="e" style="display:none">
		<input type="hidden"  name="xtitle" id="xtitle" value="" />
		<input type="hidden"  name="xhead" id="xhead" value="" />
		<input type="hidden"  name="xsql" id="xsql" value="" />
	</form>
	<iframe name="e" style="display:none"></iframe>
	
    <script type="text/javascript" src="assets/js/jquery.js"></script>

    <!-- GAGE -->


   <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
   <!-- /MAIN EFFECT -->
    <script type="text/javascript" src="assets/js/stackable/stacktable.js"></script>
	   <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.js"></script>
	 <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
	 <!--socoket start-->
  <script type="text/javascript" src="/js/swfobject.js"></script>
  <script type="text/javascript" src="/js/web_socket.js"></script>
  <script type="text/javascript" src="/js/json.js"></script>
<script type="text/javascript" src="/static/websocket_config.js"  media="all"></script>

<script>
	
 function init() {
		ws = new WebSocket("ws://"+WS_HOST2+":"+WS_PORT2);
		ws.onopen = function() {
    	    timeid && window.clearTimeout(timeid);
		   	console.log("连接成功");
      };
		if(reconnect>6){
			window.clearTimeout(timeid);
		}
		ws.onmessage = function(e) {
				console.log("收到数据："+e.data);
				var data = JSON.parse(e.data);
				switch(data['type']){
	
					case 'queryuser':
						if(data['code']==200){
							var dataobj=data['data'];
							$("#mt4-yue").val(dataobj[0].yue);	
						
						}	
					 break;

				}
		
		};
	  ws.onclose = function() {
		  reconnect ++;
    	  console.log("连接关闭，定时重连");
    	  timeid = window.setTimeout(init, 3000);
      };
      ws.onerror = function() {
			reconnect ++;
    	    console.log("出现错误");
		    timeid = window.setTimeout(init, 3000);
      };
	}
	init();
	
$('#reservation').daterangepicker({
				
		timePicker: true,
		timePicker12Hour : false, //是否使用12小时制来显示时间  
		maxDate: '<?=date('Y-m-d H:i')?>', 
		format: 'YYYY-MM-DD HH:mm',
		separator:'~',
		 ranges : {  
			//'最近1小时': [moment().subtract('hours',1), moment()],  
			'今日': [moment().startOf('day'), moment()],  
			'昨日': [moment().subtract('days', 1).startOf('day'), moment().subtract('days', 1).endOf('day')],  
			'最近7日': [moment().subtract('days', 6).startOf('day'), moment()],  
		
		},
		locale : {  
			applyLabel : '确定',  
			cancelLabel : '取消',  
			fromLabel : '起始时间',  
			toLabel : '结束时间',  
			customRangeLabel : '自定义',  
			daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],  
			monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',  
					'七月', '八月', '九月', '十月', '十一月', '十二月' ],  
			firstDay : 1  
		}  
	},
	function(start, end, label) {
		console.log(start.toISOString(), end.toISOString(), label);
	});
				
				
	function bonus(bid,mt4,jine){
		$("#bid").val(bid);
		$("#mt4").val(mt4);
		$("#jine").val(jine);
		$('#myModal1').modal('show')
	}
	
	$("#bonus-btn").click(function(){
	
		$.post("action.php?type=bonus",{bid:$("#bid").val()},function(data,status){
			
		if(typeof data =='object'){
				var json='{"type":"payment","name":"Bonus_'+$("#bid").val()+'","key":"'+data.mt4+'","money":"'+data.jine+'","ptype":1,"beizhu":"xxx","token":"pong"}';
				console.log(json);
				ws.send(json);
				$('#myModal2').modal('show');
				$('#myModal2 .modal-body').html('入金成功'); 
				$('#myModal2').on('hidden.bs.modal', function (e) {
					window.location.reload();
				});
		}else{
				$("#myModal2").modal('show');
				$("#myModal2 .modal-body").html("操作失败");
		}
		},'json');
	});
	
	
	$("#export-btn").click(function(){
		var xtitle="奖金列表";
		var xhead='ID,用户名,MT4账号,奖金,时间';
		var xsql="<?="select t1.bid,t2.nickname,t3.mt4,t1.money,FROM_UNIXTIME(t1.time,'%Y-%m-%d %H:%i:%s') time from bonus t1 left join users t2  on t1.uid=t2.uid  left join mt4 t3 on t1.uid=t3.uid where 1=1 $sqlxs order by bid desc"?>";
		$("#xtitle").val(xtitle);
		$("#xhead").val(xhead);
		$("#xsql").val(xsql);
		$("#export-form").submit();
		return false;
	});
	
	</script>
<?php include_once 'foot.php'; ?>
</body>

</html>
