<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

	
     <link href="assets/js/stackable/stacktable.css" rel="stylesheet">
    <link href="assets/js/stackable/responsive-table.css" rel="stylesheet">
<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />


    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">

            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>入金管理</span>
                        </h2>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">入金管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">入金通道列表</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
				<div class="row">
				
							
                   <div class="col-sm-12">

                        <div class="nest" id="StackableClose">
                            <div class="title-alt">
                                <h6>入金通道列表</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#tStackableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                             

                            </div>

                            <div class="body-nest" id="StackableStatic">

                                <table id="responsive-example-table" class="table large-only">
                                    <tbody>
                                        <tr class="text-right">
									
                                            <th style="width:5%;">ID</th>
                                            <th style="width:20%;">通道名称</th>
                                            <th style="width:20%;">通道地址</th>
											<th style="width:20%;">最小入金金额</th>
                                            <th style="width:20%;">最大入金金额</th>
											 <th style="width:15%;">状态</th>
											<th style="width:10%;">操作</th>
                                        </tr>
										
<?php 
$pagesize=10;
if($_GET[pagesize]!= null){
	$pagesize=$_GET[pagesize];
	$aplus="&pagesize=$pagesize";
}

if($_GET[page]){
	$page=$_GET[page];
}else{
	$page=1;
}
$qian=($page-1)*$pagesize;


$sql="select * from rujin_channel where 1=1 $sqlxs order by rcid asc limit $qian,$pagesize";

	$channels =$res->fn_rows($sql);
	foreach($channels as $channel){
?>	
                                        <tr>
                                            <td><?=$channel[rcid]?></td>
                                            <td><?=$channel[rc_name]?></td>
											<td><?=$channel[rc_action]?></td>
                                       <td><?=$channel[min_money]?></td>
											<td><?=$channel[max_money]?></td>
											<td>
<?php 
   if($channel[status]==1){
 ?>
	<span class="label label-success" onclick="editChannel('<?=$channel[rcid]?>','<?=$channel[rc_name]?>','<?=$channel[rc_action]?>','<?=$channel[status]?>','<?=$channel[min_money]?>','<?=$channel[max_money]?>')">显示</span>
<?php  }else{ ?>
	<span class="label label-warning" onclick="editChannel('<?=$channel[rcid]?>','<?=$channel[rc_name]?>','<?=$channel[rc_action]?>','<?=$channel[status]?>','<?=$channel[min_money]?>','<?=$channel[max_money]?>')" >隐藏</span>
<?php    }?>
											</td>
										
											<td>
	<div class="btn-group">
		<button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">操作
			<span class="caret"></span>
		</button>
		<ul role="menu" class="dropdown-menu">
			<li><a href="javascript:;" onclick="editChannel('<?=$channel[rcid]?>','<?=$channel[rc_name]?>','<?=$channel[rc_action]?>','<?=$channel[status]?>','<?=$channel[min_money]?>','<?=$channel[max_money]?>')" ><i class="icon-document-edit"></i>&nbsp;&nbsp;修改资料</a></li>
		
			<li><a href="/sys/delete.php?table=rujin_channel&field=rcid&id=<?=$channel[rcid]?>"><i class="entypo-cancel-circled"></i>&nbsp;&nbsp;删除</a></li>
		</ul>
	</div>
											</td>
                                        </tr>
	<?php } ?>
                                    </tbody>
                                </table>

 <ul class="pagination">
     <?php
	 $sql2="select * from rujin_channel ";
$num=$res->fn_num($sql2);

    $myPage=new pager($num,intval($page),$pagesize,"active");     
     $pageStr= $myPage->GetPagerContent();    
     echo $pageStr;   
?>
 
           </ul>
		   
                            </div>

                        </div>


                    </div>


                </div>
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->
	<!-- Modal -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
	<!-- Modal12-->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
			<div class="form-horizontal" >
					<input type="hidden" name="rcid" id="rcid" class="form-control round-input" value="" readonly>
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">通道名称:</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="rc_name" id="rc_name" class="form-control round-input" value="" >
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">通道地址：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="rc_action" id="rc_action" class="form-control round-input" value="" >
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">最小入金金额：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="min_money" id="min_money" class="form-control round-input" value="" >
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">最大入金金额：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="max_money" id="max_money" class="form-control round-input" value="" >
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">
						状态：</label>
						<div class="col-lg-6 input-group">
							<select name="rc_status" id="rc_status" class="form-control round-input" >
									<option value="1">显示</option>
									<option value="0">隐藏</option>
							
							</select>
						</div>
					</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="save-btn">提交</button>
      </div>
    </div>
  </div>
</div>
    <script type="text/javascript" src="assets/js/jquery.js"></script>

    <!-- GAGE -->


   <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
   <!-- /MAIN EFFECT -->
    <script type="text/javascript" src="assets/js/stackable/stacktable.js"></script>
	   <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.js"></script>
	 <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>

<script>
function editChannel(rcid,rc_name,rc_action,rc_status,max_money,min_money){

	$("#rcid").val(rcid);
	$("#rc_name").val(rc_name);
	$("#rc_action").val(rc_action);	
	$("#rc_status").val(rc_status);
	$("#min_money").val(min_money);	
	$("#max_money").val(max_money);
	
	$("#myModal2").modal('show');	
}


$("#save-btn").click(function(){
	
		var rcid=$("#rcid").val();
		var rc_name=$("#rc_name").val();
		var rc_action=$("#rc_action").val();  
		var rc_status=$("#rc_status").val();
		var min_money=$("#min_money").val();
		var max_money=$("#max_money").val();
	
		if( !rc_name ){
		    layer.tips('通道名称不能为空!', '#rc_name');
			return false;
		}
		if( !rc_action ){
		    layer.tips('通道地址不能为空!', '#rc_action');
			return false;
		}
		
		var options = {
			url: 'action.php?type=addrujinchannel',
			type: 'post',
			data: {rcid:rcid,rc_name:rc_name,rc_action:rc_action,rc_status:rc_status,min_money:min_money,max_money:max_money},
			success: function (data) {
					$('#myModal2').modal('hide')
					if( data == 'success'){
						
						$('#myModal1').modal('show')
						$('#myModal1 .modal-body').html('提交成功'); 
						$('#myModal1').on('hidden.bs.modal', function (e) {
							window.location.reload();
						})

					}else{
						
						$('#myModal1').modal('show')
						$('#myModal1 .modal-body').html('提交失败'); 
					
					}
			}
		};
		$.ajax(options);
		
});
	
</script>
<?php include_once 'foot.php'; ?>
</body>

</html>
