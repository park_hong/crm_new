<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css?123">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="assets/js/progress-bar/number-pb.css">
  <link rel="stylesheet" type="text/css" href="assets/js/parsley/src/parsley.css">
<link href="assets/js/bootstrap-datepicker/css/bootstrap-datetimepicker.css" rel="stylesheet" />
    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>会员管理</span>
                        </h2>

                    </div>

                  
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">会员管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">修改密码</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
			
				  <!--  新增会员-->
		
                    <div class="col-sm-12">
                        <div class="nest" id="elementClose">
                            <div class="title-alt">
                                <h6>修改密码</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#elementClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#element">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="element">

		<div class="panel-body">
			<form  method="post" class="demo-form form-horizontal bucket-form" data-parsley-validate >
				<input type="hidden" name="uid" id="uid" value="<?=$user[uid]?>" />
				<input type="hidden" name="mtid" id="mtid"  value="<?=$_GET[mtid]?>" />
				<div class="form-group">
					<label class="col-sm-1 control-label">账号</label>
					<div class="col-sm-3">
						<input type="text" name="username" id="username" class="form-control" data-parsley-required="true" data-parsley-trigger="focusout" value="<?=$u[username]?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label">密码</label>
					<div class="col-sm-3">
						<input type="text" name="password" id="password" class="form-control" value="" onfocus="this.type='password'">
					</div>
				</div>

			<div class="form-group">
					<label class="col-sm-1 control-label">确认密码</label>
					<div class="col-sm-3">
						<input type="text" name="rpassword" id="rpassword" class="form-control" value="" onfocus="this.type='password'">
					</div>
				</div>
				
				
				
				<div class="col-lg-offset-1 col-lg-10">
					<button class="btn btn-info"  id="submit-btn">提交</button>
				</div>
			</form>
		</div>

                            </div>

							</div>
						</div>
				   <!-- end 新增会员-->
				 
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->

	
	<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


    <!-- RIGHT SLIDER CONTENT -->
   

    <!-- END OF RIGHT SLIDER CONTENT-->
    <script type="text/javascript" src="assets/js/jquery.js"></script>
    <script src="assets/js/progress-bar/src/jquery.velocity.min.js"></script>
    <script src="assets/js/progress-bar/number-pb.js"></script>
    <script src="assets/js/progress-bar/progress-app.js"></script>



    <!-- MAIN EFFECT -->
    <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>




    <!-- GAGE -->

<script type="text/javascript" src="assets/js/bootstrap-datepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-datepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
	
<script type="text/javascript" src="assets/js/parsley/dist/parsley.js"></script>
<script type="text/javascript" src="assets/js/parsley/dist/i18n/zh_cn.js"></script>
	  <!--socoket start-->
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/web_socket.js"></script>
<script type="text/javascript" src="/js/json.js"></script>
<script type="text/javascript" src="/static/websocket_config.js"  media="all"></script>
<script>
   $('.demo-form').parsley().on('form:submit', function (formInstance) {

	
	return false;
  });

  $("#submit-btn").click(function(){
		
		var password = $("#password").val();
		var rpassword = $("#rpassword").val();
		if( !password || !rpassword){
			$('#myModal').modal('show');
			$('#myModal .modal-body').html('密码和确认密码不能为空'); 
		}
		if(password != rpassword){
			$('#myModal').modal('show');
			$('#myModal .modal-body').html('密码和确认密码不一致'); 
		}
		$.post("action.php?type=updatepwd",{password:password,rpassword:rpassword},function(data,status){
			
			if(data=="success"){
			
				$('#myModal').modal('show');
				$('#myModal .modal-body').html('提交成功'); 
				$('#myModal').on('hidden.bs.modal', function (e) {
					window.location.reload();
				});
			}else {
				$('#myModal').modal('show')
				$('#myModal .modal-body').html('提交失败'); 
			
			}
		});
		
		return;
	});
	
	

</script>

<?php include_once 'foot.php'; ?>
</body>

</html>
