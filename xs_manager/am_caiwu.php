<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

    
     <link href="assets/js/stackable/stacktable.css" rel="stylesheet">
    <link href="assets/js/stackable/responsive-table.css" rel="stylesheet">
<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />


    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
        <?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>财务管理</span>
                        </h2>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">财务管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">财务列表</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
                <div class="row">
                
                        <div class="body-nest" id="inlineClose">
                               <div class="form_center" style="width:100%">
                                    <form role="form" class="form-inline " method="get">
                                    
                                        <div class="form-group">
                                    
                                             <select class="form-control" name="pagesize">
                                                <option value="10" <?php if($_GET['pagesize']=='10'){ echo 'selected="selected"'; } ?>>每页10条</option>
                                                <option value="30" <?php if($_GET['pagesize']=='30'){ echo 'selected="selected"'; } ?>>每页30条</option>
                                                <option value="50" <?php if($_GET['pagesize']=='50'){ echo 'selected="selected"'; } ?>>每页50条</option>
                                                <option value="100"  <?php if($_GET['pagesize']=='100'){ echo 'selected="selected"'; } ?>>每页100条</option>
                                             </select>
                                        
                                            <input type="text" class="form-control" placeholder="MT4账号 / 姓名" size="12" name="search" value="<?=$_GET[search]?>">
                                            <select class="form-control" name="selecttype">
                                                <option value="" <?php if($_GET['selecttype']==null){ echo 'selected="selected"'; } ?>>全部</option>
                                                <option value="奖金" <?php if($_GET['selecttype']=='奖金'){ echo 'selected="selected"'; } ?>>奖金</option>
                                                <option value="调整余额" <?php if($_GET['selecttype']=='调整余额'){ echo 'selected="selected"'; } ?>>调整余额</option>
                                                <option value="代理佣金" <?php if($_GET['selecttype']=='代理佣金'){ echo 'selected="selected"'; } ?>>代理佣金</option>
                                            </select>
                                                

                                            <input type="text" placeholder="时间" class="form-control" id="reservation" name="reservation" style="width:320px" value="<?=$_GET[reservation]?>" autocomplete="off"> 
                                                
                                            <button type="submit" class="btn btn-success">搜索</button>
                                                
                                            <button class="btn btn-primary" id="export-btn" >导出数据</button>
                                        </div>
                                            
                                        
                                    </form>
                                </div>

                        </div>
                            
                   <div class="col-sm-12">

                        <div class="nest" id="StackableClose">
                            <div class="title-alt">
                                <h6>财务列表</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#tStackableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a type="button" class="btn btn-primary" id="add_btn" href="adduser.php">
                                        <span class="entypo-plus-squared"></span>&nbsp;&nbsp;添加账号
                                    </a>
                                    <a class="nav-toggle-alt" href="#StackableStatic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                              

                            </div>

                            <div class="body-nest" id="StackableStatic">

                                <table id="responsive-example-table" class="table large-only">
                                    <tbody>
                                        <tr class="text-right">
                                    
                                            <th style="width:5%;">ID</th>
                                            <th style="width:10%;">MT4账号</th>
                                            <th style="width:10%;">真实姓名</th>
                                            <th style="width:15%;">金额</th>
                                            <th style="width:15%;">类别</th>
                                            <th style="width:15%;">备注</th>
                                            <th style="width:15%;">时间</th>
                                            
                                        </tr>

<?php
$pagesize=10;
if($_GET[pagesize]!= null){
    $pagesize=$_GET[pagesize];
    $aplus="&pagesize=$pagesize";
}

if($_GET[page]){
    $page=$_GET[page];
}else{
    $page=1;
}
$qian=($page-1)*$pagesize;

if($_GET[search]!= null){
    $sqlxs.=" and (t3.realname like '%$_GET[search]%' or t4.mt4 like '%$_GET[search]%')";
}
if($_GET[selecttype]!= null){
    $sqlxs.=" and t1.type = '$_GET[selecttype]'";
}



if($_GET['reservation']!= null){
        $reservation=explode("~",$_GET['reservation']);
        $start_time=strtotime($reservation[0]);
        $end_time=strtotime($reservation[1]);
        $sqlxs.=" and t1.time>'$start_time' and t1.time<'$end_time'";
        $aplus.="&reservation=$_GET[reservation]";
}

if($_GET[uid]){
        $sqlxs.=" and t1.uid='$_GET[uid]' ";
        $aplus.="&uid=$_GET[uid]";
}
    
    $sql="select t1.*,t2.nickname, t3.realname ,t4.mt4 from caiwu t1 left join users t2 on t1.uid=t2.uid left join idcard t3 on t1.uid=t3.uid left join mt4 t4 on t1.uid=t4.uid where 1=1 $sqlxs  order by cid desc limit $qian,$pagesize";

    $caiwus =$res->fn_rows($sql);
    foreach($caiwus as $caiwu){
?>  
<tr>
    <td><?=$caiwu[cid]?></td>
    <td><?=$caiwu[mt4]?></td>
    <td><a href="am_users.php?mid=<?=$caiwu[uid]?>"><?=$caiwu[realname]?></a></td>
    <td><?=$caiwu[money]?></td>
    <td><?=$caiwu[type]?></td>
    <td><?=$caiwu[beizhu]?></td>
    <td><?=date('Y-m-d H:i:s',$caiwu[time])?></td>
    <?php } ?>
                                    </tbody>
                                </table>

 <ul class="pagination">
     <?php
if($_GET[uid]){
    $sql2="select t1.*,t2.nickname from caiwu t1 left join users t2 on t1.uid=t2.uid left join idcard t3 on t1.uid=t3.uid left join mt4 t4 on t1.uid=t4.uid where 1=1 and t1.uid='$_GET[uid]' $sqlxs   order by cid desc";
}else{
    $sql2="select t1.*,t2.nickname from caiwu t1 left join users t2 on t1.uid=t2.uid left join idcard t3 on t1.uid=t3.uid left join mt4 t4 on t1.uid=t4.uid where 1=1 $sqlxs  order by cid desc";
}

$num=$res->fn_num($sql2);

    $myPage=new pager($num,intval($page),$pagesize,"active");     
     $pageStr= $myPage->GetPagerContent();    
     echo $pageStr;   
?>   
 
           </ul>
           
                            </div>

                        </div>


                    </div>


                </div>
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->

    <form name="export-form" id="export-form" action="action.php?type=exportexcel" method="post" target="e" style="display:none">
        <input type="hidden"  name="xtitle" id="xtitle" value="" />
        <input type="hidden"  name="xhead" id="xhead" value="" />
        <input type="hidden"  name="xsql" id="xsql" value="" />
    </form>
    <iframe name="e" style="display:none"></iframe>
    
    <script type="text/javascript" src="assets/js/jquery.js"></script>

    <!-- GAGE -->


    <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
   <!-- /MAIN EFFECT -->
    <script type="text/javascript" src="assets/js/stackable/stacktable.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
     
<script>

            $('#reservation').daterangepicker({
                            
                    timePicker: true,
                    timePicker12Hour : false, //是否使用12小时制来显示时间  
                    maxDate: '<?=date('Y-m-d H:i')?>', 
                    format: 'YYYY-MM-DD HH:mm',
                    separator:'~',
                     ranges : {  
                        //'最近1小时': [moment().subtract('hours',1), moment()],  
                        '今日': [moment().startOf('day'), moment()],  
                        '昨日': [moment().subtract('days', 1).startOf('day'), moment().subtract('days', 1).endOf('day')],  
                        '最近7日': [moment().subtract('days', 6).startOf('day'), moment()],  
                    
                    },
                    locale : {  
                        applyLabel : '确定',  
                        cancelLabel : '取消',  
                        fromLabel : '起始时间',  
                        toLabel : '结束时间',  
                        customRangeLabel : '自定义',  
                        daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],  
                        monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',  
                                '七月', '八月', '九月', '十月', '十一月', '十二月' ],  
                        firstDay : 1  
                    }  
                },
                function(start, end, label) {
                    console.log(start.toISOString(), end.toISOString(), label);
                });

    $("#export-btn").click(function(){
        var xtitle="财务列表";
        var xhead='ID,MT4账号,真实姓名,金额,类别,备注,时间';
        var xsql="<?="select t1.cid,t4.mt4,t3.realname,t1.money,t1.type,t1.beizhu,FROM_UNIXTIME(t1.time,'%Y-%m-%d %H:%i:%s') time from caiwu t1 left join users t2 on t1.uid=t2.uid left join idcard t3 on t2.uid=t3.uid left join mt4 t4 on t4.uid =t1.uid where 1=1  $sqlxs order by cid desc"?>";
        $("#xtitle").val(xtitle);
        $("#xhead").val(xhead);
        $("#xsql").val(xsql);
        $("#export-form").submit();
        return false;
    });
</script>


<?php include_once 'foot.php'; ?>
</body>

</html>

select t1.*,t2.nickname, t3.realname ,t4.mt4 from caiwu t1 
    left join users t2 on t1.uid=t2.uid 
    left join idcard t3 on t1.uid=t3.uid 
    left join mt4 t4 on t1.uid=t4.uid where 1=1 $sqlxs  order by cid desc limit $qian,$pagesize