	  <div id="logo">
         <h1>网站后台</h1>
        </div>

        <a id="toggle">
            <span class="entypo-menu"></span>
        </a>
        <div class="dark">
            <form action="#">
                <span>
                    <input type="text" name="search" value="" class="search rounded id_search" placeholder="搜索菜单..." autofocus="">
                </span>
            </form>
        </div>

        <div class="search-hover hide">
            <form id="demo-2">
                <input type="search" placeholder="搜索菜单..." class="id_search">
            </form>
        </div>


        <div class="skin-part">
            <div id="tree-wrap">	
		
		<div class="side-bar">
		<ul class="topnav menu-left-nest" >
			<li>
				<a href="index.php" style="border-left:0px solid!important;" class="title-menu-left">

					<i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>
					<span>    控制面板     </span>
				</a>
			</li>
<?php 
	$quanxin=explode(',',$u[quanxian]);
?>
<?php if(in_array('会员管理',$quanxin)) {?>		
			<li>
				<a class="tooltip-tip ajax-load " href="#" title="会员管理">
					<i class="entypo-users"></i>
					<span>会员管理</span>
			<?php
	   if($weishenhe_idcard){ ?>
		<div class="noft-blue" style="display: inline-block; float: none;background: none repeat scroll 0 0 #EA3F3F;" onclick="location.href='am_users_daishenhe.php'"><?=$weishenhe_idcard?></div>
		<?php } ?>
				</a>
				<ul>
					<li <?php if($php_self=='adduser.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="adduser.php" title="新增会员"><i class="entypo-user-add"></i><span>新增会员</span></a>
					</li>
					
					<li <?php if($php_self=='am_users.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_users.php" title="会员列表"><i class="entypo-list"></i><span>会员列表</span>
						</a>
					</li>
					<li>
					<a class="tooltip-tip2 ajax-load" href="am_users.php?daishenhe=1" title="待审核会员列表"><i class="entypo-list"></i><span>待审核会员列表</span>
		<?php
	   if($weishenhe_idcard){ ?>
						<div class="noft-blue" style="display: inline-block; float: none;background: none repeat scroll 0 0 #EA3F3F;" ><?=$weishenhe_idcard?></div>
		<?php } ?>
						</a>
					</li>
				</ul>
			</li>
<?php  }  if(in_array('信号源管理',$quanxin)) {?>
			<li class="hide">
				<a class="tooltip-tip ajax-load"  href="#" title="信号源管理">
					<i class="icon-feed"></i>
					<span>信号源管理</span>
				</a>
				<ul>
			<li <?php if($php_self=='adduser2.php') echo 'class="active"'; ?>>
				<a class="tooltip-tip2 ajax-load" href="adduser2.php" title="新增信号源"><i class="entypo-plus-squared"></i><span>新增信号源</span></a>
			</li>
			<li <?php if($php_self=='am_users2.php') echo 'class="active"'; ?>>
				<a class="tooltip-tip2 ajax-load" href="am_users2.php" title="信号源列表"><i class="entypo-list"></i><span>信号源列表</span></a>
			</li>
			<li <?php if($php_self=='am_mt42.php') echo 'class="active"'; ?>>
				<a class="tooltip-tip2 ajax-load" href="am_mt42.php.php" title="查看跟随用户"><i class="entypo-list"></i><span>查看跟随用户</span></a>
			</li>
				
				</ul>
			</li>
<?php  }  if(in_array('MT4管理',$quanxin)) {?>
			<li>
				<a class="tooltip-tip ajax-load" href="#" title="MT4管理">
					<i class="icon-window"></i>
					<span>MT4管理</span>

				</a>
				<ul>
					<li <?php if($php_self=='am_mt4.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_mt4.php" title="会员MT4列表"><i class="entypo-list"></i><span>会员MT4列表</span>
						</a>
					</li>
				
					
				</ul>
			</li>
<?php  }  if(in_array('牛人申请管理',$quanxin)) {?>
			<li  class="hide">
				<a class="tooltip-tip ajax-load" href="#" title="MT4管理">
					<i class="fontawesome-volume-up"></i>
					<span>牛人申请管理</span>
<?php
	   if($weishenhe_handan){ ?>
		<div class="noft-blue" style="display: inline-block; float: none;background: none repeat scroll 0 0 #EA3F3F;" onclick="location.href='am_handan_daishenhe.php'"><?=$weishenhe_handan?></div>
		<?php } ?>
				</a>
				<ul>
					<li <?php if($php_self=='am_handan.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_handan.php" title="喊单申请列表"><i class="entypo-list"></i><span>牛人申请列表</span>

						</a>
					</li>
					<li <?php if($php_self=='am_handan.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_handan.php?daishenhe=1" title="待审核喊单申请列表"><i class="entypo-list"></i><span>待审核牛人申请列表</span>
			<?php
	   if($weishenhe_handan){ ?>
						<div class="noft-blue" style="display: inline-block; float: none;  background: none repeat scroll 0 0 #EA3F3F;"><?=$weishenhe_handan?></div>
		<?php } ?>
						</a>
					</li>
				</ul>
			</li>
<?php  }  if(in_array('平台服务器管理',$quanxin)) {?>			
			<li>
				<a class="tooltip-tip ajax-load" href="#" title="平台服务器管理">
					<i class="entypo-paper-plane"></i>
					<span>平台服务器管理</span>
				</a>
				<ul>
					<li <?php if($php_self=='addserver.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="addserver.php" title="新增平台"><i class="entypo-plus-squared"></i><span>新增平台</span></a>
					</li>
					 <li <?php if($php_self=='am_server.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_server.php" title="平台列表"><i class="entypo-list"></i><span>平台列表</span></a>
					</li>

				</ul>
			</li>
<?php  }  if(in_array('策略管理',$quanxin)) {?>	
			<li  class="hide">
				<a class="tooltip-tip ajax-load" href="javascript:;"  title="策略管理">
					<i class="icon-direction"></i>
					<span>策略管理</span>
				</a>
				<ul>
					<li <?php if($php_self=='am_mt4_contact.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_mt4_contact.php" title="策略列表"><i class="entypo-list"></i><span>策略列表</span>
						</a>
					</li>
				
					
				</ul>
			</li>
<?php } ?>
		</ul>

		<ul class="topnav menu-left-nest"  style="margin:10px">
			<li>
				<a href="#" style="border-left:0px solid!important;" class="title-menu-left">
					<i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>
				</a>
			</li>

		<?php  if(in_array('订单管理',$quanxin)) {?>

			<li>
				<a class="tooltip-tip ajax-load" href="#" title="订单管理">
					<i class="entypo-briefcase"></i>
					<span>订单管理</span>
				</a>
				<ul>
				<li <?php if($php_self=='am_guanxi.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_guanxi.php" title="仓位总结"><i class="entypo-list"></i><span>仓位总结</span>
						</a>
					</li>
						<li <?php if($php_self=='adddingdan.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="adddingdan.php" title="新增订单"><i class="entypo-plus-squared"></i><span>新增订单</span></a>
					</li>
					<li <?php if($php_self=='am_dingdan.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_dingdan.php" title="历史订单列表"><i class="entypo-list"></i><span>订单列表总览</span></a>
					</li>
					<li <?php if($php_self=='am_chicang.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_chicang.php" title="当前持仓列表"><i class="entypo-list"></i><span>当前持仓列表</span></a>
					</li>
				</ul>
			</li>
		<?php } if(in_array('财务管理',$quanxin)) {?>

			<li>
		<a class="tooltip-tip ajax-load" href="#" title="财务管理">
					<i class="entypo-briefcase"></i>
					<span>财务管理</span>
				</a>
				<ul>
					<li <?php if($php_self=='am_caiwu.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_caiwu.php" title="财务列表"><i class="entypo-list"></i><span>财务列表</span></a>
					</li>
					<li <?php if($php_self=='addbonus1.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="addbonus1xs.php" title="冲账管理"><i class="entypo-list"></i><span>冲账管理</span></a>
					</li>

				</ul>
			</li>
	<?php } if(in_array('会员日志管理',$quanxin)) {?>	

			<li>
		<a class="tooltip-tip ajax-load" href="#" title="财务管理">
					<i class="entypo-briefcase"></i>
					<span>会员日志管理</span>
				</a>
				<ul>
					<li <?php if($php_self=='am_user_log.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_user_log.php" title="会员日志列表"><i class="entypo-list"></i><span>会员日志列表</span></a>
					</li>

				</ul>
			</li>
	<?php } if(in_array('出金管理',$quanxin)) {?>
			<li>
				<a class="tooltip-tip ajax-load" href="#" title="出金管理">
					<i class="fontawesome-money"></i>
					<span>出金管理</span>
		<?php
	   if($weishenhe_tixian){ ?>
	<div class="noft-blue" style="display: inline-block; float: none;  background: none repeat scroll 0 0 #EA3F3F;" onclick="location.href='am_tixian_daishenhe.php'"><?=$weishenhe_tixian?></div>
		<?php } ?>
				</a>
				<ul>
					<li <?php if($php_self=='am_tixian.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_tixian.php" title="出金列表"><i class="entypo-list"></i><span>出金列表</span>
						</a>
					</li>
					<li <?php if($php_self=='am_tixian.php' && $_GET[daishenhe]) echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_tixian.php?daishenhe=1" title="未审核出金列表"><i class="entypo-list"></i><span>未审核出金列表</span>
		<?php
	   if($weishenhe_tixian){ ?>
						<div class="noft-blue" style="display: inline-block; float: none; background: none repeat scroll 0 0 #EA3F3F;"><?=$weishenhe_tixian?></div>
		<?php } ?>	
						</a>
					</li>
				</ul>
			</li>
		<?php } if(in_array('入金管理',$quanxin)) {?>
			<li>
				<a class="tooltip-tip ajax-load" href="#" title="入金管理">
					<i class="fontawesome-money"></i>
					<span>入金管理</span>
				</a>
				<ul>
					<li <?php if($php_self=='am_rujin.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_rujin.php" title="入金列表"><i class="entypo-list"></i><span>入金列表</span>
						</a>
					</li>
					<li <?php if($php_self=='am_rujin_channel.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_rujin_channel.php" title="入金通道列表"><i class="entypo-list"></i><span>入金通道列表</span>
						</a>
					</li>
				</ul>
			</li>
		<?php } if(in_array('奖金管理',$quanxin)) {?>
			<li>
				<a class="tooltip-tip ajax-load" href="#" title="奖金管理">
					<i class="fontawesome-money"></i>
					<span>奖金管理</span>
				</a>
				<ul>
					<li <?php if($php_self=='am_bonus.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_bonus.php" title="提现列表"><i class="entypo-list"></i><span>奖金列表</span>
						</a>
					</li>
				</ul>
			</li>
		<?php } if(in_array('转账管理',$quanxin)) {?>
			<li>
				<a class="tooltip-tip ajax-load" href="#" title="转账管理">
					<i class="fontawesome-money"></i>
					<span>转账管理</span>
	
				</a>
				<ul>
					<li <?php if($php_self=='am_zhuanzhang.php' ) echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_zhuanzhang.php" title="转账列表"><i class="entypo-list"></i><span>转账列表</span>
						</a>
					</li>
					
				</ul>
			</li>
		<?php  }  if(in_array('代理商专区',$quanxin)) {?>
			<li>
				<a class="tooltip-tip ajax-load" href="#" title="代理商专区">
					<i class="entypo-globe"></i>
					<span>代理商专区</span>
				</a>
				<ul>
					
					<li <?php if($php_self=='am_fanyong.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_fanyong.php" title="计算返佣"><i class="entypo-database"></i><span>计算返佣</span></a>
					</li>
					<li <?php if($php_self=='am_fanyong_list.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_fanyong_list.php" title="历史返佣记录"><i class="entypo-list"></i><span>历史返佣记录</span></a>
					</li>
					<li <?php if($php_self=='am_fanyong_list.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_fanyong_list.php?weijiesuan=1" title="未结算返佣记录"><i class="entypo-list"></i><span>未结算返佣记录</span></a>
					</li>
				</ul>
			</li>
	<?php  }  if(in_array('分润管理',$quanxin)) {?>		
			<li>
				<a class="tooltip-tip ajax-load" href="#" title="分润管理">
					<i class="fontawesome-exchange"></i>
					<span>分润管理</span>
				</a>
				<ul>
					<li <?php if($php_self=='am_fenrun.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_fenrun.php" title="计算分润"><i class="entypo-database"></i><span>计算分润</span></a>
					</li>
					<li <?php if($php_self=='am_fenrun_list.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_fenrun_list.php" title="历史分润记录"><i class="entypo-list"></i><span>历史分润记录</span></a>
					</li>
					<li <?php if($php_self=='am_fenrun_list.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_fenrun_list.php?weijiesuan=1" title="未结算分润记录"><i class="entypo-list"></i><span>未结算分润记录</span></a>
					</li>
				</ul>
			</li>

	<?php  }  if(in_array('同步数据管理',$quanxin)) {?>	
			<li>
				<a class="tooltip-tip ajax-load" href="#" title="同步数据管理">
					<i class="entypo-globe"></i>
					<span>同步数据管理</span>
				</a>
				<ul>
					<li <?php if($php_self=='am_fanyong_data.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_fanyong_data.php" title="同步客户数据"><i class="entypo-database"></i><span>同步客户数据</span></a>
					</li>

				</ul>
			</li>
		<?php } ?>
		</ul>
	
		<ul class="topnav menu-left-nest"  style="margin:10px">
		
			<li>
				<a href="#" style="border-left:0px solid!important;" class="title-menu-left">
					<i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>
				</a>
			</li>
	<?php   if(in_array('单页管理',$quanxin)) {?>			
			<li>
				<a class="tooltip-tip ajax-load" href="#" title="单页管理">
					<i class="entypo-newspaper"></i>
					<span>单页管理</span>

				</a>
				<ul>
					<li <?php if($php_self=='adddanye.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="adddanye.php" title="新增单页"><i class="entypo-plus-squared"></i><span>新增单页</span></a>
					</li>
					<li <?php if($php_self=='am_danye.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_danye.php" title="单页列表"><i class="entypo-list"></i><span>单页列表</span></a>
					</li>
				
				</ul>
			</li>
	<?php  }  if(in_array('文章管理',$quanxin)) {?>		
			<li>
				<a class="tooltip-tip ajax-load" href="#" title="文章管理">
					<i class="entypo-newspaper"></i>
					<span>文章管理</span>

				</a>
				<ul>
				<?php 
					$newstypes=$res->fn_rows("select * from newstype where islist=1 and typeid=10");
					foreach($newstypes as $newstype){
				?>
					<li <?php if($php_self=='addnews.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="addnews.php?typeid=<?=$newstype[typeid]?>&typename=<?=$newstype[typename]?>" title="新增<?=$newstype[typename]?>"><i class="entypo-plus-squared"></i><span>新增<?=$newstype[typename]?></span></a>
					</li>
				<?php  }
						foreach($newstypes as $newstype){
				?>
					<li <?php if($php_self=='am_news.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_news.php?typeid=<?=$newstype[typeid]?>&typename=<?=$newstype[typename]?>" title="<?=$newstype[typename]?>列表"><i class="entypo-list"></i><span><?=$newstype[typename]?>列表</span></a>
					</li>
				<?php }　?>
				
				</ul>
			</li>
	<?php }  if(in_array('幻灯片管理',$quanxin)) {?>			
			<li>
				<a class="tooltip-tip ajax-load" href="#" title="幻灯片管理">
					<i class="entypo-newspaper"></i>
					<span>幻灯片管理</span>

				</a>
				<ul>
					
					<li <?php if($php_self=='am_slide.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_slide.php" title="幻灯片列表"><i class="entypo-list"></i><span>幻灯片列表</span></a>
					</li>
				
				</ul>
			</li>
	<?php  }  if(in_array('系统设置',$quanxin)) {?>			
			<li>
				<a class="tooltip-tip ajax-load" href="#" title="系统设置">
					<i class="fontawesome-cogs"></i>
					<span>系统设置</span>
				</a>
				<ul>
					<li <?php if($php_self=='am_admin.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_admin.php" title="管理员设置"><i class="fontawesome-wrench"></i><span>管理员设置</span></a>
					</li>
					<li <?php if($php_self=='baseconfig.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="baseconfig.php" title="基础设置"><i class="fontawesome-wrench"></i><span>基础设置</span></a>
					</li>
					<li <?php if($php_self=='am_log.php') echo 'class="active"'; ?>>
						<a class="tooltip-tip2 ajax-load" href="am_log.php" title="管理员日志"><i class="entypo-list"></i><span>管理员日志</span></a>
					</li>
				</ul>
			</li>
	<?php } ?>
		</ul>

                </div>
     </div>
        </div>