<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

	
     <link href="assets/js/stackable/stacktable.css" rel="stylesheet">
    <link href="assets/js/stackable/responsive-table.css" rel="stylesheet">
<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>

    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>MT4管理</span>
                        </h2>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">MT4管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">会员MT4列表</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
				<div class="row">
				
					    <div class="body-nest" id="inlineClose">
                               <div class="form_center" style="width:100%">
                                    <form role="form" class="form-inline " method="get">
									
										<div class="form-group">
									
											 <select class="form-control" name="pagesize">
												<option value="10" <?php if($_GET['pagesize']=='10'){ echo 'selected="selected"'; } ?>>每页10条</option>
												<option value="30" <?php if($_GET['pagesize']=='30'){ echo 'selected="selected"'; } ?>>每页30条</option>
												<option value="50" <?php if($_GET['pagesize']=='50'){ echo 'selected="selected"'; } ?>>每页50条</option>
												<option value="100"  <?php if($_GET['pagesize']=='100'){ echo 'selected="selected"'; } ?>>每页100条</option>
											 </select>
										
												 <input type="text" class="form-control" placeholder="MT4号" size="12" name="search" value="<?=$_GET[search]?>">
									
												<button type="submit" class="btn btn-success">搜索</button>
										</div>
											
										
                                    </form>
                                </div>

                        </div>
							
                   <div class="col-sm-12">

                        <div class="nest" id="StackableClose">
                            <div class="title-alt">
                                <h6>系统账户列表</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#tStackableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
								    <a type="button" class="btn btn-primary" id="add_btn" href="adduser.php">
										<span class="entypo-plus-squared"></span>&nbsp;&nbsp;添加账号
									</a>
                                    <a class="nav-toggle-alt" href="#StackableStatic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="StackableStatic">

                                <table id="responsive-example-table" class="table large-only ">
                                    <thead class="cf">
                                        <tr class="text-right">
									
                                            <th style="width:5%;">ID</th>
                                            <th style="width:20%;">MT4</th>
               
											 <th style="width:15%;">MT4密码</th>
                                            <th style="width:10%;">MT4-IP</th>
											<th style="width:10%;">MT4-Port</th>
											<th style="width:20%;">所属用户</th>
											<th style="width:10%;">状态</th>
											<th style="width:10%;">操作</th>
                                        </tr>
									 </thead>
						    <tbody>
										
<?php 
$pagesize=10;
if($_GET[pagesize]!= null){
	$pagesize=$_GET[pagesize];
	$aplus="&pagesize=$pagesize";
}

if($_GET[page]){
	$page=$_GET[page];
}else{
	$page=1;
}
$qian=($page-1)*$pagesize;

if($_GET[search]!= null){
	$sqlxs.=" and  t1.mt4 like '%$_GET[search]%' ";
	$aplus.="&search=$_GET[search]";
}

  if($_GET[mid]){
		$sql="select t1.mtid,t1.mt4,t1.password mt4pwd ,t1.port,t1.status,t1.listenstatus,t1.svid,t1.shenhe,t2.uid,t2.nickname,t2.groupid,t3.ip mt4ip,t3.field from mt4 t1 left join users t2 on t1.uid=t2.uid left join server t3 on t1.svid=t3.svid where 1=1    and t1.uid='$_GET[mid]' $sqlxs order by t1.mtid desc limit $qian,$pagesize";  
	}else{
		$sql="select t1.mtid,t1.mt4,t1.password mt4pwd ,t1.port,t1.status,t1.listenstatus,t1.svid,t1.shenhe,t2.uid,t2.nickname,t2.groupid,t3.ip mt4ip,t3.field from mt4 t1 left join users t2 on t1.uid=t2.uid left join server t3 on t1.svid=t3.svid  where 1=1  and t2.groupid!=10 $sqlxs order by t1.mtid desc limit $qian,$pagesize";  
	 }

  if($_GET[mt4]){
		$sql="select t1.mtid,t1.mt4,t1.password mt4pwd ,t1.port,t1.status,t1.listenstatus,t1.svid,t1.shenhe,t2.uid,t2.nickname,t2.groupid,t3.ip mt4ip,t3.field from mt4 t1 left join users t2 on t1.uid=t2.uid left join server t3 on t1.svid=t3.svid   where 1=1  and t4.mmt4='$_GET[mt4]' and t1.shenhe!=1 $sqlxs order by t1.mtid desc limit $qian,$pagesize";  
	}
	
	$users =$res->fn_rows($sql);
	foreach($users as $user){
?>	
	<tr>
		<td><?=$user[mtid]?></td>
		<td><?=$user[mt4]?>  &nbsp;&nbsp;&nbsp;&nbsp;
<?php 
   if($user[status]==1){
		echo '<span class="label label-success">已登录</span>';
   }else{
		echo '<span class="label label-warning">未登录</span>';
   }?>
	<a class="btn btn-danger"><i class="fontawesome-refresh"></i></a>   
  
	</td>
	<td><?=$user[mt4pwd]?></td>
	<td><?=$user[mt4ip]?></td>
	<td><?=$user[port]?></td>
	<td>
	<a href="am_users.php?mid=<?=$user[uid]?>"><?=$user[nickname]?></a>

	</td>
	<td>
<?php if($user[shenhe]==0){ ?>
	<a type="button"  class="btn btn-warning" onclick="editMt4('<?=$user[mtid]?>','<?=$user[uid]?>','<?=$user[mt4]?>','<?=$user[mt4pwd]?>','<?=$user[svid]?>','<?=$user[port]?>','<?=$user[shenhe]?>');">待审核</a>
<?php }else if($user[shenhe]==-1){　?>
	<a type="button"  class="btn btn-danger" onclick="editMt4('<?=$user[mtid]?>','<?=$user[uid]?>','<?=$user[mt4]?>','<?=$user[mt4pwd]?>','<?=$user[svid]?>','<?=$user[port]?>','<?=$user[shenhe]?>');">已驳回</a>
<?php }else { ?>
	<a type="button"  class="btn btn-success"  onclick="editMt4('<?=$user[mtid]?>','<?=$user[uid]?>','<?=$user[mt4]?>','<?=$user[mt4pwd]?>','<?=$user[svid]?>','<?=$user[port]?>','<?=$user[shenhe]?>');">已审核</a>
<?php } ?>
	</td>
	<td>
	<div class="btn-group">
		<button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">操作
			<span class="caret"></span>
		</button>
	<ul role="menu" class="dropdown-menu">

<li><a href="javascript:;" onclick="editMt4('<?=$user[mtid]?>','<?=$user[uid]?>','<?=$user[mt4]?>','<?=$user[mt4pwd]?>','<?=$user[svid]?>','<?=$user[port]?>','<?=$user[shenhe]?>');"><i class="icon-document-edit"></i>&nbsp;&nbsp;修改MT4</a></li>

<li><a href="am_dingdan.php?mt4=<?=$user[mt4]?>" ><i class="entypo-list"></i>&nbsp;&nbsp;查看订单</a></li>	

<li><a href="javascript:;" onclick="mt4Delete('<?=$user[mt4]?>','<?=$user[port]?>','<?=$user[uid]?>')"><i class="entypo-cancel-circled"></i>&nbsp;&nbsp;删除用户</a></li>

		</ul>
	</div>	
			
			
											</td>
                                        </tr>
	<?php } ?>
                                    </tbody>
                                </table>

 <ul class="pagination">
     <?php
	 
	 if($_GET[mid]){
	 $sql2="select t1.* from mt4 t1 left join users t2 on t1.uid=t2.uid where t1.uid='$_GET[mid]'  left join mt4_contact t4 on t1.mt4=t4.mt4 where 1=1  and t2.groupid!=10 and t1.shenhe!=1 and t1.uid='$_GET[mid]' $sqlxs";  
	}else{
	  $sql2="select t1.* from mt4 t1 left join users t2 on t1.uid=t2.uid  where t2.groupid!=10 and t1.shenhe!=1 $sqlxs";
	 }
	 
	if($_GET[mt4]){
	  $sql2="select t1.* from mt4 t1 left join users t2 on t1.uid=t2.uid   left join mt4_contact t4 on t1.mt4=t4.mt4 where 1=1  and t4.mmt4='$_GET[mt4]' $sqlxs";  
	}
$num=$res->fn_num($sql2);
$ye=(int)($num/$pagesize+1);
?>
 <li> <a href="am_mt4.php?page=1<?=$aplus?>" >首页</a></li>

<?php
if($page<5){
	$iiikaishi=1;
	if($ye-$page>5){
		$iiijishu=5;
	}else{
			$iiijishu=$ye;
	}
}elseif($ye-$page<5){
	$iiikaishi=$ye-5;
	$iiijishu=$ye;
}else{
	$iiikaishi=$page-2;
	$iiijishu=$page+2;
}

for($iii=$iiikaishi;$iii<=$iiijishu;$iii++){
?>
 <li  <?php if($page==$iii){echo 'class="active"';}?>> <a href="am_mt4.php?page=<?=$iii?><?=$aplus?>" ><?=$iii?></a></li>
<?php
}
?>
<li >  <a href="am_mt4.php?page=<?=$ye?><?=$aplus?>" >尾页</a></li>
           </ul>
		   
                            </div>

                        </div>


                    </div>


                </div>
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->


<!-- Modal2 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="height:500px;overflow:auto;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
	  	<input type="hidden" name="uid" id="uid" class="form-control" value="" >
				<div class="form-horizontal" >
				<div class="form-group">
					<label class="col-lg-2 col-sm-2 control-label">MT4账号</label>
					<div class="col-lg-10">
						<input type="text" name="mt4" id="mt4" class="form-control" data-parsley-required="true" data-parsley-trigger="focusout" value="" >
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-sm-2  control-label">MT4密码</label>
					<div class="col-lg-10">
						<input type="text" name="mt4pwd" id="mt4pwd" class="form-control" value="" onfocus="this.type='password'">
					</div>
				</div>

				<div class="form-group ">
					<label class="col-lg-2 col-sm-2  control-label">MT4-IP</label>
					<div class="col-lg-10">
					
						 <select name="svid" id="svid" class="form-control round-input" >
							<?php
						$rs=$res->fn_sql("select * from server  where status=1  order by ip asc ");
						while($server=mysql_fetch_array($rs)){
						?>
						<option value="<?=$server[svid]?>" ><?=$server[ip]?></option>

						<?php } ?>
						</select>  
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-lg-2 col-sm-2  control-label">MT4-端口</label>
					<div class="col-lg-10">
						<input type="text" name="mt4port" id="mt4port" class="form-control round-input" value="">
						 <span>可以使用端口号20000-21000</span>
					</div>
				</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">审核状态：</label>
						<div class="col-lg-10">
							<select name="shenhe" id="shenhe" class="form-control round-input" onchange="if(this.value=='-1') $('#bohui').show();else $('#bohui').hide();">
									<option value="0">未审核</option>
									<option value="1">已审核</option>
									<option value="-1">驳回</option>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="bohui" style="display:none">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">驳回原因：</label>
						<div class="col-lg-10">
							<input type="text" name="message" id="message" class="form-control round-input" value="">
						</div>
					</div>
				</div>
			
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-primary"  id="submit-btn2">提交修改</button>
      </div>
    </div>
  </div>
</div>
    <script type="text/javascript" src="assets/js/jquery.js"></script>

    <!-- GAGE -->


   <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
   <!-- /MAIN EFFECT -->
    <script type="text/javascript" src="assets/js/stackable/stacktable.js"></script>
	   <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.js"></script>
	 <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
	  <script type="text/javascript" src="/js/layer/layer.js"></script>

 <!--socoket start-->
  <script type="text/javascript" src="/js/swfobject.js"></script>
  <script type="text/javascript" src="/js/web_socket.js"></script>
  <script type="text/javascript" src="/js/json.js"></script>
<script type="text/javascript" src="/static/websocket_config.js"  media="all"></script>

 <script>

	
	   function quanxuan(){
			$("tbody .checkboxes").each(function(){
				  if($(this).attr('checked')){
					 $(this).removeAttr('checked');
					 $(this).parent().removeClass("checked");
				 }else{
				 	 $(this).attr('checked','checked');
					 $(this).parent().addClass("checked");
				 }
			
			});
		
	   }
   
       
	   
		function mt4Login(mt4,mt4pwd,mt4ip,mt4port,field){

		var data=JSON.stringify({"type":"loginuser","login":mt4,"password":mt4pwd,"ip":mt4ip,"port":mt4port,"symbol":field});
		console.log(data);
		ws.send(data);
		layer.load(0, {shade: [0.4,'#eee']});
	}
	
	function mt4Loginoff(mt4,mt4pwd,mt4ip,mt4port){
		var data=JSON.stringify({"type":"loginout","port":mt4port});
		console.log(data);
		ws.send(data);
		layer.load(0, {shade: [0.4,'#eee']});
	}
	
	function mt4Listen(mt4,mt4pwd,mt4ip,mt4port){
		var data=JSON.stringify({"type":"addlistener","port":mt4port});
		console.log(data);
		ws.send(data);
		layer.load(0, {shade: [0.4,'#eee']});
	}
	
	function mt4DelListen(mt4,mt4pwd,mt4ip,mt4port){
		var data=JSON.stringify({"type":"delistener","port":mt4port});
		console.log(data);
		ws.send(data);
	    layer.load(0, {shade: [0.4,'#eee']});
	}
	
	function mt4Reflash(mt4,mt4pwd,mt4ip,mt4port){
		$.get("action.php?type=mt4status&status=0",{port:mt4port},function(data,status){});
		var data=JSON.stringify({"type":"history","port":mt4port});
		console.log(data);
		ws.send(data);
	    layer.load(0, {shade: [0.4,'#eee']});
		setTimeout(function(){
			layer.closeAll();
		},6000);
	}
	function mt4Reflash2(mt4,mt4pwd,mt4ip,mt4port){
		$.get("action.php?type=mt4status2&status=0",{port:mt4port},function(data,status){});
		var data=JSON.stringify({"type":"lisquery","port":mt4port});
		console.log(data);
		ws.send(data);
	    layer.load(0, {shade: [0.4,'#eee']});
		setTimeout(function(){
			layer.closeAll();
		},6000);
	}
	
	function mt4Delete(mt4,mt4pwd,mt4ip,mt4port,uid){
		if(!window.confirm('确认删除操作吗？')){ return false;}
	
		$.get("action.php?type=delmt4",{uid:uid,mt4:mt4},function(data,status){
			window.location.reload();
		});
	}
	
	function mt4Shenhe(mtid,op){
		$.get("action.php?type=mt4shenhe",{mtid:mtid,op:op},function(data,status){
			window.location.reload();
		});
	}
	
	
	
	
	var mtid;
function editMt4(id,uid,mt4,mt4pwd,svid,mt4port,shenhe,message){
	mtid=id;
	$("#uid").val(uid);
	$("#mt4").val(mt4);
	$("#mt4pwd").val(mt4pwd);
	$("#svid").val(svid);
	$("#mt4port").val(mt4port);

	$("#myModal").modal('show');	
	$("#shenhe").val(shenhe).trigger('onchange');
	$("#message").val(message);
}
$("#submit-btn2").click(function(){
		var uid=$("#uid").val();
		var mt4=$("#mt4").val();
		var mt4pwd=$("#mt4pwd").val();
		var svid=$("#svid").val();
		var mt4port=$("#mt4port").val();
		
		
		var shenhe=$("#shenhe").val();
		var message=$("#message").val();
	
		
		if(!message && status==-1){
			    layer.tips('驳回原因不能为空!', '#message');
				return false;
		}
		
		
		var options = {
		url: 'action.php?type=addmt4',
		type: 'post',
		data: {mtid:mtid,uid:uid,mt4:mt4,mt4pwd:mt4pwd,svid:svid,mt4port:mt4port,shenhe:shenhe,message:message},
		success: function (data) {
			if(data=="success"){
		
				alert('提交成功'); 
				window.location.reload();

			}else if(data=="invalidate_mt4"){
				
				$('#myModal').modal('show')
				$('#myModal .modal-body').html('MT4已存在'); 
			
			}else if(data=="invalidate_port"){
				
				$('#myModal').modal('show')
				$('#myModal .modal-body').html('端口不可用已'); 
			
			}
			}
		};
		$.ajax(options);
		
	});
	
   </script>



<?php include_once 'foot.php'; ?>
</body>

</html>
