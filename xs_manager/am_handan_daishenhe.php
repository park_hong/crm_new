<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

	
     <link href="assets/js/stackable/stacktable.css" rel="stylesheet">
    <link href="assets/js/stackable/responsive-table.css" rel="stylesheet">
<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>


    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">

            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>喊单管理</span>
                        </h2>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">喊单管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">待审核喊单申请列表</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
				<div class="row">
				
					    <div class="body-nest" id="inlineClose">
                               <div class="form_center" style="width:100%">
                                    <form role="form" class="form-inline " method="get">
									
										<div class="form-group">
									
											 <select class="form-control" name="pagesize">
												<option value="10" <?php if($_GET['pagesize']=='10'){ echo 'selected="selected"'; } ?>>每页10条</option>
												<option value="30" <?php if($_GET['pagesize']=='30'){ echo 'selected="selected"'; } ?>>每页30条</option>
												<option value="50" <?php if($_GET['pagesize']=='50'){ echo 'selected="selected"'; } ?>>每页50条</option>
												<option value="100"  <?php if($_GET['pagesize']=='100'){ echo 'selected="selected"'; } ?>>每页100条</option>
											 </select>
											
												 <input type="text" class="form-control" placeholder="" size="12" name="search">
									
												<button type="submit" class="btn btn-success">搜索</button>
										</div>
											
										
                                    </form>
                                </div>

                        </div>
							
                   <div class="col-sm-12">

                        <div class="nest" id="StackableClose">
                            <div class="title-alt">
                                <h6>待审核喊单申请列表</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#tStackableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
								   
                                    <a class="nav-toggle-alt" href="#StackableStatic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="StackableStatic">

                                <table id="responsive-example-table" class="table large-only">
                                    <tbody>
                                        <tr class="text-right">
									
                                             <th style="width:5%;">ID</th>
                                            <th style="width:15%;">MT4服务器</th>
                                            <th style="width:10%;">MT4账号</th>
											<th style="width:10%;">观摩密码</th>
                                            <th style="width:10%;">交易描述</th>
											<th style="width:10%;">资金量</th>
											<th style="width:5%;">交易年限</th>
											<th style="width:5%;">申请人</th>
											<th style="width:10%;">申请人手机号</th>
											<th style="width:10%;">申请时间</th>
											<th style="width:5%;">状态</th>
											<th style="width:5%;">操作</th>
                                        </tr>
										
<?php 
$pagesize=10;
if($_GET[pagesize]!= null){
	$pagesize=$_GET[pagesize];
	$aplus="&pagesize=$pagesize";
}

if($_GET[page]){
	$page=$_GET[page];
}else{
	$page=1;
}
$qian=($page-1)*$pagesize;



if($_GET[search]!= null){
	$sqlxs.=" and  t1.mt4 like '%$_GET[mt4]%' ";
	$aplus.="&search=$_GET[search]";
}
if($_GET[uid]){
	$sql="select t1.*,t2.nickname,t2.mobile,t3.message from handan t1 left join users t2 on t1.uid=t2.uid left join (select * from message where type=2) t3 on t1.uid=t3.uid where 1=1 and t1.status!=1 and t1.uid='$_GET[uid]'   $sqlxs  order by t1.hid asc limit $qian,$pagesize";
}else{
	$sql="select t1.*,t2.nickname,t2.mobile,t3.message from handan t1 left join users t2 on t1.uid=t2.uid left join (select * from message where type=2) t3 on t1.uid=t3.uid where 1=1 and t1.status!=1 $sqlxs order by t1.hid asc limit $qian,$pagesize";
}


	$handans =$res->fn_rows($sql);
	foreach($handans as $handan){
?>	
                                        <tr>
                                            <td><?=$handan[hid]?></td>
                                            <td><?=$handan[platform]?></td>
											<td><?=$handan[mt4]?></td>
                                            <td><?=$handan[mt4pwd]?></td>
											<td><?=$handan[style]?></td>
											<td><?=$handan[zijin]?></td>
											<td><?=$handan[cycle]?></td>
											<td><?=$handan[nickname]?></td>
											<td><?=$handan[mobile]?></td>
											<td><?=date('Y-m-d H:i:s',$handan[time])?></td>
	<td>
			<?php if($handan[status]==0){ ?>
				<a type="button"  class="btn btn-warning idcard"  onclick="editHandan('<?=$handan[hid]?>','<?=$handan[status]?>','<?=$handan[message]?>')">待审核</a>
			<?php }else if($handan[status]==-1){　?>
				<a type="button"  class="btn btn-danger idcard"  onclick="editHandan('<?=$handan[hid]?>','<?=$handan[status]?>','<?=$handan[message]?>')">已驳回</a>
			<?php }else { ?>
				<a type="button"  class="btn btn-success idcard"  onclick="editHandan('<?=$handan[hid]?>','<?=$handan[status]?>','<?=$handan[message]?>')">已审核</a>
			<?php } ?>
			</td>
			
											<td>
	<div class="btn-group">
		<button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">操作
			<span class="caret"></span>
		</button>
		<ul role="menu" class="dropdown-menu">
			<li><a href="javascript:;"  onclick="editHandan('<?=$handan[hid]?>','<?=$handan[status]?>','<?=$handan[message]?>')"><i class="entypo-edit"></i>&nbsp;&nbsp;修改</a></li>
			<li><a href="/sys/delete.php?table=handan&field=hid&id=<?=$handan[hid]?>"><i class="entypo-cancel-circled"></i>&nbsp;&nbsp;删除</a></li>
		</ul>
	</div>
											</td>
                                        </tr>
	<?php } ?>
                                    </tbody>
                                </table>

 <ul class="pagination">
     <?php
	 if($_GET[uid]){
	 $sql2="select *  from handan t1 where 1=1 and t1.status!=1 and uid='$_GET[uid]' $sqlxs ";
	 }else{
	  $sql2="select *  from handan t1 where 1=1 and t1.status!=1 $sqlxs ";
	 }
$num=$res->fn_num($sql2);
$ye=(int)($num/$pagesize+1);
?>
 <li> <a href="am_handan_daishenhe.php?page=1<?=$aplus?>" >首页</a></li>

<?php
if($page<5){
	$iiikaishi=1;
	if($ye-$page>5){
		$iiijishu=5;
	}else{
			$iiijishu=$ye;
	}
}elseif($ye-$page<5){
	$iiikaishi=$ye-5;
	$iiijishu=$ye;
}else{
	$iiikaishi=$page-2;
	$iiijishu=$page+2;
}

for($iii=$iiikaishi;$iii<=$iiijishu;$iii++){
?>
 <li  <?php if($page==$iii){echo 'class="active"';}?>> <a href="am_handan_daishenhe.php?page=<?=$iii?><?=$aplus?>" ><?=$iii?></a></li>
<?php
}
?>
<li >  <a href="am_handan_daishenhe.php?page=<?=$ye?><?=$aplus?>" >尾页</a></li>
           </ul>
		   
                            </div>

                        </div>


                    </div>


                </div>
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->
<!-- Modal2 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="height:500px;overflow:auto;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
				<div class="form-horizontal" >
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">审核状态：</label>
						<div class="col-lg-10">
							<select name="idstatus" id="idstatus" class="form-control round-input" onchange="if(this.value=='-1') $('#bohui').show();else $('#bohui').hide();">
									<option value="0">未审核</option>
									<option value="1">已审核</option>
									<option value="-1">驳回</option>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="bohui" style="display:none">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">驳回原因：</label>
						<div class="col-lg-10">
							<input type="text" name="message" id="message" class="form-control round-input" value="">
						</div>
					</div>
				</div>
			
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-primary"  id="submit-btn">提交修改</button>
      </div>
    </div>
  </div>
</div>
	
    <script type="text/javascript" src="assets/js/jquery.js"></script>

    <!-- GAGE -->


   <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
		 <script type="text/javascript" src="/js/layer/layer.js"></script>
   <!-- /MAIN EFFECT -->
    <script type="text/javascript" src="assets/js/stackable/stacktable.js"></script>
	   <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.js"></script>
	 <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
	
<script>
var hid;
function editHandan(id,status,message){
	hid=id;
	$("#myModal").modal('show');	
	$("#idstatus").val(status).trigger('onchange');
	$("#message").val(message);
}
$("#submit-btn").click(function(){
		
		var status=$("#idstatus").val();
		var message=$("#message").val();
	
		
		if(!message && status==-1){
			    layer.tips('驳回原因不能为空!', '#message');
				return false;
		}
		
		
		$.post("action.php?type=addhandan",{hid:hid,status:status,message:message},function(data,status){
			if(data=="success"){
				alert("提交成功");
				window.location.reload();
			}else{
				alert("提交失败");
			}
		
		});
	 
		
	});
	
</script>

<?php include_once 'foot.php'; ?>
</body>

</html>
