<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css?123">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="assets/js/progress-bar/number-pb.css">
  <link rel="stylesheet" type="text/css" href="assets/js/parsley/src/parsley.css">
<link href="assets/js/bootstrap-datepicker/css/bootstrap-datetimepicker.css" rel="stylesheet" />
    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>订单管理</span>
                        </h2>

                    </div>

                  
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">订单管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">新增订单</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
			
				  <!--  新增会员-->
		
                    <div class="col-sm-12">
                        <div class="nest" id="elementClose">
                            <div class="title-alt">
                                <h6>新增订单</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#elementClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#element">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="element">

		 <?php
$danzi=$res->fn_select("select * from danzi where did ='$_GET[did]'");
?>
		<div class="panel-body">
			<form action="action.php?type=adddingdan&did=<?=$_GET[did]?>" method="post" class="demo-form form-horizontal bucket-form" data-parsley-validate >
				<div class="form-group">
					<label class="col-sm-1 control-label">MT4:</label>
					<div class="col-sm-3">
						<input type="text" name="mt4" id="mt4" class="form-control" data-parsley-required="true" data-parsley-trigger="focusout" value="<?=$danzi[mt4]?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label">单号:</label>
					<div class="col-sm-3">
						<input type="text" name="Ticket" id="Ticket" class="form-control" data-parsley-required="true" data-parsley-trigger="focusout" value="<?=$danzi[Ticket]?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label">商品:</label>
					<div class="col-sm-3">
						<input type="text" name="Symbol" id="Symbol" class="form-control" data-parsley-required="true" data-parsley-trigger="focusout" value="<?=$danzi[Symbol]?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label">操作:</label>
					<div class="col-sm-3">
						<select name="Operation" id="Operation" class="form-control round-input" >
							<option value="0" <?php if($danzi[Operation]==0)echo "selected='selected'";?>>买入</option>
							<option value="1" <?php if($danzi[Operation]==1)echo "selected='selected'";?>>卖出</option>
							
						</select>
					</div>
				</div>
		
				<div class="form-group">
					<label class="col-sm-1 control-label">开仓价:</label>
					<div class="col-sm-3">
						<input type="text" name="OpenPrice" id="OpenPrice" class="form-control" data-parsley-required="true" data-parsley-trigger="focusout" value="<?=$danzi[OpenPrice]?>">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-1 control-label">平仓价:</label>
					<div class="col-sm-3">
						<input type="text" name="ClosePrice" id="ClosePrice" class="form-control" data-parsley-required="true" data-parsley-trigger="focusout" value="<?=$danzi[ClosePrice]?>">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-1 control-label">手数:</label>
					<div class="col-sm-3">
						<input type="text" name="Lots" id="Lots" class="form-control" data-parsley-required="true" data-parsley-trigger="focusout" value="<?=$danzi[Lots]?>">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-1 control-label">盈利:</label>
					<div class="col-sm-3">
						<input type="text" name="Profit" id="Profit" class="form-control" data-parsley-required="true" data-parsley-trigger="focusout" value="<?=$danzi[Profit]?>">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-1 control-label">开仓时间</label>
					<div class="col-sm-3">
						<input type="text" name="OpenTime" id="OpenTime" class="form-control round-input" value="<?=date('Y-m-d H:i',$danzi[OpenTime]?$danzi[OpenTime]:time())?>">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-1 control-label">平仓时间</label>
					<div class="col-sm-3">
						<input type="text" name="CloseTime" id="CloseTime" class="form-control round-input" value="<?=date('Y-m-d H:i',$danzi[CloseTime]?$danzi[CloseTime]:time())?>">
					</div>
				</div>
				
				<div class="col-lg-offset-1 col-lg-10">
					<button class="btn btn-info" type="submit">提交</button>
				</div>
			</form>
		</div>

                            </div>

							</div>
						</div>
				   <!-- end 新增会员-->
				 
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->

	
	<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

    <!-- RIGHT SLIDER CONTENT -->
   

    <!-- END OF RIGHT SLIDER CONTENT-->
    <script type="text/javascript" src="assets/js/jquery.js"></script>
    <script src="assets/js/progress-bar/src/jquery.velocity.min.js"></script>
    <script src="assets/js/progress-bar/number-pb.js"></script>
    <script src="assets/js/progress-bar/progress-app.js"></script>



    <!-- MAIN EFFECT -->
    <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>




    <!-- GAGE -->

<script type="text/javascript" src="assets/js/bootstrap-datepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-datepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
	
<script type="text/javascript" src="assets/js/parsley/dist/parsley.js"></script>
<script type="text/javascript" src="assets/js/parsley/dist/i18n/zh_cn.js"></script>



 <script src="assets/js/jhere-custom.js"></script>
<script>
	$('#OpenTime').datetimepicker({
				format: 'yyyy-mm-dd hh:ii',
				language:'zh-CN'
	});
	
	$('#CloseTime').datetimepicker({
				format: 'yyyy-mm-dd hh:ii',
				language:'zh-CN'
	});
  $('.demo-form').parsley().on('form:submit', function (formInstance) {

	var options = {
		url: 'action.php?type=adddingdan&did=<?=$danzi[did]?>',
		type: 'post',
		data: $(".demo-form").serialize(),
		success: function (data) {
			if(data=="success"){
				$('#myModal').modal('show')
				$('#myModal .modal-body').html('提交成功'); 
				$('#myModal').on('hidden.bs.modal', function (e) {
					<?php if($_GET[did]){ ?>
					setTimeout(function(){window.history.back()},1000);
					<?php }else{ ?>
					setTimeout(function(){window.location.reload()},1000);
					<?php } ?>
				})
				
			}else if(data=="invalidate_danzi"){
				
				$('#myModal').modal('show')
				$('#myModal .modal-body').html('单号已存在'); 
			
			}
		}
     };
	$.ajax(options);
	return false;
  });
</script>

<?php include_once 'foot.php'; ?>
</body>

</html>
