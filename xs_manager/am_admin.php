<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

	
     <link href="assets/js/stackable/stacktable.css" rel="stylesheet">
    <link href="assets/js/stackable/responsive-table.css" rel="stylesheet">
<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />


    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>系统管理</span>
                        </h2>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">系统管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">管理员设置</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
				<div class="row">
				
					<div class="body-nest" id="inlineClose">
					 <div class="panel-body">
				 <form role="form" class="form-horizontal">
					<input type="hidden" name="aid" id="aid" value=""/>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label">用户名：</label>
						<div class="col-lg-4">
							<input type="text" placeholder="用户名" id="username" name="username" class="form-control">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label">密  码：</label>
						<div class="col-lg-4">
							<input type="text" placeholder="密码" id="password" name="password" class="form-control" onfocus="this.type=password">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label">权限：</label>
						<div class="col-lg-10">
		<input type="checkbox" name="quanxian" value="会员管理" />    会员管理&nbsp;&nbsp;
	
		<input type="checkbox" name="quanxian" value="MT4管理"  />    MT4管理&nbsp;&nbsp;

		<input type="checkbox" name="quanxian" value="平台服务器管理"  />    平台服务器管理&nbsp;&nbsp;
		
		<input type="checkbox" name="quanxian" value="订单管理"  />    订单管理&nbsp;&nbsp;
		<input type="checkbox" name="quanxian" value="财务管理"  />    财务管理&nbsp;&nbsp;
		<input type="checkbox" name="quanxian" value="会员日志管理"  />    会员日志管理&nbsp;&nbsp;
		<input type="checkbox" name="quanxian" value="出金管理" />    出金管理&nbsp;&nbsp;
		<input type="checkbox" name="quanxian" value="入金管理" />    入金管理&nbsp;&nbsp;
		<input type="checkbox" name="quanxian" value="奖金管理" />    奖金管理&nbsp;&nbsp;
			<input type="checkbox" name="quanxian" value="转账管理" />    转账管理&nbsp;&nbsp;
		<input type="checkbox" name="quanxian" value="代理商专区" />    代理商专区&nbsp;&nbsp;
		<input type="checkbox" name="quanxian" value="分润管理" />    分润管理&nbsp;&nbsp;
		<input type="checkbox" name="quanxian" value="同步数据管理" />    同步数据管理&nbsp;&nbsp;
		
		<input type="checkbox" name="quanxian" value="单页管理"  />    单页管理&nbsp;&nbsp;
		<input type="checkbox" name="quanxian" value="文章管理" />    文章管理&nbsp;&nbsp;
		<input type="checkbox" name="quanxian" value="幻灯片管理" />    幻灯片管理&nbsp;&nbsp;
		<input type="checkbox" name="quanxian" value="系统设置" />    系统设置&nbsp;&nbsp;
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" >管理员分类：</label>
						
						<div class="col-lg-4">
							<select class="form-control" name="admingroup" id="admingroup">
								<option value='超级管理员'>超级管理员</option>
								<option value="信号源管理员" >信号源管理员</option>
							</select>
						</div>
					</div> 
				</form>
						<div class="col-lg-offset-2 ">
							<button class="btn btn-info" id="save_btn">提交</button>
						</div>
              
						</div>
					</div>

					
                   <div class="col-sm-12">

                        <div class="nest" id="StackableClose">
                            <div class="title-alt">
                                <h6>管理员设置</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#tStackableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
								    <a type="button" class="btn btn-primary" id="add_btn" >
										<span class="entypo-plus-squared"></span>&nbsp;&nbsp;添加账号
									</a>
                                    <a class="nav-toggle-alt" href="#StackableStatic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="StackableStatic">

                                <table id="responsive-example-table" class="table large-only">
                                    <tbody>
                                        <tr class="text-right">
									
                                            <th style="width:5%;">ID</th>
                                            <th style="width:10%;">账号</th>
                                            <th style="width:20%;">密码</th>
											 <th style="width:35%;">权限</th>
                                            <th style="width:15%;">管理员分类</th>
											<th style="width:15%;">操作</th>
                                        </tr>
										
<?php 
$pagesize=10;

$sql="select *  from admin ";

	$admins =$res->fn_rows($sql);
	foreach($admins as $admin){
?>	
                                        <tr>
                                            <td><?=$admin[aid]?></td>
                                            <td><?=$admin[username]?></td>
                                            <td><?=md5($admin[password])?></td>
											<td><?=$admin[quanxian]?></td>
											<td><?=$admin[admingroup]?></td>
											<td>
											<div class="btn-group">
											    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">操作
													<span class="caret"></span>
												</button>
											    <ul role="menu" class="dropdown-menu">
													<li><a onclick="editManager('<?=$admin[aid]?>','<?=$admin[username]?>','<?=$admin[password]?>','<?=$admin[quanxian]?>')"><i class="icon-document-edit"></i>&nbsp;&nbsp;修改资料</a></li>
													<li><a href="/sys/delete.php?table=admin&field=aid&id=<?=$admin[aid]?>"><i class="entypo-cancel-circled"></i>&nbsp;&nbsp;删除用户</a></li>
												</ul>
											</div>
											</td>
                                        </tr>
	<?php } ?>
                                    </tbody>
                                </table>


		   
                            </div>

                        </div>


                    </div>


                </div>
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->
<?php print_r($_GET);?>


	<!-- Modal2 -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

    <script type="text/javascript" src="assets/js/jquery.js"></script>

    <!-- GAGE -->


   <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
   <!-- /MAIN EFFECT -->
    <script type="text/javascript" src="assets/js/stackable/stacktable.js"></script>

<script>
	$("#add_btn").click(function(){
	
		$('#myModal1').modal('show')

	});
	
	    function editManager(aid,username,password,quanxian)
       {
		
           $("#aid").val(aid);
           $("#username").val(username);
		   
		   var arr=quanxian.split(',');
		   for(i in arr){
			  $("input[name='quanxian'][value='"+arr[i]+"']").click();
		   }
		  
           $("#password").val(password);
	
       }
	   
	   
  $("#save_btn").click(function () {
			  
			   var aid = $("#aid").val();
               var username = $("#username").val();
               var password = $("#password").val();
			  var admingroup = $("#admingroup").val();
				
			   var arr=new Array();
			   $("input[name='quanxian']").each(function(){
					console.log(this);
					console.log($(this).prop('checked'));
					if($(this).prop('checked')){
						arr.push($(this).val());
					}
			   });
			   var quanxian=arr.join(',');
			console.log(quanxian);
             if(username != "" && password != "")
               {
                   $.ajax({
                       type: "post",
                       url: "action.php?type=addadmin",
                       data: {aid:aid,username:username,password:password,quanxian:quanxian,admingroup:admingroup},
                     
                       success: function (data) {
                           if (data=="success") {
                             	$('#myModal2').modal('show')
								$('#myModal2 .modal-body').html('提交成功'); 
								$('#myModal2').on('hidden.bs.modal', function (e) {
									setTimeout(function(){window.location.reload()},3000);
								})
                           }
                           else {
                               $("#myModal2").modal('show');
                               $("#myModal2 .modal-body").html("账号已存在，请重新输入！");
                           }
                       },
                       error: function (err) {
                           $("#myModal2").modal('show');
                           $("#myModal2 .modal-body").html("服务器内部错误！");
                       }
                   });
               }
           });
</script>

<?php include_once 'foot.php'; ?>
</body>

</html>
