<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->

    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

	
     <link href="assets/js/stackable/stacktable.css" rel="stylesheet">
    <link href="assets/js/stackable/responsive-table.css" rel="stylesheet">
<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/tree/jquery.treeview.css">
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>

    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>会员管理</span>
                        </h2>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">会员管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">待审核会员列表</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
				<div class="row">
				
					    <div class="body-nest" id="inlineClose">
                               <div class="form_center" style="width:100%">
                                    <form role="form" class="form-inline " method="get">
									
										<div class="form-group">
									
											 <select class="form-control" name="pagesize">
												<option value="10" <?php if($_GET['pagesize']=='10'){ echo 'selected="selected"'; } ?>>每页10条</option>
												<option value="30" <?php if($_GET['pagesize']=='30'){ echo 'selected="selected"'; } ?>>每页30条</option>
												<option value="50" <?php if($_GET['pagesize']=='50'){ echo 'selected="selected"'; } ?>>每页50条</option>
												<option value="100"  <?php if($_GET['pagesize']=='100'){ echo 'selected="selected"'; } ?>>每页100条</option>
											 </select>
												 <select class="form-control" name="groupid">
													<option value="">所有用户组</option>
					<?php 
					$groups=$res->fn_rows("select * from user_group ") ;
					foreach($groups as $group){ 
					?>
						<option value="<?=$group[gid]?>" <?php if($_GET['groupid']==$group[gid]){ echo 'selected="selected"'; } ?>><?=$group[gname]?></option>
					<?php }　?>
												 </select>
										
										
									
												 <input type="text" class="form-control" placeholder="账号/手机号" size="12" name="search">
									
												<input type="text" placeholder="申请时间" class="form-control" id="reservation" name="reservation" style="width:320px" value="<?=$_GET[reservation]?>" autocomplete="off"> 
												
												<button type="submit" class="btn btn-primary">搜索</button>
										</div>
											
										
                                    </form>
                                </div>

                        </div>
							
                   <div class="col-sm-12">

                        <div class="nest" id="StackableClose">
                            <div class="title-alt">
                                <h6>待审核会员列表</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#tStackableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
								    <a type="button" class="btn btn-primary" id="add_btn" href="adduser.php">
										<span class="entypo-plus-squared"></span>&nbsp;&nbsp;添加账号
									</a>
                                    <a class="nav-toggle-alt" href="#StackableStatic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="StackableStatic">

                                <table id="responsive-example-table" class="table large-only">
                                    <tbody>
                                        <tr class="text-right">
									
                                            <th style="width:5%;">ID</th>
                                            <th style="width:5%;">昵称</th>
											<th style="width:10%;">MT4账号</th>
											<th style="width:5%;">上家</th>
                                            <th style="width:5%;">用户组</th>
											<th style="width:10%;">直推人数</th>
											<th style="width:10%;">推荐关系</th>
											<th style="width:10%;">手机号</th>
											<th style="width:10%;">Email</th>
											<th style="width:10%;">身份证</th>
                                            <th style="width:10%;">注册时间</th>
											<th style="width:10%;">操作</th>
                                        </tr>
										
<?php 
$pagesize=10;
if($_GET[pagesize]!= null){
	$pagesize=$_GET[pagesize];
	$aplus="&pagesize=$pagesize";
}

if($_GET[page]){
	$page=$_GET[page];
}else{
	$page=1;
}
$qian=($page-1)*$pagesize;

if($_GET[groupid]!= null){
	$sqlxs.=" and t1.groupid = '$_GET[groupid]'";
	
	$aplus.="&groupid=$_GET[groupid]";
}

if($_GET[search]!= null){
	$sqlxs.=" and ( t1.nickname like '%$_GET[search]%' || t1.mobile like '%$_GET[search]%' )";
	$aplus.="&search=$_GET[search]";
}

if($_GET['reservation']!= null){
		$reservation=explode("~",$_GET['reservation']);
		$start_time=strtotime($reservation[0]);
		$end_time=strtotime($reservation[1]);
		$sqlxs.=" and t1.regtime>'$start_time' and t1.regtime<'$end_time'";
		$aplus.="&reservation=$_GET[reservation]";
}

   if($_GET[tmid]){
$sql="select t1.uid,t1.nickname,t1.mobile,t1.email,t1.money,t2.equity,t1.regtime,t1.timelimit,t2.mt4,t2.password mt4pwd ,t2.port,t2.status,t2.listenstatus,t3.gname,t4.nickname tuijianname , t5.num,t6.id idid,t6.idstatus from users t1 left join (select group_concat(mt4) mt4,password,group_concat(port) port,uid,status,listenstatus,svid,money,equity from mt4 group by uid ) t2 on t1.uid=t2.uid left join user_group t3 on t1.groupid=t3.gid left join users t4 on t1.tuid=t4.uid left join (select count(*) num,tuid  from users group by tuid) t5 on t1.uid=t5.tuid left join idcard t6 on t1.uid=t6.uid where 1=1 and t1.groupid!=10  and t6.idstatus!=1 and t1.tuid='$_GET[tmid]' $sqlxs order by t1.uid desc limit $qian,$pagesize";
	}else if($_GET[mid]){
$sql="select t1.uid,t1.nickname,t1.mobile,t1.email,t1.money,t2.equity,t1.regtime,t1.timelimit,t2.mt4,t2.password mt4pwd ,t2.port,t2.status,t2.listenstatus,t3.gname,t4.nickname tuijianname,t5.num,t6.id idid,t6.idstatus from users t1 left join (select group_concat(mt4) mt4,password,group_concat(port) port,uid,status,listenstatus,svid,money,equity from mt4 group by uid ) t2 on t1.uid=t2.uid left join user_group t3 on t1.groupid=t3.gid left join users t4 on t1.tuid=t4.uid left join (select count(*) num,tuid  from users group by tuid) t5 on t1.uid=t5.tuid left join idcard t6 on t1.uid=t6.uid where 1=1 and t1.groupid!=10  and t6.idstatus!=1 and t1.uid='$_GET[mid]' $sqlxs order by t1.uid desc limit $qian,$pagesize";
	}else{
$sql="select t1.uid,t1.nickname,t1.mobile,t1.email,t1.money,t2.equity,t1.regtime,t1.timelimit,t2.mt4,t2.password mt4pwd ,t2.port,t2.status,t2.listenstatus,t3.gname,t4.nickname tuijianname , t5.num,t6.id idid,t6.idstatus from users t1 left join (select group_concat(mt4) mt4,password,group_concat(port) port,uid,status,listenstatus,svid,money,equity from mt4 group by uid ) t2 on t1.uid=t2.uid left join user_group t3 on t1.groupid=t3.gid left join users t4 on t1.tuid=t4.uid left join (select count(*) num,tuid  from users group by tuid) t5 on t1.uid=t5.tuid left join idcard t6 on t1.uid=t6.uid where 1=1 and t1.groupid!=10  and t6.idstatus!=1 $sqlxs order by t1.uid desc limit $qian,$pagesize";
	 }
	 global $count;
		$count=0;
		function tuijianCount($xianxians){
			global $count;
			$count+=count($xianxians);
			foreach($xianxians as $xiaxian){
				tuijianCount($xiaxian[xiaxian]);
			}
		}
	$users =$res->fn_rows($sql);
	foreach($users as $user){
	//	$xianxians=$res->getXiaxian($user[uid]);
	//	$count=0;
	//tuijianCount($xianxians)
?>	
		<tr>
			<td><?=$user[uid]?></td>
			<td><?=$user[nickname]?></td>
			<td><?=$user[mt4]?></td>
			<td><?=$user[tuijianname]?'<a href="am_users.php?mid='.$user[uid].'">'.$user[tuijianname]."</a>":'无'?></td>
			<td><?=$user[gname]?></td>
			<td><?php
			if($user[num]){
				echo '<a href="am_users.php?tmid='.$user[uid].'">'.$user[num]."</a>";
			}else{
				echo 0;
			}?></td>
			<td>
			<?php if($user[num]){ ?>
			<a type="button" class="tuijiantree " rel="<?=$user[uid]?>">点击查看</a>
			<?php }else{ ?>
				无
			<?php } ?>
			</td>
			<td><?=$user[mobile]?></td>
			<td><?=$user[email]?></td>
			<td>
			<?php if(!$user[idid]){ ?>
				<a type="button"  class="btn btn-default idcard" rel="<?=$user[uid]?>">未上传</a>
			<?php }else if($user[idstatus]==0){ ?>
				<a type="button"  class="btn btn-warning idcard" rel="<?=$user[uid]?>">待审核</a>
			<?php }else if($user[idstatus]==-1){　?>
				<a type="button"  class="btn btn-danger idcard" rel="<?=$user[uid]?>">已驳回</a>
			<?php }else { ?>
				<a type="button"  class="btn btn-primary idcard" rel="<?=$user[uid]?>">已审核</a>
			<?php } ?>
			</td>
		
			<td><?=date('Y-m-d H:i:s',$user[regtime])?></td>
		
			<td>
			<div class="btn-group">
				<button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">操作
					<span class="caret"></span>
				</button>
			<ul role="menu" class="dropdown-menu">
				<li><a href="adduser.php?mid=<?=$user[uid]?>"><i class="icon-document-edit"></i>&nbsp;&nbsp;修改资料</a></li>
				<li><a href="addmt4.php?mid=<?=$user[uid]?>"><i class="entypo-user-add"></i>&nbsp;&nbsp;添加MT4</a></li>
				<li><a href="am_mt4.php?mid=<?=$user[uid]?>"><i class="entypo-briefcase"></i>&nbsp;&nbsp;查看MT4</a></li>
				<li><a href="am_dingdan.php?mt4=<?=$user[mt4]?>"><i class="entypo-list"></i>&nbsp;&nbsp;查看订单</a></li>
				<li><a href="javascript:;" onclick="chiCang('<?=$user[mt4]?>')"><i class="entypo-list"></i>&nbsp;&nbsp;当前持仓</a></li>
				<li><a href="am_caiwu.php?uid=<?=$user[uid]?>"><i class="entypo-basket"></i>&nbsp;&nbsp;查看财务</a></li>
				<li><a href="addbonus.php?uid=<?=$user[uid]?>"><i class="fontawesome-money"></i>&nbsp;&nbsp;添加奖金</a></li>
				<li><a href="javascript:;" onclick="mt4Delete('<?=$user[mt4]?>','<?=$user[port]?>','<?=$user[uid]?>')"><i class="entypo-cancel-circled"></i>&nbsp;&nbsp;删除用户</a></li>
			</ul>
											</div>
											</td>
                                        </tr>
	<?php } ?>
                                    </tbody>
                                </table>

 <ul class="pagination">
     <?php
	   if($_GET[tmid]){
	 $sql2="select t1.*,t2.gname from users t1 left join user_group t2 on t1.groupid=t2.gid left join idcard t6 on t1.uid=t6.uid where 1=1 and groupid!=10 and tuid='$_GET[tmid]' and t6.idstatus!=1 $sqlxs  order by uid desc ";
	 }else if($_GET[mid]){
	 $sql2="select t1.*,t2.gname from users t1 left join user_group t2 on t1.groupid=t2.gid left join idcard t6 on t1.uid=t6.uid where 1=1 and groupid!=10 and uid='$_GET[mid]' and t6.idstatus!=1 $sqlxs  order by uid desc ";
	 }else{
	  $sql2="select t1.*,t2.gname from users t1 left join user_group t2 on t1.groupid=t2.gid left join idcard t6 on t1.uid=t6.uid where 1=1 and groupid!=10  and t6.idstatus!=1 $sqlxs  order by uid desc ";
	 }
$num=$res->fn_num($sql2);
$ye=(int)($num/$pagesize+1);
?>
 <li> <a href="am_users_daishenhe.php?page=1<?=$aplus?>" >首页</a></li>

<?php
if($page<5){
	$iiikaishi=1;
	if($ye-$page>5){
		$iiijishu=5;
	}else{
			$iiijishu=$ye;
	}
}elseif($ye-$page<5){
	$iiikaishi=$ye-5;
	$iiijishu=$ye;
}else{
	$iiikaishi=$page-2;
	$iiijishu=$page+2;
}

for($iii=$iiikaishi;$iii<=$iiijishu;$iii++){
?>
 <li  <?php if($page==$iii){echo 'class="active"';}?>> <a href="am_users_daishenhe.php?page=<?=$iii?><?=$aplus?>" ><?=$iii?></a></li>
<?php
}
?>
<li >  <a href="am_users_daishenhe.php?page=<?=$ye?><?=$aplus?>" >尾页</a></li>
           </ul>
		   
                            </div>

                        </div>


                    </div>


                </div>
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->
	
		<!-- Modal1 -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
	<!-- Modal2 -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="height:500px;overflow:auto;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
			<input type="hidden" name="uid" id="uid" value=""/>
				<div class="form-horizontal" >
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">真实姓名：</label>
						<div class="col-lg-10">
							<input type="text" name="realname" id="realname" class="form-control round-input" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">身份证号：</label>
						<div class="col-lg-10">
							<input type="text" name="idnum" id="idnum" class="form-control round-input" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">身份正面：</label>
						<div class="col-lg-10">
							<input type="hidden" id="idcard1_input">
							<form id="idcard1_form" action="action.php?type=idcard1" method="post" enctype="multipart/form-data" target="e">
								<input type="file" name="file" id="idcard1_file" onchange="$('#idcard1_form').submit()" style="width: 164px;">
							</form>
							<img id="idcard1" style="width:90%;display:none"  />
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">身份背面：</label>
						<div class="col-lg-10">
							<input type="hidden" id="idcard2_input">
							<form id="idcard2_form" action="action.php?type=idcard2" method="post" enctype="multipart/form-data" target="e">
								<input type="file" name="file"  id="idcard2_file"  onchange="$('#idcard2_form').submit()" style="width: 164px;">
							</form>
							
							<img id="idcard2" style="width:90%;display:none" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">审核状态：</label>
						<div class="col-lg-10">
							<select name="idstatus" id="idstatus" class="form-control round-input" onchange="if(this.value=='-1') $('#bohui').show();else $('#bohui').hide();">
									<option value="0">未审核</option>
									<option value="1">已审核</option>
									<option value="-1">驳回</option>
							</select>
						</div>
					</div>
					<div class="form-group" id="bohui" style="display:none">
						<label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">驳回原因：</label>
						<div class="col-lg-10">
							<input type="text" name="message" id="message" class="form-control round-input" value="">
						</div>
					</div>
				</div>
				<iframe name="e" style="display:none"></iframe>
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-primary"  id="submit-btn">提交修改</button>
      </div>
    </div>
  </div>
</div>
	
	<!-- Modal13 -->
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
			<div class="form-horizontal" >
					<input type="hidden" name="tid" id="tid" class="form-control round-input" value="" readonly>
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">MT4账号：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="mt4" id="mt4" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">MT4密码：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="mt4pwd" id="mt4pwd" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4 col-sm-4 control-label" for="inputPassword1">MT4邮箱：</label>
						<div class="col-lg-6 input-group">
							<input type="text" name="mt4mail" id="mt4mail" class="form-control round-input" value="" readonly>
						</div>
					</div>
					
					
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="email-btn">发送邮件</button>
      </div>
    </div>
  </div>
</div>
	
    <script type="text/javascript" src="assets/js/jquery.js"></script>

    <!-- GAGE -->


   <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
   <!-- /MAIN EFFECT -->
<script type="text/javascript" src="assets/js/stackable/stacktable.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>
<script src="assets/js/tree/lib/jquery.cookie.js" type="text/javascript"></script>
<script src="assets/js/tree/jquery.treeview.js" type="text/javascript"></script>
	  <!--socoket start-->
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/web_socket.js"></script>
<script type="text/javascript" src="/js/json.js"></script>
<script type="text/javascript" src="/static/websocket_config.js"  media="all"></script>


<script>
	 function init() {
		ws = new WebSocket("ws://"+WS_HOST2+":"+WS_PORT2);
		ws.onopen = function() {
    	    timeid && window.clearTimeout(timeid);
		   	console.log("连接成功");

      };
		if(reconnect>6){
			window.clearTimeout(timeid);
		}
		ws.onmessage = function(e) {
				console.log("收到数据："+e.data);
				var data = JSON.parse(e.data);
				switch(data['type']){
				   
					case 'current':
						
						var html='<div style=" height: 400px;overflow: auto;">';
						html+='<table id="responsive-example-table" class="table large-only">'
                                +'     <tbody>'
                                +'         <tr class="text-right">'
								+'			<th style="width:10%;">单号</th>'
								+'			<th style="width:10%;">MT4</th>'
								+'			<th style="width:10%;">商品</th>'
								+'			<th style="width:10%;">操作</th>'
								+'			<th style="width:10%;">开仓价</th>'
								+'			<th style="width:10%;">止损价/止盈价</th>'
								+'			<th style="width:10%;">手数</th>'
								+'			<th style="width:10%;">开仓时间</th>'
								+'			<th style="width:10%;">获利</th>'
                                +'        </tr>';	
						for(i in data['data']){ 
							var order=data['data'][i];
							html+='       <tr class="text-right">'
								+'			<th style="width:10%;">'+order.order+'</th>'
								+'			<th style="width:10%;">'+order.login+'</th>'
								+'			<th style="width:10%;">'+order.symbol+'</th>'
								+'			<th style="width:10%;">'+(order.cmd==0?'买':'卖')+'</th>'
								+'			<th style="width:10%;">'+order.openprice+'</th>'
								+'			<th style="width:10%;">'+order.sl+'/'+order.tp+'</th>'
								+'			<th style="width:10%;">'+order.volume+'</th>'
								+'			<th style="width:10%;">'+(new Date(parseInt(order.opentime) * 1000).toLocaleString().replace('上午','').replace('下午',''))+'</th>'
								+'			<th style="width:10%;">'+order.profit.toFixed(2)+'</th>'
                                +'        </tr>';	
						}
						html+='</table></div>';
						
						html+='</ul></div>';
					
						$("#myModal1").modal('show');
		
						$("#myModal1 .modal-body").html(html);
					break;
					case 'adduser':
						$('#myModal2').modal('hide');
						if(data['code']=200){
							var uinfo=data['data'];
							$.post("action.php?type=addmt4",{uid:data['beizhu'],mt4:uinfo.key,mt4pwd:uinfo.password,svid:124},function(data,status){
								if(data=="success"){
									$("#mt4").val(uinfo.key);
									$("#mt4pwd").val(uinfo.password);
									$("#mt4mail").val(uinfo.email);
									$('#myModal3').modal('show');
								}
							});
						}else{
							$('#myModal1').modal('show');
							$('#myModal1 .modal-body').html('申请失败'); 
						}
						
					break;
				}
		
		};
	  ws.onclose = function() {
		  reconnect ++;
    	  console.log("连接关闭，定时重连");
    	  timeid = window.setTimeout(init, 3000);
      };
      ws.onerror = function() {
			reconnect ++;
    	    console.log("出现错误");
		    timeid = window.setTimeout(init, 3000);
      };
	}
	   init();
			$('#reservation').daterangepicker({
							
					timePicker: true,
					timePicker12Hour : false, //是否使用12小时制来显示时间  
					maxDate: '<?=date('Y-m-d H:i')?>', 
					format: 'YYYY-MM-DD HH:mm',
					separator:'~',
					 ranges : {  
						//'最近1小时': [moment().subtract('hours',1), moment()],  
						'今日': [moment().startOf('day'), moment()],  
						'昨日': [moment().subtract('days', 1).startOf('day'), moment().subtract('days', 1).endOf('day')],  
						'最近7日': [moment().subtract('days', 6).startOf('day'), moment()],  
					
					},
					locale : {  
						applyLabel : '确定',  
						cancelLabel : '取消',  
						fromLabel : '起始时间',  
						toLabel : '结束时间',  
						customRangeLabel : '自定义',  
						daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],  
						monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',  
								'七月', '八月', '九月', '十月', '十一月', '十二月' ],  
						firstDay : 1  
					}  
				},
				function(start, end, label) {
					console.log(start.toISOString(), end.toISOString(), label);
				});
				
	function mt4Delete(mt4,mt4port,uid){
		
		if(!window.confirm('确认删除操作吗？')){ return false;}

		ports=mt4port.split(","); //字符分割 
		for (i=0;i<ports.length ;i++ ) 
		{ 

			var data=JSON.stringify({"type":"loginout","port":ports[i]});
			console.log(data);
			ws.send(data);
		} 
			console.log(mt4);
		$.get("action.php?type=deluser",{uid:uid,mt4:mt4},function(data,status){
			window.location.reload();
		});
	}
	
	$(".idcard").click(function(){
		var rel=$(this).attr('rel');
		$("#uid").val(rel);
		$.get("action.php?type=getidcard",{uid:rel},function(data,status){
			
			$("#realname").val(data.realname);
			$("#idnum").val(data.idnum);
			$("#idcard1_input").val(data.idcard1);
			$("#idcard2_input").val(data.idcard2);
			$("#idstatus").val(data.idstatus).trigger('onchange');	
			$("#message").val(data.message);
			
			$("#idcard1").hide().prop('src','');
			$("#idcard2").hide().prop('src','');
			data.idcard1 && $("#idcard1").show().prop('src','/up/'+data.idcard1);
			data.idcard2 && $("#idcard2").show().prop('src','/up/'+data.idcard2);
			$("#myModal2").modal('show');	
		},'json');
	});
	
	$("#submit-btn").click(function(){
		
		var uid=$("#uid").val();
		var realname=$("#realname").val();
		var idnum=$("#idnum").val();
		var idstatus=$("#idstatus").val();
		var idcard1_input=$("#idcard1_input").val();
		var idcard2_input=$("#idcard2_input").val();
		var message=$("#message").val();
	
		if(!realname){
			    layer.tips('真实姓名不能为空!', '#realname');
				return false;
		}
		if(!idnum){
			    layer.tips('身份证号码不能为空!', '#idnum');
				return false;
		}
		if(!idcard1_input){
			    layer.tips('身份证正面不能为空!', '#idcard1_file');
				return false;
		}
		if(!idcard2_input){
			    layer.tips('身份证反面不能为空!', '#idcard2_file');
				return false;
		}
		if(!message && idstatus==-1){
			    layer.tips('驳回原因不能为空!', '#message');
				return false;
		}
		
		
		$.post("action.php?type=addidcard",{uid:uid,realname:realname,idnum:idnum,idcard1:idcard1_input,idcard2:idcard2_input,idstatus:idstatus,message:message},function(data,status){
			if(typeof data == 'object'){
				alert("提交成功");
				console.log('{"type":"adduser","name":"'+data.realname+'","email":"'+data.email+'","password":"'+data.mt4pwd+'","group":"demoforex","city":"suzhou","province":"jiangsu","country":"china","address":"东吴北路299号吴中大厦807","postcode":"215007","telephone":"'+data.mobile+'","leverage":400,"beizhu":"'+data.uid+'","token":"pong"}');
				ws.send('{"type":"adduser","name":"'+data.realname+'","email":"'+data.email+'","password":"'+data.mt4pwd+'","group":"demoforex","city":"suzhou","province":"jiangsu","country":"china","address":"东吴北路299号吴中大厦807","postcode":"215007","telephone":"'+data.mobile+'","leverage":400,"beizhu":"'+data.uid+'","token":"pong"}');
				
			//	window.location.reload();
				$('#myModal2').modal('hide');
				$('#myModal1').modal('show');
				$('#myModal1 .modal-body').html('MT4申请中。。。。'); 
			}else if(data == 'success'){
				$('#myModal2').modal('hide');
				$('#myModal1').modal('show');
				$('#myModal1 .modal-body').html('修改成功'); 
			}else {
				$('#myModal2').modal('hide');
				$('#myModal1').modal('show');
				$('#myModal1 .modal-body').html('提交失败'); 
			}
		
		},'json');
	 
		
	});
	
	$(".tuijiantree").click(function(){
		var rel=$(this).attr('rel');
		$("#uid").val(rel);
		$.get("/admin_user/action.php?type=gettuijian",{uid:rel},function(data,status){
			tree='<div style=" height: 400px;overflow: auto;">';
		    tree+='<ul id="browser" class="treeview" > <span class="entypo-users" rel="'+rel+'">我自己</span> <ul>';	
			for(i in data){
				var xiaxian=data[i];
				if(xiaxian.num){
					tree += '<li><span class="entypo-users" rel="'+xiaxian.uid+'" onclick="getTuijian(this)">'+xiaxian.nickname+'('+xiaxian.gname+')</span></li>';
				}else{
					tree += '<li><span>'+xiaxian.nickname+'('+xiaxian.gname+')</span></li>';
				}
			}
			tree+='</ul></ul></div>';
		
			$("#myModal1").modal('show');
			console.log(tree);
			$("#myModal1 .modal-body").html(tree);
			$("#browser").treeview({
				animated: "fast",
				collapsed: false,
				unique: true,
				persist: "cookie",
				toggle: function() {
					window.console && console.log("%o was toggled", this);
				}
			});
		},'json');
	});
	var tree='';	
	function domBuit(xianxians){
	    tree += "<ul>";
		for(i in xianxians){
			xiaxian=xianxians[i];
			if(xiaxian.xiaxian){
				tree += '<li><span class="entypo-home">'+xiaxian.nickname+'('+xiaxian.gname+')</span> ';
				domBuit(xiaxian.xiaxian);
			}else{
				tree += '<span>'+xiaxian.nickname+'('+xiaxian.gname+')</span> ';
			}
			
		
			tree += '</li>';
		}
		
		tree += "</ul>";
	}
	
   function  getTuijian(e){
	
		var uid=$(e).attr('rel');
		if($(e).parent().find(' > ul > li').length>0){
			return false;
		}
		var that=e;
		$.get("/admin_user/action.php?type=gettuijian",{uid:uid},function(data,status){

			if(typeof data=='object'){
				var  html='<ul>';
				for(i in data){
					var xiaxian=data[i];
					if(xiaxian.num){
						html += '<li><span class="entypo-users"  rel="'+xiaxian.uid+'" onclick="getTuijian(this)">'+xiaxian.nickname+'('+xiaxian.gname+')</span></li>';
					}else{
						html += '<li><span>'+xiaxian.nickname+'('+xiaxian.gname+')</span></li>';
					}
				}
				  html +='</ul>';
				$(that).parent().append(html);
				/*$("#browser").treeview({
					animated: "fast",
					collapsed: false,
					unique: true,
					persist: "cookie",
					toggle: function() {
						window.console && console.log("%o was toggled", that);
					}
				});*/
			}
		},'json');
		
	}
	var tree='';	
	function domBuit(xianxians){
	    tree += "<ul>";
		for(i in xianxians){
			xiaxian=xianxians[i];
			if(xiaxian.xiaxian){
				tree += '<li><span class="entypo-home">'+xiaxian.nickname+'('+xiaxian.gname+')</span> ';
				domBuit(xiaxian.xiaxian);
			}else{
				tree += '<span>'+xiaxian.nickname+'('+xiaxian.gname+')</span> ';
			}
			
		
			tree += '</li>';
		}
		
		tree += "</ul>";
	}
	
	function chiCang(key){
		ws.send('{"type":"current","key":"'+key+'","token":"pong"}');
	}
	
	$("#email-btn").click(function(){
		var mt4=$("#mt4").val();
		var mt4pwd=$("#mt4pwd").val();
		var email=$("#mt4mail").val();
		$.post("/mail/send_mt4.php",{mt4:mt4,mt4pwd:mt4pwd,email:email},function(data,status){
				$('#myModal3').modal('hide');
				$('#myModal1').modal('show');
				$('#myModal1 .modal-body').html('发送成功'); 
				$('#myModal1').on('hidden.bs.modal', function (e) {
					window.location.reload();
				});
		});
	});
</script>


<?php include_once 'foot.php'; ?>
</body>

</html>
