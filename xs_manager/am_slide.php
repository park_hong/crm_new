<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

	
     <link href="assets/js/stackable/stacktable.css" rel="stylesheet">
    <link href="assets/js/stackable/responsive-table.css" rel="stylesheet">
<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>

    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>幻灯片管理</span>
                        </h2>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">幻灯片管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">幻灯片列表</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
				<div class="row">
				
				<div class="body-nest" id="inlineClose">
				 <div class="panel-body">
				 <div role="form" class="form-horizontal">
					<input type="hidden" name="slid" id="slid" value=""/>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label">图片：</label>
						<div class="col-lg-4">
							<input type="hidden" id="image_input" name="image_input">
							<form id="image_form" action="action.php?type=slide_image" method="post" enctype="multipart/form-data" target="e">
								<input type="file" name="file" id="image_file" onchange="$('#image_form').submit()" style="width: 164px;">
							</form>
							<img id="image" style="width:210px;display:none"  />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label">连接：</label>
						<div class="col-lg-4">
							<input type="text" placeholder="连接" id="link" name="link" class="form-control" >
						</div>
					</div>
					
					
				</div>
				<iframe name="e" style="display:none"></iframe>
						<div class="col-lg-offset-2 ">
							<button class="btn btn-info" id="save_btn">提交</button>
						</div>
              
						</div>
					</div>

					
                   <div class="col-sm-12">

                        <div class="nest" id="StackableClose">
                            <div class="title-alt">
                                <h6>幻灯片管理</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#tStackableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                              

                            </div>

                            <div class="body-nest" id="StackableStatic">

                                <table id="responsive-example-table" class="table large-only">
                                    <tbody>
                                        <tr class="text-right">
									
                                            <th style="width:10%;">ID</th>
                                            <th style="width：40%;">图片</th>
											<th style="width：40%;">连接</th>
											<th style="width:10%;">操作</th>
                                        </tr>
										
<?php 
$pagesize=10;

$sql="select *  from slide ";

	$slides =$res->fn_rows($sql);
	foreach($slides as $slide){
?>	
                                        <tr>
                                            <td><?=$slide[slid]?></td>
                                            <td>
											<img src="<?=$slide[image]?>"  style="height:160px" />
											</td>
                                          <td><?=$slide[link]?></td>
											<td>
											<div class="btn-group">
											    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">操作
													<span class="caret"></span>
												</button>
											    <ul role="menu" class="dropdown-menu">
													<li><a onclick="editSlide('<?=$slide[slid]?>','<?=$slide[image]?>','<?=$slide[link]?>')"><i class="icon-document-edit"></i>&nbsp;&nbsp;修改资料</a></li>
													<li><a href="/sys/delete.php?table=slide&field=slid&id=<?=$slide[slid]?>"><i class="entypo-cancel-circled"></i>&nbsp;&nbsp;删除图片</a></li>
												</ul>
											</div>
											</td>
                                        </tr>
	<?php } ?>
                                    </tbody>
                                </table>


		   
                            </div>

                        </div>


                    </div>


                </div>
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->
<?php print_r($_GET);?>


	<!-- Modal2 -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">系统提示</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

    <script type="text/javascript" src="assets/js/jquery.js"></script>

    <!-- GAGE -->


   <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
   <!-- /MAIN EFFECT -->
    <script type="text/javascript" src="assets/js/stackable/stacktable.js"></script>
	<script type="text/javascript" src="/js/layer/layer.js"></script>
<script>
	$("#add_btn").click(function(){
	
		$('#myModal1').modal('show')

	});
	
	function editSlide(slid,image,link){
		
           $("#slid").val(slid);
		   $("#link").val(link);
           $("#image_input").val(image);
		   $("#image").attr("src",image).show();
       }
	   
	   
  $("#save_btn").click(function () {
			  
	   var slid = $("#slid").val();
	   var link = $("#link").val();
	   var image_input = $("#image_input").val();
	   if(!image_input){
		    layer.tips('图片不能为空!', '#image_file');
			return false;
	   }
		
	   $.ajax({
		   type: "post",
		   url: "action.php?type=addslide",
		   data: {slid:slid,link:link,image_input:image_input},
		 
		   success: function (data) {
			   if (data=="success") {
					$('#myModal2').modal('show')
					$('#myModal2 .modal-body').html('提交成功'); 
					$('#myModal2').on('hidden.bs.modal', function (e) {
						setTimeout(function(){window.location.reload()},1000);
					})
			   }
			
		   },
		   error: function (err) {
			   $("#myModal2").modal('show');
			   $("#myModal2 .modal-body").html("服务器内部错误！");
		   }
	   });
           
     });
</script>

<?php include_once 'foot.php'; ?>
</body>

</html>
