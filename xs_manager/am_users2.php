<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>baihueigd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

	
     <link href="assets/js/stackable/stacktable.css" rel="stylesheet">
    <link href="assets/js/stackable/responsive-table.css" rel="stylesheet">
<link href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>

    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>

<body> 
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
  <?php include_once "head.php"; ?>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
		<?php include_once 'left.php' ?>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid" style="width:auto;margin-left:250px">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-lg-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>信号源管理</span>
                        </h2>

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">信号源管理</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">信号源列表</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
				<div class="row">
				
					    <div class="body-nest" id="inlineClose">
                               <div class="form_center" style="width:100%">
                                    <form role="form" class="form-inline " method="get">
									
										<div class="form-group">
									
											 <select class="form-control" name="pagesize">
												<option value="10" <?php if($_GET['pagesize']=='10'){ echo 'selected="selected"'; } ?>>每页10条</option>
												<option value="30" <?php if($_GET['pagesize']=='30'){ echo 'selected="selected"'; } ?>>每页30条</option>
												<option value="50" <?php if($_GET['pagesize']=='50'){ echo 'selected="selected"'; } ?>>每页50条</option>
												<option value="100"  <?php if($_GET['pagesize']=='100'){ echo 'selected="selected"'; } ?>>每页100条</option>
											 </select>
												 <select class="form-control" name="groupid">
													<option value="">所有用户组</option>
					<?php 
					$groups=$res->fn_rows("select * from user_group ") ;
					foreach($groups as $group){ 
					?>
						<option value="<?=$group[gid]?>" <?php if($_GET['groupid']==$group[gid]){ echo 'selected="selected"'; } ?>><?=$group[gname]?></option>
					<?php }　?>
												 </select>
										
										
									
												 <input type="text" class="form-control" placeholder="MT4/账号/手机号" size="12" name="search" value="<?=$_GET[search]?>">
									
												<input type="text" placeholder="申请时间" class="form-control" id="reservation" name="reservation" style="width:320px" value="<?=$_GET[reservation]?>" autocomplete="off"> 
												
												<button type="submit" class="btn btn-primary">搜索</button>
										</div>
											
										
                                    </form>
                                </div>

                        </div>
							
                   <div class="col-sm-12">

                        <div class="nest" id="StackableClose">
                            <div class="title-alt">
                                <h6>信号源列表</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#tStackableClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
								    <a type="button" class="btn btn-primary" id="add_btn" href="adduser2.php">
										<span class="entypo-plus-squared"></span>&nbsp;&nbsp;添加账号
									</a>
                                    <a class="nav-toggle-alt" href="#StackableStatic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="StackableStatic">

                                <table id="responsive-example-table" class="table large-only ">
                                    <thead class="cf">
                                        <tr class="text-right">
											<th style="width:5%;">
											
											</th>
                                            <th style="width:5%;">ID</th>
                                            <th style="width:5%;">MT4</th>
                                            <th style="width:15%;">状态</th>
											 <th style="width:10%;">MT4密码</th>
                                            <th style="width:10%;">MT4-IP</th>
											<th style="width:5%;">MT4-Port</th>
											<th style="width:5%;">所属用户</th>
											<th style="width:10%;">初始资金</th>
											<th style="width:5%;">跟随人数/基数</th>
											<th style="width:5%;">最大跟单人数</th>
											<th style="width:5%;">分润比例</th>
											<th style="width:10%;">信号等级</th>
											<th style="width:5%;">操作</th>
                                        </tr>
									 </thead>
						    <tbody>
										
<?php 
$pagesize=10;
if($_GET[pagesize]!= null){
	$pagesize=$_GET[pagesize];
	$aplus="&pagesize=$pagesize";
}

if($_GET[page]){
	$page=$_GET[page];
}else{
	$page=1;
}
$qian=($page-1)*$pagesize;

if($_GET[groupid]!= null){
	$sqlxs.=" and t1.groupid = '$_GET[groupid]'";
	
	$aplus.="&groupid=$_GET[groupid]";
}

if($_GET[search]!= null){
	$sqlxs.=" and (t1.mt4 like '%$_GET[search]%' || t2.nickname like '%$_GET[search]%' || t2.mobile like '%$_GET[search]%' )";
	$aplus.="&search=$_GET[search]";
}

if($_GET['reservation']!= null){
		$reservation=explode("~",$_GET['reservation']);
		$start_time=strtotime($reservation[0]);
		$end_time=strtotime($reservation[1]);
		$sqlxs.=" and t1.regtime>'$start_time' and t1.regtime<'$end_time'";
		$aplus.="&reservation=$_GET[reservation]";
}

	$sql="select t1.mtid,t1.mt4,t1.password mt4pwd ,t1.port,t1.level,t1.money2,t1.status,t1.listenstatus,t1.base,t1.max_gendan,t1.fenrun,t2.uid,t2.nickname,t2.groupid,t3.ip mt4ip,t3.field,t4.mt4num from users t2 left join mt4 t1 on t1.uid=t2.uid left join server t3 on t1.svid=t3.svid   left join (

                select count(*) mt4num,mmt4 from mt4_contact left join mt4 on mt4_contact.mt4=mt4.mt4 where 1=1 and mt4.mt4>0 group by mmt4

                ) t4 on t1.mt4=t4.mmt4  where 1=1  and t2.groupid=10 $sqlxs order by t1.mtid desc limit $qian,$pagesize"; 
	
	$users =$res->fn_rows($sql);
	foreach($users as $user){
?>	
<tr data-mt4="<?=$user[mt4]?>" data-mt4pwd="<?=$user[mt4pwd]?>" data-mt4port="<?=$user[port]?>" data-mt4ip="<?=$user[mt4ip]?>"  data-field="<?=$user[field]?>" data-loginstatus="<?=$user[status]?>" data-listenstatus="<?=$user[listenstatus]?>">
		<td><input type="checkbox"  name="port" value="<?=$user[port]?>" /></td>
		<td><?=$user[mtid]?></td>
		<td><?=$user[mt4]?></td>
		<td>
<?php 
   if($user[status]==1){
		echo '<span class="label label-success loginstatus">已登录</span>';
   }else{
		echo '<span class="label label-warning loginstatus">未登录</span>';
   }?>
	<a class="btn btn-danger" onclick="mt4Reflash('<?=$user[mt4]?>','<?=$user[mt4pwd]?>','<?=$user[mt4ip]?>','<?=$user[port]?>')"><i class="fontawesome-refresh"></i></a>   
	<?php 
   if($user[listenstatus]==1){
		echo '<span class="label label-success listenstatus">已监听</span>';
   }else{
		echo '<span class="label label-warning listenstatus">未监听</span>';
   }?>	
	<a class="btn btn-danger" onclick="mt4Reflash2('<?=$user[mt4]?>','<?=$user[mt4pwd]?>','<?=$user[mt4ip]?>','<?=$user[port]?>')"><i class="fontawesome-refresh"></i></a>      
	</td>
	<td><?=$user[mt4pwd]?></td>
	<td><?=$user[mt4ip]?></td>
	<td><?=$user[port]?></td>
	<td><?=$user[nickname]?></td>
	<td><?=$user[money2]?></td>
	<td><?=$user[mt4num]?$user[mt4num]:0?>/<?=$user[base]?$user[base]:0?></td>
	<td><?=$user[max_gendan]?></td>
	<td><?=$user[fenrun]?>%</td>
	<td>
	<?php if($user[level]==1){ ?>
		<span class="label label-success">稳健型</span>
	<?php }else if($user[level]==2){ ?>
		<span class="label label-important">激进型</span>
	<?php }else if($user[level]==3){ ?>
		<span class="label label-danger">反向跟单型</span>
	<?php }?></td>
	<td>
	<div class="btn-group">
		<button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">操作
			<span class="caret"></span>
		</button>
	<ul role="menu" class="dropdown-menu">
	<?php if($u['admingroup']!="信号源管理员"){ ?>
	<li><a href="adduser2.php?mid=<?=$user[uid]?>"><i class="icon-document-edit"></i>&nbsp;&nbsp;修改资料</a></li>

	<li><a href="javascript:;" onclick="editMt4('<?=$user[port]?>','<?=$user[uid]?>','<?=$user[mtid]?>')" ><i class="icon-document-edit"></i>&nbsp;&nbsp;修改MT4</a></li>
	<li><a href="javascript:;" onclick="mt4Login('<?=$user[mt4]?>','<?=$user[mt4pwd]?>','<?=$user[mt4ip]?>','<?=$user[port]?>','<?=$user[field]?>')"><i class="entypo-login"></i>&nbsp;&nbsp;MT4登录</a></li>

	<li><a href="javascript:;" onclick="mt4Loginoff('<?=$user[mt4]?>','<?=$user[mt4pwd]?>','<?=$user[mt4ip]?>','<?=$user[port]?>')"><i class="entypo-logout"></i>&nbsp;&nbsp;MT4退出</a></li>

	<li><a href="javascript:;" onclick="mt4Listen('<?=$user[mt4]?>','<?=$user[mt4pwd]?>','<?=$user[mt4ip]?>','<?=$user[port]?>')"><i class="entypo-user-add"></i>&nbsp;&nbsp;挂起监听</a></li>

	<li><a href="javascript:;" onclick="mt4DelListen('<?=$user[mt4]?>','<?=$user[mt4pwd]?>','<?=$user[mt4ip]?>','<?=$user[port]?>')"><i class="entypo-cancel-circled"></i>&nbsp;&nbsp;删除监听</a></li>
	<?php } ?>
		<li><a href="am_dingdan.php?mt4=<?=$user[mt4]?>"><i class="entypo-list"></i>&nbsp;&nbsp;查看订单</a>	</li>							
		<li><a><i class="entypo-basket"></i>&nbsp;&nbsp;查看消费情况</a></li>
		<li><a href="am_mt4_contact.php?mmt4=<?=$user[mt4]?>"><i class="icon-attachment"></i>&nbsp;&nbsp;查看跟随用户</a></li>
			<?php if($u['admingroup']!="信号源管理员"){ ?>
			
		<li><a href="javascript:;" onclick="mt4GetData('<?=$user[mt4]?>','<?=$user[mt4pwd]?>','<?=$user[mt4ip]?>','<?=$user[port]?>')"><i class="entypo-download"></i>&nbsp;&nbsp;同步数据</a></li>
		<li><a href="javascript:;" onclick="mt4Delete('<?=$user[mt4]?>','<?=$user[mt4pwd]?>','<?=$user[mt4ip]?>','<?=$user[port]?>','<?=$user[uid]?>')"><i class="entypo-cancel-circled"></i>&nbsp;&nbsp;删除用户</a></li>
			
			<?php } ?>
		</ul>
	</div>	
			
			
											</td>
                                        </tr>
	<?php } ?>
                                    </tbody>
                                </table>
<div class="row">						
<div class="col-sm-6" style="    padding: 20px 20px;">
	<button type="button" class="btn btn-primary  m-r-5 m-b-5" onclick="selectAll()">全选</button>
	<button type="button" class="btn btn-primary  m-r-5 m-b-5" onclick="mt4LoginAll()">批量MT4登录</button>
	<button type="button" class="btn btn-primary  m-r-5 m-b-5" onclick="mt4LoginOutAll()">批量MT4退出</button>
	<button type="button" class="btn btn-primary  m-r-5 m-b-5" onclick="addListenAll()">批量添加监听</button>
	<button type="button" class="btn btn-primary  m-r-5 m-b-5" onclick="delListenAll()">批量删除监听</button>
</div>
<div class="col-sm-6 pull-right">

 <?php
$sql2="select t1.* from mt4 t1 left join users t2 on t1.uid=t2.uid where t2.groupid=10 $sqlxs";
$num=$res->fn_num($sql2);

?>
 
	<ul class="pagination">
 <?php

    $myPage=new pager($num,intval($page),$pagesize,"active");     
     $pageStr= $myPage->GetPagerContent();    
     echo $pageStr;    

?>	
	</ul>

	</div>
</div>
	
                            </div>

                        </div>


                    </div>


                </div>
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->

	
    <script type="text/javascript" src="assets/js/jquery.js"></script>

    <!-- GAGE -->


   <script type="text/javascript" src="assets/js/preloader.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/load.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
   <!-- /MAIN EFFECT -->
    <script type="text/javascript" src="assets/js/stackable/stacktable.js"></script>
	   <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.js"></script>
	 <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
	   <script type="text/javascript" src="/js/layer/layer.js"></script>
  <!--socoket start-->
  <script type="text/javascript" src="/js/swfobject.js"></script>
  <script type="text/javascript" src="/js/web_socket.js"></script>
  <script type="text/javascript" src="/js/json.js"></script>
<script type="text/javascript" src="/static/websocket_config.js"  media="all"></script>

 <script>
   
	 function init() {
		ws = new WebSocket("ws://"+WS_HOST+":"+WS_PORT);
		ws.onopen = function() {
    	    timeid && window.clearTimeout(timeid);
		   	console.log("连接成功");

      };
		if(reconnect>6){
			window.clearTimeout(timeid);
		}
		ws.onmessage = function(e) {
				console.log("收到数据："+e.data);
				var data = JSON.parse(e.data);
				switch(data['type']){
				   
				case 'ping':
						ws.send(JSON.stringify({"type":"pong"}));
						break;;
				case 'loginuser':
						    if(data['result']=="SUCCESS"){
								$.get("action.php?type=mt4status&status=1",{port:data['port']},function(data,status){});
								$("tbody tr[data-mt4port='"+data['port']+"']").attr('data-loginstatus',1);
								$("tbody tr[data-mt4port='"+data['port']+"'] .loginstatus").removeClass("label-warning").addClass("label-success").html("已登录");
							 }
						break;
				case 'loginout':
						    if(data['result']=="SUCCESS"){
								$.get("action.php?type=mt4status&status=0",{port:data['port']},function(data,status){});
								$("tbody tr[data-mt4port='"+data['port']+"']").attr('data-loginstatus',0);
								$("tbody tr[data-mt4port='"+data['port']+"'] .loginstatus").removeClass("label-success").addClass("label-warning").html("未登录");
								
							
								var data=JSON.stringify({"type":"delistener","port":data['port']});
								ws.send(data);
							
							 }
					break;
					case 'addlistener':
					       if(data['result']=="SUCCESS"){
								$.get("action.php?type=mt4status2&status=1",{port:data['port']},function(data,status){});
								$("tbody tr[data-mt4port='"+data['port']+"']").attr('data-listenstatus',1);
								$("tbody tr[data-mt4port='"+data['port']+"'] .listenstatus").removeClass("label-warning").addClass("label-success").html("已监听");
							}
					 break;
					case 'delistener':
					       if(data['result']=="SUCCESS"){
								$.get("action.php?type=mt4status2&status=0",{port:data['port']},function(data,status){});
								$("tbody tr[data-mt4port='"+data['port']+"']").attr('data-listenstatus',0);
								$("tbody tr[data-mt4port='"+data['port']+"'] .listenstatus").removeClass("label-success").addClass("label-warning").html("未监听");
							}

					break;
					case 'current':
						    if(data['code']=="200"){
						
								$.get("action.php?type=mt4status&status=1",{port:data['port']},function(data,status){});
								$("tbody tr[data-mt4port='"+data['port']+"']").attr('data-loginstatus',1);
								$("tbody tr[data-mt4port='"+data['port']+"'] .loginstatus").removeClass("label-warning").addClass("label-success").html("已登录");
					
							 }
					break;
					case 'current_message':
							$.get("action.php?type=mt4status&status=0",{port:data['port']},function(data,status){});
							$("tbody tr[data-mt4port='"+data['port']+"']").attr('data-loginstatus',0);
							$("tbody tr[data-mt4port='"+data['port']+"'] .loginstatus").removeClass("label-success").addClass("label-warning").html("未登录");
					break;
					case 'lisquery':
						
						   if(data['code']=="200"){
						
								if(data['flag']==1){
									$.get("action.php?type=mt4status2&status=1",{port:data['port']},function(data,status){});
									$("tbody tr[data-mt4port='"+data['port']+"']").attr('data-listenstatus',1);
									$("tbody tr[data-mt4port='"+data['port']+"'] .listenstatus").removeClass("label-warning").addClass("label-success").html("已监听");
								}else{
									$.get("action.php?type=mt4status2&status=0",{port:data['port']},function(data,status){});
									$("tbody tr[data-mt4port='"+data['port']+"']").attr('data-listenstatus',0);
									$("tbody tr[data-mt4port='"+data['port']+"'] .listenstatus").removeClass("label-success").addClass("label-warning").html("未监听");
								}
							}
					break;
					
					case 'userinfo':
						
						   if(data['code']=="200"){
								//$.post("action.php?type=setdata&op=userinfo",{port:data['port'],data:data['data']},function(data,status){});
								$.get("action.php?type=mt4status&status=1",{port:data['port']},function(data,status){});
								$("tbody tr[data-mt4port='"+data['port']+"']").attr('data-loginstatus',1);
								$("tbody tr[data-mt4port='"+data['port']+"'] .loginstatus").removeClass("label-warning").addClass("label-success").html("已登录");
							 }
					break;
					case 'userinfo_message':
							$.get("action.php?type=mt4status&status=0",{port:data['port']},function(data,status){});
							$("tbody tr[data-mt4port='"+data['port']+"']").attr('data-loginstatus',0);
							$("tbody tr[data-mt4port='"+data['port']+"'] .loginstatus").removeClass("label-success").addClass("label-warning").html("未登录");
					break;
					
					case 'his_report':
						
						   if(data['code']=="200"){
								var port=data['port'];
								for(i=0;i<data['data'].length;i+=30){
								
									var arr=data['data'].slice(i,i+30);
									$.post("action.php?type=setdata&op=his_report",{port:port,data:arr},function(data,status){});
								}
								
						
							}
					break;
					
					
				}
		
		};
	  ws.onclose = function() {
		  reconnect ++;
    	  console.log("连接关闭，定时重连");
    	  timeid = window.setTimeout(init, 3000);
      };
      ws.onerror = function() {
			reconnect ++;
    	    console.log("出现错误");
		    timeid = window.setTimeout(init, 3000);
      };
	}
    function selectAll(){
		$("tbody input[type='checkbox']").each(function(){
			 if($(this).prop('checked')){
				 $(this).removeAttr('checked');
			 }else{
				 $(this).prop('checked','checked');
			
			 }
		
		});
		
	}
     

	init();
	function editMt4(mt4port,uid,mtid){
		if($("tbody tr[data-mt4port='"+mt4port+"']").attr("data-loginstatus")=='0'){
			location.href="addmt4.php?mid="+uid+"&mtid="+mtid;
		}else{
			alert("请先退出后操作");
		}
	}
	
	function mt4Login(mt4,mt4pwd,mt4ip,mt4port,field){

		var data=JSON.stringify({"type":"loginuser","login":mt4,"password":mt4pwd,"ip":mt4ip,"port":mt4port,"symbol":field});
		console.log(data);
		ws.send(data);
		$("tbody tr[data-mt4port='"+mt4port+"'] .loginstatus").html("处理中...");
		//layer.load(0, {shade: [0.4,'#eee']});
	}
	
	function mt4Loginoff(mt4,mt4pwd,mt4ip,mt4port){
		var data=JSON.stringify({"type":"loginout","port":mt4port});
		console.log(data);
		ws.send(data);
		$("tbody tr[data-mt4port='"+mt4port+"'] .loginstatus").html("处理中...");
		//layer.load(0, {shade: [0.4,'#eee']});
	}
	
	function mt4Listen(mt4,mt4pwd,mt4ip,mt4port){
		var data=JSON.stringify({"type":"addlistener","port":mt4port});
		console.log(data);
		ws.send(data);
		$("tbody tr[data-mt4port='"+mt4port+"'] .listenstatus").html("处理中...");
	//	layer.load(0, {shade: [0.4,'#eee']});
	}
	
	function mt4DelListen(mt4,mt4pwd,mt4ip,mt4port){
		var data=JSON.stringify({"type":"delistener","port":mt4port});
		console.log(data);
		ws.send(data);
		$("tbody tr[data-mt4port='"+mt4port+"'] .listenstatus").html("处理中...");
	}
	
	function mt4Reflash(mt4,mt4pwd,mt4ip,mt4port){
	var data=JSON.stringify({"type":"userinfo","port":mt4port});
			console.log(data);
		ws.send(data);
		$("tbody tr[data-mt4port='"+mt4port+"'] .loginstatus").html("处理中...");
	}
	function mt4Reflash2(mt4,mt4pwd,mt4ip,mt4port){
		var data=JSON.stringify({"type":"lisquery","port":mt4port});
		console.log(data);
		ws.send(data);
		$("tbody tr[data-mt4port='"+mt4port+"'] .listenstatus").html("处理中...");
	}
	
	function mt4Delete(mt4,mt4pwd,mt4ip,mt4port,uid){
		if(!window.confirm('确认删除操作吗？')){ return false;}
		var data=JSON.stringify({"type":"loginout","port":mt4port});
		ws.send(data);
		var data=JSON.stringify({"type":"delistener","port":mt4port});
		console.log(data);
		ws.send(data);
		$.get("action.php?type=deluser",{uid:uid,mt4:mt4},function(data,status){
			window.location.reload();
		});
	}
	
	function mt4GetData(mt4,mt4pwd,mt4ip,mt4port){
		var data=JSON.stringify({"type":"userinfo","port":mt4port});
		ws.send(data);
		var data2=JSON.stringify({"type":"his_report","port":mt4port});
		ws.send(data2);
		var data3=JSON.stringify({"type":"current","port":mt4port});
		ws.send(data3);
	    layer.load(0, {shade: [0.4,'#eee']});
			setTimeout(function(){
			layer.closeAll();
		},35000);
	}
	
	function mt4LoginAll(){
		$("tbody tr[data-loginstatus='0'] input[type='checkbox']").each(function(){
			if($(this).prop('checked')){
				
				var mt4=$(this).parent().parent().attr('data-mt4');
				var mt4pwd=$(this).parent().parent().attr('data-mt4pwd');
				var mt4ip=$(this).parent().parent().attr('data-mt4ip');
				var mt4port=$(this).parent().parent().attr('data-mt4port');
				var field=$(this).parent().parent().attr('data-field');
				
				var data=JSON.stringify({"type":"loginuser","login":mt4,"password":mt4pwd,"ip":mt4ip,"port":mt4port,"symbol":field});
				console.log(data);
				ws.send(data);
				$("tbody tr[data-mt4port='"+mt4port+"'] .loginstatus").html("处理中...");
			}	
		});	
	}
	
	function mt4LoginOutAll(){
		$("tbody tr[data-loginstatus='1'] input[type='checkbox']").each(function(){
			if($(this).prop('checked')){
				
				var mt4port=$(this).parent().parent().attr('data-mt4port');

				var data=JSON.stringify({"type":"loginout","port":mt4port});
				console.log(data);
				ws.send(data);
				$("tbody tr[data-mt4port='"+mt4port+"'] .loginstatus").html("处理中...");
			}	
		});	
	}
	
	
	function addListenAll(){
		$("tbody tr[data-listenstatus='0'] input[type='checkbox']").each(function(){
			if($(this).prop('checked')){
				
				var mt4port=$(this).parent().parent().attr('data-mt4port');

				var data=JSON.stringify({"type":"addlistener","port":mt4port});
				console.log(data);
				ws.send(data);
				$("tbody tr[data-mt4port='"+mt4port+"'] .listenstatus").html("处理中...");
			}	
		});	
	}
	
	function delListenAll(){
		$("tbody tr[data-listenstatus='1'] input[type='checkbox']").each(function(){
			if($(this).prop('checked')){
				
				var mt4port=$(this).parent().parent().attr('data-mt4port');

				var data=JSON.stringify({"type":"delistener","port":mt4port});
				console.log(data);
				ws.send(data);
				$("tbody tr[data-mt4port='"+mt4port+"'] .listenstatus").html("处理中...");
			}	
		});	
	}
   </script>

<?php include_once 'foot.php'; ?>
</body>

</html>
