<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>订单记录</title>
<link rel="stylesheet" href="./css/bootstrap.css">	
<link rel="stylesheet" href="./css/css.css">
<link type="text/css" href="/style/media.css" rel="stylesheet" />
<link type="text/css" href="style/media.css" rel="stylesheet" />
<link rel="stylesheet" href="/js/layui/dist/css/layui.css"  media="all">
<link type="text/css" href="style/css.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>
<script src="/js/layui/dist/layui.js" charset="utf-8"></script>
</head>

<script type="text/javascript">
// $(document).ready(function(){ 
//    $('.pagination').children('li').click(function(){
//    	$(this).css('background','#f00');
//    	$(this).children('a').css('color','#fff');
//     })
//  });
</script>
<body style="background:#f0f2f5;">

	

    <?php include_once "left.php";?>
    <div class="main-container">
		<style>
       .main-container {
margin-left:239px;}
       </style>		
		<?php  include("./head.php") ?>
            <div class="padding-md">
					<div class="md_lf"><h2>交易记录</h2></div>
                  
                  
			</div>
            
            <div class="jl">
            	<table cellpadding="0" cellspacing="0">
                	<tr>
                    	<th>单号</th>
						<th>MT4</th>
                        <th>品种</th>
						<th>方向</th>
                        <th>开仓时间</th>
                        <th>平仓时间</th>
                        <th>手数</th>
						<th>盈亏</th>
                        <th>类别</th>
                        <th>入场/出场</th>
                        <th>止盈/止损</th>
                    </tr>
                  	<?php 
if($_GET[page]){
	$page=$_GET[page];
}else{
	$page=1;
}	$qian=($page-1)*10;
		$danzis=$res->fn_rows("select t1.*,t3.Ticket tticket, t5.nickname tname from danzi t1 left join mt4  t2 on t1.mt4=t2.mt4  left join danzi t3 on t1.ddid=t3.did left join mt4 t4 on t3.mt4 = t4.mt4 left join users t5 on t5.uid = t4.uid where t1.status=2 and t1.Ticket>0  and t1.Operation<=1 and  t2.uid='$_GET[xsuid]'  order by t1.did desc limit $qian,10");
		$sum = $res->fn_select("select count(*) dnum,sum(t1.Lots) Lots,sum(t1.Profit) Profit from danzi t1 left join mt4  t2 on t1.mt4=t2.mt4  left join danzi t3 on t1.ddid=t3.did left join mt4 t4 on t3.mt4 = t4.mt4 left join users t5 on t5.uid = t4.uid where t1.status=2 and t1.Ticket>0  and t1.Operation<=1 and  t2.uid='$_GET[xsuid]'  order by t1.did desc ");
						foreach($danzis as $danzi){ ?>
							<tr >
								<td><?=$danzi[Ticket]?>
								<?php 
									if($danzi[ddid]){ 
								?>
								跟随：<?=$danzi[tname]?> 单号：<?=$danzi[tticket]?> 
									<?php }?>
								</td>
								<td><?=$danzi[mt4]?></td>
								<td><?=$danzi[Symbol]?></td>
								<td><?=$danzi[Operation]==0?"买入":"卖出"?></td>
								<td><?=date('Y-m-d H:i',$danzi[OpenTime])?></td>
								<td><?=date('Y-m-d H:i',$danzi[CloseTime])?></td>
								<td><?=$danzi[Lots]?></td>
								<td><?=$danzi[Profit]?></td>
								<td>
								<?php 
									if($danzi[Operation]==0){echo "BUY";}else if($danzi[Operation]==1){ echo "SELL";}
								?>
								</td>
								<td><?=$danzi[OpenPrice]?><p><?=$danzi[ClosePrice]?></p></td>
								<td><?=$danzi[TakeProfit]?><p><?=$danzi[StopLoss]?></p></td>
							
							</tr>
						<?php } ?>
                </table>
				<p>合计：总单数：<?=$sum['num']?> 总手数：<?=round($sum['Lots'],2)?> 总获利:<?=round($sum['Profit'],2)?></p>
            </div>
           
            <div class="ym">
            	<ul class="pagination">
     <?php
	 
  $sql2="select t1.did from danzi t1 left join mt4 t2 on t1.mt4=t2.mt4 where t1.status=2 and t1.Ticket>0  and  t2.uid='$u[uid]'  ";
  
$num=$res->fn_num($sql2);
$ye=(int)($num/10+1);
?>
 <li> <a href="danzi.php?page=1" >首页</a></li> 

<?php
if($page<5){
	$iiikaishi=1;
	if($ye-$page>5){
		$iiijishu=5;
	}else{
			$iiijishu=$ye;
	}
}elseif($ye-$page<5){
	$iiikaishi=$ye-5;
	$iiijishu=$ye;
}else{
	$iiikaishi=$page-2;
	$iiijishu=$page+2;
}

for($iii=$iiikaishi;$iii<=$iiijishu;$iii++){
?>
 <li  <?php if($page==$iii){echo 'class="active"';}?>> <a href="danzi.php?page=<?=$iii?>" ><?=$iii?></a></li>
<?php
}
?>
<li >  <a href="danzi.php?page=<?=$ye?>" >尾页</a></li>
            </div>
            
            
            
		</div>
	<div class="clear"></div>
	</div>
<script>
layui.use('laydate', function(){
  var laydate = layui.laydate;
   laydate.render({
    elem: '#reservation'
    ,type: 'datetime'
    ,range: true
	,format:'yyyy/MM/dd HH:mm:ss'
  });
 });	
 $(".active").css("color","#fff");
</script>
</body>
</html>
