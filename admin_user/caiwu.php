<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>我的财务</title>
<link rel="stylesheet" href="./css/bootstrap.css">	
<link rel="stylesheet" href="./css/css.css">
 <link rel="stylesheet" href="/js/layui/dist/css/layui.css"  media="all">
<link type="text/css" href="style/css.css" rel="stylesheet" />
<link type="text/css" href="/style/media.css" rel="stylesheet" />
<link type="text/css" href="style/media.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>
<script src="/js/layui/dist/layui.js" charset="utf-8"></script>
</head>
<style>
.active{
	color: #fff;
	background:#0183a5;
}
.list ul li a{
	margin-top: 10px;
}
.list ul li i {
    position: relative;
    top: -2px;}
    .list ul li {
    font-size: 18px;
    padding: 3px 20px 8px;}
    .right>div{
    padding: 12px 42px 12px 4%;
    background:#fff;
    border-bottom: 2px solid #af9350;
    margin: 0;
}
.right>div>h3{
	float: left;
    width: 30%;
	font-size: 22px;
	margin: 0;
	padding:0;
    line-height: 38px;
    border:0;
}
.right>div>form{
    float: right;
}
</style>
<script type="text/javascript">
// $(document).ready(function(){ 
//    $('.pagination').children('li').click(function(){
//    	$(this).css('background','#f00');
//    	$(this).children('a').css('color','#fff');
//     })
//  });
</script>
<body style="background:#f0f2f5;">
  <?php include_once "left.php";?>
    <?php include_once "./head.php"; ?>
	<div class="padding-md">
		<div class="md_lf"><h2>我的财务</h2></div>
	</div>
    <div class="main-container" >
    	<form method="get" class="form-inline">
				<div class="form-group fy" style="width:100%">
					<input type="text" class="form-control" placeholder="用户名" size="12" name="search" value="<?=$_GET['search']?>">
					 <select name="type" id="type" class="form-control round-input"  placeholder="类别" >
						<option value="" >全部</option>
						<option value="1" <?php if($_GET[type]==1) echo 'selected="selected"'; ?>>入金</option>
						<option value="2" <?php if($_GET[type]==2) echo 'selected="selected"'; ?>>提现</option>
					</select> 
					<input type="text" placeholder="时间范围" class="form-control" id="reservation" name="reservation" value="<?=$_GET['reservation']?>">
					<button type="submit" class="btn btn-default  colyy">搜索</button>
				</div>
			</form>
		<div class="jl">
			<table cellpadding="0" cellspacing="0"  class="table striped">
				<tr>
					<th>金额</th>
					<th>备注</th>
					<th>用户名</th>
					<th>时间</th>							
				</tr>
<?php 
						
	$pagesize=10;
	if($_GET[pagesize]!= null){
		$pagesize=$_GET[pagesize];
		$aplus="&pagesize=$pagesize";
	}

	if($_GET[page]){
		$page=$_GET[page];
	}else{
		$page=1;
	}
	$qian=($page-1)*$pagesize;


	if($_GET[reservation]!= null){
		$reservation=explode("-",$_GET[reservation]);
		$start_time=strtotime($reservation[0]);
		$end_time=strtotime($reservation[1]);
		$sqlxs.=" and t1.time>'$start_time' and t1.time<'$end_time'";
		$aplus.="&reservation=$_GET[reservation]";
	}

	if($_GET[search]!= null){
		$sqlxs.=" and t2.nickname like '%$_GET[search]%'";
		$aplus.="&search=$_GET[search]";
	}

	if($_GET[type]==1) { 
		$sqlxs.= " and t1.beizhu='入金' or (t1.beizhu = '奖金' and t1.money > 0)";
		$aplus.="&type=$_GET[type]";
	}else if($_GET[type]==2) {
		$sqlxs.= " and t1.beizhu='提现' or (t1.beizhu='奖金'  and t1.money < 0) ";
		$aplus.="&type=$_GET[type]";
	}


	$uids=$res->fn_select("select getChildList($u[uid]) uids ");
	$uids_arr = array_slice(explode(",",$uids[uids]),2);
	$uids_str = "t1.uid = '$u[uid]'";
	foreach($uids_arr as $uid){
		$uids_str .= " or t1.uid = '$uid'";
	}

		
	$caiwus=$res->fn_rows("select t1.*,t2.nickname from caiwu t1 left join users t2 on t1.uid=t2.uid   where  1=1 and  ($uids_str) $sqlxs order by cid desc ");
	
	foreach($caiwus as $caiwu){ ?>
		<tr >
			<td><?=$caiwu['money']?></td>
			<td><?=$caiwu['beizhu']?></td>
			<td><?=$caiwu['nickname']?></td>
			<td><?=date("Y-m-d H:i",$caiwu['time'])?></td>
		</tr>
		<?php } ?>
		</table>
	</div>
	
						
	</div>
<script>
layui.use('laydate', function(){
  var laydate = layui.laydate;
   laydate.render({
    elem: '#reservation'
    ,type: 'datetime'
    ,range: true
	,format:'yyyy/MM/dd HH:mm:ss'
  });
 });	
 $(".active").css("color","#fff");
</script>
</body>
</html>
