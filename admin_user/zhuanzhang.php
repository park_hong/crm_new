<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>MT4转账</title>
<link type="text/css" href="style/css.css" rel="stylesheet" />
<link type="text/css" href="css/css.css" rel="stylesheet" />
<link type="text/css" href="/style/media.css" rel="stylesheet" />
<link type="text/css" href="style/media.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>
<style>
.bt{
	padding: 10px 0;
    overflow: hidden;
    width: 1456px;
}
.hy_r {
    float: left;
    padding: 22px 40px;
    width: 92%;
}
</style>
</head>

<body style="background:#f0f2f5;">

    <?php include_once "../head.php"; ?>
    
    	<?php include_once "left.php";


	$mt4s=$res->fn_rows("select t1.mt4 from mt4 t1  where t1.uid='$u[uid]'");

		?>
		<div class="right">
        <div class="hy_r" >
        	<div class="bt">
            	<h2><span>MT4转账</span></h2>
            </div>
	
    	<form name="dinpayForm" method="post" action="" target="_blank">        
            <div class="tk">
                <ul>
                    <li>
						<span>转出MT4：</span> <select id="mtid" name="mtid"  onchange="getYue()" style="width:274px;">
							<?php 
							$mt4s=$res->fn_rows("select * from mt4 where uid='$u[uid]' ");
							foreach($mt4s as $v){
							?>
								<option value="<?=$v[mtid]?>" <?php if($_GET[mtid] == $v[mtid]){ echo 'selected="selected"';} ?>><?=$v[mt4]?></option>
							<?php } ?>
						</select>
							
						<p style="display:inline-block;margin-left:18px;">(美元：<span id="money" style="width:auto;color:#f00;margin:0;">0</span>)</p>
						
					</li>
                    <li>
						<span>转入MT4：</span>

						
						<input type="text" id="mmt4" value="" >

					</li>
					<li>
						<span>邮箱验证码：</span>

						
						<input type="text" id="validate" value="" >
						<button type="button" id="send_msg" style=" border: 0px;  padding:  5px 10px; cursor: pointer;">获取验证码</button>
					</li>
					<li>
						<span>金&nbsp;&nbsp;&nbsp;&nbsp;额： </span> <input type="text" id="jine" value="0" ><p style="display:inline-block;margin-left:18px;">(美元)</p>
					</li>
					<div class="clear"></div>
					<div class="tk_btn">
            	<button id="tx-btn" class="btn btn-default ty">提交</button>
                </ul>
            </div>
            </div>
		</div>
		</form>
        </div>
    </div>
	
	
			

		
<script>
  
$(function(){

	$("#tx-btn").click(function(){
		$("#tx-btn").attr("disabled",true);
		$("#tx-btn").text("申请中。。。");
		var mtid=$("#mtid").val();
		var jine=$("#jine").val();
		var money=$("#money").text();
		var mmt4=$("#mmt4").val();
		var validate=$("#validate").val();
		if(!mtid){
			    layer.msg('尚无MT4!');
				return false;
		}
		if(!money){
			    layer.tips('余额不足!', '#money');
				return false;
		}
		if(!jine){
			    layer.tips('金额不能为空!', '#jine');
				return false;
		}
		if(!validate){
			    layer.tips('验证码不能为空!', '#validate');
				return false;
		}
		
		$.post("action.php?type=zhuanzhang",{mtid:mtid,mmt4:mmt4,jine:jine,validate:validate},function(data,status){
			if(typeof data=="object"){
				alert("提交成功");
				location.reload();
			}else if(data=="invalidate_jine"){
				alert("金额不能为空");
			}else if(data=="invalidate_jine_number"){
				alert("金额不正确");
			}else if(data=="invalidate_money"){
				alert("余额不足");
			}else if(data=="invalidate_current"){
				alert("MT4存在持仓单不允许转账");
				
			}else if(data=="invalidate_mmt4"){
				alert("MT4不存在");
				
			}else if(data=="invalidate_validate"){
				alert("验证码不正确");
				
			}else{
				alert("提交失败");
			}
				$("#tx-btn").removeAttr("disabled");
				$("#tx-btn").text("提交");
		},'json');
	 
		return false;
	});
});

function getYue(){
	
	var mtid=$("#mtid").val();
	$.post("action.php?type=getyue",{mtid:mtid},function(data,status){
			if(typeof data=="object"){
				$("#money").text(data.yue);
			}
		},'json');
}
 getYue();
 
 
 
  $('#send_msg').click(function(){//发送短信

	
	$.ajax({
         url: "/mail/send_validate.php?type=sendmsg_zhuanzhang",  
         type: "POST",
         data:{},
         //dataType: "json",
         error: function(){  
             alert('Error loading XML document');  
         },  
         success: function(data,status){ 
		
			if(data=='success'){
				timeCount=setInterval("waitMsg()",1000);
				alert('发送成功');
			}else if(data=='invalidate_count'){
				alert('短信超出限度');
			}else{
				alert('发送失败');
			}
         }
     }); 
});	
	
WAIT=60;
function waitMsg(){
	
	$('#send_msg').text(WAIT+'秒后重新发送');
	$('#send_msg').attr('disabled','disabled');
	WAIT--;
	if(WAIT==0){
		clearInterval(timeCount);
		WAIT=60;
		$('#send_msg').text('获取验证码');
		$('#send_msg').removeAttr('disabled');
	}
}
</script>
</body>
</html>
