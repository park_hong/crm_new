<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>填写mt4</title>
<link type="text/css" href="style/css.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>
  <!-- Include these three JS files: -->
<!--socoket start-->
  <script type="text/javascript" src="/js/swfobject.js"></script>
  <script type="text/javascript" src="/js/web_socket.js"></script>
  <script type="text/javascript" src="/js/json.js"></script>
<script type="text/javascript" src="/static/websocket_config.js"  media="all"></script>

</head>

<body style="background:#f0f2f5;">

    <?php include_once "../head.php"; ?>
    
	<div class="hy_main w1200">
    	<?php include_once "left.php";


		?>
        <div class="hy_r" >
        	<div class="bt">
            	<h2><span>填写MT4</span></h2>
            </div>
	<?php 
	
	if($_GET[mtid]){
		$mt4=$res->fn_select("select * from mt4 where mtid='$_GET[mtid]' and uid='$u[uid]'");
	}
	?>
			<input type="hidden" name="mtid" id="mtid" value="<?=$mt4[mtid]?>" />
            <div class="tk">
            	
                <div class="xm">"交易密码"仅仅是用于将MT4帐户与网站账户进行关联以便获得订单推送，『世纪集团金业』会严格保密，请放心提交<span><a href="http://user.heroli.com/v2.0/common/register.html?code=366YQ" style="background:#f00;margin:0 30px;color:#fff;border:none;padding:12px 15px;border-radius:5px;" target="_blank">没有帐号？立即开户>></a></span></div>
                <ul>
                	<li><span>MT4经纪商:</span>
						<select id="server-input"><option value="">请选择经纪商</option><option value="GCG">通用</option></select>
					</li>
                    <li><span>MT4服务器:</span>
<select id="ip-input">
	<?php
	$rs=$res->fn_sql("select * from server  where status=1 and svid=110 order by ip asc ");
	while($server=mysql_fetch_array($rs)){
	?>
	<option value="<?=$server[svid]?>" data-ip="<?=$server[ip]?>" data-symbol="<?=$server[field]?>"  <?php if($server[svid]==$mt4[svid]){echo 'selected="selected"';} ?>><?=$server[ip]?></option>

	<?php } ?>
</select>
					</li>
                    <li><span>MT4帐号：</span><input type="text" placeholder="请输入MT4账号" id="mt4-input" value="<?=$mt4[mt4]?>"/></li>
                    <li><span>MT4登录密码：</span><input type="password" placeholder="请输入交易密码" id="password-input"  value="<?=$mt4[password]?>"/></li>

                </ul>
            </div>
            
            <div class="tk_btn">
            	<button id="mt4-submit">添加用户</button>
            </div>
			
        </div>
    </div>
 <script>

	
$(function(){
//	init();
	$("#mt4-submit").click(function(){
		var mtid=$("#mtid").val();
		var login=$("#mt4-input").val();
		var password=$("#password-input").val();
		var svid=$("#ip-input").val();	
	
		if(!login){
			    layer.tips('MT4账户不能为空!', '#mt4-input');
				return false;
		}
		if(!password){
			    layer.tips('交易密码不能为空!', '#password-input');
				return false;
		}
		
		var mt4ip=$("#ip-input option:selected").attr('data-ip');
		
		var mt4symbol=$("#ip-input option:selected").attr('data-symbol');
		mt4symbol=mt4symbol?mt4symbol:'';
		$.get("action.php?type=addmt4",{mtid:mtid,svid:svid,login:login,password:password},function(msg,status){
			if(msg=="success"){
				alert("提交成功,请等待管理员审核");
				location.reload();
			}else if(msg=="invalidate_mt4"){
				alert("MT4已绑定");
			}else if(msg == "invalidate_yue"){
				alert("余额小于100美金");
			}else if(msg == "invalidate_password"){
				alert("密码不正确");
			}
		});
		
	/*	$.get("action.php?type=checkmt4",{mt4:login},function(data,status){
			if(data=="success"){
				$(this).attr("disabled",true).text("提交中...(35)");
				 layer.load(0, {shade: [0.4,'#eee']});
				var data=JSON.stringify({"type":"validateuser","login":login,"password":password,"ip":mt4ip,"symbol":mt4symbol,"port":20000});
				console.log(data);
				ws.send(data);
				timeCount=setInterval("waitMsg()",1000);
			}else if(data=="invalidate_mt4"){
				alert("mt4已绑定");
			}
		
		});*/
	 
		
	});
});

WAIT=35;
function waitMsg(){
	
	$('#mt4-submit').text("提交中...("+WAIT+")");

	WAIT--;
	if(WAIT==0){
		clearInterval(timeCount);
		WAIT=35;
		$('#mt4-submit').text('获取验证码');
		$('#mt4-submit').removeAttr('disabled').text('提交');
	}
}
 
</script>   
    <?php include_once "../foot.php"; ?>
</body>
</html>
