<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>入金记录</title>
	<link rel="stylesheet" href="./css/bootstrap.css">	
	<link rel="stylesheet" href="./style/css.css">
	<link rel="stylesheet" href="./css/css.css">
<link type="text/css" href="/style/media.css" rel="stylesheet" />
<link type="text/css" href="style/media.css" rel="stylesheet" />
	<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="/js/layer/layer.js"></script>
</head>
<body style="background:#f3f3f3;overflow-x:hidden;">
<!-- head  started-->
<div class="w100">
	<!-- head end -->
	<?php include_once "./left.php";?>
    <?php include_once "./head.php"; ?>
	<!-- body  started-->


	<!-- left started-->
    	
	<!-- left end-->
	
	<!-- 外汇跟单 started-->
	 <div class="padding-md">
					<div class="md_lf"><h2>入金记录</h2></div>
                  
                  
			</div>
		<div class="main-container">
		<div class="jl">
			<table cellpadding="0" cellspacing="0" class="table striped">
							<tr>
								<th>MT4账户</th>
								<th>用户名</th>
								<th>金额</th>
								<th>时间</th>
								<th>状态</th>
							</tr>
						<?php 
	if($_GET[page]){
	$page=$_GET[page];
}else{
	$page=1;
}	$qian=($page-1)*10;

$uids=$res->fn_select("select getChildList($u[uid]) uids ");
$uids_arr = array_slice(explode(",",$uids[uids]),2);
$uids_str = "t1.uid = '$u[uid]'";
foreach($uids_arr as $uid){
	$uids_str .= " or t1.uid = '$uid'";
}

 
		$tixians=$res->fn_rows("select t1.*,t2.nickname,t3.mt4  from rujin t1 left join users t2 on t1.uid=t2.uid left join mt4 t3 on t1.mtid=t3.mtid where   ($uids_str) and t1.status=1 order by t1.rid desc limit $qian,10");

						foreach($tixians as $tixian){ ?>
							<tr >
								<td><?=$tixian[mt4]?></td>
								<td><?=$tixian[nickname]?></td>
								<td><?=$tixian[jine]?></td>
							
								<td><?=date("Y-m-d H:i:s",$tixian[time])?></td>
			
								<td><?=$tixian[paystatus]?'<font color="#af9350">入账成功</font>':'<font color="#af9350">已付款等待审核入账</font>'?></td>
								
							</tr>
						<?php } ?>
						</table>
						</div>
					 <div class="ym">				
		<ul class="pagination">
     <?php
	 
   $sql2="select t1.*,t2.nickname,t3.mt4  from rujin t1 left join users t2 on t1.uid=t2.uid left join mt4 t3 on t1.mtid=t3.mtid where   ($uids_str) and t1.status=1 ";
  
 $num=$res->fn_num($sql2);
$ye=ceil($num/10);
?>
 <li> <a href="rujinjilu.php?page=1" >首页</a></li>

<?php
if($page<5){
	$iiikaishi=1;
	if($ye-$page>5){
		$iiijishu=5;
	}else{
			$iiijishu=$ye;
	}
}elseif($ye-$page<5){
	$iiikaishi=$ye-5;
	$iiijishu=$ye;
}else{
	$iiikaishi=$page-2;
	$iiijishu=$page+2;
}

for($iii=$iiikaishi;$iii<=$iiijishu;$iii++){
?>
 <li  <?php if($page==$iii){echo 'class="active"';}?>> <a href="rujinjilu.php?page=<?=$iii?>" ><?=$iii?></a></li>
<?php
}
?>
<li >  <a href="rujinjilu.php?page=<?=$ye?>" >尾页</a></li>		
				</ul>

</div>				
	</div></div>
<script>
  
$(function(){

	$("#tx-btn").click(function(){
	
		var mt4=$("#mt4").val();
		var jine=$("#jine").val();
		var bank=$("#bank").val();
		var bankaddress=$("#bankaddress").val();
		var name=$("#name").val();
		var account=$("#account").val();
	
		if(!mt4){
			    layer.tips('MT4不能为空!', '#mt4');
				return false;
		}
		if(!jine){
			    layer.tips('金额不能为空!', '#jine');
				return false;
		}
		if(!bank){
			    layer.tips('收款银行名称不能为空!', '#bank');
				return false;
		}
		if(!bankaddress){
			    layer.tips('支出银行地址不能为空!', '#bankaddress');
				return false;
		}
		if(!name){
			    layer.tips('收款人名称（接收方）不能为空!', '#name');
				return false;
		}
		if(!account){
			    layer.tips('收款人账号不能为空!', '#account');
				return false;
		}
		
		
		$.post("action.php?type=tixian",{mt4:mt4,jine:jine,bank:bank,bankaddress:bankaddress,name:name,account:account},function(data,status){
			if(data=="success"){
				alert("提交成功");
				window.location.reload();
			}else if(data=="invalidate_jine"){
				alert("金额不能为空");
			}else if(data=="invalidate_jine_number"){
				alert("金额不正确");
			}else if(data=="invalidate_account"){
				alert("收款银行名称为空");
			}else if(data=="invalidate_name"){
				alert("接收方不能为空");
			}else if(data=="invalidate_money"){
				alert("余额不足");
			}else{
				alert("提交失败");
			}
		
		});
	 
		
	});
});


 
</script>
</body>
</html>