<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>外汇跟单</title>
	<link type="text/css" href="style/css.css" rel="stylesheet" />
	<link rel="stylesheet" href="./css/bootstrap.css">
	<link rel="stylesheet" href="./css/css.css">
<link type="text/css" href="/style/media.css" rel="stylesheet" />
<link type="text/css" href="style/media.css" rel="stylesheet" />
	<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
	<style>
		@media screen and (max-width: 750px) {
			a.btn{margin:0;}
		}
	</style>
</head>
<body style="background:#EEF3FA;overflow-x:hidden;">
<!-- head  started-->
<div class="w100">
	
	<!-- head end -->
    <?php include_once "../head.php"; ?>
	<!-- body  started-->


	<!-- left started-->
	<?php include_once "left.php"; 
		
		$mt4s=$res->fn_rows("select t1.mt4,t2.*,t4.nickname xinhaoyuan from mt4 t1 left join mt4_contact t2 on t1.mt4=t2.mt4 left join mt4 t3 on t2.mmt4=t3.mt4 left join users t4 on t3.uid=t4.uid where t2.mt4!='' and t1.uid='$u[uid]'");
		?>
	<!-- left end-->
	
	<!-- right started-->
		<div class="right">
			<h3>外汇跟单</h3>
			<?php if($mt4s) { ?>
			 <div class="star_list">
            
						<table cellpadding="0" cellspacing="0">
							<tr>
								<th>MT4账户</th>
								<th>信号源</th>
								<th>跟单比例</th>
								<th>最大跟单单量</th>
								<th>最大交易手数</th>
								<th>最小交易手数</th>
								<th>强制止盈点数</th>
								<th>强制止损点数</th>
								<th>反向跟单</th>
								<th style="text-align:left;">操作</th>
							</tr>
						<?php 
					
				
						foreach($mt4s as $mt4){ ?>
							<tr >
								<td><?=$mt4[mt4]?></td>
								<td><?=$mt4[xinhaoyuan]?></td>
								<td><?=$mt4[percent]?>%</td>
								<td><?=$mt4[maxgendan]?></td>
								<td><?=$mt4[maxvolume]?></td>
								<td><?=$mt4[minvolume]?></td>
								<td><?=$mt4[takeprofit]?></td>
								<td><?=$mt4[stoploss]?></td>
								<td><?=$mt4[isreverse]?'是':'否'?></td>
								<td style="text-align:left;">
									<a href="javascript:;" onclick="mt4Delete('<?=$mt4[mcid]?>')">解除绑定</a>
								</td>
							</tr>
						<?php }?>
						</table>
			</div>
			<?php } ?>		
			<p>在这里,您可以看到您的跟单情况。在此之前，您需要选择一个信号源跟随。<a class="btn btn-default" href="/gendan.php">信号源列表</a></p>

	<?php if(!$mt4s) { ?>
			<ul class="row waihui">
				<li class="col-lg-3">
					<img src="./images/waihui1.jpg" alt="">
					<h4>跟一个或多个账号</h4>
					<hr>
					<p>绑定一个用户跟单MT4账户,该账户不限经纪商。可以是真实账户，也可以是模拟账户。</p>
				</li>
				<li class="col-lg-3">
					<img src="./images/waihui2.jpg" alt="">
					<h4>选择要跟随的策略</h4>
					<hr>
					<p>在我们合作的策略师中，任意选择一名或者多名策略师并跟随他们。</p>
				</li>
				<li class="col-lg-3">
					<img src="./images/waihui3.jpg" alt="">
					<h4>开始跟单了！</h4>
					<hr>
					<p>跟随后，您的MT4就会自动接收来自策略师的交易信号。您可以完全关闭电脑，因为整个跟单过程都由系统完成，完全不需要您亲自过问。</p>
				</li>
			</ul>
			 <?php } ?>
			<div class="clear"></div>
		</div>
	<!-- right end-->
	<div class="clear"></div>
	<!-- body end-->

</div>

  <script>
  function mt4Delete(mcid){
		if(!window.confirm('确认解除操作吗？')){ return false;}
		$.get("action.php?type=deletemt4contact",{mcid:mcid},function(data,status){
			if(data=='success'){
				window.location.reload();
			}
		});
	
	}
 
</script>   
</body>
</html>