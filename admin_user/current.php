<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>baihueigd</title>
<link rel="stylesheet" href="./css/bootstrap.css">	
<link rel="stylesheet" href="./css/css.css">
<link type="text/css" href="/style/media.css" rel="stylesheet" />
<link type="text/css" href="style/media.css" rel="stylesheet" />
<link rel="stylesheet" href="/js/layui/dist/css/layui.css"  media="all">
<link type="text/css" href="style/css.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>
<script src="/js/layui/dist/layui.js" charset="utf-8"></script>
</head>
<style>
.active{
	color: #fff;
	background: #f00;
}
.list ul li a{
	margin-top: 10px;
}
.list ul li i {
    position: relative;
    top: -2px;}
    .list ul li {
    font-size: 18px;
    padding: 3px 20px 8px;}
</style>
<script type="text/javascript">
// $(document).ready(function(){ 
//    $('.pagination').children('li').click(function(){
//    	$(this).css('background','#f00');
//    	$(this).children('a').css('color','#fff');
//     })
//  });
</script>
<body style="background:#f0f2f5;">

    <?php 
	
	include_once "../head.php"; ?>
    <?php include_once "left.php";?>
    <div class="right">
		<h3 style="padding-left:72px;">直推下级总持仓记录（含自己账户）</h3>
	
<?php

	
if($_GET[search]!= null){
	$sqlxs.=" and (t1.Ticket like '%$_GET[search]%' || t2.mt4 like '%$_GET[search]%' )";
	$aplus.="&search=$_GET[search]";
}

if($_GET['reservation']!= null){
		$reservation=explode("-",$_GET['reservation']);
		$start_time=strtotime($reservation[0]);
		$end_time=strtotime($reservation[1]);
		 $sqlxs.=" and t1.CloseTime>'$start_time' and t1.CloseTime<'$end_time'";
		$aplus.="&reservation=$_GET[reservation]";
}

$uids=$res->fn_select("select getChildList($u[uid]) uids ");
$uids_arr = array_slice(explode(",",$uids[uids]),2);
$uids_str = "t1.uid = '$u[uid]'";
foreach($uids_arr as $uid){
	$uids_str .= " or t1.uid = '$uid'";
}

$mt4s = $res->fn_rows("select DISTINCT(t1.mt4) mt4,t2.nickname from mt4 t1 left join users t2 on t1.uid=t2.uid where 1=1 and ($uids_str) ");
		
?>		
        
			 <!-- <div class="star_list"> -->
						<table cellpadding="0" cellspacing="0" class="table striped" id="current-table">
						<thead>
							<tr>
								<th>单号</th>
								<th>MT4</th>
								<th>用户</th>
								<th>品种</th>
						
								<th>时间</th>							
								<th>手数</th>
								<th>类别</th>
								<th>价位</th>
								<th>止盈/止损</th>
								<th>盈利</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						</table>
	

</nav>				
						
			</div>
	<div class="clear"></div>
	</div>
 <!--socoket start-->
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/web_socket.js"></script>
<script type="text/javascript" src="/js/json.js"></script>
<script type="text/javascript" src="/static/websocket_config.js"  media="all"></script> 
<script>
	var MT4S = [<?php foreach($mt4s as $mt4 ) { echo "['$mt4[mt4]','$mt4[nickname]']".",";} ?>];
	console.log(MT4S);
	

	function queryInfo(){
		if(MT4S.length==0){return false}
		var mt4=MT4S.shift();
		ws.send('{"type":"current","key":"'+mt4[0]+'","beizhu":"'+mt4[1]+'","token":"pong"}');
	}
	
	
	
 function init() {
		ws = new WebSocket("ws://"+WS_HOST2+":"+WS_PORT2);
		ws.onopen = function() {
    	    timeid && window.clearTimeout(timeid);
		   	console.log("连接成功");
			queryInfo();
      };
		if(reconnect>6){
			window.clearTimeout(timeid);
		}
		ws.onmessage = function(e) {
				console.log("收到数据："+e.data);
				var data = JSON.parse(e.data);
				switch(data['type']){
	
					
					case 'current':
						
						var html='';	
						var operation={0:"BUY",1:"SELL",2:"BUY LIMIT",3:"SELL LIMIT",4:"BUY STOP",5:"SELL STOP"};
						var nickname = data['beizhu'];
						for(i in data['data']){ 
						
							var order = data['data'][i];
							
							html+='       <tr >'
								+'			<td>'+order.order+'</td>'
								+'			<td>'+order.login+'</td>'
								+'			<td>'+nickname+'</td>'
								+'			<td>'+order.symbol+'</td>'
								+'			<td>'+(new Date(parseInt(order.opentime-3600*8) * 1000).format("yyyy-MM-dd  HH:mm:ss"))+'</td>'
								+'			<td>'+(order.volume/10000).toFixed(2)+'</td>'
								+'			<td>'+operation[order.cmd]+'</td>'
								+'			<td>'+order.openprice+'</td>'
								+'			<td>'+order.sl+'/'+order.tp+'</td>'
								+'			<td>'+order.profit.toFixed(2)+'</td>'
                                +'        </tr>';	
						}
						$("#current-table tbody").append(html);
						queryInfo();
					break;
				

				}
		
		};
	  ws.onclose = function() {
		  reconnect ++;
    	  console.log("连接关闭，定时重连");
    	  timeid = window.setTimeout(init, 3000);
      };
      ws.onerror = function() {
			reconnect ++;
    	    console.log("出现错误");
		    timeid = window.setTimeout(init, 3000);
      };
	}
	
	init();

layui.use('laydate', function(){
  var laydate = layui.laydate;
   laydate.render({
    elem: '#reservation'
    ,type: 'datetime'
    ,range: true
	,format:'yyyy/MM/dd HH:mm:ss'
  });
 });	
 $(".active").css("color","#fff");
 
 	Date.prototype.format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月           
        "d+": this.getDate(), //日           
        "h+": this.getHours() % 12 == 0 ? 12 : this.getHours() % 12, //时           
        "H+": this.getHours(), //小时           
        "m+": this.getMinutes(), //分           
        "s+": this.getSeconds(), //秒           
        "q+": Math.floor((this.getMonth() + 3) / 3), //季          
        "S": this.getMilliseconds() //毫秒           
    };
    var week = {
        "0": "\u65e5",
        "1": "\u4e00",
        "2": "\u4e8c",
        "3": "\u4e09",
        "4": "\u56db",
        "5": "\u4e94",
        "6": "\u516d"
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    if (/(E+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, ((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "\u661f\u671f" : "\u5468") : "") + week[this.getDay() + ""]);
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return fmt;
}
</script>
</body>
</html>
