<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>出金记录</title>
<link rel="stylesheet" href="./css/bootstrap.css">	
<link rel="stylesheet" href="./css/css.css">
<link type="text/css" href="style/css.css" rel="stylesheet" />
<link type="text/css" href="/style/media.css" rel="stylesheet" />
<link type="text/css" href="style/media.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>

</head>

<body style="background:#f0f2f5;">


    	<?php include_once "left.php";

		?>
		<style>
       .main-container {
margin-left:239px;}
       </style>	
       <div class="main-container">
				
			<?php  include("./head.php") ?>
            <div class="padding-md">
					<div class="md_lf"><h2>出金记录</h2></div>
                  
                  
			</div>
            
            <div class="jl">
            	<table cellpadding="0" cellspacing="0">
                	<tr>
								<th>MT4账户</th>
								<th>金额</th>
								<th>收款银行</th>
								<th>收款银行地址</th>
								<th>收款人</th>
								<th>收款人账号</th>
								<th>时间</th>
								<th>状态</th>
                    </tr>
                 <?php 
	if($_GET[page]){
	$page=$_GET[page];
}else{
	$page=1;
}	$qian=($page-1)*10;

		$tixians=$res->fn_rows("select t1.*,t2.nickname,t3.mt4  from tixian t1 left join users t2 on t1.uid=t2.uid left join mt4 t3 on t1.mtid=t3.mtid where  t1.uid='$u[uid]'  order by tid desc  limit $qian,10");
$sum=$res->fn_select("select  count(*) num ,sum(t1.jine) money from tixian t1  where 1=1  and   t1.uid='$u[uid]' and t1.status=1 $sqlxs  ");
						foreach($tixians as $tixian){ ?>
							<tr >
								<tr >
								<td><?=$tixian[mt4]?></td>
								<td><?=$tixian[jine]?></td>
								<td><?=$tixian[bank]?></td>
								<td><?=$tixian[bankaddress]?></td>
								<td><?=$tixian[name]?></td>
								<td><?=$tixian[account]?></td>
								<td><?=date("Y-m-d H:i:s",$tixian[time])?></td>
			
								<td><?=$tixian[status]==1?'<font color="#038627">申请成功</font>':($tixian[status]==-1?'<font color="#C13737">已驳回</font>':'<font color="#f00">申请中</font>')?></td>
								
							</tr>
								
							</tr>
						<?php } ?>
                </table>
				
				<p style="width:425px;padding: 0px 0px 10px;font-size: 13px;position: relative;top:12px;left:0px;color:#000;">合计：<?=$sum[num]?$sum[num]:0?> 笔数： 总出金：$<?=round($sum[money]?$sum[money]:0,2)?></p>	
            </div>
           
            <div class="ym">
            	<ul class="pagination">
     <?php
	 
  $sql2="select * from rujin  where 1=1  and status=1  and uid='$u[uid]'";
  
$num=$res->fn_num($sql2);
$ye=(int)($num/10+1);
?>
 <li> <a href="tixianjilu.php?page=1" >首页</a></li>

<?php
if($page<5){
	$iiikaishi=1;
	if($ye-$page>5){
		$iiijishu=5;
	}else{
			$iiijishu=$ye;
	}
}elseif($ye-$page<5){
	$iiikaishi=$ye-5;
	$iiijishu=$ye;
}else{
	$iiikaishi=$page-2;
	$iiijishu=$page+2;
}

for($iii=$iiikaishi;$iii<=$iiijishu;$iii++){
?>
 <li  <?php if($page==$iii){echo 'class="active"';}?>> <a href="tixianjilu.php?page=<?=$iii?>" ><?=$iii?></a></li>
<?php
}
?>
<li >  <a href="tixianjilu.php?page=<?=$ye?>" >尾页</a></li>		
            </div>
            
            
            
		</div>
<script>
  
$(function(){

	$("#tx-btn").click(function(){
	
		var mt4=$("#mt4").val();
		var jine=$("#jine").val();
		var bank=$("#bank").val();
		var bankaddress=$("#bankaddress").val();
		var name=$("#name").val();
		var account=$("#account").val();
	
		if(!mt4){
			    layer.tips('MT4不能为空!', '#mt4');
				return false;
		}
		if(!jine){
			    layer.tips('金额不能为空!', '#jine');
				return false;
		}
		if(!bank){
			    layer.tips('收款银行名称不能为空!', '#bank');
				return false;
		}
		if(!bankaddress){
			    layer.tips('支出银行地址不能为空!', '#bankaddress');
				return false;
		}
		if(!name){
			    layer.tips('收款人名称（接收方）不能为空!', '#name');
				return false;
		}
		if(!account){
			    layer.tips('收款人账号不能为空!', '#account');
				return false;
		}
		
		
		$.post("action.php?type=tixian",{mt4:mt4,jine:jine,bank:bank,bankaddress:bankaddress,name:name,account:account},function(data,status){
			if(data=="success"){
				alert("提交成功");
				window.location.reload();
			}else if(data=="invalidate_jine"){
				alert("金额不能为空");
			}else if(data=="invalidate_jine_number"){
				alert("金额不正确");
			}else if(data=="invalidate_account"){
				alert("收款银行名称为空");
			}else if(data=="invalidate_name"){
				alert("接收方不能为空");
			}else if(data=="invalidate_money"){
				alert("余额不足");
			}else{
				alert("提交失败");
			}
		
		});
	 
		
	});
});


 
</script>
</body>
</html>
