<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>个人设置</title>
    <link type="text/css" href="style/css.css" rel="stylesheet" />
    <link rel="stylesheet" href="./css/bootstrap.css">  
    <link rel="stylesheet" href="./css/css.css">
    <link type="text/css" href="/style/media.css" rel="stylesheet" />
    <link type="text/css" href="style/media.css" rel="stylesheet" />
    <link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/js/layer/layer.js"></script>
    <style>
    h4{
        font-size: 16px;
        color:#666;
    }

/*.file {
    background: #efac1a;
    border-radius: 3px;
    padding: 10px 16px;
    color: #fff;
    text-decoration: none;
    font-size: 14px;
    display: inline-block;
    width: 88px;
    height: 37px;
    position: relative;
    top: 0px;
}
.file input {
    position: absolute;
    font-size: 1px;
    right: 0; 
    top: 0;
    opacity: 0;
    width: 1px;
}
.file:hover {
    background:#d49713;
    color: #fff;
    text-decoration: none;
}*/
    </style>
</head>
<body style="background:#f3f3f3;overflow-x:hidden;">
<!-- head  started-->
<div class="w100">
    <?php include_once "left.php"; ?> 
    <!-- head end -->

    <!-- body  started-->


    <!-- left started-->
       <?php include_once "./head.php";
$idcard=$res->fn_select("select * from idcard where uid='$u[uid]'");
    ?>
    <!-- left end-->
    
    <!-- right started-->
     
          <div class="main-container">
	
            <div class="padding-md">
					<div class="md_lf"><h2>个人设置</h2></div>
                  
                  
			</div>	
            
           <div class="gr" style="margin-left:3.8%;width:80%">
           		<div class="gr_lf">昵称:<span><?=$u[nickname]?></span></div>
           		<div class="gr_mid">真实姓名:<span><?=$idcard[realname]?></span></div>
                <div class="gr_fr">创建时间:<span><?=date("Y-m-d H:i:s",$u[regtime])?></span></div>
           </div>
           
           <div class="sz">
                <ul>
                    <li>
                    	<h3>登录密码</h3>
                        <P>登录账户时需要输入的密码</P>
                        <a href="editpwd.php">修改密码</a> 
                    </li>
					<!--
                    <li>
                    	<h3>交易密码</h3>
                        <P>交易账户时需要输入的交易密码</P>
                        <a href="edittradepwd.php">修改密码</a>
                    </li>
					
					-->
                    <li>
                    	<h3>绑定邮箱：<?=$u[email]?></h3>
                        <P>绑定邮箱可以当用户名登录，接收该系统的短信通知以及一些安全验证</P>
                      <!--  <a href="editemail.php">修改邮箱</a>  -->
                    </li>
                    <li>
                    	<h3>银行卡号：<?=$idcard[bankcardnum]?></h3>
                        <P>出金的收款银行</P>
                        <!--<a href="get_email_validate.php?ac=editidcard">修改银行卡</a>-->
                    </li>
                   <li>
                                <h4>实名认证</h4>
                                <P>为了您的账户与资金安全，个人身份证信息、MT4账号须同名</P>
                                <?php 
        if($idcard[idstatus]==null || $idcard[idstatus]==-1){ ?>   
              <a href="blindidcard.php">点击上传身份证信息</a>
        <?php }else if($idcard[idstatus]==0){ ?>
           <div class="tk_btn">
                <div class="list_a"><span style="color:#3583df;">待审核<span></div>
            </div>
        <?php }else{ ?> 
                <div class="list_a"><span style="color:#3583df;">已认证<span></div>
        <?php } ?>
                                <div class="clear"></div>
                            </li>
                </ul>
           </div>
            
            
		</div>
       
    <!-- right end-->
    <div class="clear"></div>
    <!-- body end-->

</div>
</body>
</html>