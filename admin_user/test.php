<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title>baihueigd</title>
    </head>
    
    <body>

     	<!-- document.getElementsByTagName("input");//获取标签名 -->
     	<!-- document.getElementsByName("input");//获取class名 -->
        <!-- <form>
          请选择你爱好:<br>
          <input type="checkbox" name="hobby" id="hobby1">  音乐
          <input type="checkbox" name="hobby" id="hobby2">  登山
          <input type="checkbox" name="hobby" id="hobby3">  游泳
          <input type="checkbox" name="hobby" id="hobby4">  阅读
          <input type="checkbox" name="hobby" id="hobby5">  打球
          <input type="checkbox" name="hobby" id="hobby6">  跑步 <br>
          <input type="button" value = "全选" onclick = "checkall();">
          <input type="button" value = "全不选" onclick = "clearall();">
          <p>请输入您要选择爱好的序号，序号为1-6:</p>
          <input id="wb" name="wb" type="text" >
          <input name="ok" type="button" value="确定" onclick = "checkone();">
        </form> -->
    <!--     <script type="text/javascript">
            function checkall(){
                var hobby = document.getElementsByTagName("input");//
                for(var i=0;i<hobby.length&&hobby[i].type=="checkbox";i++){
    					hobby[i].checked=true; 
    				}
               
            }
            function clearall(){
                var hobby = document.getElementsByName("hobby");
                for(var i=0;i<hobby.length&&hobby[i].type=="checkbox";i++){
    					hobby[i].checked=false; 
    				}
            }
            
            function checkone(){
               var j=document.getElementById("wb").value;

                var string="hobby"+j;

                var hobby=document.getElementById(string)

                    hobby.checked=true;
    		}


        
		// JavaScript中保留小数点后两位
    // 		var money=0.005426;//0.006;  
		// alert(Number(money).toFixed(5));  
		  
		// var num=22.127456;
		// alert( Math.round(num*100000)/100000);

        </script> -->

	
	<?php 
	$num = 10.4567;
     
   //第一种：利用round()对浮点数进行四舍五入 -->
    echo round($num,6)."<br>"; //10.4567
     
    //第二种：利用sprintf格式化字符串 -->
    $format_num = sprintf("%.6f",$num);
    echo $format_num."<br>"; //10.456700
     
    //第三种：利用千位分组来格式化数字的函数number_format()
    echo number_format($num, 6)."<br>"; //10.456700
    //或者如下
    echo number_format($num, 6, '.', ''); //10.456700

 ?>
  <!-- <?php
   // PHP中保留小数点后两位
    $num = 4999.9;
    $formattedNum = number_format($num)."<br>";
    echo $formattedNum;
    echo '你好';
    $formattedNum = number_format($num, 2);
    echo $formattedNum;
  ?>  -->
    </body>
</html>