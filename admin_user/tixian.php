<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>申请出金</title>
<link rel="stylesheet" href="./css/bootstrap.css">	
<link rel="stylesheet" href="./css/css.css">
<link type="text/css" href="style/css.css" rel="stylesheet" />
<link type="text/css" href="/style/media.css" rel="stylesheet" />
<link type="text/css" href="style/media.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>
<style>
#fenrun-tii{background: #af9350; color: #fff; width: 24px;  height: 24px;  line-height: 24px; display: inline-block;  text-align: center;   border-radius: 12px;}
.tk>ul{
	width: 680px;
	background:#fff;
}
.tk ul li {text-align: left;}
</style>
</head>

<body style="background:#f0f2f5;">


    <?php include_once "left.php";?>

  <?php include_once "./head.php";
	$mt4=$res->fn_select("select t1.mt4 from mt4 t1  where t1.uid='$u[uid]'");
	$idcard=$res->fn_select("select * from idcard t1  where t1.uid='$u[uid]'");
	$baseconfig=$res->fn_select("select * from baseconfig");
		?>
		<div class="padding-md">
					<div class="md_lf"><h2>在线出金</h2></div>
		</div>
       <div class="main-container" style="height:auto;">
	 <div class="rj">
            	<div class="rj_fl" style="color:black">出款时间为1-3个工作天不计申请日与周末，具体时间以银行端作业流程所需时间为准</div>
               &lt; <div class="rj_fr"><a href="tixianjilu.php">查看出金记录</a></div>
            </div>
            <?php if($idcard[idstatus]==1){ ?>

			<div class="zxrj tk" style="padding-top:0">
                <ul>
                <input type="hidden" id="yue" value="" />
				<div style="margin-left:150px"><span style="padding-left:20px; margin-top:5px;font-size:14px;color: #efac1a;" class="kcj">可出金金额:$<b class="yue">0</b></span></div>
                    <li >
						<span>账户：</span>
						<select id="mtid" name="mtid" onchange="getYue()">
		<?php 
		$mt4s=$res->fn_rows("select * from mt4 where uid='$u[uid]' ");
		foreach($mt4s as $mt4){
		?>
			<option value="<?=$mt4[mtid]?>"><?=$mt4[mt4]?></option>
		<?php } ?>
		<?php if($u[groupid]==1){ ?>
				<option value="<?=$u[uid]?>">网站账户</option>
		<?php } ?>
						</select>
					</li>
                    <li>
						<span>金额：</span>
						<input type="text" id="jine"  value="" >(美元,最低金额：<?=$baseconfig[min_chujin]?>)
                    	<a href="javascript:;" id="fenrun-tii" title="按银行实时汇率结算">?</a>
                    </li>

                    <li>
						<span >银行/省市支行：</span>
						<input type="text"  id="bankaddress" value="<?=$idcard[bankcardaddr]?>" disabled="disabled">
					</li>
                    <li>
					<span>收款人名称：</span>
					<input type="text" value="<?=$idcard[realname]?>" disabled="disabled" style="color:#969696"/>
					</li>
                    <li><span>银行卡号：</span>
						<input type="text"  id="account" value="<?=$idcard[bankcardnum]?>" disabled="disabled">
					</li>
					<div class="clear"></div>
					<div class="tk_btn">
            	<button id="tx-btn" class="btn btn-default tyong" style="padding: 7px 40px;">申请出金</button>
            	</div>
            <?php }else{ ?>
			 <div class="tk" >
            	
                <div class="xm" style="    font-size: 20px; text-align: center; padding-bottom: 20px;">
				请完成实名认证再进行出金操作  
				<a href="./blindidcard.php" style="color:#0183a5; padding-left:50px;">去实名认证</a>
				</div>
			</div>
			<?php } ?>
			<div style="font-size:14px;color:#777;margin-top:36px;">
				<!-- <p>1.每月免手续费出金一次，超过一次每笔收取25美元手续费;</p> -->
				<p>1.每笔出金额须大于20美元。</p>
			</div>
                </ul>
			  
			</div>
        </div>
	


<script>

  $("#fenrun-tii").mouseover(function(){
	   layer.tips(this.title, '#fenrun-tii');
  });
 
$(function(){

	$("#tx-btn").click(function(){
		$("#tx-btn").attr("disabled",true);
		$("#tx-btn").text("申请中。。。");
		var mtid=$("#mtid").val();
		var jine=parseFloat($("#jine").val());
		var yue=parseFloat($("#yue").val());
	
		var bankaddress=$("#bankaddress").val();
	//	var name=$("#name").val();
		var account=$("#account").val();
		var accountType=0;  
		
		if(!jine || jine<=20){
			    layer.tips('金额不能小于20!', '#jine');
				return false;
		}
		if(jine>yue){
			    layer.tips('余额不足!', '#jine');
				return false;
		}
		/*if(!bank){
			    layer.tips('收款银行名称不能为空!', '#bank');
				return false;
		}*/
		if(!bankaddress){
			    layer.tips('支出银行地址不能为空!', '#bankaddress');
				return false;
		}
		if(!name){
			//    layer.tips('收款人名称（接收方）不能为空!', '#name');
			//	return false;
		}
		if(!account){
			    layer.tips('收款人账号不能为空!', '#account');
				return false;
		}
		if($("#mtid option:selected").html()=="网站账户"){
			accountType =1; 
		}
		
		$.post("action.php?type=tixian",{mtid:mtid,jine:jine,bankaddress:bankaddress,name:name,account:account,accounttype:accountType},function(data,status){
			if(data=="success"){
				alert("提交成功");
				window.location.reload();
			}else if(data=="invalidate_jine"){
				alert("金额不能小于<?=$baseconfig[min_chujin]?>");
			
			}else if(data=="invalidate_jine_number"){
				alert("出金金额不符合出金条件，请正确填写!");
			
			}else if(data=="invalidate_account"){
				alert("收款银行名称为空");
			
			}else if(data=="invalidate_name"){
				alert("接收方不能为空");
			
			}else if(data=="invalidate_money"){
				alert("余额不足");
			
			}else if(data=="invalidate_current"){
					alert("MT4存在持仓单不允许出金");
			
			}else if(data=="invalidate_time"){
					alert("不在出金时间内");
			
			}else{
				alert("提交失败");
			
			}
			$("#tx-btn").removeAttr("disabled");
			$("#tx-btn").text("申请出金");
		});
	 
		
	});
});


 function getYue(){
	    var mtid=$("#mtid").val();
		if($("#mtid option:selected").html()=="网站账户"){
			$("#yue").val(<?=$u[money]?>);	
			$(".yue").text(<?=$u[money]?>);	
			return ;
		}
	 	$.post("action.php?type=getyue",{mtid:mtid},function(data,status){
			if(typeof data=='object'){
				
				$("#yue").val(data.yue);	
				$(".yue").text(data.yue);	
			}
			
		},'json');
 }
  getYue();
</script>
</body>
</html>
