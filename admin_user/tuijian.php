<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>邀请好友</title>
	<link rel="stylesheet" href="./css/bootstrap.css">	
	<link rel="stylesheet" href="./css/css.css">
  	<link rel="stylesheet" href="share.min.css">
<link type="text/css" href="style/css.css" rel="stylesheet" />
<link type="text/css" href="/style/media.css" rel="stylesheet" />
<link type="text/css" href="style/media.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="./qrcodejs-master/qrcode.js"></script>
<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>
<script src="jquery.share.min.js"></script>
<style type="text/css">
	.wdyq {width: 740px;}
	.tj span {
    color: #fff;
    background: #af9350;
    padding: 4px 20px;
    border-radius: 0;
    display: inline-block;
     float: none; 
    margin-left: 114px;
    margin-right:0px;
}
</style>
</head>
<body style="background:#EEF3FA;overflow-x:hidden;">
<!-- head  started-->
<div class="w100">
	<?php 
	include_once "left.php";
$uids=$res->fn_select("select getChildList($u[uid]) uids ");	    

$count=count(explode(",",$uids[uids]))-2;
?> 
   <?php 
   include_once "./head.php";
   ?> 
		<div class="right">
			<h3><span style="width:134px;">开户邀请连结</span></h3>
			<div class="wdyq">
				<div>
					<div class="tj">
		                <div class="me">我的交易账户：<?=$u['mt4']?> 我的经纪人编号： <?=$u['uid']?></div>
						<span style="opacity: 0;">
							<?php 
								$news=$res->fn_select("select t1.* from news t1 where t1.typeid=11");
							?>
							
						</span>
					</div>
				</div>
				<div>
				  	<?php 
					$mt4_groups=$res->fn_rows("select * from mt4_group where mt4_groupid ='$u[mt4_groupid]' order by mt4_groupid asc ");	
					?>
	                <div>
						<?php foreach($mt4_groups as $mt4_group){ ?>
						<li>
							<span style="padding-left:10px;padding-top:10px;line-height:2.5;color:#0183a5"><?=$mt4_group[groupname]?>__会员开户链接:</span>
							<input type="text" value="http://<?=$_SERVER['HTTP_HOST']?>/register.php?_=<?=$u[uid]?>" style="font-size: 16px;color:#808080;text-align:center;width: 360px;" class="imp" id="biao2"/>	
							<input type="button" onClick="copyUrl2(this)" value="复制" style="padding: 4px 12px;background:#af9350;border:0;color:#fff;"/>
						</li>
						<?php } ?>
					</div>
				</div>
				<div id="share-2"></div>
			    <div class="maright">
			    	<p style="display: inline-block;padding: 10px;">开户二维码：</p>
			        <div id="qrcode" style="width:150px; height:150px; margin: 0px auto;"></div>
			    </div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var qrcode = new QRCode(document.getElementById("qrcode"), {
	    width : 150,
	    height : 150
	});
	function makeCode () {      
	    var elText = document.getElementById("biao2");
	    qrcode.makeCode(elText.value);
	}makeCode();
$(function(){
    $('.tree').on('click','li.parent_li  span[rel]', function (e) { 
	that=this;
	var uid=$(this).attr('rel');

	if($(that).siblings('ul').length>0){
		var children = $(that).siblings('ul');
		if (children.is(":visible")) {
			children.hide('fast');
			$(that).find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
		} else {
			
			children.show('fast');
			$(that).find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
			
		}
		return;
	}
	$.ajax({
        url:"action.php?type=gettuijian",
        type:"get",
        data:{"uid":uid},
        dataType:"json",//返回的数据类型
        success:function(data){
        	console.log(data);
          if(data.length>0){
			$(that).parent().addClass('parent_li');
          	 var html='<ul >';
          	 for(i in data){
          	 	var uinfo=data[i];
				if(uinfo.num>0){
					html+='<li class="parent_li" ><span rel="'+uinfo.uid+'" ><i class="icon-plus-sign"></i>'+uinfo.nickname+'('+uinfo.gname+')</span><span data-uid="'+uinfo.uid+'" data-type=3>入金量</span><span data-uid="'+uinfo.uid+'" data-type=2>订单详细</span></li> ';
				}else{
					html+='<li><span >'+uinfo.nickname+'('+uinfo.gname+')</span> <span data-uid="'+uinfo.uid+'" data-type=1>入金量</span><span data-uid="'+uinfo.uid+'" data-type=2>订单详细</span></li> ';
				}
          	 	
          	 }
          	  html+='</ul>';
			 $(that).parent().append(html);
			$(that).find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
			
			e.stopPropagation();
          }
       
       
		 }
     });
	 
	 
    });

});


	
$('.tree').on('click','li span[data-uid]', function (e) {
	var uid=$(this).attr('data-uid');
	var type=$(this).attr('data-type');
	switch(type){
	
		case '1':
			var url='iframe_rujin.php?uid='+uid;
			layer.open({
					type:2,
					title:'入金量',
					fix: false,
					shadeClose: true,
					maxmin: true,
					area: ['970px','75%'],
					content:url, 
			});
			break;
	
		case '2':
			var url='iframe_danzi.php?uid='+uid;
			layer.open({
					type:2,
					title:'做单记录',
					fix: false,
					shadeClose: true,
					maxmin: true,
					area: ['970px','75%'],
					content:url, 
			});
			break;
		
	}
 });

 function copyUrl2(e)
  {    
		 $(e).siblings("input").select(); // 选择对象
		document.execCommand("Copy");
  }
</script>
</body>
</html>