<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>申请喊单</title>
<link type="text/css" href="style/css.css" rel="stylesheet" />
<link type="text/css" href="css/css.css" rel="stylesheet" />
<link type="text/css" href="/style/media.css" rel="stylesheet" />
<link type="text/css" href="style/media.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>

</head>

<body style="background:#f0f2f5;">

    <?php include_once "../head.php" ;?>
    <?php include_once "left.php" ;?>
   <div class="right" style="height:83%;">
			<h3 style="width:auto">申请喊单</h3>            
            <div class="tk">
                <ul style="background:#fff;">
                	<li><span>MT4服务器：</span><input type="text" id="platform"></li>
                    <li><span>MT4账号：</span><input type="text" id="mt4"></li>
                    <li><span>观摩密码：</span><input type="text" id="mt4pwd"></li>
                    <li><span>交易描述：</span><input type="text" id="style"></li>
                    <li><span>资金量：</span><input type="text" id="zijin"></li>
                    <li><span>交易年限：</span><input type="text" id="cycle"></li>
                    <div class="clear"></div>
            		<button id="save-btn" class="btn btn-default tyong" style="margin:4% 35%;">申请喊单</button>
                </ul>
            </div>
            
            <div class="tk_btn">
            </div>
            
        </div>

	<script>
  
$(function(){

	$("#save-btn").click(function(){
	
		var mt4=$("#mt4").val();
		var platform=$("#platform").val();
		var mt4pwd=$("#mt4pwd").val();
		var style=$("#style").val();
		var zijin=$("#zijin").val();
		var cycle=$("#cycle").val();
		if(!platform){
			    layer.tips('平台不能为空!', '#platform');
				return false;
		}
		if(!mt4){
			    layer.tips('MT4不能为空!', '#mt4');
				return false;
		}
	
		if(!mt4pwd){
			    layer.tips('MT4密码不能为空!', '#mt4pwd');
				return false;
		}
		if(!style){
			    layer.tips('交易风格不能为空!', '#style');
				return false;
		}
		if(!zijin){
			    layer.tips('资金量不能为空!', '#zijin');
				return false;
		}
		if(!cycle){
			    layer.tips('交易周期不能为空!', '#cycle');
				return false;
		}
		
		
		$.post("action.php?type=shenqinghandan",{mt4:mt4,platform:platform,mt4pwd:mt4pwd,style:style,zijin:zijin,cycle:cycle},function(data,status){
			if(data=="success"){
				alert("提交成功");
				window.location.reload();
			}else{
				alert("提交失败");
			}
		
		});
	 
		
	});
});


 
</script>
</body>
</html>