<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>修改密码</title>
<link type="text/css" href="style/css.css" rel="stylesheet" />
<link rel="stylesheet" href="./css/css.css">
<link type="text/css" href="/style/media.css" rel="stylesheet" />
<link type="text/css" href="style/media.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>
<style>
.hy_r {
	background: #fff;
    margin: 55px 258px;
}
</style>
</head>

<body style="background:#f0f2f5;">
<?php include_once "left.php"; ?>
    <?php include_once "./head.php"; ?>
    
    		<div class="padding-md">
					<div class="md_lf"><h2 style="font-size: 20px;">修改密码</h2></div>
                  
                  
			</div>
	<div class="main-container">
        
        <div class="hy_r" >
        	<div class="bt">
            	<h2><span>修改密码</span></h2>
            </div>
	
           <div class="members_lr">
                <div class="tx_input">
                	<ul>
                       	<li>原&nbsp;&nbsp;密&nbsp;&nbsp;码：<input type="text" placeholder="请输入原密码" id="password"/></li>
                        <li>新&nbsp;&nbsp;密&nbsp;&nbsp;码：<input type="text" placeholder="请输入新密码" id="npassword"/></li>
                        <li>确认密码：<input type="text" placeholder="请输入确认密码" id="rpassword"/></li>
                 
                    </ul>
                </div>
             <button class="czn" style="float:left;border:0;margin-left: 142px;" id="mt4-submit">提交</button>
			 <span style="float:left;border:none;margin-left:50px;">
				<a href="shezhi.php" class="czn" style="display:inline-block;">返回上一步</a>
			 </span>
     
            </div>
        </div>
        <div class="clear"></div>
    </div>
<script>
  
$(function(){

	$("#mt4-submit").click(function(){
	
		
		var password=$("#password").val();
		var npassword=$("#npassword").val();
		var rpassword=$("#rpassword").val();
	
	
		if(!password){
			    layer.tips('原密码不能为空!', '#password');
				return false;
		}
		if(!npassword){
			    layer.tips('新密码不能为空!', '#npassword');
				return false;
		}
		if(!rpassword){
			    layer.tips('确认密码不能为空!', '#rpassword');
				return false;
		}
		
		if(npassword!=rpassword){
			   layer.tips('新密码和确认密码不一致!', '#rpassword');
				return false;
		}
		
		
		$.get("action.php?type=updatepwd",{password:password,npassword:npassword,rpassword:rpassword},function(data,status){
			if(data=="success"){
				alert("修改成功");
				window.location.reload();
			}else{
				alert("修改失败");
			}
		
		});
	 
		
	});
});


 
</script>
</body>
</html>
