<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>跟随关系</title>

<link type="text/css" href="style/css.css" rel="stylesheet" />
<link type="text/css" href="css/css.css" rel="stylesheet" />
<link type="text/css" href="/style/media.css" rel="stylesheet" />
<link type="text/css" href="style/media.css" rel="stylesheet" />

</head>

<body style="background:#f0f2f5;">

<?php include_once "../head.php";?>    

    	<?php include_once "left.php";?>
    <div class="right">    
        <div class="hy_r" style="width:92%;">
        	
    	<div class="bt">
            	<h2><span>跟随关系</span></h2>
            </div>	

	 <div class="star_list">
            
                <table cellpadding="0" cellspacing="0">
                	<tr>
                    	<th>MT4账户</th>
                        <th>牛人MT4</th>
						<th>所属用户</th>
                    </tr>
				<?php

	$mt4s=$res->fn_rows("select t1.*,t5.nickname sname from mt4_contact t1 left join mt4 t2 on t1.mmt4=t2.mt4 left join users t3 on t2.uid=t3.uid left join mt4 t4 on t1.mt4=t4.mt4 left join users t5 on t4.uid=t5.uid  where t2.uid='$u[uid]' ");
				foreach($mt4s as $mt4){ ?>
                    <tr >
                    	<td><?=$mt4[mt4]?></td>
                        <td><?=$mt4[mmt4]?></td>
						<td><?=$mt4[sname]?></td>
                    </tr>
                <?php }?>
                </table>
            </div>

             </div>
        </div>
    </div>
    
        


<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
$(function(){
    $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
    $('.tree li.parent_li > span').on('click', function (e) {
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
            children.hide('fast');
            $(this).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
        } else {
            children.show('fast');
            $(this).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
        }
        e.stopPropagation();
    });
});
</script>

</body>
</html>