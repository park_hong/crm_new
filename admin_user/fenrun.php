<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>分润记录</title>
 <link rel="stylesheet" href="/js/layui/dist/css/layui.css"  media="all">
<link type="text/css" href="style/css.css" rel="stylesheet" />
<link type="text/css" href="css/css.css" rel="stylesheet" />
<link type="text/css" href="/style/media.css" rel="stylesheet" />
<link type="text/css" href="style/media.css" rel="stylesheet" />
<link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>
<script src="/js/layui/dist/layui.js" charset="utf-8"></script>

</head>

<body style="background:#f0f2f5;">

    <?php include_once "../head.php"; ?>
    
    	<?php include_once "left.php";

		?>
	<div class="right">
        <div class="hy_r" style="width:92%;">
        	<div class="bt">
            	<h2><span>分润记录</span></h2>
            </div>
<div style="margin:10px 0px">
		<form method="get">
			<input type="text" class="form-control" placeholder="订单号/MT4账号" size="12" name="search" value="<?=$_GET['search']?>" style="line-height:24px;padding:0px 10px;margin-right:10px">
			<input type="text" placeholder="平仓时间" class="form-control" id="reservation" name="reservation" style="width:320px;line-height:24px;padding:0px 10px;margin-right:10px" value="<?=$_GET['reservation']?>">
			<button type="submit" class="btn btn-success" style="line-height:22px;padding:0px 10px;">搜索</button>
		</form>
<?php

	
if($_GET[search]!= null){
	$sqlxs.=" and (t2.Ticket like '%$_GET[search]%' || t2.mt4 like '%$_GET[search]%' )";
	$aplus.="&search=$_GET[search]";
}

if($_GET['reservation']!= null){
		$reservation=explode("-",$_GET['reservation']);
		$start_time=strtotime($reservation[0]);
		$end_time=strtotime($reservation[1]);
		 $sqlxs.=" and t2.CloseTime>'$start_time' and t2.CloseTime<'$end_time'";
		$aplus.="&reservation=$_GET[reservation]";
}

$fenrun=$res->fn_select("select  sum(t2.Lots) Lots,  sum(t2.Profit) Profit, sum(t1.money) sum from fenrun t1 left join danzi t2 on t1.did=t2.did  where  t1.status='1' $sqlxs and t1.tuid=$u[uid]");
?>	
<p style="padding: 10px 0px;
    font-size: 16px;
    font-weight: bold; float:right;  margin-top: -36px;
    margin-right: 20px;">总手数：<?=round($fenrun[Lots]?$fenrun[Lots]:0,2)?>手,&nbsp;&nbsp;总获利：<?=round($fenrun[Profit]?$fenrun[Profit]:0,2)?>,&nbsp;&nbsp;总分润：<?=round($fenrun[sum]?$fenrun[sum]:0,2)?></p>
	</div>
			 <div class="star_list">
            
						<table cellpadding="0" cellspacing="0">
							<tr>
								<th>MT4账户</th>
								<th>商品</th>
								<th>订单号</th>
								<th>建仓时间</th>
								<th>平仓时间</th>
								<th>开仓价</th>
								<th>平仓价</th>
								<th>手数</th>
								<th>返佣</th>
							</tr>
						<?php 
$pagesize=10;
if($_GET[pagesize]!= null){
	$pagesize=$_GET[pagesize];
	$aplus="&pagesize=$pagesize";
}

if($_GET[page]){
	$page=$_GET[page];
}else{
	$page=1;
}
$qian=($page-1)*$pagesize;


		$danzis=$res->fn_rows("select t1.*,t2.* from fenrun t1 left join danzi t2 on t1.did=t2.did  where  t1.status='1' and t1.tuid=$u[uid] $sqlxs order by frid desc limit $qian,10");

						foreach($danzis as $danzi){ ?>
							<tr >
			<td><?=$danzi['mt4']?></td>
			<td><?=$danzi['Symbol']?></td>
			<td><?=$danzi['Ticket']?></td>
			<td><?=date("Y-m-d H:i",$danzi['OpenTime'])?></td>
			<td><?=date("Y-m-d H:i",$danzi['CloseTime'])?></td>
			<td><?=$danzi['OpenPrice']?></td>
			<td><?=$danzi['ClosePrice']?></td>
			<td><?=$danzi['Lots']?></td>
			<td><?=$danzi['money']?></td>
								
							</tr>
						<?php } ?>
						</table>
	
		 <div class="ym">				
		<ul class="pagination">
     <?php
	 
  $sql2="select t1.*,t2.* from fenrun t1 left join danzi t2 on t1.did=t2.did  where  t1.status='1' and t1.tuid=$u[uid] $sqlxs ";
  
$num=$res->fn_num($sql2);
$ye=(int)($num/$pagesize+1);
?>
 <li> <a href="fenrun.php?page=1<?=$aplus?>" >首页</a></li>

<?php
if($page<5){
	$iiikaishi=1;
	if($ye-$page>5){
		$iiijishu=5;
	}else{
			$iiijishu=$ye;
	}
}elseif($ye-$page<5){
	$iiikaishi=$ye-5;
	$iiijishu=$ye;
}else{
	$iiikaishi=$page-2;
	$iiijishu=$page+2;
}

for($iii=$iiikaishi;$iii<=$iiijishu;$iii++){
?>
 <li  <?php if($page==$iii){echo 'class="active"';}?>> <a href="fenrun.php?page=<?=$iii?><?=$aplus?>" ><?=$iii?></a></li>
<?php
}
?>
<li >  <a href="fenrun.php?page=<?=$ye?><?=$aplus?>" >尾页</a></li>		
				</ul>

</div>				
						
			</div>
	
			
        </div>
    </div>
<script>
layui.use('laydate', function(){
  var laydate = layui.laydate;
   laydate.render({
    elem: '#reservation'
    ,type: 'datetime'
    ,range: true
	,format:'yyyy/MM/dd HH:mm:ss'
  });
 });		
</script>
</body>
</html>
