<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>找回密码</title>
<link type="text/css" href="style/css.css" rel="stylesheet" />
<link type="text/css" href="style/media.css" rel="stylesheet" />
 <link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>
<script type="text/javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/js/messages_cn.js"></script>  

</head>

<body style="background:#f0f2f5;">
 <?php include_once 'head.php' ; ?>   
    
   <div class="zc">
     	<div style="width:840px;margin:0 auto;">
        	<div class="tit">
            	<h3 style="color:#fff"><i></i>找回密码<i></i></h3>
                
            </div>
            
            <div class="login1">
            	<div class="login1_lf">
            	  <form class="login-form" action="/action.php?type=getpasswd1" method="POST">
                	
					
					<label><span>邮箱</span><input type="text" name="email" id="email" class="email" placeholder="输入您的邮箱"/></label>
					
                     <label><span>验证码</span><input type="text" name="validate" id="validate" class="validate" placeholder="请输入验证码"/ style="width:395px;">
					<button style="width:190px;text-align:center;background:#ff9000;border:none;padding:17px 10px;margin-left:20px;color:#fff;cursor:pointer" id="send_msg">发送邮箱验证码</button>
					</label>
					
					<div class="btn"><button  type="submit" style="width:75%;">提交</button></div>
                 </form>
                
                </div>
           
            </div>
            
        </div>
     </div>
   <script>
   	$(".login-form").validate({
	onfocusout: function(element) { $(element).valid(); },
	rules:{
		email:{ 
			required:true,
			
			remote:{                                          //验证用户名是否存在
               type:"POST",
               url:"/action.php?type=check_email1",          
               data:{
                 email:function(){return $(".login-form .email").val();}
               } 
               
            },

		},
	},
	 messages: {
			username: {
				required: "请输入用户名",
				remote: "用户名不存在",
			},
			password: {
				required: "请输入密码",
				minlength: "密码不能小于5个字符",
			},
			mobile:{
				required: "请输入手机号",
				remote: "手机号不存在",
			}
	},
	submitHandler: function(form) 
   {      
       $(form).ajaxSubmit({success:function(data){
			data=data.replace(/(^\s*)|(\s*$)/g, ""); 
			switch(data){
				
				case 'invalidate_password':
				alert('验证码不正确');
				break;
				case 'success':
				window.location.href="/getpasswd2.php";
				break;
			
			}
			
		}
		});     
	 }  
	});
            	


 $('#send_msg').click(function(){//发送短信


	var email=$('#email').val();
	$.ajax({
         url: "/mail/send_email.php",  
         type: "POST",
         data:{email:email},
         //dataType: "json",
         error: function(){  
             alert('Error loading XML document');  
         },  
         success: function(data,status){ 
		
			if(data=='success'){
				timeCount2=setInterval("waitMsg2()",1000);
				alert('发送成功');
			}else{
				alert('发送失败');
			}
         }
     }); 
});	
	


jQuery.validator.addMethod("isMobile", function(value, element) {  
    var length = value.length;  
    var mobile = /1[34578]{1}\d{9}$/;  
    return this.optional(element) || (length == 11 && mobile.test(value));  
}, "手机号码不正确");  

WAIT=60;
function waitMsg(){
	
	$('#send_msg').text(WAIT+'秒后重新发送');
	$('#send_msg').attr('disabled','disabled');
	WAIT--;
	if(WAIT==0){
		clearInterval(timeCount);
		WAIT=60;
		$('#send_msg').text('获取验证码');
		$('#send_msg').removeAttr('disabled');
	}
}
	
   </script>
</body>
</html>
