<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>找回密码</title>
<link type="text/css" href="style/css.css" rel="stylesheet" />
<link type="text/css" href="style/media.css" rel="stylesheet" />
 <link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>
<script type="text/javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/js/messages_cn.js"></script>  

</head>

<body style="background:#f0f2f5;">
 <?php include_once 'head.php' ; ?>   
    
   <div class="zc">
     	<div style="width:840px;margin:0 auto;">
        	<div class="tit">
            	<h3 style="color:#fff"><i></i>找回密码<i></i></h3>
                
            </div>
            
            <div class="login1">
            	<div class="login1_lf">
            	  <form class="login-form" action="/action.php?type=getpasswd2" method="POST">
                	
					<label><span>新密码</span><input type="password" name="password" id="password" placeholder="输入您的密码"/></label>
                    <label><span>确认新密码</span><input type="password" name="rpassword"  id="rpassword" placeholder="请再次输入您的密码"/></label>
					
					<div class="btn"><button  type="submit" style="width:75%;">提交</button></div>
				
                 </form>
                
                </div>
           
            </div>
            
        </div>
     </div>
   <script>
   	$(".login-form").validate({
	onfocusout: function(element) { $(element).valid(); },
	rules:{
		password: {
			required: true,
			minlength: 5
	    },
		rpassword: {
			required: true,
			minlength: 5,
			equalTo: "#password"
		}
	},
	 messages: {
			password: {
				required: "请输入密码",
				minlength: "密码不能小于5个字符",
			},
			rpassword: {
				required: "请输入确认密码",
				minlength: "确认密码不能小于5个字符",
			   equalTo: "两次输入密码不一致不一致"
			}
	},
	submitHandler: function(form) 
   {      
       $(form).ajaxSubmit({success:function(data){
			data=data.replace(/(^\s*)|(\s*$)/g, ""); 
			switch(data){
				
				case 'success':
					alert('修改成功');
					window.location.href="/index.php";
				break;
			
			}
			
		}
		});     
	 }  
	});


	
   </script>
</body>
</html>
