(function ($) {
	$.fn.extend({
		tab: function (options) {
			var param = {
				tabHead: '.tabBtnBox .tabBtn',
				tabBody: '.tabContBox .tabCont',
				tabHead_hotClass: 'current',
				tabBody_hotClass: 'current',
				event: 'click',
				defaultShow: 1,
				callbackFunc: null
			}
			$.extend(param, options);

			var J_head = $(param.tabHead);
			var J_body = $(param.tabBody);
			var J_headClass = param.tabHead_hotClass;
			var J_bodyClass = param.tabBody_hotClass;

			// 默认显示项
			 var J_index = param.defaultShow;
			 J_head.eq(J_index).addClass(J_headClass);
			 J_body.hide().eq(J_index).addClass(J_bodyClass).show();

			// tab标签切换事件
			J_head.each(function (i) {
				$(this).bind(param.event, function () {
					$(this).addClass(J_headClass).siblings().removeClass(J_headClass);
					J_body.eq(i).show().addClass(J_bodyClass).siblings().removeClass(J_bodyClass).hide();
				});
			})
		}
	});
})(jQuery);