	  <div id="logo">
         <h1>CloudSoft<span>v1.2</span></h1>
        </div>

        <a id="toggle">
            <span class="entypo-menu"></span>
        </a>
        <div class="dark">
            <form action="#">
                <span>
                    <input type="text" name="search" value="" class="search rounded id_search" placeholder="Search Menu..." autofocus="">
                </span>
            </form>
        </div>

        <div class="search-hover hide">
            <form id="demo-2">
                <input type="search" placeholder="Search Menu..." class="id_search">
            </form>
        </div>




        <div class="skin-part">
            <div id="tree-wrap">	
		
		<div class="side-bar">
                    <ul class="topnav menu-left-nest" >
                        <li>
                            <a href="#" style="border-left:0px solid!important;" class="title-menu-left">

                                <i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>

                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip ajax-load" href="#" title="账户管理">
                                <i class="icon-document-edit"></i>
                                <span>账户管理</span>

                            </a>
                            <ul>
                                <li>
                                    <a class="tooltip-tip2 ajax-load" href="baseconfig.php" title="账户信息"><i class="entypo-doc-text"></i><span>账户信息</span></a>
                                </li>
                                <li>
                                    <a class="tooltip-tip2 ajax-load" href="webconfig.php" title="银行卡信息"><i class="entypo-newspaper"></i><span>银行卡信息</span></a>
                                </li>
								 <li>
                                    <a class="tooltip-tip2 ajax-load" href="webconfig.php" title="更换密码"><i class="entypo-newspaper"></i><span>更换密码</span></a>
                                </li>
								 <li>
                                    <a class="tooltip-tip2 ajax-load" href="webconfig.php" title="更换手机"><i class="entypo-newspaper"></i><span>更换手机</span></a>
                                </li>
								 <li>
                                    <a class="tooltip-tip2 ajax-load" href="webconfig.php" title="更换电邮"><i class="entypo-newspaper"></i><span>更换电邮</span></a>
                                </li>
								 <li>
                                    <a class="tooltip-tip2 ajax-load" href="webconfig.php" title="上传头像"><i class="entypo-newspaper"></i><span>上传头像</span></a>
                                </li>
								 <li>
                                    <a class="tooltip-tip2 ajax-load" href="webconfig.php" title="消息公告"><i class="entypo-newspaper"></i><span>消息公告</span></a>
                                </li>
                            </ul>
                        </li>
                     
                        <li>
                            <a class="tooltip-tip ajax-load"  href="#" title="业务管理">
                                <i class="icon-feed"></i>
                                <span>出入金管理</span>
                            </a>
							<ul>
                                <li>
                                    <a class="tooltip-tip2 ajax-load" href="rujinshenhe.php" title="账户入金"><i class="entypo-doc-text"></i><span>账户入金</span></a>
                                </li>
                                <li>
                                    <a class="tooltip-tip2 ajax-load" href="chujinshenhe.php" title="入金记录"><i class="entypo-newspaper"></i><span>入金记录</span></a>
                                </li>
								 <li>
                                    <a class="tooltip-tip2 ajax-load" href="churujinjilu.php".php" title="账户出金"><i class="entypo-mail"></i><span>账户出金</span></a>
                                </li>
							 <li>
                                    <a class="tooltip-tip2 ajax-load" href="churujinjilu.php".php" title="出金记录"><i class="entypo-mail"></i><span>出金记录</span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>

                    <ul class="topnav menu-left-nest"  style="margin:10px">
                        <li>
                            <a href="#" style="border-left:0px solid!important;" class="title-menu-left">
                                <i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>
                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip ajax-load" href="#" title="交易管理">
                                <i class="icon-window"></i>
                                <span>交易明细</span>
                            </a>
							<ul>
                                <li>
                                    <a class="tooltip-tip2 ajax-load" href="tradelist.php" title="当前持仓"><i class="entypo-doc-text"></i><span>当前持仓</span></a>
                                </li>
								 <li>
                                    <a class="tooltip-tip2 ajax-load" href="tradelist.php" title="挂单"><i class="entypo-doc-text"></i><span>挂单</span></a>
                                </li>
								<li>
                                    <a class="tooltip-tip2 ajax-load" href="tradelist.php" title="历史记录"><i class="entypo-doc-text"></i><span>历史记录</span></a>
                                </li>
								
                            </ul>
                        </li>
                        <li>
                            <a class="tooltip-tip ajax-load" href="#" title="代理专区">
                                <i class="icon-mail"></i>
                                <span>代理专区</span>
                            
                            </a>
							<ul>
                                <li>
                                    <a class="tooltip-tip2 ajax-load" href="xitongzhanghu.php" title="交易统计"><i class="entypo-doc-text"></i><span>交易统计</span></a>
                                </li>
								 <li>
                                    <a class="tooltip-tip2 ajax-load" href="jiaoyizhanghu.php" title="下级交易历史"><i class="entypo-doc-text"></i><span>下级交易历史</span></a>
                                </li>
								
								 <li>
                                    <a class="tooltip-tip2 ajax-load" href="bankcard.php" title="下级当前持仓"><i class="entypo-doc-text"></i><span>下级当前持仓</span></a>
                                </li>
								 <li>
                                    <a class="tooltip-tip2 ajax-load" href="bankcard.php" title="下级当前挂单"><i class="entypo-doc-text"></i><span>下级当前挂单</span></a>
                                </li>
								 <li>
                                    <a class="tooltip-tip2 ajax-load" href="bankcard.php" title="推广链接/创建下级"><i class="entypo-doc-text"></i><span>推广链接/创建下级</span></a>
                                </li>
								
                            </ul>
							
                        </li>

                        <li>
                            <a class="tooltip-tip ajax-load" href="#" title="高手跟单">
                                <i class="icon-preview"></i>
                                <span>高手跟单</span>
                             
                            </a>
							<ul>
                                <li>
                                    <a class="tooltip-tip2 ajax-load" href="adminlist.php" title="推荐高手"><i class="entypo-doc-text"></i><span>推荐高手</span></a>
                                </li>
								<li>
                                    <a class="tooltip-tip2 ajax-load" href="adminlist.php" title="关注的高手"><i class="entypo-doc-text"></i><span>关注的高手</span></a>
                                </li>
								<li>
                                    <a class="tooltip-tip2 ajax-load" href="adminlist.php" title="复制的高手"><i class="entypo-doc-text"></i><span>复制的高手</span></a>
                                </li>
								<li>
                                    <a class="tooltip-tip2 ajax-load" href="adminlist.php" title="编辑高手资料"><i class="entypo-doc-text"></i><span>编辑高手资料</span></a>
                                </li>
								<li>
                                    <a class="tooltip-tip2 ajax-load" href="adminlist.php" title="申请/取消成为高手"><i class="entypo-doc-text"></i><span>申请/取消成为高手</span></a>
                                </li>
								<li>
                                    <a class="tooltip-tip2 ajax-load" href="adminlist.php" title="消息"><i class="entypo-doc-text"></i><span>消息</span></a>
                                </li>
                            </ul>
                        </li>

						  <li>
                            <a class="tooltip-tip ajax-load" href="#" title="高手跟单">
                                <i class="icon-preview"></i>
                                <span>二元期权</span>
                             
                            </a>
							<ul>
                                <li>
                                    <a class="tooltip-tip2 ajax-load" href="adminlist.php" title="统计"><i class="entypo-doc-text"></i><span>统计</span></a>
                                </li>
							
                            </ul>
                        </li>
						
                    </ul>

                </div>
     </div>
        </div>