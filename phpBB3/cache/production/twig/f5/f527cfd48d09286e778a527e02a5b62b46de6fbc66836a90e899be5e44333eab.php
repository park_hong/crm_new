<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* acp_reasons.html */
class __TwigTemplate_f8244f1234eff92f514b4aec3e78dd9fe82634ff1d04ca84cabf032ea6e6f3f6 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $location = "overall_header.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("overall_header.html", "acp_reasons.html", 1)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
        // line 2
        echo "
<a id=\"maincontent\"></a>

";
        // line 5
        if ((isset($context["S_EDIT_REASON"]) ? $context["S_EDIT_REASON"] : null)) {
            // line 6
            echo "
\t<a href=\"";
            // line 7
            echo (isset($context["U_BACK"]) ? $context["U_BACK"] : null);
            echo "\" style=\"float: ";
            echo (isset($context["S_CONTENT_FLOW_END"]) ? $context["S_CONTENT_FLOW_END"] : null);
            echo ";\">&laquo; ";
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("BACK");
            echo "</a>

\t<h1>";
            // line 9
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("TITLE");
            echo "</h1>

\t<p>";
            // line 11
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("REASON_EDIT_EXPLAIN");
            echo "</p>

\t";
            // line 13
            if ((isset($context["S_ERROR"]) ? $context["S_ERROR"] : null)) {
                // line 14
                echo "\t\t<div class=\"errorbox\">
\t\t\t<h3>";
                // line 15
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("WARNING");
                echo "</h3>
\t\t\t<p>";
                // line 16
                echo (isset($context["ERROR_MSG"]) ? $context["ERROR_MSG"] : null);
                echo "</p>
\t\t</div>
\t";
            }
            // line 19
            echo "
\t";
            // line 20
            if ( !(isset($context["S_TRANSLATED"]) ? $context["S_TRANSLATED"] : null)) {
                // line 21
                echo "\t\t<h3>";
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("AVAILABLE_TITLES");
                echo "</h3>

\t\t<p>";
                // line 23
                echo (isset($context["S_AVAILABLE_TITLES"]) ? $context["S_AVAILABLE_TITLES"] : null);
                echo "</p>
\t";
            }
            // line 25
            echo "
\t<form id=\"acp_reasons\" method=\"post\" action=\"";
            // line 26
            echo (isset($context["U_ACTION"]) ? $context["U_ACTION"] : null);
            echo "\">

\t<fieldset>
\t\t<legend>";
            // line 29
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("TITLE");
            echo "</legend>
\t\t<p>";
            // line 30
            if ((isset($context["S_TRANSLATED"]) ? $context["S_TRANSLATED"] : null)) {
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("IS_TRANSLATED_EXPLAIN");
            } else {
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("IS_NOT_TRANSLATED_EXPLAIN");
            }
            echo "</p>
\t<dl>
\t\t<dt><label for=\"reason_title\">";
            // line 32
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("REASON_TITLE");
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("COLON");
            echo "</label></dt>
\t\t<dd><input name=\"reason_title\" type=\"text\" id=\"reason_title\" value=\"";
            // line 33
            echo (isset($context["REASON_TITLE"]) ? $context["REASON_TITLE"] : null);
            echo "\" maxlength=\"255\" /></dd>
\t</dl>
\t";
            // line 35
            if ((isset($context["S_TRANSLATED"]) ? $context["S_TRANSLATED"] : null)) {
                // line 36
                echo "\t<dl>
\t\t<dt>";
                // line 37
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("REASON_TITLE_TRANSLATED");
                echo "</dt>
\t\t<dd>";
                // line 38
                echo (isset($context["TRANSLATED_TITLE"]) ? $context["TRANSLATED_TITLE"] : null);
                echo "</dd>
\t</dl>
\t";
            }
            // line 41
            echo "\t<dl>
\t\t<dt><label for=\"reason_description\">";
            // line 42
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("REASON_DESCRIPTION");
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("COLON");
            echo "</label></dt>
\t\t<dd><textarea name=\"reason_description\" id=\"reason_description\" rows=\"8\" cols=\"80\">";
            // line 43
            echo (isset($context["REASON_DESCRIPTION"]) ? $context["REASON_DESCRIPTION"] : null);
            echo "</textarea></dd>
\t</dl>
\t";
            // line 45
            if ((isset($context["S_TRANSLATED"]) ? $context["S_TRANSLATED"] : null)) {
                // line 46
                echo "\t<dl>
\t\t<dt>";
                // line 47
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("REASON_DESC_TRANSLATED");
                echo "</dt>
\t\t<dd>";
                // line 48
                echo (isset($context["TRANSLATED_DESCRIPTION"]) ? $context["TRANSLATED_DESCRIPTION"] : null);
                echo "</dd>
\t</dl>
\t";
            }
            // line 51
            echo "
\t<p class=\"submit-buttons\">
\t\t<input class=\"button1\" type=\"submit\" id=\"submit\" name=\"submit\" value=\"";
            // line 53
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("SUBMIT");
            echo "\" />&nbsp;
\t\t<input class=\"button2\" type=\"reset\" id=\"reset\" name=\"reset\" value=\"";
            // line 54
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("RESET");
            echo "\" />
\t\t";
            // line 55
            echo (isset($context["S_FORM_TOKEN"]) ? $context["S_FORM_TOKEN"] : null);
            echo "
\t</p>
\t</fieldset>
\t</form>

";
        } else {
            // line 61
            echo "
\t<h1>";
            // line 62
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("ACP_REASONS");
            echo "</h1>

\t<p>";
            // line 64
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("ACP_REASONS_EXPLAIN");
            echo "</p>

\t<form id=\"reasons\" method=\"post\" action=\"";
            // line 66
            echo (isset($context["U_ACTION"]) ? $context["U_ACTION"] : null);
            echo "\">
\t<fieldset class=\"tabulated\">
\t<legend>";
            // line 68
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("ACP_REASONS");
            echo "</legend>

\t";
            // line 70
            if (twig_length_filter($this->env, $this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "reasons", []))) {
                // line 71
                echo "\t\t<table class=\"table1\">
\t\t\t<col class=\"row1\" /><col class=\"row1\" /><col class=\"row2\" />
\t\t<thead>
\t\t<tr>
\t\t\t<th>";
                // line 75
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("REASON");
                echo "</th>
\t\t\t<th>";
                // line 76
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("USED_IN_REPORTS");
                echo "</th>
\t\t\t<th>";
                // line 77
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OPTIONS");
                echo "</th>
\t\t</tr>
\t\t</thead>
\t\t<tbody>
\t\t";
                // line 81
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "reasons", []));
                foreach ($context['_seq'] as $context["_key"] => $context["reasons"]) {
                    // line 82
                    echo "\t\t\t<tr>
\t\t\t\t<td>
\t\t\t\t\t<i style=\"float: ";
                    // line 84
                    echo (isset($context["S_CONTENT_FLOW_END"]) ? $context["S_CONTENT_FLOW_END"] : null);
                    echo "; font-size: .9em;\">";
                    if ($this->getAttribute($context["reasons"], "S_TRANSLATED", [])) {
                        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("IS_TRANSLATED");
                    } else {
                        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("IS_NOT_TRANSLATED");
                    }
                    echo "</i>
\t\t\t\t\t<strong>";
                    // line 85
                    echo $this->getAttribute($context["reasons"], "REASON_TITLE", []);
                    if ($this->getAttribute($context["reasons"], "S_OTHER_REASON", [])) {
                        echo " *";
                    }
                    echo "</strong>
\t\t\t\t\t<br /><span>";
                    // line 86
                    echo $this->getAttribute($context["reasons"], "REASON_DESCRIPTION", []);
                    echo "</span>
\t\t\t\t</td>
\t\t\t\t<td style=\"width: 100px;\">";
                    // line 88
                    echo $this->getAttribute($context["reasons"], "REASON_COUNT", []);
                    echo "</td>
\t\t\t\t<td class=\"actions\" style=\"width: 80px;\">
\t\t\t\t\t<span class=\"up-disabled\" style=\"display:none;\">";
                    // line 90
                    echo (isset($context["ICON_MOVE_UP_DISABLED"]) ? $context["ICON_MOVE_UP_DISABLED"] : null);
                    echo "</span>
\t\t\t\t\t<span class=\"up\"><a href=\"";
                    // line 91
                    echo $this->getAttribute($context["reasons"], "U_MOVE_UP", []);
                    echo "\" data-ajax=\"row_up\">";
                    echo (isset($context["ICON_MOVE_UP"]) ? $context["ICON_MOVE_UP"] : null);
                    echo "</a></span>
\t\t\t\t\t<span class=\"down-disabled\" style=\"display:none;\">";
                    // line 92
                    echo (isset($context["ICON_MOVE_DOWN_DISABLED"]) ? $context["ICON_MOVE_DOWN_DISABLED"] : null);
                    echo "</span>
\t\t\t\t\t<span class=\"down\"><a href=\"";
                    // line 93
                    echo $this->getAttribute($context["reasons"], "U_MOVE_DOWN", []);
                    echo "\" data-ajax=\"row_down\">";
                    echo (isset($context["ICON_MOVE_DOWN"]) ? $context["ICON_MOVE_DOWN"] : null);
                    echo "</a></span>
\t\t\t\t\t<a href=\"";
                    // line 94
                    echo $this->getAttribute($context["reasons"], "U_EDIT", []);
                    echo "\">";
                    echo (isset($context["ICON_EDIT"]) ? $context["ICON_EDIT"] : null);
                    echo "</a> 
\t\t\t\t\t";
                    // line 95
                    if ($this->getAttribute($context["reasons"], "U_DELETE", [])) {
                        // line 96
                        echo "\t\t\t\t\t\t<a href=\"";
                        echo $this->getAttribute($context["reasons"], "U_DELETE", []);
                        echo "\" data-ajax=\"row_delete\">";
                        echo (isset($context["ICON_DELETE"]) ? $context["ICON_DELETE"] : null);
                        echo "</a>
\t\t\t\t\t";
                    } else {
                        // line 98
                        echo "\t\t\t\t\t\t";
                        echo (isset($context["ICON_DELETE_DISABLED"]) ? $context["ICON_DELETE_DISABLED"] : null);
                        echo "
\t\t\t\t\t";
                    }
                    // line 100
                    echo "\t\t\t\t</td>
\t\t\t</tr>
\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reasons'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 103
                echo "\t\t</tbody>
\t\t</table>

\t";
            }
            // line 107
            echo "
\t<p class=\"quick\">
\t\t<input type=\"hidden\" name=\"action\" value=\"add\" />

\t\t<input type=\"text\" name=\"reason_title\" /> 
\t\t<input class=\"button2\" name=\"addreason\" type=\"submit\" value=\"";
            // line 112
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("ADD_NEW_REASON");
            echo "\" />
\t\t";
            // line 113
            echo (isset($context["S_FORM_TOKEN"]) ? $context["S_FORM_TOKEN"] : null);
            echo "
\t</p>
\t</fieldset>
\t
\t</form>

";
        }
        // line 120
        echo "
";
        // line 121
        $location = "overall_footer.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("overall_footer.html", "acp_reasons.html", 121)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
    }

    public function getTemplateName()
    {
        return "acp_reasons.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  359 => 121,  356 => 120,  346 => 113,  342 => 112,  335 => 107,  329 => 103,  321 => 100,  315 => 98,  307 => 96,  305 => 95,  299 => 94,  293 => 93,  289 => 92,  283 => 91,  279 => 90,  274 => 88,  269 => 86,  262 => 85,  252 => 84,  248 => 82,  244 => 81,  237 => 77,  233 => 76,  229 => 75,  223 => 71,  221 => 70,  216 => 68,  211 => 66,  206 => 64,  201 => 62,  198 => 61,  189 => 55,  185 => 54,  181 => 53,  177 => 51,  171 => 48,  167 => 47,  164 => 46,  162 => 45,  157 => 43,  152 => 42,  149 => 41,  143 => 38,  139 => 37,  136 => 36,  134 => 35,  129 => 33,  124 => 32,  115 => 30,  111 => 29,  105 => 26,  102 => 25,  97 => 23,  91 => 21,  89 => 20,  86 => 19,  80 => 16,  76 => 15,  73 => 14,  71 => 13,  66 => 11,  61 => 9,  52 => 7,  49 => 6,  47 => 5,  42 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "acp_reasons.html", "");
    }
}
