<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>注册</title>
<link type="text/css" href="style/css.css" rel="stylesheet" />
<link type="text/css" href="style/media.css" rel="stylesheet" />
 <link href="/js/layer/skin/layer.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>
<script type="text/javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/js/messages_cn.js"></script>
<style>
	.login1 label label#read-error{
		left: 25px;

	}
</style>
</head>

<body style="background:#f0f2f5;">
 <?php include_once 'head.php' ; 
 
  $baseconfig=$res->fn_select("select * from baseconfig where id=1");
 ?>   
   <div class="zc">
     	<div class="w1200">
        	<div class="tit">
            	<h3 style="color:#fff"><i></i>注册百汇金融账号<i></i></h3>
                
            </div>
            
            <div class="login1">
            	<div class="login1_lf">
            	<form class="register-form" action="/action.php?type=register" method="POST" >
					<label><span>真实姓名</span><input type="text" name="nickname" id="nickname" class="nickname" placeholder="输入您的真实姓名"/></label>
                	                <label><span>手机号</span><input type="text" name="mobile" id="mobile" class="mobile" placeholder="输入您的手机号"/></label>
					<label><span>邮箱</span><input type="text" name="email" id="email" class="email" placeholder="输入您的邮箱"/></label>
                    <label><span>密码</span><input type="password" name="password" id="password" placeholder="输入您的密码"/></label>
					 <label><span>确认密码</span><input type="password" name="rpassword"  id="rpassword" placeholder="请再次输入您的密码"/></label>

					<label><span>邮箱验证码</span><input type="text" name="validate" id="validate" class="validate" placeholder="请输入邮箱验证码"/ style="width:395px;">
					<button style="width:190px;text-align:center;background:#0d81e4;border:none;padding:17px 10px;margin-left:20px;color:#fff;cursor:pointer" id="send_msg2">发送邮箱验证码</button>
					</label> 
					
                    <label><span>推荐码</span><input type="text" name="tuijian" id="tuijian" class="tuijian" placeholder="请输入推荐码" value="<?=$_SESSION['tuijian']?>" <? if($_SESSION['tuijian']){echo 'readonly="readonly"';}?>>
					</label>
					<div class="mz">
					   <label style="    height: 50px; line-height: 50px;">
					   	<b style="padding:12px;position:absolute;top:-11px;left:12px;">我已阅读并接受"百汇金融"<a href="./article.php?nid=1" target="_blank" style="color:#333">《免责声明》</a></b>
					<input type="checkbox"  name="read" id="read" value="true" style="width:20px"/>
						</label>
					</div>
					<div class="btn"><button type="submit">立即注册</button></div>
                </form>
                </div>
            </div>
        </div>
     </div>
    <script>
	$(".register-form").validate({
	
	onfocusout: function(element) { $(element).valid(); },
	rules:{
		nickname:{
			required:true,
			remote:{                                          //验证用户名是否存在
               type:"POST",
               url:"/action.php?type=check_nickname",          
               data:{
                 nickname:function(){return $(".register-form .nickname").val();}
               } 
               
            },

		},
		
		mobile:{
			required:true,
			remote:{                                          //验证用户名是否存在
               type:"POST",
               url:"/action.php?type=check_mobile2",          
               data:{
                 nickname:function(){return $(".register-form .mobile").val();}
               } 
               
            },

		},

		email:{
			required:true,
			minlength: 5,
			isEmail:true,
			remote:{                                          //验证用户名是否存在
               type:"POST",
               url:"/action.php?type=check_email",          
               data:{
                 email:function(){return $(".register-form .email").val();}
               } 
               
            },
		},
		password: {
			required: true,
			minlength: 5
	    },
		rpassword: {
			required: true,
			minlength: 5,
			equalTo: "#password"
		},
		validate:{
			required: true,
			remote:{
               type:"POST",
               url:"/action.php?type=check_validate",             
               data:{
                 validate:function(){return $(".register-form .validate").val();}
               } 
            } 
			
		},
		validate2:{
			required: true,
			remote:{
               type:"POST",
               url:"/action.php?type=check_validate2",             
               data:{
                 validate2:function(){return $(".register-form .validate2").val();}
               } 
            } 
			
		},
		tuijian:{
			required:true,
			remote:{                                          //验证邀請碼是否存在
               type:"POST",
               url:"/action.php?type=check_tuijian",          
               data:{
                 tuijian:function(){return $(".register-form .tuijian").val();}
               } 
               
            },
		},
		
		read:{
			required: true
		}
	},
	 messages: {
			nickname: {
				required: "请输入用户名",
				remote: "用户名已存在",
			},
			password: {
				required: "请输入密码",
				minlength: "密码不能小于5个字符",
			},
			rpassword: {
				required: "请输入确认密码",
				minlength: "确认密码不能小于5个字符",
			   equalTo: "两次输入密码不一致不一致"
			},
			mobile: {
				required: "请输入手机号",
				remote: "手机号已存在",
			},
			email: {
				required: "请输入邮箱",
				remote: "邮箱已存在",
			},
			validate: {
				required: "请输入验证码",
				remote: "验证码不正确",
			},
			validate2: {
				required: "请输入验证码",
				remote: "验证码不正确",
			},
			tuijian: {
				required: "请输入邀请码",
				remote: "邀请码不正确",
			},

			read:{
				required: "如您已阅读并接受“百汇金融《免责声明》请勾选同意,方可继续注册。”"
			}
	},

	submitHandler: function(form) 
	{      
          $(form).ajaxSubmit({success:function(data){
			data=data.replace(/(^\s*)|(\s*$)/g, ""); 
			switch(data){
				case 'invalidate_nickname':
				alert('用户名已存在');
				break;
				case 'invalidate_mobile':
				alert('手机号已存在');
				break;
				case 'invalidate_email':
				alert('邮箱已存在');
				break;
				case 'invalidate_code':
				alert('验证码不正确');
				break;
				case 'invalidate_tuijian':
				alert('邀请码不正确');
				break;
				case 'success':
				alert('注册成功');
				window.location.href="/admin_user/blindidcard.php ";
				break;
				default:
				alert('注册失败');
			}
		}
		});     
	}  
 });
 
 $('#send_msg').click(function(){//发送短信
	$('.register-form .mobile').blur();
	if($('.register-form .mobile').hasClass('error')){
		return false;
	}

	var telephone=$('.register-form .mobile').val();
	$.ajax({
         url: "/duanxin/demo.php?type=sendmsg",  
         type: "POST",
         data:{telephone:telephone},
         //dataType: "json",
         error: function(){  
             alert('Error loading XML document');  
         },  
         success: function(data,status){ 
		
			if(data=='success'){
				timeCount=setInterval("waitMsg()",1000);
				alert('发送成功');
			}else if(data=='invalidate_count'){
				alert('短信超出限度');
			}else{
				alert('发送失败');
			}
         }
     }); 
});	
	
 $('#send_msg2').click(function(){//发送短信
	$('.register-form .email').blur();
	if($('.register-form .email').hasClass('error')){
		return false;
	}

	var email=$('.register-form .email').val();
	$.ajax({
         url: "/mail/send_email.php",  
         type: "POST",
         data:{email:email},
         //dataType: "json",
         error: function(){  
             alert('Error loading XML document');  
         },  
         success: function(data,status){ 
		
			if(data=='success'){
				timeCount2=setInterval("waitMsg2()",1000);
				alert('发送成功');
			}else{
				alert('发送失败');
			}
         }
     }); 
});	

jQuery.validator.addMethod("isMobile", function(value, element) {  
    var length = value.length;  
    var mobile = /1[3456789]{1}\d{9}$/;  
    return this.optional(element) || (length == 11 && mobile.test(value));  
}, "手机号码不正确");  
jQuery.validator.addMethod("isEmail", function(value, element) {  
    var length = value.length;  
    var ismail = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;  
    return this.optional(element) ||  ismail.test(value);  
}, "邮箱格式不正确");  
WAIT=60;
function waitMsg(){
	
	$('#send_msg').text(WAIT+'秒后重新发送');
	$('#send_msg').attr('disabled','disabled');
	WAIT--;
	if(WAIT==0){
		clearInterval(timeCount);
		WAIT=60;
		$('#send_msg').text('获取验证码');
		$('#send_msg').removeAttr('disabled');
	}
}
WAIT2=60;
function waitMsg2(){
	
	$('#send_msg2').text(WAIT+'秒后重新发送');
	$('#send_msg2').attr('disabled','disabled');
	WAIT2--;
	if(WAIT2==0){
		clearInterval(timeCount2);
		WAIT=60;
		$('#send_msg2').text('发送邮箱验证码');
		$('#send_msg2').removeAttr('disabled');
	}
}
	</script>
	
</body>
</html>
