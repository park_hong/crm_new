<?php
/**
 * @author    漫步云端
 */
header("content-Type:text/html; charset=UTF-8");

class PHPCsv
{
	public $Title="Simple";
	public $SheetHeader=[];
	public $SheetBody=[];
	public $fp;
	
	public function __construct()
	{
		
	}
	public function setTile($title)
	{
		$this->$Title=$title;
	}
	
	public function init()
	{
		header("content-Type:text/html; charset=UTF-8");
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$this->Title.'.csv"');
		header('Cache-Control: max-age=0');	
		$this->fp = fopen('php://output',  'w');
		fwrite($this->fp,chr(0xEF).chr(0xBB).chr(0xBF));  
		foreach($this->SheetHeader as $key => $item) {
			 $this->SheetHeader[$key] = $item;
		   // $this->SheetHeader[$key] = iconv('UTF-8', 'GBK', $item);
		}
		fputcsv($this->fp, $this->SheetHeader);
	
	}
	
	public function addSheetBody($datas){
		foreach($datas as $key => $data){
			
			foreach($this->SheetHeader as $k => $cell){
				
				if( strlen($data[$k])>10 ){
					$row[$k] = (string)$data[$k]."\t";
				}else{
					$row[$k] = $data[$k];
				}
				//$row[$k] = iconv('UTF-8','GBK',$data[$k]);
			}
			 fputcsv($this->fp, $row);
		}
		
	}
}