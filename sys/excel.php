<?php

class Excel
{
	public $Title="Simple";
	
	public $Creator="Maarten Balliauw";
	public $LastModifiedBy="Maarten Balliauw";
	public $SubjectTitle="Office 2017 XLSX Test Document";
	public $Subject="Office 2017 XLSX Test Document";
	public $Description="Test document for Office 2017 XLSX, generated using PHP classes.";
	public $Keywords="office 2017 openxml php";
	public $Category="Test result file";
	
	public $SheetHeader=[];
	public $SheetBody=[];
	
	public function __construct()
	{
		
	}

    public function init()
    {
		$sheets=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
		
        date_default_timezone_set('Europe/London');

		/** Include PHPExcel */
		require_once 'PHPExcel.php';


		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set document properties
		
		$objPHPExcel->getProperties()->setCreator($this->Creator)
									 ->setLastModifiedBy($this->LastModifiedBy)
									 ->setTitle($this->SubjectTitle)
									 ->setSubject($this->Subject)
									 ->setDescription($this->Description)
									 ->setKeywords($this->Keywords)
									 ->setCategory($this->Category);


	// Add some data
		foreach($this->SheetHeader as $key => $cell){
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue($sheets[$key].'1', $cell);
				//	echo $sheets[$key].'1'.":".$cell;	
		}
		
		foreach($this->SheetBody as $key => $data){
				$num=$key+2;
		
			foreach($this->SheetHeader as $k => $cell){
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue($sheets[$k].$num, $data[$k]);
					//echo $sheets[$k].$num.":".$data[$k];	
			}
		}

		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Simple');


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);


		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$this->Title.'.xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
				exit;
    }

	

}
